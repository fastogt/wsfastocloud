FROM ghcr.io/cirruslabs/flutter:stable AS build

LABEL maintainer="Alexandr Topilski <atopilski@fastocloud.com>"

RUN mkdir /app/
COPY . /app/
WORKDIR /app/

RUN python3 scripts/build_version.py

FROM nginx:latest

ENV USER fastocloud
RUN useradd -m -U -d /home/$USER $USER -s /bin/bash

COPY --from=build /app/nginx/wsfastocloud /etc/nginx/conf.d/default.conf
COPY --from=build /app/build/web /home/$USER/wsfastocloud_build

RUN chown -R $USER:$USER /home/$USER/wsfastocloud_build && chmod -R 755 /home/$USER/wsfastocloud_build && \
        chown -R $USER:$USER /var/cache/nginx && \
        chown -R $USER:$USER /var/log/nginx && \
        chown -R $USER:$USER /etc/nginx/conf.d
RUN touch /var/run/nginx.pid && \
        chown -R $USER:$USER /var/run/nginx.pid

USER $USER 

WORKDIR /home/$USER

EXPOSE 80