import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart' as models;
import 'package:fastotv_dart/commands_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wsfastocloud/l10n/app_localization.dart';

class LocalStorageService {
  static LocalStorageService? _instance;
  SharedPreferences? _preferences;

  static Future<LocalStorageService> getInstance() async {
    _instance ??= LocalStorageService();
    _instance!._preferences ??= await SharedPreferences.getInstance();
    return _instance!;
  }

  static const String _connectedUrl = 'connected_url';

  String? connectedUrl() {
    return _preferences!.getString(_connectedUrl);
  }

  void setConnectedUrl(String url) {
    _preferences!.setString(_connectedUrl, url);
  }

  void removeConnectedUrl() {
    _preferences!.remove(_connectedUrl);
  }

  models.WSServer? server() {
    final url = connectedUrl();
    if (url != null) {
      final result = models.makeWSServerFromUrl(url);
      if (result != null) {
        return result.server;
      }
    }
    return null;
  }

  GlobalTheme theme() {
    final url = connectedUrl();
    if (url != null) {
      final result = models.makeWSServerFromUrl(url);
      if (result != null && result.theme != null) {
        return GlobalTheme.fromInt(result.theme!);
      }
    }
    return GlobalTheme.LIGHT;
  }

  String locale() {
    final url = connectedUrl();
    if (url != null) {
      final result = models.makeWSServerFromUrl(url);
      if (result != null && result.locale != null) {
        return result.locale!;
      }
    }
    return AppLocalization.en.languageCode;
  }

  WsMode? mode() {
    final url = connectedUrl();
    if (url != null) {
      final result = models.makeWSServerFromUrl(url);
      if (result != null) {
        return result.mode;
      }
    }
    return null;
  }
}
