import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dart_common/dart_common.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:wsfastocloud/presenter/connection_bloc/connection_bloc.dart';

Future<bool> launchExternalUrl(String url, {LaunchMode mode = LaunchMode.platformDefault}) {
  return launchUrlString(url, mode: mode);
}

class Defaults {
  final String streamLogoIcon;
  final String vodTrailerUrl;
  final String backgroundUrl;

  Defaults(this.streamLogoIcon, this.vodTrailerUrl, this.backgroundUrl);
}

class LocalesResp {
  LocalesResp(this.countries, this.languages, this.currentCountry, this.currentLanguage);

  final List<List<String>> countries;
  final List<List<String>> languages;
  final String currentCountry;
  final String currentLanguage;
}

class Fetcher {
  final ConnectionBloc connectionBloc;
  late final StreamSubscription _connectionSubscription;
  String? _backendServerUrl;
  String? _encodedAuth;

  WSServer? server() {
    return connectionBloc.server;
  }

  Fetcher(this.connectionBloc) {
    _connectionSubscription = connectionBloc.stream.listen((state) {
      {
        final info = state.info;

        if (info == null) {
          _backendServerUrl = null;
          _encodedAuth = null;
        } else {
          _setBackendUrl(info.restApiUrl);
          _setAuthorizationHeader(info.encodedAuth);
        }
      }
    });
  }

  Defaults defaults() {
    final unk = _generateBackEndEndpoint('/install/assets/unknown_channel.png');
    final def = _generateBackEndEndpoint('/install/assets/unknown_background.png');
    final inv = _generateBackEndEndpoint('/install/assets/invalid_trailer.m3u8');
    return Defaults(unk.toString(), inv.toString(), def.toString());
  }

  String? get backendServerUrl => _backendServerUrl;

  String? get authorization {
    if (_encodedAuth == null) {
      return null;
    }
    return 'Basic $_encodedAuth';
  }

  void _setBackendUrl(String url) {
    _backendServerUrl = url;
  }

  void _setAuthorizationHeader(String? encodedAuth) {
    if (encodedAuth != null) {
      _encodedAuth = encodedAuth;
    }
  }

  Future<http.Response> getMediaServiceStatistics() {
    return get('/media/stats');
  }

  Future<http.Response> getServerStatistics() {
    return get('/server/stats');
  }

  Future<LocalesResp> getLocale() async {
    final _response = await get('/locale');
    final List<List<String>> _countries = [];
    final List<List<String>> _languages = [];

    final respData = httpDataResponseFromString(_response.body);
    final data = respData!.contentMap()!;

    for (final List c in data['countries']) {
      _countries.add(c.cast<String>());
    }
    for (final List l in data['languages']) {
      _languages.add(l.cast<String>());
    }
    return LocalesResp(_countries, _languages, data['current_country'], data['current_language']);
  }

  Future<http.Response> getServiceConfig() {
    return get('/server/config');
  }

  Future<http.Response> getServiceInfo() {
    return get('/server/info');
  }

  Future<http.Response> importContent(WSServer server) {
    return post('/server/import', server.toJson());
  }

  Uri getBackendEndpoint(String path) {
    return _generateBackEndEndpoint(path);
  }

  Future<http.Response> get(String endpoint) {
    assert(_backendServerUrl != null); // TODO: Exception

    final url = _generateBackEndEndpoint(endpoint);

    return http.get(url, headers: _getJsonHeaders());
  }

  Future<http.Response> delete(String endpoint) {
    assert(_backendServerUrl != null); // TODO: Exception

    final url = _generateBackEndEndpoint(endpoint);

    return http.delete(url, headers: _getJsonHeaders());
  }

  Future<http.Response> patch(String endpoint, Map<String, dynamic> data) {
    assert(_backendServerUrl != null); // TODO: Exception

    final url = _generateBackEndEndpoint(endpoint);

    final body = json.encode(data);
    return http.patch(url, headers: _getJsonHeaders(), body: body);
  }

  Future<bool> launchUrlEx(String endpoint) async {
    assert(_backendServerUrl != null); // TODO: Exception

    if (kIsWeb) {
      if (_encodedAuth != null) {
        endpoint += '?token=$_encodedAuth';
      }
    }
    final url = _generateBackEndEndpoint(endpoint);

    return launchUrl(url, webViewConfiguration: WebViewConfiguration(headers: _getJsonHeaders()));
  }

  Future<http.Response> sendFiles(
      String path, Map<String, List<int>> data, Map<String, dynamic> fields) async {
    final http.MultipartRequest request =
        http.MultipartRequest('POST', _generateBackEndEndpoint(path));
    if (authorization != null) {
      request.headers[HttpHeaders.authorizationHeader] = authorization!;
    }

    data.forEach((key, data) {
      final mf = http.MultipartFile.fromBytes('file', data,
          contentType: MediaType('application', 'octet-stream'), filename: key);
      request.files.add(mf);
    });

    final body = json.encode(fields);
    request.fields.addAll({'params': body});
    final sent = await request.send();
    return http.Response.fromStream(sent);
  }

  Future<http.Response> post(String endpoint, Map<String, dynamic> data) async {
    assert(_backendServerUrl != null); // TODO: Exception

    final url = _generateBackEndEndpoint(endpoint);

    final body = json.encode(data);
    return http.post(url, headers: _getJsonHeaders(), body: body);
  }

  Future<http.Response> put(String endpoint, Map<String, dynamic> data) async {
    assert(_backendServerUrl != null); // TODO: Exception

    final url = _generateBackEndEndpoint(endpoint);

    final body = json.encode(data);
    return http.put(url, headers: _getJsonHeaders(), body: body);
  }

  Map<String, String> _getJsonHeaders() {
    if (authorization == null) {
      return {'content-type': 'application/json', 'accept': 'application/json'};
    }
    return {
      'content-type': 'application/json',
      'accept': 'application/json',
      'authorization': authorization!,
    };
  }

  // private
  Uri _generateBackEndEndpoint(String path) {
    return Uri.parse('$_backendServerUrl$path');
  }

  void dispose() {
    _connectionSubscription.cancel();
  }
}
