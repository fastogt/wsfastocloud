import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';

class StreamStatistics extends StreamStatisticsBack {
  String name;
  String icon;

  StreamStatistics(
      {required this.name,
      required this.icon,
      required String id,
      required StreamType type,
      required double cpu,
      required int loopStartTime,
      required int rss,
      required StreamStatus status,
      required int restarts,
      required int startTime,
      required int timestamp,
      required int idleTime,
      required List<ChannelStats> inputStreams,
      required List<ChannelStats> outputStreams,
      required double quality})
      : super(
            id: id,
            type: type,
            cpu: cpu,
            loopStartTime: loopStartTime,
            rss: rss,
            status: status,
            restarts: restarts,
            startTime: startTime,
            timestamp: timestamp,
            idleTime: idleTime,
            inputStreams: inputStreams,
            outputStreams: outputStreams,
            quality: quality);

  factory StreamStatistics.fromBase(String name, String icon, StreamStatisticsBack base) {
    return StreamStatistics(
        name: name,
        icon: icon,
        id: base.id,
        type: base.type,
        cpu: base.cpu,
        loopStartTime: base.loopStartTime,
        rss: base.rss,
        status: base.status,
        restarts: base.restarts,
        startTime: base.startTime,
        timestamp: base.timestamp,
        idleTime: base.idleTime,
        inputStreams: base.inputStreams,
        outputStreams: base.outputStreams,
        quality: base.quality);
  }

  factory StreamStatistics.fromJson(String name, String icon, Map<String, dynamic> json) {
    final base = StreamStatisticsBack.fromJson(json);
    return StreamStatistics.fromBase(name, icon, base);
  }
}

class StreamStatisticsBack extends StreamRuntimeStats {
  final String id;
  final StreamType type;

  StreamStatisticsBack(
      {required this.id,
      required this.type,
      required double cpu,
      required int loopStartTime,
      required int rss,
      required StreamStatus status,
      required int restarts,
      required int startTime,
      required int timestamp,
      required int idleTime,
      required List<ChannelStats> inputStreams,
      required List<ChannelStats> outputStreams,
      required double quality}) {
    this.cpu = cpu;
    this.loopStartTime = loopStartTime;
    this.rss = rss;
    this.status = status;
    this.restarts = restarts;
    this.startTime = startTime;
    this.timestamp = timestamp;
    this.idleTime = idleTime;
    this.inputStreams = inputStreams;
    this.outputStreams = outputStreams;
    this.quality = quality;
  }

  factory StreamStatisticsBack.fromJson(Map<String, dynamic> json) {
    final base = StreamRuntimeStats.fromJson(json);

    return StreamStatisticsBack(
        id: json['id'],
        type: StreamType.fromInt(json['type']),
        cpu: base.cpu,
        loopStartTime: base.loopStartTime,
        rss: base.rss,
        status: base.status,
        restarts: base.restarts,
        startTime: base.startTime,
        timestamp: base.timestamp,
        idleTime: base.idleTime,
        inputStreams: base.inputStreams,
        outputStreams: base.outputStreams,
        quality: base.quality);
  }

  double get cpuFixed => fixedDouble(cpu);

  double get qualityFixed => fixedDouble(quality);

  double get rssInMegabytes => fixedDouble(rss / (1024 * 1024));

  int get inputBps => inputStreams.fold(0, (prev, e) => prev + e.bps);

  double get inputMbps => fixedDouble(8 * inputBps / (1024 * 1024));

  int get outputBps => outputStreams.fold(0, (prev, e) => prev + e.bps);

  double get outputMbps => fixedDouble(8 * outputBps / (1024 * 1024));

  double get startDuration => fixedDouble((timestamp - startTime) / 1000);

  double get loopDuration => fixedDouble((timestamp - loopStartTime) / 1000);
}
