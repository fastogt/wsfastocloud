import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';

class UploadM3u {
  static const _TYPE_FIELD = 'type';
  static const _STREAM_LOGO_URL_FIELD = 'stream_logo_url';
  static const _TRAILER_URL_FIELD = 'trailer_url';
  static const _BACKGROUND_URL_FIELD = 'background_url';
  static const _IMAGE_TIME_TO_CHECK_FIELD = 'image_time_to_check';
  static const _SKIP_GROUPS_FIELD = 'skip_groups';
  static const _FILL_INFO_FIELD = 'fill_info';
  static const _GROUPS_FIELD = 'groups';
  static const _IS_SERIES_FIELD = 'is_series';

  final StreamType type;
  final String streamLogoIcon;
  final String vodTrailerUrl;
  final String backgroundUrl;
  final double? imageCheckTime;
  final bool skipGroups;
  final bool fillInfo;
  final List<String> groups;
  final bool isSeries;

  UploadM3u(
      {required this.type,
      required this.streamLogoIcon,
      required this.vodTrailerUrl,
      required this.backgroundUrl,
      this.imageCheckTime,
      required this.skipGroups,
      required this.fillInfo,
      required this.groups,
      required this.isSeries});

  factory UploadM3u.fromJson(Map<String, dynamic> json) {
    double? imgc;
    if (json.containsKey(_IMAGE_TIME_TO_CHECK_FIELD)) {
      imgc = json[_IMAGE_TIME_TO_CHECK_FIELD];
    }

    return UploadM3u(
        type: StreamType.fromInt(json[_TYPE_FIELD]),
        streamLogoIcon: json[_STREAM_LOGO_URL_FIELD],
        vodTrailerUrl: json[_TRAILER_URL_FIELD],
        backgroundUrl: json[_BACKGROUND_URL_FIELD],
        imageCheckTime: imgc,
        skipGroups: json[_SKIP_GROUPS_FIELD],
        fillInfo: json[_FILL_INFO_FIELD],
        groups: json[_GROUPS_FIELD],
        isSeries: json[_IS_SERIES_FIELD]);
  }

  Map<String, dynamic> toJson() {
    final result = {
      _TYPE_FIELD: type.toInt(),
      _STREAM_LOGO_URL_FIELD: streamLogoIcon,
      _TRAILER_URL_FIELD: vodTrailerUrl,
      _BACKGROUND_URL_FIELD: backgroundUrl,
      _SKIP_GROUPS_FIELD: skipGroups,
      _FILL_INFO_FIELD: fillInfo,
      _GROUPS_FIELD: groups,
      _IS_SERIES_FIELD: isSeries
    };
    if (imageCheckTime != null) {
      result[_IMAGE_TIME_TO_CHECK_FIELD] = imageCheckTime!;
    }
    return result;
  }
}

class UploadM3uPackage {
  static const _TYPE_FIELD = 'type';
  static const _STREAM_LOGO_URL_FIELD = 'stream_logo_url';
  static const _TRAILER_URL_FIELD = 'trailer_url';
  static const _BACKGROUND_URL_FIELD = 'background_url';
  static const _IMAGE_TIME_TO_CHECK_FIELD = 'image_time_to_check';
  static const _SKIP_GROUPS_FIELD = 'skip_groups';
  static const _FILL_INFO_FIELD = 'fill_info';
  static const _GROUPS_FIELD = 'groups';

  final StreamType type;
  final String streamLogoIcon;
  final String vodTrailerUrl;
  final String backgroundUrl;
  final double? imageCheckTime;
  final bool skipGroups;
  final bool fillInfo;
  final List<String> groups;

  UploadM3uPackage(
      {required this.type,
      required this.streamLogoIcon,
      required this.vodTrailerUrl,
      required this.backgroundUrl,
      this.imageCheckTime,
      required this.skipGroups,
      required this.fillInfo,
      required this.groups});

  factory UploadM3uPackage.fromJson(Map<String, dynamic> json) {
    double? imgc;
    if (json.containsKey(_IMAGE_TIME_TO_CHECK_FIELD)) {
      imgc = json[_IMAGE_TIME_TO_CHECK_FIELD];
    }
    return UploadM3uPackage(
        type: StreamType.fromInt(json[_TYPE_FIELD]),
        streamLogoIcon: json[_STREAM_LOGO_URL_FIELD],
        vodTrailerUrl: json[_TRAILER_URL_FIELD],
        backgroundUrl: json[_BACKGROUND_URL_FIELD],
        imageCheckTime: imgc,
        skipGroups: json[_SKIP_GROUPS_FIELD],
        fillInfo: json[_FILL_INFO_FIELD],
        groups: json[_GROUPS_FIELD]);
  }

  Map<String, dynamic> toJson() {
    final result = {
      _TYPE_FIELD: type.toInt(),
      _STREAM_LOGO_URL_FIELD: streamLogoIcon,
      _TRAILER_URL_FIELD: vodTrailerUrl,
      _BACKGROUND_URL_FIELD: backgroundUrl,
      _SKIP_GROUPS_FIELD: skipGroups,
      _FILL_INFO_FIELD: fillInfo,
      _GROUPS_FIELD: groups
    };
    if (imageCheckTime != null) {
      result[_IMAGE_TIME_TO_CHECK_FIELD] = imageCheckTime!;
    }
    return result;
  }
}
