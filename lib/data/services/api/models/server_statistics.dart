import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastotv_dart/commands_info.dart';

class ServerStatistics extends Machine {
  static const _OS_FIELD = 'os';
  static const _VSYSTEM_FIELD = 'vsystem';
  static const _VROLE_FIELD = 'vrole';

  final OperationSystem os;
  final LicenseProjectInfo project;
  final String vsystem;
  final String vrole;

  ServerStatistics(
      {required double cpu,
      required double gpu,
      required String loadAverage,
      required int memoryTotal,
      required int memoryFree,
      required int hddTotal,
      required int hddFree,
      required int bandwidthIn,
      required int bandwidthOut,
      required int timestamp,
      required int uptime,
      required int totalBytesIn,
      required int totalBytesOut,
      required this.os,
      required this.project,
      required this.vsystem,
      required this.vrole})
      : super(
            cpu: cpu,
            gpu: gpu,
            loadAverage: loadAverage,
            memoryTotal: memoryTotal,
            memoryFree: memoryFree,
            hddTotal: hddTotal,
            hddFree: hddFree,
            bandwidthIn: bandwidthIn,
            bandwidthOut: bandwidthOut,
            timestamp: timestamp,
            uptime: uptime,
            totalBytesIn: totalBytesIn,
            totalBytesOut: totalBytesOut);

  ServerStatistics.createInit()
      : project = LicenseProjectInfo(project: 'Unknown', version: 'Unknown', expirationTime: 0),
        os = OperationSystem.createUnknown(),
        vsystem = '',
        vrole = '',
        super.createInit();

  factory ServerStatistics.fromJson(Map<String, dynamic> json) {
    final mach = Machine.fromJson(json);
    final project = LicenseProjectInfo.fromJson(json);
    final os = json[_OS_FIELD];
    final vsys = json[_VSYSTEM_FIELD];
    final vrole = json[_VROLE_FIELD];

    return ServerStatistics(
        gpu: mach.gpu,
        cpu: mach.cpu,
        loadAverage: mach.loadAverage,
        memoryTotal: mach.memoryTotal,
        memoryFree: mach.memoryFree,
        hddTotal: mach.hddTotal,
        hddFree: mach.hddFree,
        bandwidthIn: mach.bandwidthIn,
        bandwidthOut: mach.bandwidthOut,
        totalBytesIn: mach.totalBytesIn,
        totalBytesOut: mach.totalBytesOut,
        uptime: mach.uptime,
        timestamp: mach.timestamp,
        project: project,
        os: OperationSystem.fromJson(os),
        vsystem: vsys,
        vrole: vrole);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = super.toJson();
    data[_OS_FIELD] = os.toJson();
    data[_VSYSTEM_FIELD] = vsystem;
    data[_VROLE_FIELD] = vrole;
    final proj = project.toJson();
    data.addAll(proj);
    return data;
  }
}
