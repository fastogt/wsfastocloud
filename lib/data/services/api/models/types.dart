import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';

class IpAddress {
  static const _IP = 'ip';

  String ip;

  IpAddress({required this.ip});

  factory IpAddress.fromJson(Map<String, dynamic> json) {
    return IpAddress(ip: json[_IP]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {_IP: ip};
    return data;
  }
}

class ScanFolder {
  static const _MEDIA_TYPE = 'media_type';
  static const _PATH = 'path';

  StreamType mediaType;
  String path;

  ScanFolder({required this.mediaType, required this.path});

  factory ScanFolder.fromJson(Map<String, dynamic> json) {
    return ScanFolder(mediaType: StreamType.fromInt(json[_MEDIA_TYPE]), path: json[_PATH]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {_MEDIA_TYPE: mediaType.toInt(), _PATH: path};
    return data;
  }
}

class Node {
  static const _HOST = 'host';
  static const _KEY = 'key';

  String host;
  String key;

  Node({required this.host, required this.key});

  Node.createDefault()
      : host = 'http://127.0.0.1:6317',
        key =
            '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';

  factory Node.fromJson(Map<String, dynamic> json) {
    return Node(host: json[_HOST], key: json[_KEY]);
  }

  Node copy() {
    return Node(host: host, key: key);
  }

  Map<String, dynamic> toJson() {
    return {_HOST: host.toString(), _KEY: key};
  }
}

class Https {
  static const _CERT = 'cert';
  static const _KEY = 'key';

  String cert;
  String key;

  Https({required this.cert, required this.key});

  Https.createDefault()
      : cert = '/etc/letsencrypt/live/fastocloud.com/fullchain.pem',
        key = '/etc/letsencrypt/live/fastocloud.com/privkey.pem';

  factory Https.fromJson(Map<String, dynamic> json) {
    return Https(cert: json[_CERT], key: json[_KEY]);
  }

  Https copy() {
    return Https(cert: cert, key: key);
  }

  Map<String, dynamic> toJson() {
    return {_CERT: cert, _KEY: key};
  }
}

class Webrtc {
  static const _STUN = 'stun';
  static const _TURN = 'turn';

  String stun;
  String turn;

  Webrtc({required this.stun, required this.turn});

  Webrtc.createDefault()
      : stun = 'stun://stun.fastocloud.com:34789',
        turn = 'turn://fastocloud:fastocloud@turn.fastocloud.com:53499';

  factory Webrtc.fromJson(Map<String, dynamic> json) {
    return Webrtc(stun: json[_STUN], turn: json[_TURN]);
  }

  Map<String, dynamic> toJson() {
    return {_STUN: stun, _TURN: turn};
  }

  Webrtc copy() {
    return Webrtc(stun: stun, turn: turn);
  }
}

class Auth {
  static const LOGIN_FIELD = 'login';
  static const PASSWORD_FIELD = 'password';

  String login;
  String password;

  Auth({required this.login, required this.password});

  Auth.createDefault()
      : login = 'fastocloud',
        password = 'fastocloud';

  factory Auth.fromString(String data) {
    final split = data.split(':');
    return Auth(login: split[0], password: split[1]);
  }

  factory Auth.fromJson(Map<String, dynamic> json) {
    return Auth(login: json[LOGIN_FIELD], password: json[PASSWORD_FIELD]);
  }

  Map<String, dynamic> toJson() {
    return {LOGIN_FIELD: login, PASSWORD_FIELD: password};
  }

  @override
  String toString() {
    return '$login:$password';
  }

  Auth copy() {
    return Auth(login: login, password: password);
  }
}

class TypeVodService {
  static const int _TMDB = 1;

  final int _value;

  const TypeVodService._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    assert(_value == 1);
    return 'TMDB';
  }

  factory TypeVodService.fromInt(int type) {
    if (type == _TMDB) {
      return TypeVodService.TMDB;
    }

    throw 'Unknown Vod service type: $type';
  }

  static List<TypeVodService> get values => [TMDB];

  static const TypeVodService TMDB = TypeVodService._(_TMDB);
}

class TmdbSettings {
  static const KEY = 'key';

  String key;

  TmdbSettings(this.key);

  Map<String, dynamic> toJson() {
    return {KEY: key};
  }

  factory TmdbSettings.fromJson(Map<String, dynamic> json) {
    return TmdbSettings(json[KEY]);
  }
}

class VodInfoSettings {
  static const _TYPE = 'type';
  static const _TMDB = 'tmdb';

  TypeVodService service;
  TmdbSettings tmdb;

  VodInfoSettings({required this.service, required this.tmdb});

  VodInfoSettings.createDefault()
      : service = TypeVodService.TMDB,
        tmdb = TmdbSettings('06dc198084fe47aa0dfd8986a665ec76');

  Map<String, dynamic> toJson() {
    return {
      _TYPE: service.toInt(),
      _TMDB: tmdb.toJson(),
    };
  }

  factory VodInfoSettings.fromJson(Map<String, dynamic> json) {
    final service = TypeVodService.fromInt(json[_TYPE]);
    final tmdb = TmdbSettings.fromJson(json[_TMDB]);
    return VodInfoSettings(service: service, tmdb: tmdb);
  }
}

class AuthContentType {
  static const _JWT_CONTENT_AUTH = 0;
  static const _BASE_CONTENT_AUTH = 1;

  final int _value;

  const AuthContentType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _JWT_CONTENT_AUTH) {
      return 'JWT';
    }

    assert(_value == _BASE_CONTENT_AUTH);
    return 'Base';
  }

  factory AuthContentType.fromInt(int type) {
    if (type == _JWT_CONTENT_AUTH) {
      return AuthContentType.JWT;
    } else if (type == _BASE_CONTENT_AUTH) {
      return AuthContentType.BASE;
    }

    throw 'Unknown Auth content type: $type';
  }

  static List<AuthContentType> get values => [JWT, BASE];

  static const AuthContentType JWT = AuthContentType._(_JWT_CONTENT_AUTH);
  static const AuthContentType BASE = AuthContentType._(_BASE_CONTENT_AUTH);
}
