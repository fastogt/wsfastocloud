import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:wsfastocloud/data/services/api/models/types.dart';

class MasterConfigInfo {
  static const _HOST = 'host';
  static const _ALIAS = 'alias';
  static const _LICENSE_KEY = 'license_key';
  static const _HLS_HOST = 'hls_host';
  static const _VODS_HOST = 'vods_host';
  static const _CODS_HOST = 'cods_host';
  static const _PREFERRED_URL_SCHEME = 'preferred_url_scheme'; // enum (http, https)
  static const _BLACK_LIST = 'blacklist';
  static const _SCAN_FOLDERS = 'scan_folders';
  static const _FILES_TTL = 'files_ttl';
  static const _NODE = 'node';
  static const _LOG_LEVEL = 'log_level'; // enum (TRACE, DEBUG, INFO, WARNING, ERROR, FATAL, PANIC)
  static const _LOG_PATH = 'log_path';
  static const _CORS = 'cors';
  static const _AUTH = 'auth';
  static const _MAX_UPLOAD_FILE_SIZE = 'max_upload_file_size';
  static const _WEBRTC = 'webrtc';
  static const _AUTH_CONTENT = 'auth_content';
  static const _VOD_INFO = 'vod_info';
  static const _STREAM_NOTIFICATION = 'alarm';
  static const _HTTPS_FIELD = 'https';

  HostAndPort host;
  String alias;
  String licenseKey;
  String hlsHost;
  String vodsHost;
  String codsHost;
  String preferredUrlScheme;

  List<IpAddress> blacklist;
  List<ScanFolder> scanFolders;
  int filesTTL;
  Node? node;
  String logLevel;
  String logPath;
  bool cors;
  Auth? auth;
  INotification? streamNotification;
  int maxUploadFileSize;
  Webrtc? webrtc;
  AuthContentType authContent;
  VodInfoSettings? vodInfo;
  Https? https;

  MasterConfigInfo(
      {required this.host,
      required this.alias,
      required this.licenseKey,
      required this.hlsHost,
      required this.vodsHost,
      required this.codsHost,
      required this.preferredUrlScheme,
      required this.blacklist,
      required this.scanFolders,
      required this.filesTTL,
      this.node,
      required this.logLevel,
      required this.logPath,
      required this.cors,
      this.auth,
      this.streamNotification,
      required this.maxUploadFileSize,
      this.webrtc,
      required this.authContent,
      this.vodInfo,
      this.https});

  factory MasterConfigInfo.fromJson(Map<String, dynamic> json) {
    Webrtc? webrtc;
    if (json.containsKey(_WEBRTC)) {
      webrtc = Webrtc.fromJson(json[_WEBRTC]);
    }
    Auth? auth;
    if (json.containsKey(_AUTH)) {
      auth = Auth.fromString(json[_AUTH]);
    }
    INotification? streamNotification;
    if (json.containsKey(_STREAM_NOTIFICATION)) {
      streamNotification = INotification.fromJson(json[_STREAM_NOTIFICATION]);
    }
    final List<IpAddress> black = [];
    if (json.containsKey(_BLACK_LIST)) {
      json[_BLACK_LIST].forEach((b) {
        black.add(IpAddress.fromJson(b));
      });
    }
    final List<ScanFolder> scan = [];
    if (json.containsKey(_SCAN_FOLDERS)) {
      json[_SCAN_FOLDERS].forEach((b) {
        scan.add(ScanFolder.fromJson(b));
      });
    }
    Node? node;
    if (json.containsKey(_NODE)) {
      node = Node.fromJson(json[_NODE]);
    }
    VodInfoSettings? vodInfo;
    if (json.containsKey(_VOD_INFO)) {
      vodInfo = VodInfoSettings.fromJson(json[_VOD_INFO]);
    }
    Https? https;
    if (json.containsKey(_HTTPS_FIELD)) {
      https = Https.fromJson(json[_HTTPS_FIELD]);
    }
    final host = HostAndPort.fromString(json[_HOST]);
    final alias = json[_ALIAS];
    final license = json[_LICENSE_KEY];
    final hlsHost = json[_HLS_HOST];
    final vodsHost = json[_VODS_HOST];
    final codsHost = json[_CODS_HOST];
    final upload = json[_MAX_UPLOAD_FILE_SIZE];
    final cors = json[_CORS];
    final path = json[_LOG_PATH];
    final authContent = json[_AUTH_CONTENT];
    final fileTTL = json[_FILES_TTL];
    return MasterConfigInfo(
        host: host,
        alias: alias,
        licenseKey: license,
        hlsHost: hlsHost,
        vodsHost: vodsHost,
        codsHost: codsHost,
        preferredUrlScheme: json[_PREFERRED_URL_SCHEME],
        blacklist: black,
        scanFolders: scan,
        filesTTL: fileTTL,
        node: node,
        logLevel: json[_LOG_LEVEL],
        logPath: path,
        cors: cors,
        auth: auth,
        streamNotification: streamNotification,
        maxUploadFileSize: upload,
        webrtc: webrtc,
        authContent: AuthContentType.fromInt(authContent),
        vodInfo: vodInfo,
        https: https);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data[_HOST] = host.toString();
    data[_ALIAS] = alias;
    data[_LICENSE_KEY] = licenseKey;
    data[_HLS_HOST] = hlsHost;
    data[_VODS_HOST] = vodsHost;
    data[_CODS_HOST] = codsHost;
    data[_PREFERRED_URL_SCHEME] = preferredUrlScheme;
    final List<Map<String, dynamic>> black = [];
    for (final element in blacklist) {
      black.add(element.toJson());
    }
    data[_BLACK_LIST] = black;
    final List<Map<String, dynamic>> scan = [];
    for (final element in scanFolders) {
      scan.add(element.toJson());
    }
    data[_SCAN_FOLDERS] = scan;
    if (node != null) {
      data[_NODE] = node!.toJson();
    }
    if (https != null) {
      data[_HTTPS_FIELD] = https!.toJson();
    }
    data[_FILES_TTL] = filesTTL;
    data[_LOG_LEVEL] = logLevel;
    data[_LOG_PATH] = logPath;
    data[_CORS] = cors;
    if (auth != null) {
      data[_AUTH] = auth!.toString();
    }
    if (streamNotification != null) {
      data[_STREAM_NOTIFICATION] = streamNotification!.toJson();
    }
    data[_MAX_UPLOAD_FILE_SIZE] = maxUploadFileSize;
    if (webrtc != null) {
      data[_WEBRTC] = webrtc!.toJson();
    }
    data[_AUTH_CONTENT] = authContent.toInt();
    if (vodInfo != null) {
      data[_VOD_INFO] = vodInfo!.toJson();
    }
    return data;
  }

  MasterConfigInfo copy() {
    return MasterConfigInfo(
        host: host.copy(),
        alias: alias,
        licenseKey: licenseKey,
        hlsHost: hlsHost,
        vodsHost: vodsHost,
        codsHost: codsHost,
        preferredUrlScheme: preferredUrlScheme,
        blacklist: blacklist,
        scanFolders: scanFolders,
        filesTTL: filesTTL,
        node: node,
        logLevel: logLevel,
        logPath: logPath,
        cors: cors,
        webrtc: webrtc,
        auth: auth,
        streamNotification: streamNotification,
        authContent: authContent,
        vodInfo: vodInfo,
        maxUploadFileSize: maxUploadFileSize,
        https: https);
  }
}
