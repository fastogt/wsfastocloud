import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:wsfastocloud/data/services/api/models/connection_info.dart';
import 'package:wsfastocloud/domain/websocket/websocket/websocket_delegate.dart';
import 'package:wsfastocloud/presenter/connection_bloc/connection_bloc.dart';

part 'websocket_api_event.dart';
part 'websocket_api_state.dart';

class WebSocketApiBloc extends Bloc<WebSocketApiEvent, WebSocketState> {
  WebSocketApiBloc(this.connectionBloc) : super(WebSocketDisconnected()) {
    _connectionSubscription = connectionBloc.stream.listen((state) {
      if (state.info != null) {
        add(OnConnectEvent(_generateUrl(state.info!)));
      } else {
        add(const OnDisconnectEvent());
      }
    });
    on<OnOpenEvent>(_onOpenWebSocket);
    on<OnMessageEvent>(_onMessageWebSocket);
    on<OnErrorEvent>(_onErrorWebSocket);
    on<OnClosedEvent>(_onClosedWebSocket);
    on<OnConnectEvent>(_connect);
    on<OnDisconnectEvent>(_disconnect);
  }

  final ConnectionBloc connectionBloc;
  late final StreamSubscription _connectionSubscription;
  WebSocketDelegate? _delegate;

  bool isConnected() {
    return state is! WebSocketDisconnected;
  }

  String _generateUrl(ConnectionInfo info) {
    var url = '${info.socketUrl}/updates';

    if (info.encodedAuth != null) {
      url += '?token=${info.encodedAuth}';
    }

    return url;
  }

  void _connect(OnConnectEvent event, Emitter<WebSocketState> emit) {
    _delegate?.close();

    _delegate = WebSocketDelegate(event.url,
        onOpen: () => add(const OnOpenEvent()),
        onMessage: (message) => add(OnMessageEvent(message)),
        onError: (error) => add(OnErrorEvent(error)),
        onClose: (code, reason) => add(OnClosedEvent(code, reason)));

    _delegate!.connect();
  }

  void _disconnect(OnDisconnectEvent event, Emitter<WebSocketState> emit) {
    _delegate?.close();
    emit(WebSocketDisconnected());
  }

  void dispose() {
    add(const OnDisconnectEvent());
    _connectionSubscription.cancel();
  }

  void _onOpenWebSocket(OnOpenEvent event, Emitter<WebSocketState> emit) {
    emit(WebSocketConnected());
  }

  void _onMessageWebSocket(OnMessageEvent event, Emitter<WebSocketState> emit) {
    final data = json.decode(event.message);
    if (data['type'] != null) {
      emit(WebSocketApiMessage.fromJson(data));
    }
  }

  void _onErrorWebSocket(OnErrorEvent event, Emitter<WebSocketState> emit) {
    emit(WebSocketFailure.fromError(event.error));
  }

  void _onClosedWebSocket(OnClosedEvent event, Emitter<WebSocketState> emit) {
    emit(WebSocketDisconnected());
  }
}
