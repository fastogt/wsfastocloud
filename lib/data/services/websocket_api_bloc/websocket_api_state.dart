part of 'websocket_api_bloc.dart';

class WebSocketState {}

class WebSocketConnected extends WebSocketState {}

class WebSocketDisconnected extends WebSocketState {}

class WebSocketFailure extends WebSocketState {
  final String description;

  WebSocketFailure(this.description);

  factory WebSocketFailure.fromError(dynamic error) {
    return WebSocketFailure('Socket error: $error');
  }
}

class WebSocketApiMessage extends WebSocketState {
  final String type;
  final Map<String, dynamic> data;

  WebSocketApiMessage(this.type, this.data);

  factory WebSocketApiMessage.fromJson(Map<String, dynamic> json) {
    return WebSocketApiMessage(json['type'], json['data']);
  }
}
