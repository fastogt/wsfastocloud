import 'dart:io';

import 'package:dart_common/dart_common.dart';

import 'package:fastocloud_dart_media_models/models.dart';
import 'package:http/http.dart' as http;
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/domain/repositories/resource_utilization_repository.dart';

class ResourceUtilizationRepositoryImpl implements ResourceUtilizationRepository {
  final Fetcher _fetcher;

  const ResourceUtilizationRepositoryImpl(Fetcher fetcher) : _fetcher = fetcher;

  @override
  Future<List<Machine>> getResourceUtilization() async {
    final http.Response response = await _fetcher.get('/server/db/snapshots');

    if (response.statusCode == HttpStatus.ok) {
      final respData = httpDataResponseFromString(response.body);
      final data = respData!.contentMap()!;

      final List<Machine> machines = [];
      data["snapshots"].forEach((e) {
        machines.add(Machine.fromJson(e));
      });

      return machines;
    }
    throw Exception("Error fetching resource utilization");
  }
}
