import 'package:fastocloud_dart_media_models/models.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';

class StoreEditor extends IStreamEditor {
  final StreamsStoreBloc fetcher;

  StoreEditor(this.fetcher);

  @override
  Defaults defaults() {
    return fetcher.defaults();
  }

  @override
  Future<MediaFolders?> folders() {
    return fetcher.folders();
  }

  @override
  WebRTCOutputUrl genWebRTC(int oid, String sid) {
    return fetcher.genWebRTC(oid, sid);
  }

  @override
  Future<bool> addLive(IStream stream) {
    return fetcher.addLiveStream(stream);
  }

  @override
  Future<bool> editLive(IStream stream) {
    return fetcher.editLiveStream(stream);
  }

  @override
  Future<bool> addVod(IStream stream) {
    return fetcher.addVod(stream);
  }

  @override
  Future<bool> editVod(IStream stream) {
    return fetcher.editVod(stream);
  }

  @override
  Future<bool> addEpisode(IStream stream) {
    return fetcher.addEpisode(stream);
  }

  @override
  Future<bool> editEpisode(IStream stream) {
    return fetcher.editEpisode(stream);
  }

  @override
  Future<String> probeOutUrl(OutputUrl url) {
    return fetcher.probeOutUrl(url);
  }

  @override
  Future<String> probeInUrl(InputUrl url) {
    return fetcher.probeInUrl(url);
  }

  @override
  Future<bool> uploadM3uFile(
      StreamType type,
      bool isSeries,
      double? imageCheckTime,
      final bool skipGroups,
      final bool fillInfo,
      final List<String> groups,
      Map<String, List<int>> data) {
    return fetcher.uploadM3uFile(
        type, isSeries, imageCheckTime, skipGroups, fillInfo, groups, data);
  }

  @override
  Future<bool> uploadM3uPackageFile(StreamType type, double? imageCheckTime, final bool skipGroups,
      final bool fillInfo, final List<String> groups, Map<String, List<int>> data) {
    return fetcher.uploadM3uPackageFile(type, imageCheckTime, skipGroups, fillInfo, groups, data);
  }
}
