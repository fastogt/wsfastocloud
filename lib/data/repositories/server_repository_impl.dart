import 'dart:io';

import 'package:dart_common/dart_common.dart';

import 'package:wsfastocloud/data/model/upload_image.dart';
import 'package:wsfastocloud/data/model/uploaded_image_model.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/domain/entity/file.dart';
import 'package:wsfastocloud/domain/entity/uploaded_image.dart';
import 'package:wsfastocloud/domain/repositories/server_repository.dart';

class ServerRepositoryImpl implements ServerRepository {
  final Fetcher _fetcher;

  ServerRepositoryImpl({required Fetcher fetcher}) : _fetcher = fetcher;

  @override
  Future<UploadedImage> uploadImage(File file) async {
    final UploadImageModel uploadImageModel = UploadImageModel.fromEntity(file);

    final response = await _fetcher.sendFiles('/server/image/upload', uploadImageModel.toMap(), {});
    if (response.statusCode == HttpStatus.ok) {
      final respData = httpDataResponseFromString(response.body);
      final data = respData!.contentMap()!;
      UploadedImageModel uploadedImageModel = UploadedImageModel.fromMap(data);
      final path = _fetcher.getBackendEndpoint(uploadedImageModel.path);
      uploadedImageModel = uploadedImageModel.copyWith(path: path.toString());

      return UploadedImage(path: uploadedImageModel.path);
    }
    throw ErrorHttp(response.statusCode, response.reasonPhrase);
  }
}
