import 'dart:convert';

import 'package:dart_common/dart_common.dart';

import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/stream_statistics.dart';
import 'package:wsfastocloud/domain/repositories/stream_repository.dart';

class StreamRepositoryImpl implements StreamRepository {
  final Fetcher fetcher;

  StreamRepositoryImpl(this.fetcher);

  @override
  Future<List<StreamStatisticsBack>> getStreamsStatistics() async {
    final response = await fetcher.get('/media/streams_stats');
    final respData = httpDataResponseFromString(response.body);
    final content = respData!.contentMap()!;
    final streamsData = content['statistics'];
    final result = <StreamStatisticsBack>[];

    for (final streamData in streamsData) {
      final streams = StreamStatisticsBack.fromJson(streamData);
      result.add(streams);
    }

    return result;
  }

  @override
  Future<void> stopStream(String id, [bool force = false]) {
    final requestData = {'id': id, 'force': force};

    return fetcher.post('/media/stream/stop', requestData);
  }

  @override
  Future<void> restartStream(String id) {
    final requestData = {'id': id};

    return fetcher.post('/media/stream/restart', requestData);
  }

  @override
  Future<void> getLogStream(String sid) {
    return fetcher.launchUrlEx('/media/stream/$sid/logs');
  }

  @override
  Future<void> changeSource(String id, int index) {
    final requestData = {'id': id, 'channel_id': index};

    return fetcher.post('/media/stream/change_source', requestData);
  }

  @override
  Future<void> getPipeLineStream(String sid) {
    return fetcher.launchUrlEx('/media/stream/$sid/pipeline');
  }

  @override
  Future<String> getConfigStreamString(String id) async {
    final probeData = await getConfigStreamMap(id);
    return json.encode(probeData);
  }

  @override
  Future<Map<String, dynamic>> getConfigStreamMap(String sid) async {
    final response = await fetcher.get('/media/stream/$sid/config');

    final respData = httpDataResponseFromString(response.body);
    if (respData == null) {
      throw 'Fetch error: invalid response format, code: ${response.statusCode}';
    }

    final err = respData.error();
    if (err != null) {
      throw 'Response error: ${err.message}';
    }

    final content = respData.contentMap()!;
    final probeData = content['config'];
    return probeData;
  }
}
