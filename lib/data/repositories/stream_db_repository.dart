import 'dart:convert';
import 'dart:io';

import 'package:dart_common/dart_common.dart';

import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/upload_m3u.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';

class StreamDBRepositoryImpl implements StreamDBRepository {
  final Fetcher _fetcher;

  StreamDBRepositoryImpl(Fetcher _fetcher) : _fetcher = _fetcher;

  @override
  WebRTCOutputUrl genWebRTC(int oid, String sid) {
    final server = _fetcher.server();
    if (server != null) {
      return server.genWebRTCOutputUrl(oid, sid);
    }

    return WebRTCOutputUrl.createDefault(id: oid);
  }

  @override
  Future<bool> addLiveStream(IStream stream) async {
    final requestData = stream.toJson();
    final resp = await _fetcher.post('/server/db/lives', requestData);
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> addVod(IStream stream) async {
    final requestData = stream.toJson();
    final resp = await _fetcher.post('/server/db/vods', requestData);
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> addEpisode(IStream stream) async {
    final requestData = stream.toJson();
    final resp = await _fetcher.post('/server/db/episodes', requestData);
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> uploadM3uFile(
      StreamType type,
      bool isSeries,
      double? imageCheckTime,
      final bool skipGroups,
      final bool fillInfo,
      final List<String> groups,
      Map<String, List<int>> data) async {
    final def = defaults();
    final m3u = UploadM3u(
        type: type,
        streamLogoIcon: def.streamLogoIcon,
        vodTrailerUrl: def.vodTrailerUrl,
        backgroundUrl: def.backgroundUrl,
        imageCheckTime: imageCheckTime,
        skipGroups: skipGroups,
        fillInfo: fillInfo,
        groups: groups,
        isSeries: isSeries);

    final requestData = m3u.toJson();
    final resp = await _fetcher.sendFiles('/server/db/stream/upload_file', data, requestData);
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> uploadM3uPackageFile(StreamType type, double? imageCheckTime, final bool skipGroups,
      final bool fillInfo, final List<String> groups, Map<String, List<int>> data) async {
    final def = defaults();
    final m3u = UploadM3uPackage(
        type: type,
        streamLogoIcon: def.streamLogoIcon,
        vodTrailerUrl: def.vodTrailerUrl,
        backgroundUrl: def.backgroundUrl,
        imageCheckTime: imageCheckTime,
        skipGroups: skipGroups,
        fillInfo: fillInfo,
        groups: groups);

    final requestData = m3u.toJson();
    final resp =
        await _fetcher.sendFiles('/server/db/stream/upload_package_file', data, requestData);
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> editLiveStream(IStream stream) async {
    final requestData = stream.toJson();
    final resp = await _fetcher.put('/server/db/lives/${stream.id}', requestData);
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> editVod(IStream stream) async {
    final requestData = stream.toJson();
    final resp = await _fetcher.put('/server/db/vods/${stream.id}', requestData);
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> editEpisode(IStream stream) async {
    final requestData = stream.toJson();
    final resp = await _fetcher.put('/server/db/episodes/${stream.id}', requestData);
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<String> getTokenUrl(String ip) async {
    final data = {'ip': ip};
    final response = await _fetcher.post('/server/stream/token', data);
    final respData = httpDataResponseFromString(response.body);
    if (respData == null) {
      throw 'Fetch error: invalid response format, code: ${response.statusCode}';
    }

    final err = respData.error();
    if (err != null) {
      throw 'Response error: ${err.message}';
    }

    final resp = respData.contentDynamic()!;
    return resp;
  }

  @override
  Future<bool> removeLiveStream(String id) async {
    final resp = await _fetcher.delete('/server/db/lives/$id');
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> removeCatchupStream(String id) async {
    final resp = await _fetcher.delete('/server/db/catchups/$id');
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> removeVod(String id) async {
    final resp = await _fetcher.delete('/server/db/vods/$id');
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> removeEpisode(String id) async {
    final resp = await _fetcher.delete('/server/db/episodes/$id');
    if (resp.statusCode == HttpStatus.ok) {
      return true;
    }
    return false;
  }

  @override
  Future<MediaFolders?> folders() async {
    final response = await _fetcher.get('/media/info');
    if (response.statusCode != 200) {
      return null;
    }

    final responseData = httpDataResponseFromString(response.body);
    final streamMap = responseData!.contentMap()!;
    return MediaFolders.fromJson(streamMap);
  }

  @override
  Future<String> probeOutUrl(OutputUrl url) async {
    final data = {'url': url.toJson()};
    final response = await _fetcher.post('/media/probe_out', data);
    final respData = httpDataResponseFromString(response.body);
    if (respData == null) {
      throw 'Fetch error: invalid response format, code: ${response.statusCode}';
    }

    final err = respData.error();
    if (err != null) {
      throw 'Response error: ${err.message}';
    }

    final content = respData.contentMap()!;
    final probeData = content['probe'];
    return json.encode(probeData);
  }

  @override
  Future<String> embedOutput(OutputUrl url) async {
    final resp = await _fetcher.get('/server/stream/embed?src=${url.uri}');
    if (resp.statusCode == HttpStatus.ok) {
      return resp.body;
    }

    throw 'Wrong response code ${resp.statusCode}';
  }

  @override
  Future<String> probeInUrl(InputUrl url) async {
    final data = {'url': url.toJson()};
    final response = await _fetcher.post('/media/probe_in', data);
    final respData = httpDataResponseFromString(response.body);
    if (respData == null) {
      throw 'Fetch error: invalid response format, code: ${response.statusCode}';
    }

    final err = respData.error();
    if (err != null) {
      throw 'Response error: ${err.message}';
    }

    final content = respData.contentMap()!;
    final probeData = content['probe'];
    return json.encode(probeData);
  }

  @override
  Future<void> startLive(String sid) {
    return _fetcher.get('/server/db/lives/$sid/start');
  }

  @override
  Future<void> startVod(String sid) {
    return _fetcher.get('/server/db/vods/$sid/start');
  }

  @override
  Future<void> startEpisode(String sid) {
    return _fetcher.get('/server/db/episodes/$sid/start');
  }

  @override
  Defaults defaults() {
    return _fetcher.defaults();
  }

  ///Episodes
  @override
  Future<List<IStream>> loadEpisodes() async {
    final response = await _fetcher.get('/server/db/episodes');
    final respData = httpDataResponseFromString(response.body);
    final streamsData = respData!.contentMap()!;

    final series = <IStream>[];
    for (final streamData in streamsData['episodes']) {
      final stream = makeStream(streamData);
      if (stream != null) {
        series.add(stream);
      }
    }

    return series;
  }

  @override
  Future<IStream?> loadEpisodeById(String id) async {
    final response = await _fetcher.get('/server/db/episodes/$id');
    if (response.statusCode != 200) {
      return null;
    }

    final responseData = httpDataResponseFromString(response.body);
    final streamMap = responseData!.contentMap()!;
    final IStream? episode = makeStream(streamMap);
    return episode;
  }

  ///Live streams
  @override
  Future<List<IStream>> loadLiveStreams() async {
    final response = await _fetcher.get('/server/db/lives');
    final respData = httpDataResponseFromString(response.body);
    final streamsData = respData!.contentMap()!;

    final liveStreams = <IStream>[];
    for (final streamData in streamsData['streams']) {
      final stream = makeStream(streamData);
      if (stream != null) {
        liveStreams.add(stream);
      }
    }

    return liveStreams;
  }

  @override
  Future<IStream?> loadLiveStreamById(String id) async {
    final response = await _fetcher.get('/server/db/lives/$id');
    if (response.statusCode != 200) {
      return null;
    }

    final responseData = httpDataResponseFromString(response.body);
    final streamMap = responseData!.contentMap()!;
    final IStream? live = makeStream(streamMap);
    return live;
  }

  ///Vods
  @override
  Future<List<IStream>> loadVods() async {
    final response = await _fetcher.get('/server/db/vods');
    final respData = httpDataResponseFromString(response.body);
    final streamsData = respData!.contentMap()!;

    final vods = <IStream>[];
    for (final streamData in streamsData['vods']) {
      final stream = makeStream(streamData);
      if (stream != null) {
        vods.add(stream);
      }
    }

    return vods;
  }

  @override
  Future<IStream?> loadVodById(String id) async {
    final response = await _fetcher.get('/server/db/vods/$id');
    if (response.statusCode != 200) {
      return null;
    }

    final responseData = httpDataResponseFromString(response.body);
    final streamMap = responseData!.contentMap()!;
    final IStream? vod = makeStream(streamMap);
    return vod;
  }

  ///Catchups
  @override
  Future<List<CatchupStream>> loadCatchups() async {
    final response = await _fetcher.get('/server/db/catchups');
    final respData = httpDataResponseFromString(response.body);
    final streamsData = respData!.contentMap()!;

    final List<CatchupStream> catchups = <CatchupStream>[];
    for (final streamData in streamsData['catchups']) {
      final stream = makeStream(streamData);
      if (stream != null && stream is CatchupStream && stream.type() == StreamType.CATCHUP) {
        catchups.add(stream);
      }
    }

    return catchups;
  }

  @override
  Future<CatchupStream?> loadCatchupById(String id) async {
    final response = await _fetcher.get('/server/db/catchups/$id');
    if (response.statusCode != 200) {
      return null;
    }

    final responseData = httpDataResponseFromString(response.body);
    final streamMap = responseData!.contentMap()!;
    final IStream? catchup = makeStream(streamMap);
    if (catchup is CatchupStream) {
      return catchup;
    }
    return null;
  }

  ///Seasons
  @override
  Future<List<ServerSeason>> loadSeasons() async {
    final response = await _fetcher.get('/server/db/seasons');
    final respData = httpDataResponseFromString(response.body);
    final data = respData!.contentMap()!;

    final List<ServerSeason> result = [];
    data['seasons'].forEach((s) {
      final res = ServerSeason.fromJson(s);
      result.add(res);
    });
    return result;
  }

  @override
  Future<void> addSeason(ServerSeason season) {
    final resp = _fetcher.post('/server/db/seasons', season.toJson());
    return resp;
  }

  @override
  Future<void> editSeason(ServerSeason season) {
    final resp = _fetcher.put('/server/db/seasons/${season.id}', season.toJson());
    return resp;
  }

  @override
  Future<void> removeSeason(ServerSeason season) {
    final resp = _fetcher.delete('/server/db/seasons/${season.id}');
    return resp;
  }

  ///Serials
  @override
  Future<List<ServerSerial>> loadSerials() async {
    final response = await _fetcher.get('/server/db/serials');
    final respData = httpDataResponseFromString(response.body);
    final data = respData!.contentMap()!;

    final List<ServerSerial> result = [];
    data['serials'].forEach((s) {
      final res = ServerSerial.fromJson(s);
      result.add(res);
    });
    return result;
  }

  @override
  Future<void> addSerial(ServerSerial serial) {
    final resp = _fetcher.post('/server/db/serials', serial.toJson());
    return resp;
  }

  @override
  Future<void> editSerial(ServerSerial serial) {
    final resp = _fetcher.put('/server/db/serials/${serial.id}', serial.toJson());
    return resp;
  }

  @override
  Future<void> removeSerial(ServerSerial serial) {
    final resp = _fetcher.delete('/server/db/serials/${serial.id}');
    return resp;
  }
}
