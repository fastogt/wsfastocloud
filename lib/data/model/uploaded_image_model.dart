import 'package:equatable/equatable.dart';

class UploadedImageModel extends Equatable {
  final String path;

  const UploadedImageModel({required this.path});

  factory UploadedImageModel.fromMap(Map<String, dynamic> map) {
    return UploadedImageModel(path: map["path"]);
  }

  UploadedImageModel copyWith({String? path}) {
    return UploadedImageModel(path: path ?? this.path);
  }

  @override
  List<Object?> get props => [path];
}
