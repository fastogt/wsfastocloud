import 'package:equatable/equatable.dart';
import 'package:wsfastocloud/domain/entity/file.dart';

class UploadImageModel extends Equatable {
  final String name;
  final List<int> bytes;

  const UploadImageModel({required this.name, required this.bytes});

  Map<String, List<int>> toMap() {
    return {name: bytes};
  }

  factory UploadImageModel.fromEntity(File entity) {
    return UploadImageModel(name: entity.name, bytes: entity.bytes);
  }

  @override
  List<Object?> get props => [name, bytes];
}
