import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color white_2 = Color(0xFFFAFAFA);

  static const Color orange = Color(0xFFED6D4C);

  static const Color green = Color(0xFF30D158);

  static const Color grey = Color(0xFFA8A8A8);
  static const Color grey_2 = Color(0xFF49454F);
  static const Color grey_3 = Color(0xFF171717);
  static const Color grey_4 = Color(0xFF787878);
  static const Color grey_5 = Color(0xFFE1E6EF);
  static const Color grey_6 = Color(0xFF19202F);
  static const Color grey_7 = Color(0xFFE8DFFB);
  static const Color grey_8 = Color(0xFF303030);

  static const Color violet = Color(0xFF5C16E5);
  static const Color violet_2 = Color(0xFFB28BFF);
  static const Color violet_3 = Color(0xFF29164F);
  static const Color black = Color(0xFF000000);

  static const Color pink = Color(0xFFE648A4);
  static const Color pink_4 = Color(0xFFA463C7);
  static const Color blue = Color(0xFF5F8CE6);

  static const Color pink_2 = Color(0xFFE95EAF);
  static const Color pink_3 = Color(0xFFAF76CE);
  static const Color blue_2 = Color(0xFF759CE9);

  static const Color yellow = Color(0x97F1C70C);
  static const Color yellow_2 = Color(0xFFE2DF0A);

  static const Color maroon = Color(0x81830404);
  static const Color maroon_2 = Color(0xB0E63A3A);

  static const Color aqua = Color(0x3700FFFF);
  static const Color aqua_2 = Color(0xB02DE0E0);

  static const Color brown = Color(0x84964E06);
  static const Color brown_2 = Color(0x9ED38A41);
}
