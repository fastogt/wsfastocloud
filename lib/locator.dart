import 'package:flutter_common/flutter_common.dart';
import 'package:get_it/get_it.dart';
import 'package:wsfastocloud/data/services/local_storage/local_storage_service.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  final package = await PackageManager.getInstance();
  locator.registerSingleton<PackageManager>(package);

  final storage = await LocalStorageService.getInstance();
  locator.registerSingleton<LocalStorageService>(storage);
}
