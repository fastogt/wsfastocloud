import "package:flutter/material.dart";
import "package:wsfastocloud/app_colors.dart";

class AppTheme {
  static ThemeData get lightTheme => ThemeData(
      primaryColorLight: AppColors.violet,
      scaffoldBackgroundColor: AppColors.white_2,
      tabBarTheme: const TabBarTheme(labelColor: Colors.grey),
      dropdownMenuTheme: const DropdownMenuThemeData(
          textStyle: TextStyle(
            color: AppColors.violet,
          ),
          menuStyle: MenuStyle(
            elevation: WidgetStatePropertyAll(10),
          )),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.violet,
          foregroundColor: AppColors.white_2,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
      useMaterial3: true,
      primaryColor: AppColors.violet,
      primarySwatch: Colors.grey,
      colorScheme: const ColorScheme.light(
        onPrimary: AppColors.violet,
        outlineVariant: Colors.black12,
        primary: AppColors.violet,
        surfaceTint: Colors.white,
        primaryContainer: AppColors.violet,
      ),
      checkboxTheme: CheckboxThemeData(
        side: const BorderSide(width: 2),
        checkColor: WidgetStateColor.resolveWith((_) => Colors.white),
        fillColor: WidgetStateColor.resolveWith((Set<WidgetState> states) =>
            states.contains(WidgetState.selected) ? AppColors.violet : Colors.white),
      ),
      appBarTheme: const AppBarTheme(
          iconTheme: IconThemeData(color: Colors.white),
          actionsIconTheme: IconThemeData(color: Colors.white)),
      inputDecorationTheme: const InputDecorationTheme(
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.redAccent),
          ),
          border: OutlineInputBorder(),
          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey))),
      scrollbarTheme: const ScrollbarThemeData(interactive: true),
      textButtonTheme:
          TextButtonThemeData(style: TextButton.styleFrom(foregroundColor: Colors.black)),
      textSelectionTheme: const TextSelectionThemeData(cursorColor: AppColors.violet),
      popupMenuTheme: const PopupMenuThemeData(iconColor: AppColors.white_2));

  static ThemeData get darkTheme => ThemeData(
        useMaterial3: true,
        scaffoldBackgroundColor: Colors.black87,
        tabBarTheme: const TabBarTheme(labelColor: Colors.grey),
        dropdownMenuTheme: const DropdownMenuThemeData(
            inputDecorationTheme: InputDecorationTheme(filled: true, fillColor: Colors.red),
            textStyle: TextStyle(
              color: AppColors.violet_2,
            ),
            menuStyle: MenuStyle(
              elevation: WidgetStatePropertyAll(10),
            )),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            backgroundColor: AppColors.violet_2,
            foregroundColor: AppColors.black,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        ),
        textSelectionTheme: const TextSelectionThemeData(cursorColor: Colors.white),
        dialogBackgroundColor: Colors.black,
        radioTheme: RadioThemeData(
          fillColor: WidgetStateColor.resolveWith((Set<WidgetState> states) =>
              states.contains(WidgetState.selected) ? AppColors.violet_2 : Colors.white),
          splashRadius: 20,
        ),
        popupMenuTheme: const PopupMenuThemeData(
            iconColor: AppColors.white_2, surfaceTintColor: AppColors.violet_2),
        primarySwatch: _primaryBlack,
        buttonTheme: const ButtonThemeData(disabledColor: Colors.red),
        colorScheme: const ColorScheme.dark(
            outlineVariant: Colors.white12,
            primary: AppColors.violet_2,
            secondary: Colors.white,
            onPrimary: Colors.white),
        appBarTheme: const AppBarTheme(
            iconTheme: IconThemeData(color: Colors.white),
            actionsIconTheme: IconThemeData(color: Colors.white)),
        inputDecorationTheme: const InputDecorationTheme(
            errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.redAccent)),
            border: OutlineInputBorder(),
            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12))),
        scrollbarTheme: const ScrollbarThemeData(interactive: true),
        textButtonTheme: TextButtonThemeData(
            style: TextButton.styleFrom(
                foregroundColor: Colors.black, backgroundColor: Colors.white12)),
        checkboxTheme: CheckboxThemeData(
          side: const BorderSide(color: Colors.white, width: 2),
          checkColor: WidgetStateColor.resolveWith((_) => Colors.black),
          fillColor: WidgetStateColor.resolveWith((Set<WidgetState> states) =>
              states.contains(WidgetState.selected) ? AppColors.violet_2 : Colors.black),
        ),
      );

  static const MaterialColor _primaryBlack = MaterialColor(0xFF000000, <int, Color>{
    50: Color.fromRGBO(0, 0, 0, .1),
    100: Color.fromRGBO(0, 0, 0, .2),
    200: Color.fromRGBO(0, 0, 0, .3),
    300: Color.fromRGBO(0, 0, 0, .4),
    400: Color.fromRGBO(0, 0, 0, .5),
    500: Color.fromRGBO(0, 0, 0, .6),
    600: Color.fromRGBO(0, 0, 0, .7),
    700: Color.fromRGBO(0, 0, 0, .8),
    800: Color.fromRGBO(0, 0, 0, .9),
    900: Color.fromRGBO(0, 0, 0, 1)
  });
}
