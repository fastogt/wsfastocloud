import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:provider/provider.dart';
import 'package:wsfastocloud/data/services/api/models/stream_statistics.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/streams/on_air/stream_data_source.dart';
import 'package:wsfastocloud/presenter/streams/on_air/stream_statistics_bloc/stream_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/output_player_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/change_source_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/streams/actions.dart';

class LiveWidget extends StatefulWidget {
  final LiveStreamDataSource _liveDataSource;

  const LiveWidget(this._liveDataSource);

  @override
  _LiveWidgetState createState() {
    return _LiveWidgetState();
  }
}

class _LiveWidgetState extends State<LiveWidget> {
  @override
  Widget build(BuildContext context) {
    return DataTableEx(
        widget._liveDataSource,
        DataTableSearchHeader(source: widget._liveDataSource),
        _liveActions,
        null,
        null,
        null,
        null,
        true,
        null,
        Theme.of(context).colorScheme.primary);
  }

  List<Widget> _liveActions() {
    if (widget._liveDataSource.selectedRowCount == 0) {
      return [];
    }

    final bloc = context.read<StreamsStatisticsBloc>();
    final streams = widget._liveDataSource.selectedItems();
    final List<StreamStatistics> changers = [];
    for (final stream in streams) {
      if (stream.type == StreamType.CHANGER_ENCODE ||
          stream.type == StreamType.CHANGER_RELAY ||
          stream.type == StreamType.LITE) {
        changers.add(stream);
      }
    }

    bool singleOnly = false;
    if (streams.length == 1) {
      singleOnly = true;
    }

    return [
      Row(children: [
        StreamActionIcon.stop(() => _stop(bloc, streams), context),
        StreamActionIcon.kill(() => _kill(bloc, streams), context),
        StreamActionIcon.restart(() => _restart(bloc, streams), context),
        if (changers.isNotEmpty)
          StreamActionIcon.changeStream(() => _changeSource(bloc, changers), context),
        if (singleOnly)
          StreamActionIcon.playOutput(() => _playOutputStream(bloc, streams), context),
        if (singleOnly) StreamActionIcon.getLog(() => _getLog(bloc, streams), context),
        if (singleOnly) StreamActionIcon.getConfig(() => _getConfig(bloc, streams), context),
        if (singleOnly) StreamActionIcon.getPipeline(() => _getPipeLine(bloc, streams), context)
      ])
    ];
  }

  void _stop(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (final stream in streams) {
      bloc.stopStream(stream.id);
    }
  }

  void _kill(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (final stream in streams) {
      bloc.killStream(stream.id);
    }
  }

  void _restart(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (final stream in streams) {
      bloc.restartStream(stream.id);
    }
  }

  void _changeSource(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (final stream in streams) {
      final resp = bloc.getConfigStreamMap(stream.id);
      resp.then((config) {
        final input = config[RawHardwareStream.INPUT_FIELD];
        final List<InputUrl> inputs = [];
        input.forEach((element) => inputs.add(makeInputUrl(element)));

        final result = showDialog<int>(
            context: context,
            builder: (context) {
              return ChangeSourceDialog(inputs);
            });
        result.then((value) {
          if (value == null) {
            return;
          }

          bloc.changeSource(stream.id, value);
        });
      });
    }
  }

  void _getLog(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (final stream in streams) {
      bloc.getLogStream(stream.id);
    }
  }

  void _getConfig(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (final stream in streams) {
      final resp = bloc.getConfigStreamString(stream.id);

      resp.then((body) {
        final TextEditingController _textController = TextEditingController(text: body);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return SimpleDialog(title: Text(context.l10n.getConfig), children: <Widget>[
                Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(children: [
                      TextField(
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          controller: _textController),
                      const SizedBox(height: 5.0),
                      FlatButtonEx.filled(
                          text: context.l10n.copy,
                          onPressed: () {
                            Clipboard.setData(ClipboardData(text: _textController.text));
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text(context.l10n.copied),
                            ));
                          }),
                      const SizedBox(height: 5.0),
                      FlatButtonEx.filled(
                          text: context.l10n.close,
                          onPressed: () {
                            Navigator.of(context).pop();
                          })
                    ]))
              ]);
            });
      }, onError: (error) {
        showError(context, error);
      });
    }
  }

  void _getPipeLine(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (final stream in streams) {
      bloc.getPipeLineStream(stream.id);
    }
  }

  void _playOutputStream(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (final stream in streams) {
      final resp = bloc.getConfigStreamMap(stream.id);
      resp.then((config) {
        final out = config[IStream.OUTPUT_FIELD];
        final List<OutputUrl> toplay = [];
        out.forEach((element) {
          final m = makeOutputUrl(element);
          toplay.add(m);
        });
        if (toplay.isEmpty) {
          return;
        }

        showDialog(
            context: context,
            builder: (context) {
              return OutputPlayerDialog(stream.name, stream.type, toplay);
            });
      });
    }
  }
}
