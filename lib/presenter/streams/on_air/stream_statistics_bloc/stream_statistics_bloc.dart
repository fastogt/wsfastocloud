import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:meta/meta.dart';
import 'package:wsfastocloud/data/services/api/models/stream_statistics.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/domain/repositories/stream_repository.dart';
import 'package:wsfastocloud/presenter/streams/on_air/stream_data_source.dart';

part 'stream_statistics_event.dart';

part 'stream_statistics_state.dart';

class StreamsStatisticsBloc extends Bloc<StreamsStatisticsEvent, StreamsStatisticsState> {
  StreamsStatisticsBloc(this.webSocketApiBloc, this.streamRepository, this._streamDBRepository)
      : super(const StreamsStatisticsInitial()) {
    on<ListenStatisticsEvent>(_listen);
    on<UpdateStreamEvent>(_updateStream);
    on<RemoveStreamEvent>(_removeStream);
    on<FetchStatisticEvent>(_fetch);
    on<InitialEvent>(_init);
    add(const ListenStatisticsEvent());
  }

  late final StreamSubscription _subscription;

  final WebSocketApiBloc webSocketApiBloc;
  final StreamRepository streamRepository;
  final LiveStreamDataSource _liveDataSource = LiveStreamDataSource([]);
  final StreamDBRepository _streamDBRepository;

  Future<void> stopStream(String id) {
    return streamRepository.stopStream(id);
  }

  Future<String> getConfigStreamString(String id) {
    return streamRepository.getConfigStreamString(id);
  }

  Future<Map<String, dynamic>> getConfigStreamMap(String id) {
    return streamRepository.getConfigStreamMap(id);
  }

  Future<void> changeSource(String streamId, int index) {
    return streamRepository.changeSource(streamId, index);
  }

  Future<void> getLogStream(String id) {
    return streamRepository.getLogStream(id);
  }

  Future<void> getPipeLineStream(String id) {
    return streamRepository.getPipeLineStream(id);
  }

  Future<void> killStream(String id) {
    return streamRepository.stopStream(id, true);
  }

  Future<void> restartStream(String id) {
    return streamRepository.restartStream(id);
  }

  void dispose() {
    _subscription.cancel();
  }

  void _init(InitialEvent event, Emitter<StreamsStatisticsState> emit) {
    _liveDataSource.clearItems();
    emit(const StreamsStatisticsInitial());
  }

  void _listen(ListenStatisticsEvent event, Emitter<StreamsStatisticsState> emit) {
    _subscription = webSocketApiBloc.stream.listen((event) async {
      if (event is WebSocketApiMessage) {
        const wsStatStream = 'statistic_stream';
        const wsQuitStream = 'quit_status_stream';
        const wsUpdatedStream = 'stream_updated';

        if (event.type == wsStatStream) {
          final StreamStatisticsBack base = StreamStatisticsBack.fromJson(event.data);
          if (state is StreamsStatisticsData) {
            final streams = (state as StreamsStatisticsData).streams;
            final found = streams[base.id];
            StreamStatistics? stream;
            if (found != null) {
              stream = StreamStatistics.fromBase(found.name, found.icon, base);
            } else {
              stream = await _fetchStreamStatistics(base);
            }
            add(UpdateStreamEvent(stream));
          } else {
            // status pre load
            final def = _streamDBRepository.defaults();
            const String name = 'Unknown';
            final String iconUrl = def.streamLogoIcon;
            add(UpdateStreamEvent(StreamStatistics.fromBase(name, iconUrl, base)));
          }
        } else if (event.type == wsQuitStream) {
          final streamId = event.data['id'];
          add(RemoveStreamEvent(streamId));
        } else if (event.type == wsUpdatedStream) {
          final IStream? stream = makeStream(event.data);

          if (state is StreamsStatisticsData && stream != null) {
            final streams = (state as StreamsStatisticsData).streams;
            final found = streams[stream.id];
            if (found != null) {
              found.name = stream.name;
              found.icon = stream.icon;
              add(UpdateStreamEvent(found));
            }
          }
        }
      } else if (event is WebSocketFailure) {
        emit(StreamsStatisticsFailure(event.description));
      } else if (event is WebSocketConnected) {
        add(const FetchStatisticEvent());
      } else {
        add(const InitialEvent());
      }
    });

    if (webSocketApiBloc.isConnected()) {
      add(const FetchStatisticEvent());
    }
  }

  Future<StreamStatistics> _fetchStreamStatistics(StreamStatisticsBack streamStatistics) async {
    final def = _streamDBRepository.defaults();
    String name = 'Unknown';
    String iconUrl = def.streamLogoIcon;

    try {
      if (streamStatistics.type == StreamType.CATCHUP) {
        final catchup = await _streamDBRepository.loadCatchupById(streamStatistics.id);
        if (catchup != null) {
          name = catchup.name;
          iconUrl = catchup.icon;
        }
      } else if (streamStatistics.type == StreamType.VOD_ENCODE ||
          streamStatistics.type == StreamType.VOD_RELAY) {
        IStream? vod = await _streamDBRepository.loadVodById(streamStatistics.id);
        vod ??= await _streamDBRepository.loadEpisodeById(streamStatistics.id);

        if (vod != null) {
          name = vod.name;
          iconUrl = vod.icon;
        }
      } else {
        final live = await _streamDBRepository.loadLiveStreamById(streamStatistics.id);
        if (live != null) {
          name = live.name;
          iconUrl = live.icon;
        }
      }
    } catch (_) {}

    return StreamStatistics.fromBase(name, iconUrl, streamStatistics);
  }

  void _updateStream(UpdateStreamEvent event, Emitter<StreamsStatisticsState> emit) {
    if (state is! StreamsStatisticsData) {
      return;
    }

    final streams = (state as StreamsStatisticsData).streams;

    if (streams.containsKey(event.stream.id)) {
      _liveDataSource.updateItem(event.stream);
      emit(StreamsStatisticsData.update(streams, event.stream, _liveDataSource));
    } else {
      _liveDataSource.addItem(event.stream);
      emit(StreamsStatisticsData.add(streams, event.stream, _liveDataSource));
    }
  }

  void _removeStream(RemoveStreamEvent event, Emitter<StreamsStatisticsState> emit) {
    if (state is StreamsStatisticsData) {
      final streams = (state as StreamsStatisticsData).streams;
      final removed = streams[event.id]!;
      _liveDataSource.removeItem(removed);
      emit(StreamsStatisticsData.remove(streams, removed, _liveDataSource));
    }
  }

  void _fetch(FetchStatisticEvent event, Emitter<StreamsStatisticsState> emit) async {
    try {
      final streams = await streamRepository.getStreamsStatistics();
      final result = <StreamStatistics>[];
      for (final StreamStatisticsBack stream in streams) {
        final res = await _fetchStreamStatistics(stream);
        result.add(res);
      }

      _liveDataSource.addItems(result);

      emit(const StreamsStatisticsInitial());
      emit(StreamsStatisticsData.create(result, _liveDataSource));
    } catch (e) {
      emit(StreamsStatisticsFailure('Backend error: $e'));
    }
  }
}
