part of 'stream_statistics_bloc.dart';

@immutable
abstract class StreamsStatisticsEvent extends Equatable {
  const StreamsStatisticsEvent();

  @override
  List<Object?> get props => [];
}

class ListenStatisticsEvent extends StreamsStatisticsEvent {
  const ListenStatisticsEvent();

  @override
  List<Object?> get props => [];
}

class UpdateStreamEvent extends StreamsStatisticsEvent {
  final StreamStatistics stream;

  const UpdateStreamEvent(this.stream);

  @override
  List<Object?> get props => [stream];
}

class RemoveStreamEvent extends StreamsStatisticsEvent {
  final String? id;

  const RemoveStreamEvent(this.id);

  @override
  List<Object?> get props => [id];
}

class FetchStatisticEvent extends StreamsStatisticsEvent {
  const FetchStatisticEvent();

  @override
  List<Object?> get props => [];
}

class InitialEvent extends StreamsStatisticsEvent {
  const InitialEvent();

  @override
  List<Object?> get props => [];
}
