import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/services/api/models/stream_statistics.dart';
import 'package:wsfastocloud/presenter/streams/store/utils_source.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

Widget streamTilesNameAndIcon(StreamStatistics item) {
  return NameAndIcon(
      name: Text(item.name), icon: PreviewIcon.live(item.icon, width: 40, height: 40));
}

class LiveStreamDataSource extends SortableDataSource<StreamStatistics> {
  LiveStreamDataSource(List<DataEntry<StreamStatistics>> items) : super(items: items);

  @override
  bool equalItemsCondition(StreamStatistics item, StreamStatistics listItem) {
    return item.id == listItem.id;
  }

  @override
  List<Widget> headers() {
    final headers = [
      'Id',
      'Name',
      'Type',
      'Status',
      'Restarts',
      'CPU (%)',
      'RSS (Mb)',
      'Net in (Mbps)',
      'Net out (Mbps)',
      'Start time',
      'Loop time',
      'Quality'
    ];

    return List.generate(headers.length, (index) => Text(headers[index]));
  }

  @override
  String get itemsName => 'streams';

  @override
  Widget get noItems => const _NoStreamsAvailable();

  @override
  bool searchCondition(String text, StreamStatistics item) =>
      item.name.toLowerCase().contains(text.toLowerCase());

  @override
  List<Widget> tiles(StreamStatistics item) {
    return [
      Text(item.id),
      streamTilesNameAndIcon(item),
      Text(item.type.toHumanReadable()),
      Text(item.status.toHumanReadable()),
      Text(item.restarts.toString()),
      Text(item.cpuFixed.toString()),
      Text(item.rssInMegabytes.toString()),
      Text(item.inputMbps.toString()),
      Text(item.outputMbps.toString()),
      Text(item.startDuration.toString()),
      Text(item.loopDuration.toString()),
      Text(item.qualityFixed.toString())
    ];
  }

  @override
  int compareItems(StreamStatistics a, StreamStatistics b, int index) {
    switch (index) {
      case 1:
        return a.id.compareTo(b.id);
      case 2:
        return a.name.compareTo(a.name.toString());
      case 3:
        return a.type.compareTo(a.type);
      case 4:
        return a.status.compareTo(b.status);
      case 5:
        return a.restarts.compareTo(b.restarts);
      case 6:
        return a.cpuFixed.compareTo(b.cpuFixed);
      case 7:
        return a.rssInMegabytes.compareTo(b.rssInMegabytes);
      case 8:
        return a.inputMbps.compareTo(b.inputMbps);
      case 9:
        return a.outputMbps.compareTo(b.outputMbps);
      case 10:
        return a.startDuration.compareTo(b.startDuration);
      case 11:
        return a.loopDuration.compareTo(b.loopDuration);
      case 12:
        return a.qualityFixed.compareTo(b.qualityFixed);

      default:
        throw ArgumentError.value(index, 'index');
    }
  }
}

class _NoStreamsAvailable extends StatelessWidget {
  const _NoStreamsAvailable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'No streams available'));
  }
}
