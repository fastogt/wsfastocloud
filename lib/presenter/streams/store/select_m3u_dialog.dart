import 'dart:developer';
import 'dart:io';

import 'package:fastocloud_dart_media_models/models.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';

class SelectM3uDialog extends StatefulWidget {
  final IStreamEditor editor;
  final List<StreamType> types;
  final bool isSeries;
  final String Function(StreamType type) convertTypeToText;

  const SelectM3uDialog(
      {required this.editor,
      required this.types,
      required this.isSeries,
      this.convertTypeToText = defaultConverter});

  @override
  _SelectM3uDialogState createState() {
    return _SelectM3uDialogState();
  }

  static String defaultConverter(StreamType type) {
    return type.toHumanReadableWS();
  }
}

class _SelectM3uDialogState extends State<SelectM3uDialog> {
  Map<String, List<int>> _filesData = {};
  final List<String> _tags = [];
  bool _parsingStreams = false;
  double? _imageCheckTime;
  bool _skipGroups = false;
  bool _fillInfo = true;

  late StreamType _type = widget.types[0];

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        title: Text(context.l10n.m3uUpload),
        children: _parsingStreams
            ? [
                const Center(
                    child:
                        Padding(padding: EdgeInsets.all(8.0), child: CircularProgressIndicator()))
              ]
            : _fields());
  }

  List<Widget> _fields() {
    return <Widget>[
      _typeField(),
      _tagsField(),
      _skipFileGroups(),
      _fillInfoBox(),
      Padding(padding: const EdgeInsets.symmetric(horizontal: 8.0), child: _timeImageCheckFiled()),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child:
              FlatButtonEx.filled(text: context.l10n.selectAFile, onPressed: _startWebFilePicker)),
      const SizedBox(height: 8),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(
              text: context.l10n.upload, onPressed: _filesData.isEmpty ? null : _makeRequest))
    ];
  }

  // private:
  void _startWebFilePicker() async {
    _filesData = {};

    try {
      final FilePickerResult? result = await FilePicker.platform.pickFiles(withData: true);
      if (result == null) {
        return;
      }

      for (int i = 0; i < result.files.length; ++i) {
        final PlatformFile file = result.files[i];
        if (kIsWeb) {
          final pfile = file.xFile;
          final data = await pfile.readAsBytes();
          _filesData[file.name] = data;
        } else if (file.path != null) {
          final pfile = File(file.path!);
          final data = await pfile.readAsBytes();
          _filesData[file.name] = data;
        }
      }
      setState(() {});
    } on PlatformException catch (e) {
      log('Unsupported operation: $e');
    } catch (err) {
      log('Exception: $err');
    }
  }

  Widget _typeField() {
    return DropdownButtonEx<StreamType>(
        hint: widget.convertTypeToText(_type),
        value: _type,
        values: widget.types,
        onChanged: (c) {
          _type = c;
        },
        itemBuilder: (StreamType value) {
          return DropdownMenuItem(child: Text(widget.convertTypeToText(value)), value: value);
        });
  }

  Widget _timeImageCheckFiled() {
    return OptionalFieldTile(
        title: context.l10n.imageCheckTime,
        init: _imageCheckTime != null,
        onChanged: (value) {
          _imageCheckTime = value ? 0.05 : null;
        },
        builder: () {
          return NumberTextField.decimal(
              hintText: context.l10n.imageCheckTimesec,
              initDouble: _imageCheckTime,
              minDouble: 0.01,
              maxDouble: 1,
              onFieldChangedDouble: (term) {
                _imageCheckTime = term!;
              });
        });
  }

  Widget _skipFileGroups() {
    return StateCheckBox(
        title: context.l10n.skipM3uGroups,
        init: _skipGroups,
        onChanged: (value) {
          _skipGroups = value;
        });
  }

  Widget _fillInfoBox() {
    return StateCheckBox(
        title: context.l10n.fillInfo,
        init: _fillInfo,
        onChanged: (value) {
          _fillInfo = value;
        });
  }

  Widget _tagsField() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ChipListField(
            values: _tags,
            hintText: context.l10n.groups,
            onItemAdded: (value) => setState(() => _tags.add(value)),
            onItemRemoved: (index) => setState(() => _tags.removeAt(index))));
  }

  void _makeRequest() {
    if (mounted) {
      setState(() {
        _parsingStreams = true;
      });
    }

    widget.editor
        .uploadM3uFile(
            _type, widget.isSeries, _imageCheckTime, _skipGroups, _fillInfo, _tags, _filesData)
        .then((bool value) {
      Navigator.of(context).pop();
    }, onError: (error) {
      if (mounted) {
        setState(() {
          _parsingStreams = false;
        });
      }
      showError(context, error);
    });
  }
}
