import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wsfastocloud/data/repositories/store_editor.dart';
import 'package:wsfastocloud/presenter/streams/store/make_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/options_menu.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/vod_card.dart';

class StoreCardWidget extends StatefulWidget {
  final List<IStream> items;
  final Widget emptyWidget;
  final LiveServer? liveServer;
  final bool isSerial;
  const StoreCardWidget(
      {super.key,
      required this.items,
      required this.emptyWidget,
      required this.liveServer,
      required this.isSerial});

  @override
  State<StoreCardWidget> createState() {
    return _StoreCardWidgetState();
  }
}

class _StoreCardWidgetState extends State<StoreCardWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.items.isEmpty) {
      return widget.emptyWidget;
    }

    final bloc = context.read<StreamsStoreBloc>();
    return CardGrid<IStream>(widget.items,
        (int i, IStream item, double maxWidth, double maxHeight) {
      bool canRefresh = item.type() != StreamType.VOD_PROXY;
      if (canRefresh && widget.liveServer != null) {
        canRefresh &= true;
      }
      return Stack(children: <Widget>[
        VodCard(
            iconLink: item.icon,
            width: maxWidth,
            height: maxHeight,
            onPressed: () => _editStoreStream(bloc, [item])),
        VodCardBadge(
            right: 5,
            top: 5,
            width: 36.0,
            child: VodOptionMenu(
                isSerial: widget.isSerial,
                getServer: widget.liveServer,
                canRefresh: canRefresh,
                stream: item,
                bloc: bloc))
      ]);
    });
  }

  void _editStoreStream(StreamsStoreBloc bloc, List<IStream> streams) {
    final cur = widget.liveServer;

    for (final stream in streams) {
      showDialog(
          context: context,
          builder: (context) {
            return makeEditStreamDialog(cur!, StoreEditor(bloc), stream, false);
          });
    }
  }
}
