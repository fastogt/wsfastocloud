import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/streams/store/utils_source.dart';

class StoreEpisodesDataSource extends SortableDataSource<IStream> {
  StoreEpisodesDataSource(List<DataEntry<IStream>> items) : super(items: items);

  @override
  bool equalItemsCondition(IStream item, IStream listItem) {
    return item.id == listItem.id;
  }

  @override
  List<Widget> headers() {
    final headers = ['Id', 'Name', 'Created Date', 'Type'];

    return List.generate(
        headers.length,
        (index) => Text(headers[index],
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w500)));
  }

  @override
  String get itemsName => 'episodes';

  @override
  Widget get noItems => const NoEpisodesAvailable();

  @override
  bool searchCondition(String text, IStream stream) {
    return stream.name.toLowerCase().contains(text.toLowerCase());
  }

  @override
  List<Widget> tiles(IStream s) {
    const TextStyle textStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.w200);

    return [
      Text(s.id.toString(), style: textStyle),
      streamTilesNameAndIcon(s),
      Text(date(s.createdDate!), style: textStyle),
      Text(s.type().toHumanReadableWS(), style: textStyle)
    ];
  }

  @override
  int compareItems(IStream a, IStream b, int index) {
    switch (index) {
      case 1:
        return a.id!.compareTo(b.id!);
      case 2:
        return a.name.compareTo(b.name);
      case 3:
        return a.createdDate!.compareTo(b.createdDate!);
      case 4:
        return a.type().compareTo(b.type());

      default:
        throw ArgumentError.value(index, 'index');
    }
  }
}

class NoEpisodesAvailable extends StatelessWidget {
  const NoEpisodesAvailable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: NonAvailableBuffer(icon: Icons.error, message: context.l10n.pleaseCreateEpisodes));
  }
}
