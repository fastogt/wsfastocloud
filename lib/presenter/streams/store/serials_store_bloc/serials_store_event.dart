part of 'serials_store_bloc.dart';

abstract class SerialEvent extends Equatable {
  const SerialEvent();

  @override
  List<Object?> get props => [];
}

class LoadSerialsEvent extends SerialEvent {
  const LoadSerialsEvent();

  @override
  List<Object?> get props => [];
}

class ListenSerialEvent extends SerialEvent {
  const ListenSerialEvent();

  @override
  List<Object?> get props => [];
}

class InitialEvent extends SerialEvent {
  const InitialEvent();

  @override
  List<Object?> get props => [];
}

class UpdateSerialEvent extends SerialEvent {
  const UpdateSerialEvent(this.serial);

  final ServerSerial serial;

  @override
  List<Object?> get props => [serial];
}

class RemoveSerialEvent extends SerialEvent {
  const RemoveSerialEvent(this.serial);

  final ServerSerial serial;

  @override
  List<Object?> get props => [serial];
}

class FailureEvent extends SerialEvent {
  final String description;

  const FailureEvent(this.description);

  @override
  List<Object?> get props => [description];
}
