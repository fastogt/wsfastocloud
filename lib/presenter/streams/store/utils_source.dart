import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

String date(int milliseconds) {
  return DateTime.fromMillisecondsSinceEpoch(milliseconds).toString();
}

class NameAndIcon extends StatelessWidget {
  final Widget icon;
  final Widget name;

  const NameAndIcon({required this.name, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Row(children: [icon, const SizedBox(width: 16), Expanded(child: name)]);
  }
}

Widget streamTilesNameAndIcon(IStream init) {
  return NameAndIcon(name: Text(init.name), icon: StreamIcon(init.icon, init.type()));
}
