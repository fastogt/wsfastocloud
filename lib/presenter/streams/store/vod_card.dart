import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/presenter/widgets/preview_icon.dart';

class VodCard extends StatelessWidget {
  final String iconLink;
  final double? width;
  final double? height;
  final double borderRadius;
  final Function? onPressed;
  final bool uploadVideo;

  const VodCard(
      {required this.iconLink, this.height, this.width, this.borderRadius = 2.0, this.onPressed})
      : uploadVideo = false;

  const VodCard.uploadVideo(
      {required this.iconLink, this.height, this.width, this.borderRadius = 2.0, this.onPressed})
      : uploadVideo = true;

  static const CARD_WIDTH = 376.0;
  static const ASPECT_RATIO = 16 / 9;

  @override
  Widget build(BuildContext context) {
    Size getSize() {
      if (height != null && width == null) {
        return Size(height! / ASPECT_RATIO, height!);
      } else if (height == null && width != null) {
        return Size(width!, width! * ASPECT_RATIO);
      } else if (height != null && width != null) {
        return Size(width!, height!);
      }
      return const Size(CARD_WIDTH, CARD_WIDTH * ASPECT_RATIO);
    }

    final size = getSize();
    final border = BorderRadius.circular(borderRadius);
    return SizedBox(
        width: size.width,
        height: size.height,
        child: Card(
            margin: const EdgeInsets.all(0),
            elevation: 2,
            shape: RoundedRectangleBorder(borderRadius: border),
            child: Stack(children: <Widget>[
              ClipRRect(
                  borderRadius: border,
                  child: uploadVideo
                      ? PreviewIcon(iconLink,
                          assetLink: 'install/assets/upload_video.png',
                          width: width,
                          height: height)
                      : PreviewIcon(iconLink, width: width, height: height)),
              InkWell(onTap: () {
                onPressed?.call();
              })
            ])));
  }
}

class VodCardBadge extends StatelessWidget {
  static const HEIGHT = 36.0;

  final Widget child;
  final double? top;
  final double? bottom;
  final double? left;
  final double? right;
  final double? width;

  const VodCardBadge(
      {required this.child, this.top, this.bottom, this.left, this.right, this.width});

  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).colorScheme.primary;
    final widget = Container(
        decoration: BoxDecoration(
            color: color, borderRadius: const BorderRadius.all(Radius.circular(HEIGHT / 2))),
        height: HEIGHT,
        width: width,
        child: child);
    return Positioned(left: left, top: top, right: right, bottom: bottom, child: widget);
  }
}

class CardGrid<T> extends StatefulWidget {
  static const double EDGE_INSETS = 4.0;

  final List<T> streams;
  final Widget Function(int, T, double, double) tile;
  static const int defaultVodsPerPage = 10;
  final int vodsPerPage;
  final List<int> availableVodsPerPage;

  const CardGrid(this.streams, this.tile,
      {this.vodsPerPage = defaultVodsPerPage,
      this.availableVodsPerPage = const <int>[
        defaultVodsPerPage,
        defaultVodsPerPage * 2,
        defaultVodsPerPage * 5,
        defaultVodsPerPage * 10,
        defaultVodsPerPage * 20,
        defaultVodsPerPage * 50,
        defaultVodsPerPage * 100
      ]});

  @override
  State<CardGrid<T>> createState() => _CardGridState<T>();
}

class _CardGridState<T> extends State<CardGrid<T>> {
  late int _vodsCount;
  int _vodsPerPage = 10;
  int currentPage = 0;

  num pageCount() {
    return _vodsCount / _vodsPerPage;
  }

  @override
  void didUpdateWidget(CardGrid<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    _vodsCount = widget.streams.length;
  }

  void _vodsPerPageChanged(int rows) {
    setState(() {
      currentPage = 0;
      _vodsPerPage = rows;
    });
    const UpdateScrollBar().dispatch(context);
  }

  void _nextPage() {
    if ((currentPage + 1) < pageCount()) {
      setState(() {
        currentPage += 1;
      });
    }
  }

  void _previousPage() {
    if (currentPage != 0) {
      setState(() {
        currentPage -= 1;
      });
    }
  }

  String calcPages() {
    if (pageCount().round() < pageCount()) {
      return (pageCount().round() + 1).toString();
    }
    return pageCount().round().toString();
  }

  int calcItemCount() {
    if (_vodsCount > _vodsPerPage) {
      if (currentPage == pageCount() ~/ 1) {
        return _vodsCount - currentPage * _vodsPerPage;
      }
      return _vodsPerPage;
    }
    return _vodsCount;
  }

  @override
  void initState() {
    _vodsCount = widget.streams.length;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final double maxWidthDesktop = size.width - 2 * CardGrid.EDGE_INSETS * 12;
    final double cardWidthDesktop = maxWidthDesktop / 12;
    const double aspectDesktop = 9 / 16;
    final maxHeightDesktop = maxWidthDesktop / aspectDesktop;
    final double maxWidthMobile = size.width - 2 * CardGrid.EDGE_INSETS * 2;
    final double cardWidthMobile = maxWidthDesktop / 2;
    const double aspectMobile = 9 / 16;
    final maxHeightMobile = maxWidthMobile / aspectMobile;
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      return Card(
          semanticContainer: false,
          child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            _header(),
            const Divider(),
            Expanded(
                child: ConstrainedBox(
                    constraints: BoxConstraints(minWidth: constraints.maxWidth),
                    child: Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: ScreenTypeLayout.builder(mobile: (_) {
                          return GridView.builder(
                              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: cardWidthMobile + 2 * CardGrid.EDGE_INSETS,
                                  crossAxisSpacing: CardGrid.EDGE_INSETS,
                                  mainAxisSpacing: CardGrid.EDGE_INSETS,
                                  childAspectRatio: aspectMobile),
                              itemCount: calcItemCount(),
                              itemBuilder: (BuildContext context, int index) {
                                return widget.tile(
                                    index + (currentPage * _vodsPerPage),
                                    widget.streams[index + (currentPage * _vodsPerPage)],
                                    cardWidthMobile,
                                    maxHeightMobile);
                              });
                        }, desktop: (_) {
                          return GridView.builder(
                              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: cardWidthDesktop + 2 * CardGrid.EDGE_INSETS,
                                  crossAxisSpacing: CardGrid.EDGE_INSETS,
                                  mainAxisSpacing: CardGrid.EDGE_INSETS,
                                  childAspectRatio: aspectDesktop),
                              itemCount: calcItemCount(),
                              itemBuilder: (BuildContext context, int index) {
                                return widget.tile(
                                    index + (currentPage * _vodsPerPage),
                                    widget.streams[index + (currentPage * _vodsPerPage)],
                                    cardWidthDesktop,
                                    maxHeightDesktop);
                              });
                        }))))
          ]));
    });
  }

  Widget _header() {
    return DefaultTextStyle(
        style: Theme.of(context).textTheme.bodySmall!,
        child: IconTheme.merge(
            data: const IconThemeData(opacity: 0.54),
            child: SizedBox(
                height: 56.0,
                child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    reverse: true,
                    child: Row(children: _headerWidgets())))));
  }

  List<Widget> _headerWidgets() {
    final List<Widget> footerWidgets = <Widget>[];
    final List<DropdownMenuItem<int>> availableVodsPerPage = [];

    for (final value in widget.availableVodsPerPage) {
      if (value <= _vodsCount || value == widget.vodsPerPage) {
        availableVodsPerPage.add(DropdownMenuItem<int>(value: value, child: Text('$value')));
      }
    }

    footerWidgets.addAll(<Widget>[
      Container(width: 14.0),
      const Text('VODs per page'),
      ConstrainedBox(
          constraints: const BoxConstraints(minWidth: 64.0),
          child: Align(
              alignment: AlignmentDirectional.centerEnd,
              child: DropdownButtonHideUnderline(
                  child: DropdownButton<int>(
                      items: availableVodsPerPage,
                      value: _vodsPerPage,
                      onChanged: (count) => _vodsPerPageChanged(count!),
                      style: Theme.of(context).textTheme.bodySmall))))
    ]);
    footerWidgets.addAll(<Widget>[
      Container(width: 32.0),
      Text('Page ${currentPage + 1} of ${calcPages()}'),
      Container(width: 32.0),
      IconButton(
          icon: const Icon(Icons.chevron_left),
          padding: EdgeInsets.zero,
          tooltip: 'Previous page',
          onPressed: _previousPage),
      Container(width: 24.0),
      IconButton(
          icon: const Icon(Icons.chevron_right),
          padding: EdgeInsets.zero,
          tooltip: 'Next page',
          onPressed: _nextPage),
      Container(width: 14.0)
    ]);
    return footerWidgets;
  }
}
