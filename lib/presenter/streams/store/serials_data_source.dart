import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/streams/store/utils_source.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

class SerialsSource extends SortableDataSource<ServerSerial> {
  SerialsSource(List<DataEntry<ServerSerial>> items) : super(items: items);

  @override
  String get itemsName {
    return 'serials';
  }

  @override
  bool searchCondition(String text, ServerSerial season) {
    return season.name.toLowerCase().contains(text.toLowerCase());
  }

  @override
  Widget get noItems {
    return _NoSerialsSource();
  }

  @override
  List<Widget> headers() {
    const TextStyle headerStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.w500);

    final List<String> titles = ['Name', 'Seasons', 'Created Date'];

    final List<Widget> headerWidgets = [];
    for (var title in titles) {
      headerWidgets.add(Text(title, style: headerStyle));
    }

    return headerWidgets;
  }

  @override
  List<Widget> tiles(ServerSerial item) {
    const TextStyle textStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.w200);
    return [
      NameAndIcon(name: Text(item.name), icon: PreviewIcon.vod(item.icon, width: 40, height: 60)),
      Text('${item.seasons.length}'),
      Text(date(item.createdDate!), style: textStyle)
    ];
  }

  @override
  bool equalItemsCondition(ServerSerial item, ServerSerial listItem) {
    return listItem.id == item.id;
  }

  @override
  int compareItems(ServerSerial a, ServerSerial b, int index) {
    switch (index) {
      case 1:
        return a.name.compareTo(b.name);
      case 2:
        return a.seasons.length.compareTo(b.seasons.length);
      case 3:
        return a.createdDate!.compareTo(b.createdDate!);
      default:
        throw ArgumentError.value(index, 'index');
    }
  }
}

class _NoSerialsSource extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: NonAvailableBuffer(icon: Icons.error, message: context.l10n.pleaseCreateSerials));
  }
}
