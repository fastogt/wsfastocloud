import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/data/repositories/store_editor.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/episodes_bloc/episodes_bloc.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/streams/store/add_button.dart';
import 'package:wsfastocloud/presenter/streams/store/embed_output_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/episodes_data_source.dart';
import 'package:wsfastocloud/presenter/streams/store/make_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/output_player_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/select_m3u_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/store_card.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/error_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/streams/actions.dart';

class StoreEpisodesWidget extends StatefulWidget {
  final LiveServer? mediaServer;

  const StoreEpisodesWidget({required this.mediaServer});

  @override
  State createState() {
    return _StoreEpisodesWidgetState();
  }
}

class _StoreEpisodesWidgetState extends State<StoreEpisodesWidget> {
  final TextEditingController controller = TextEditingController(text: '');
  List<IStream> searchedStreams = [];

  bool get isSearching => controller.text.isNotEmpty;

  static const availableTypesIfHaveMedia = [
    StreamType.VOD_PROXY,
    StreamType.VOD_RELAY,
    StreamType.VOD_ENCODE
  ];
  static const availableTypes = [StreamType.VOD_PROXY];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EpisodesBloc, EpisodesState>(builder: (context, state) {
      if (state.isLoading) {
        //TODO refactor widget
        return const CircularProgressIndicator();
      }

      if (state.errorMessage != null) {
        return ErrorDialog(error: state.errorMessage!);
      }

      final List<Widget> actions = [
        StatsButton(
            title: context.l10n.tableView,
            onPressed: state.isEpisodesCardsViewEnabled
                ? () {
                    context.read<EpisodesBloc>().add(EnableEpisodeTableViewEvent());
                  }
                : null),
        StatsButton(
            title: context.l10n.cardsView,
            onPressed: !state.isEpisodesCardsViewEnabled
                ? () {
                    context.read<EpisodesBloc>().add(EnableEpisodeCardsViewEvent());
                  }
                : null),
        _addStreamButton(),
        _uploadM3uFilesButton()
      ];

      final data = state.streamDataSource;
      if (state.isEpisodesCardsViewEnabled) {
        final List<IStream> current = [];
        for (final item in data.currentList) {
          current.add(item.entry);
        }
        Widget desktop(BuildContext context) {
          if (current.isEmpty) {
            return _drawEmpty(context);
          }
          return StoreCardWidget(
              isSerial: true,
              items: isSearching ? searchedStreams : current,
              emptyWidget: _drawEmpty(context),
              liveServer: widget.mediaServer);
        }

        Widget mobile(BuildContext context) {
          if (current.isEmpty) {
            return _drawEmpty(context);
          }

          return StoreCardWidget(
              isSerial: true,
              items: isSearching ? searchedStreams : current,
              emptyWidget: _drawEmpty(context),
              liveServer: widget.mediaServer);
        }

        final content = Column(children: [
          Padding(
              padding: const EdgeInsets.only(top: 12, right: 12, bottom: 24),
              child: ScreenTypeLayout.builder(mobile: (context) {
                return DataTableSearchHeader(source: data, actions: actions);
              }, desktop: (context) {
                return Row(children: [_vodsSearchHeader(current)] + actions);
              })),
          Expanded(child: ScreenTypeLayout.builder(desktop: desktop, mobile: mobile))
        ]);
        return SizedBox(height: 758, child: content);
      }

      // draw table
      final content = _drawTableContent(context, actions, data);
      return SizedBox(height: 658, child: content);
    });
  }

  Widget _drawTableContent(
      BuildContext context, List<Widget> actions, StoreEpisodesDataSource streamDataSource) {
    final bloc = context.read<StreamsStoreBloc>();
    return DataTableEx(
        streamDataSource, DataTableSearchHeader(source: streamDataSource, actions: actions), () {
      if (streamDataSource.selectedRowCount == 0) {
        return [];
      }

      final streams = streamDataSource.selectedItems();
      bool canStart = true;
      for (final stream in streams) {
        if (stream.type() == StreamType.VOD_PROXY) {
          canStart = false;
          break;
        }
      }

      final mediaConnected = widget.mediaServer is GoLiveServerWithoutFolders;
      if (canStart) {
        canStart = mediaConnected;
      }

      bool canEdit = false;
      bool canCopy = false;
      bool canPlay = false;
      bool canEmbed = false;
      bool canCopyToExternal = false;
      if (streams.length == 1) {
        final selected = streams[0];
        if (selected.type() == StreamType.VOD_PROXY) {
          canCopy = true;
          canEdit = true;
        } else {
          canCopyToExternal = true;
          canCopy = mediaConnected;
          canEdit = mediaConnected;
        }

        canPlay = true;
        canEmbed = true;
      }
      return [
        Row(children: [
          if (canStart) StreamActionIcon.refresh(() => _startStoreStream(bloc, streams), context),
          if (canEdit) CommonActionIcon.edit(() => _editStoreStream(bloc, streams)),
          if (canCopyToExternal)
            StreamActionIcon.copyToExternalStream(
                () => _copyToExternalStream(bloc, streams), context),
          if (canPlay) StreamActionIcon.playOutput(() => _playOutputStream(streams), context),
          if (canEmbed) StreamActionIcon.embedOutput(() => _embedOutput(bloc, streams), context),
          if (canCopy) StreamActionIcon.copyStream(() => _copyStream(bloc, streams), context),
          CommonActionIcon.remove(() => _removeStoreStream(bloc, streams))
        ])
      ];
    }, null, null, null, null, true, null, Theme.of(context).colorScheme.primary);
  }

  Widget _drawEmpty(BuildContext context) {
    return const NoEpisodesAvailable();
  }

  void _playOutputStream(List<IStream> streams) {
    for (final stream in streams) {
      showDialog(
          context: context,
          builder: (context) {
            return OutputPlayerDialog(stream.name, stream.type(), stream.output);
          });
    }
  }

  void _embedOutput(StreamsStoreBloc bloc, List<IStream> streams) {
    for (final stream in streams) {
      final result = showDialog(
          context: context,
          builder: (context) {
            return EmbedOutputDialog(stream.output);
          });
      result.then((value) {
        if (value == null) {
          return;
        }

        final resp = bloc.embedOutput(stream.output[value]);
        resp.then((result) {
          final TextEditingController _textController = TextEditingController(text: result);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(title: Text(context.l10n.getEmbedOutput), children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(children: [
                        TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            controller: _textController),
                        const SizedBox(height: 5.0),
                        FlatButtonEx.filled(
                            text: context.l10n.copy,
                            onPressed: () {
                              Clipboard.setData(ClipboardData(text: _textController.text));
                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text(context.l10n.copied),
                              ));
                            }),
                        const SizedBox(height: 5.0),
                        FlatButtonEx.filled(
                            text: context.l10n.close,
                            onPressed: () {
                              Navigator.of(context).pop();
                            })
                      ]))
                ]);
              });
        }, onError: (error) {
          showError(context, error);
        });
      });
    }
  }

  void _startStoreStream(StreamsStoreBloc bloc, List<IStream> streams) {
    for (final stream in streams) {
      bloc.startEpisode(stream.id!);
    }
  }

  void _copyStream(StreamsStoreBloc bloc, List<IStream> streams) {
    for (final stream in streams) {
      final copy = stream.copyWith(id: null, name: '${context.l10n.copy} ${stream.name}');
      _editStoreStream(bloc, [copy]);
    }
  }

  void _removeStoreStream(StreamsStoreBloc bloc, List<IStream> streams) {
    for (final stream in streams) {
      bloc.removeEpisode(stream.id!);
    }
  }

  void _editStoreStream(StreamsStoreBloc bloc, List<IStream> streams) {
    final cur = widget.mediaServer;
    for (final stream in streams) {
      showDialog(
          context: context,
          builder: (context) {
            return makeEditStreamDialog(cur!, StoreEditor(bloc), stream, true);
          });
    }
  }

  void _copyToExternalStream(StreamsStoreBloc bloc, List<IStream> streams) {
    for (final stream in streams) {
      final copy = ProxyStream.copyFrom(stream);
      copy.id = null;
      copy.name = '${context.l10n.copy}${stream.name}';
      _editStoreStream(bloc, [copy]);
    }
  }

  Widget _vodsSearchHeader(List<IStream> data) {
    return Expanded(
        child: SizedBox(
            width: double.infinity / 2,
            child: ListTile(
                leading: const Icon(Icons.search),
                title: TextField(
                    controller: controller,
                    decoration:
                        InputDecoration(hintText: context.l10n.search, border: InputBorder.none),
                    onChanged: (text) {
                      final List<IStream> result = [];
                      if (text != '') {
                        final title = text.toLowerCase();
                        for (final stream in data) {
                          if (stream.name.toLowerCase().contains(title)) {
                            result.add(stream);
                          }
                        }
                      }
                      searchedStreams = result;
                      setState(() {});
                    }),
                trailing: isSearching
                    ? IconButton(
                        icon: const Icon(Icons.cancel),
                        color: Theme.of(context).colorScheme.secondary,
                        onPressed: () {
                          setState(() {
                            controller.text = '';
                          });
                        })
                    : null)));
  }

  Widget _addStreamButton() {
    final bloc = context.read<StreamsStoreBloc>();
    List<StreamType> types = availableTypes;
    if (widget.mediaServer is GoLiveServerWithoutFolders) {
      types = availableTypesIfHaveMedia;
    }

    final button = AddStreamButton(widget.mediaServer, StoreEditor(bloc), types, isSerial: true);
    return Padding(padding: const EdgeInsets.symmetric(horizontal: 16), child: button);
  }

  Widget _uploadM3uFilesButton() {
    final bloc = context.read<StreamsStoreBloc>();
    return Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
        child: FlatButtonEx.filled(
            text: context.l10n.uploadM3u, onPressed: () => _uploadM3uFiles(bloc)));
  }

  void _uploadM3uFiles(StreamsStoreBloc bloc) {
    List<StreamType> types = availableTypes;
    if (widget.mediaServer is GoLiveServerWithoutFolders) {
      types = availableTypesIfHaveMedia;
    }

    showDialog(
        context: context,
        builder: (context) {
          return SelectM3uDialog(editor: StoreEditor(bloc), types: types, isSeries: true);
        });
  }
}

class EpisodeCardViews extends StatelessWidget {
  final int count;

  const EpisodeCardViews(this.count);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[const Icon(Icons.visibility), Text(' $count')]));
  }
}
