import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/repositories/store_editor.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/catchups_bloc/catchups_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/add_button.dart';
import 'package:wsfastocloud/presenter/streams/store/catchup_data_source.dart';
import 'package:wsfastocloud/presenter/streams/store/embed_output_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/make_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/output_player_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/error_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/streams/actions.dart';

class StoreCatchupsWidget extends StatefulWidget {
  final LiveServer? mediaServer;

  const StoreCatchupsWidget({required this.mediaServer});

  @override
  State<StoreCatchupsWidget> createState() {
    return _StoreCatchupsWidgetState();
  }
}

class _StoreCatchupsWidgetState extends State<StoreCatchupsWidget> {
  static const availableTypesIfHaveMedia = [StreamType.CATCHUP];
  static const List<StreamType> availableTypes = [];

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<StreamsStoreBloc>();
    return BlocBuilder<CatchupsBloc, CatchupsState>(builder: (context, state) {
      if (state.isLoading) {
        //TODO refactor widget
        return const CircularProgressIndicator();
      }

      if (state.errorMessage != null) {
        return ErrorDialog(error: state.errorMessage!);
      }

      final mediaConnected = widget.mediaServer is GoLiveServerWithoutFolders;
      final content = DataTableEx(
          state.catchupDataSource,
          DataTableSearchHeader(
              source: state.catchupDataSource,
              actions: mediaConnected ? [_addStreamButton(bloc)] : []), () {
        return _storeActions(catchupDataSource: state.catchupDataSource);
      }, null, null, null, null, true, null, Theme.of(context).colorScheme.primary);
      return Padding(padding: const EdgeInsets.only(right: 10.0), child: content);
    });
  }

  List<Widget> _storeActions({required StoreCatchupDataSource catchupDataSource}) {
    if (catchupDataSource.selectedRowCount == 0) {
      return [];
    }

    final bloc = context.read<StreamsStoreBloc>();
    final streams = catchupDataSource.selectedItems();
    bool canStart = true;
    for (final stream in streams) {
      if (stream.type() == StreamType.PROXY) {
        canStart = false;
        break;
      }
    }

    final mediaConnected = widget.mediaServer is GoLiveServerWithoutFolders;
    if (canStart) {
      canStart = mediaConnected;
    }

    bool canEdit = false;
    bool canCopy = false;
    bool canPlay = false;
    bool canEmbed = false;
    bool canCopyToExternal = false;
    if (streams.length == 1) {
      canCopyToExternal = mediaConnected;
      canCopy = mediaConnected;
      canEdit = mediaConnected;
      canPlay = true;
      canEmbed = true;
    }
    return [
      Row(children: [
        if (canEdit)
          CommonActionIcon.edit(
            () => _editStoreStream(bloc, streams),
          ),
        if (canCopyToExternal)
          StreamActionIcon.copyToExternalStream(
              () => _copyToExternalStream(bloc, streams), context),
        if (canPlay) StreamActionIcon.playOutput(() => _playOutputStream(streams), context),
        if (canEmbed) StreamActionIcon.embedOutput(() => _embedOutput(bloc, streams), context),
        if (canCopy) StreamActionIcon.copyStream(() => _copyStream(bloc, streams), context),
        CommonActionIcon.remove(() => _removeStoreStream(bloc, streams))
      ])
    ];
  }

  void _copyToExternalStream(StreamsStoreBloc bloc, List<IStream> streams) {
    for (final stream in streams) {
      final copy = ProxyStream.copyFrom(stream);
      copy.id = null;
      copy.name = '${context.l10n.copy} ${stream.name}';
      _editStoreStream(bloc, [copy]);
    }
  }

  void _playOutputStream(List<IStream> streams) {
    for (final stream in streams) {
      showDialog(
          context: context,
          builder: (context) {
            return OutputPlayerDialog(stream.name, stream.type(), stream.output);
          });
    }
  }

  void _embedOutput(StreamsStoreBloc bloc, List<IStream> streams) {
    for (final stream in streams) {
      final result = showDialog(
          context: context,
          builder: (context) {
            return EmbedOutputDialog(stream.output);
          });
      result.then((value) {
        if (value == null) {
          return;
        }

        final resp = bloc.embedOutput(stream.output[value]);
        resp.then((String result) {
          final TextEditingController _textController = TextEditingController(text: result);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(title: const Text('Get embed output'), children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(children: [
                        TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            controller: _textController),
                        const SizedBox(height: 5.0),
                        FlatButtonEx.filled(
                            text: 'Copy',
                            onPressed: () {
                              Clipboard.setData(ClipboardData(text: _textController.text));
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(content: Text('Copied')));
                            }),
                        const SizedBox(height: 5.0),
                        FlatButtonEx.filled(
                            text: 'Close',
                            onPressed: () {
                              Navigator.of(context).pop();
                            })
                      ]))
                ]);
              });
        }, onError: (error) {
          showError(context, error);
        });
      });
    }
  }

  void _editStoreStream(StreamsStoreBloc bloc, List<IStream> streams) {
    final cur = widget.mediaServer;
    for (final stream in streams) {
      showDialog(
          context: context,
          builder: (context) {
            return makeEditStreamDialog(cur!, StoreEditor(bloc), stream, false);
          });
    }
  }

  // many
  void _removeStoreStream(StreamsStoreBloc bloc, List<IStream> streams) {
    for (final stream in streams) {
      bloc.removeCatchup(stream.id!);
    }
  }

  Widget _addStreamButton(StreamsStoreBloc bloc) {
    List<StreamType> types = availableTypes;
    if (widget.mediaServer is GoLiveServerWithoutFolders) {
      types = availableTypesIfHaveMedia;
    }

    final button = AddStreamButton(widget.mediaServer, StoreEditor(bloc), types, isSerial: false);
    return Padding(padding: const EdgeInsets.symmetric(horizontal: 16), child: button);
  }

  void _copyStream(StreamsStoreBloc bloc, List<IStream> streams) {
    for (final stream in streams) {
      final copy = stream.copyWith(id: null, name: 'Copy ${stream.name}');
      _editStoreStream(bloc, [copy]);
    }
  }
}
