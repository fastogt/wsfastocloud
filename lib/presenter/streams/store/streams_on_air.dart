import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wsfastocloud/presenter/streams/on_air/live_widget.dart';
import 'package:wsfastocloud/presenter/streams/on_air/stream_data_source.dart';
import 'package:wsfastocloud/presenter/streams/on_air/stream_statistics_bloc/stream_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/error_dialog.dart';

class OnAirStreamsTabs extends StatelessWidget {
  const OnAirStreamsTabs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StreamsStatisticsBloc, StreamsStatisticsState>(builder: (context, state) {
      if (state is StreamsStatisticsData) {
        return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          // ignore: prefer_if_elements_to_conditional_expressions
          _liveStreams(state.dataSource)
        ]);
      } else if (state is StreamsStatisticsFailure) {
        return ErrorDialog(error: state.description);
      }
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        // ignore: prefer_if_elements_to_conditional_expressions
        _liveStreams(LiveStreamDataSource([]))
      ]);
    });
  }

  Widget _liveStreams(LiveStreamDataSource _liveDataSource) {
    return LiveWidget(_liveDataSource);
  }
}
