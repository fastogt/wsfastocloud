import 'dart:async';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:fastocloud_dart_media_models/models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/domain/entity/file.dart';
import 'package:wsfastocloud/domain/entity/uploaded_image.dart';
import 'package:wsfastocloud/domain/repositories/server_repository.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/add_edit_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/data_picker.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';
import 'package:wsfastocloud/presenter/widgets/price_pack.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';
import 'package:wsfastocloud/utils/file_picker.dart';

class SerialDialog extends StatefulWidget {
  final ServerSerial init;
  final List<ServerSeason> availableSeasons;

  SerialDialog.add(String icon, this.availableSeasons)
      : init = ServerSerial(icon: icon, seasons: []);

  const SerialDialog.edit(ServerSerial serial, this.availableSeasons) : init = serial;

  @override
  _SerialDialogState createState() {
    return _SerialDialogState();
  }
}

class _SerialDialogState extends State<SerialDialog> {
  late ServerSerial _serial;
  final TextEditingController _textEditingController = TextEditingController();
  ServerSeason? _currentEid;
  final StreamController<String> _iconsUpdates = StreamController<String>.broadcast();

  @override
  void initState() {
    super.initState();
    _serial = widget.init.copy();
  }

  bool isAdd() {
    return _serial.id == null;
  }

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.narrow(
        add: isAdd(),
        children: <Widget>[
          Row(children: <Widget>[
            if (!isAdd()) Expanded(flex: 2, child: roIDField()),
            Expanded(flex: 3, child: _nameField())
          ]),
          _groupField(),
          _descriptionField(),
          _iconField(),
          _backgroundUrlField(),
          Row(children: <Widget>[
            Expanded(child: _priceField(), flex: 2),
            Expanded(child: _iarcField())
          ]),
          Row(children: <Widget>[Expanded(child: _castField()), Expanded(child: _genresField())]),
          Row(children: <Widget>[
            Expanded(child: _productionField()),
            Expanded(child: _directorsField())
          ]),
          Row(children: <Widget>[
            Expanded(child: _primeDateField()),
            Expanded(child: _userScoreField()),
            Expanded(child: _countryField())
          ]),
          ..._seasons(),
          _addSeason(),
          _visibleField()
        ],
        onSave: _save);
  }

  TextFieldEx roIDField() {
    return TextFieldEx(
        cursorColor: Theme.of(context).colorScheme.primary,
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        init: _serial.id!,
        readOnly: true,
        hintText: 'ID',
        decoration: const InputDecoration().copyWith(
            suffixIcon: IconButton(
                icon: const Icon(Icons.copy),
                tooltip: 'Copy',
                onPressed: () {
                  Clipboard.setData(ClipboardData(text: _serial.id!));
                })));
  }

  Widget _nameField() {
    return TextFieldEx(
        hintText: context.l10n.name,
        errorText: context.l10n.enterName,
        init: _serial.name,
        onFieldChanged: (term) {
          _serial.name = term;
        });
  }

  Widget _groupField() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: ChipListField(
            values: _serial.groups,
            hintText: context.l10n.groups,
            onItemAdded: (value) => setState(() => _serial.groups.add(value)),
            onItemRemoved: (index) => setState(() => _serial.groups.removeAt(index))));
  }

  Widget _iarcField() {
    return NumberTextField.integer(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minInt: IARC.MIN,
        maxInt: IARC.MAX,
        hintText: context.l10n.iarc,
        canBeEmpty: false,
        initInt: _serial.iarc,
        onFieldChangedInt: (term) {
          if (term != null) {
            _serial.iarc = term;
          }
        });
  }

  TextFieldEx _iconField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: context.l10n.icon,
        errorText: context.l10n.enterIconUrl,
        init: _serial.icon,
        decoration: InputDecoration(
            icon: StreamBuilder<Object>(
                initialData: _serial.icon,
                stream: _iconsUpdates.stream,
                builder: (_, snapshot) {
                  return InkWell(
                      onTap: _browseImage,
                      child: PreviewIcon.live(_serial.icon, width: 40, height: 40));
                })),
        onFieldChanged: (term) {
          setState(() {
            _serial.icon = term;
          });
        });
  }

  Widget _backgroundUrlField() {
    final bloc = context.read<StreamsStoreBloc>();
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        hintText: context.l10n.backgroundUrl,
        init: _serial.background.isEmpty
            ? onBackgroundUrl(bloc.defaults().backgroundUrl)
            : _serial.background,
        onFieldChanged: onBackgroundUrl);
  }

  Widget _descriptionField() {
    return TextFieldEx(
        maxSymbols: Description.MAX_LENGTH,
        hintText: context.l10n.description,
        init: _serial.description,
        onFieldChanged: (term) {
          _serial.description = term;
        });
  }

  Widget _primeDateField() {
    return DatePicker(context.l10n.premiereDate, _serial.primeDate, (int date) {
      _serial.primeDate = date;
    });
  }

  Widget _priceField() {
    return OptionalFieldTile(
        title: context.l10n.price,
        init: _serial.price != null,
        onChanged: (value) {
          _serial.price =
              value ? PricePack(currency: Currency.USD, price: 1, type: PriceType.LIFE_TIME) : null;
        },
        builder: () {
          return PricePackField(_serial.price!);
        });
  }

  Widget _castField() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ChipListField(
            hintText: context.l10n.actors,
            values: _serial.cast,
            onItemAdded: (item) => setState(() {
                  final List<String> copy = [];
                  for (final c in _serial.cast) {
                    copy.add(c);
                  }
                  copy.add(item);
                  _serial.cast = copy;
                }),
            onItemRemoved: (index) => setState(() {
                  _serial.cast.removeAt(index);
                })));
  }

  Widget _productionField() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ChipListField(
            hintText: context.l10n.production,
            values: _serial.production,
            onItemAdded: (item) => setState(() {
                  final List<String> copy = [];
                  for (final c in _serial.production) {
                    copy.add(c);
                  }
                  copy.add(item);
                  _serial.production = copy;
                }),
            onItemRemoved: (index) => setState(() {
                  _serial.production.removeAt(index);
                })));
  }

  Widget _genresField() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ChipListField(
            hintText: context.l10n.genres,
            values: _serial.genres,
            onItemAdded: (item) => setState(() {
                  final List<String> copy = [];
                  for (final c in _serial.genres) {
                    copy.add(c);
                  }
                  copy.add(item);
                  _serial.genres = copy;
                }),
            onItemRemoved: (index) => setState(() {
                  _serial.genres.removeAt(index);
                })));
  }

  Widget _directorsField() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ChipListField(
            hintText: context.l10n.directors,
            values: _serial.directors,
            onItemAdded: (item) => setState(() {
                  final List<String> copy = [];
                  for (final c in _serial.directors) {
                    copy.add(c);
                  }
                  copy.add(item);
                  _serial.directors = copy;
                }),
            onItemRemoved: (index) => setState(() {
                  _serial.directors.removeAt(index);
                })));
  }

  Widget _userScoreField() {
    return NumberTextField.decimal(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minDouble: UserScore.MIN,
        maxDouble: UserScore.MAX,
        hintText: context.l10n.userScore,
        canBeEmpty: false,
        initDouble: _serial.userScore,
        onFieldChangedDouble: (double? term) {
          _serial.userScore = term ?? 0;
        });
  }

  Widget _countryField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        maxSymbols: Country.MAX_LENGTH,
        hintText: context.l10n.country,
        errorText: context.l10n.enterCountry,
        init: _serial.country,
        onFieldChanged: (term) {
          _serial.country = term;
        });
  }

  Widget _visibleField() {
    return StateCheckBox(
        title: context.l10n.visibleForSubscribers,
        init: _serial.visible,
        onChanged: (value) {
          _serial.visible = value;
        });
  }

  List<Widget> _seasons() {
    final List<Widget> result = [];
    for (final String eid in _serial.seasons) {
      for (final avail in widget.availableSeasons) {
        if (avail.id == eid) {
          result.add(_SeasonEntry(eid, avail.name, () {
            _deleteSeason(eid);
          }));
          break;
        }
      }
    }

    return result;
  }

  void _deleteSeason(String eid) {
    setState(() {
      _serial.seasons.remove(eid);
    });
  }

  Widget _addSeason() {
    final Widget add = IconButton(
        tooltip: context.l10n.addSeason,
        onPressed: () {
          if (_currentEid == null) {
            return;
          }

          setState(() {
            final sid = _currentEid!.id!;
            _serial.seasons.add(sid);
            _currentEid = null;
          });
        },
        icon: const Icon(Icons.add));

    final List<DropdownMenuItem<ServerSeason>> items = [];
    for (final serial in widget.availableSeasons) {
      final drop = DropdownMenuItem<ServerSeason>(value: serial, child: Text(serial.name));
      items.add(drop);
    }

    final search = DropdownSearchData<ServerSeason>(
        searchController: _textEditingController,
        searchInnerWidgetHeight: 0,
        searchInnerWidget: Padding(
            padding: const EdgeInsets.only(top: 8, bottom: 4, right: 8, left: 8),
            child: TextFormField(
                controller: _textEditingController,
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                  hintText: context.l10n.searchForAnSerial,
                ))),
        searchMatchFn: (item, searchValue) {
          final val = item.value as ServerSeason;
          return val.name.toLowerCase().contains(searchValue.toLowerCase());
        });
    final Widget seasonsWidget = DropdownButton2<ServerSeason>(
        hint: Text(context.l10n.availableSeasons),
        value: _currentEid,
        items: items,
        onChanged: (item) {
          setState(() {
            _currentEid = item;
          });
        },
        dropdownStyleData: DropdownStyleData(maxHeight: MediaQuery.of(context).size.height * 0.25),
        dropdownSearchData: search,
        onMenuStateChange: (isOpen) {
          if (!isOpen) {
            _textEditingController.clear();
          }
        });

    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(children: <Widget>[Expanded(child: seasonsWidget), add]));
  }

  String onBackgroundUrl(String term) {
    _serial.background = term;
    return _serial.background;
  }

  void _save() {
    if (!_serial.isValid()) {
      return;
    }

    Navigator.of(context).pop(_serial);
  }

  Future<void> _browseImage() async {
    final ImageFilePicker picker = ImageFilePicker();
    final File? image = await picker.pickFile();
    if (image == null) {
      return;
    }

    final ServerRepository serverRepository = context.read<ServerRepository>();
    final res = serverRepository.uploadImage(image);
    res.then((UploadedImage uploadedFile) {
      _refreshIcon(uploadedFile.path);
    });
  }

  void _refreshIcon(String icon) {
    setState(() {
      _refreshIconInternal(icon);
    });
  }

  void _refreshIconInternal(String icon) {
    _serial.icon = icon;
    _iconsUpdates.add(icon);
  }
}

class _SeasonEntry extends StatelessWidget {
  final String id;
  final String name;
  final void Function() onDelete;

  const _SeasonEntry(this.id, this.name, this.onDelete);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(child: TextFieldEx(hintText: context.l10n.name, init: name)),
      IconButton(tooltip: context.l10n.remove, icon: const Icon(Icons.close), onPressed: onDelete)
    ]);
  }
}
