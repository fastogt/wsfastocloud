import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';

part 'streams_store_event.dart';
part 'streams_store_state.dart';

class StreamsStoreBloc extends Bloc<StreamsStoreEvent, StreamsDBState> {
  final StreamDBRepository streamRepository;

  StreamsStoreBloc({required this.streamRepository}) : super(const StreamsDBState());

  Defaults defaults() {
    return streamRepository.defaults();
  }

  Future<void> startLive(String id) {
    return streamRepository.startLive(id);
  }

  Future<void> startVod(String id) {
    return streamRepository.startVod(id);
  }

  Future<void> startEpisode(String id) {
    return streamRepository.startEpisode(id);
  }

  Future<bool> addLiveStream(IStream stream) {
    return streamRepository.addLiveStream(stream);
  }

  Future<bool> editLiveStream(IStream stream) {
    return streamRepository.editLiveStream(stream);
  }

  Future<bool> removeLiveStream(String id) {
    return streamRepository.removeLiveStream(id);
  }

  Future<bool> removeCatchup(String id) {
    return streamRepository.removeCatchupStream(id);
  }

  Future<bool> addVod(IStream stream) {
    return streamRepository.addVod(stream);
  }

  Future<bool> editVod(IStream stream) {
    return streamRepository.editVod(stream);
  }

  Future<bool> removeVod(String id) {
    return streamRepository.removeVod(id);
  }

  Future<bool> addEpisode(IStream stream) {
    return streamRepository.addEpisode(stream);
  }

  Future<bool> editEpisode(IStream stream) {
    return streamRepository.editEpisode(stream);
  }

  Future<bool> removeEpisode(String id) {
    return streamRepository.removeEpisode(id);
  }

  Future<String> probeOutUrl(OutputUrl url) {
    return streamRepository.probeOutUrl(url);
  }

  Future<String> getTokenUrl(String ip) {
    return streamRepository.getTokenUrl(ip);
  }

  Future<String> probeInUrl(InputUrl url) {
    return streamRepository.probeInUrl(url);
  }

  Future<String> embedOutput(OutputUrl url) {
    return streamRepository.embedOutput(url);
  }

  Future<bool> uploadM3uFile(
      StreamType type,
      bool isSeries,
      double? imageCheckTime,
      final bool skipGroups,
      final bool fillInfo,
      final List<String> groups,
      Map<String, List<int>> data) {
    return streamRepository.uploadM3uFile(
        type, isSeries, imageCheckTime, skipGroups, fillInfo, groups, data);
  }

  Future<bool> uploadM3uPackageFile(StreamType type, double? imageCheckTime, final bool skipGroups,
      final bool fillInfo, final List<String> groups, Map<String, List<int>> data) {
    return streamRepository.uploadM3uPackageFile(
        type, imageCheckTime, skipGroups, fillInfo, groups, data);
  }

  WebRTCOutputUrl genWebRTC(int oid, String sid) {
    return streamRepository.genWebRTC(oid, sid);
  }

  Future<MediaFolders?> folders() {
    return streamRepository.folders();
  }
}
