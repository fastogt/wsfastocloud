import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/presenter/streams/store/utils_source.dart';

class StoreCatchupDataSource extends SortableDataSource<CatchupStream> {
  StoreCatchupDataSource(List<DataEntry<CatchupStream>> items) : super(items: items);

  @override
  bool equalItemsCondition(CatchupStream item, CatchupStream listItem) {
    return item.id == listItem.id;
  }

  @override
  List<Widget> headers() {
    final headers = ['Id', 'Name', 'Start', 'End', 'Created Date', 'Type'];

    return List.generate(headers.length, (index) => Text(headers[index]));
  }

  @override
  String get itemsName => 'catchups';

  @override
  Widget get noItems => const _NoStreamsAvailable();

  @override
  bool searchCondition(String text, CatchupStream stream) {
    return stream.name.toLowerCase().contains(text.toLowerCase());
  }

  @override
  List<Widget> tiles(CatchupStream s) {
    return [
      Text(s.id.toString()),
      streamTilesNameAndIcon(s),
      Text(date(s.start!)),
      Text(date(s.stop!)),
      Text(date(s.createdDate!)),
      Text(s.type().toHumanReadableWS())
    ];
  }

  @override
  int compareItems(CatchupStream a, CatchupStream b, int index) {
    switch (index) {
      case 1:
        return a.id!.compareTo(b.id!);
      case 2:
        return a.name.compareTo(b.name);
      case 3:
        return a.createdDate!.compareTo(b.createdDate!);
      case 4:
        return a.start!.compareTo(b.start!);
      case 5:
        return a.stop!.compareTo(b.stop!);
      case 6:
        return a.type().compareTo(b.type());

      default:
        throw ArgumentError.value(index, 'index');
    }
  }
}

class _NoStreamsAvailable extends StatelessWidget {
  const _NoStreamsAvailable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Catchups'));
  }
}
