import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/hardware_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/proxy_page.dart';

Widget makeAddStreamDialog(
    LiveServer server, IStreamEditor editor, StreamType type, bool isEpisode) {
  return FutureBuilder(
      builder: (context, snap) {
        if (snap.connectionState == ConnectionState.done) {
          if (server is GoLiveServerWithoutFolders && snap.hasData) {
            final wserver = server as GoLiveServerWithoutFolders;
            server = GoLiveServer(wserver: wserver, folders: snap.data!); // upgrade server
          }
          switch (type) {
            case StreamType.PROXY:
              return ProxyStreamPage.add(server, editor);
            case StreamType.VOD_PROXY:
              return VodProxyStreamPage.add(server, editor, isEpisode);
            case StreamType.RELAY:
              return RelayStreamPage.add(server as GoLiveServer, editor);
            case StreamType.ENCODE:
              return EncodeStreamPage.add(server as GoLiveServer, editor);
            case StreamType.TIMESHIFT_PLAYER:
              return TimeshiftRecorderStreamPage.add(server as GoLiveServer, editor);
            case StreamType.TIMESHIFT_RECORDER:
              return TimeshiftRecorderStreamPage.add(server as GoLiveServer, editor);
            case StreamType.CATCHUP:
              return CatchupStreamPage.add(server as GoLiveServer, editor);
            case StreamType.VOD_RELAY:
              return VodRelayStreamPage.add(server as GoLiveServer, editor, isEpisode);
            case StreamType.VOD_ENCODE:
              return VodEncodeStreamPage.add(server as GoLiveServer, editor, isEpisode);
            case StreamType.COD_RELAY:
              return CodRelayStreamPage.add(server as GoLiveServer, editor);
            case StreamType.COD_ENCODE:
              return CodEncodeStreamPage.add(server as GoLiveServer, editor);
            case StreamType.EVENT:
              return EventStreamPage.add(server as GoLiveServer, editor);
            case StreamType.CV_DATA:
              return CvDataStreamPage.add(server as GoLiveServer, editor);
            case StreamType.CHANGER_RELAY:
              return ChangerRelayStreamPage.add(server as GoLiveServer, editor);
            case StreamType.CHANGER_ENCODE:
              return ChangerEncoderStreamPage.add(server as GoLiveServer, editor);
            case StreamType.LITE:
              return LiteStreamPage.add(server as GoLiveServer, editor);
            case StreamType.MOSAIC:
              return MosaicStreamPage.add(server as GoLiveServer, editor);
            default:
              throw 'Un implemented type';
          }
        }
        return const Center(child: CircularProgressIndicator());
      },
      future: editor.folders());
}

Widget makeEditStreamDialog(
    LiveServer server, IStreamEditor editor, IStream stream, bool isEpisode) {
  final type = stream.type();
  return FutureBuilder(
      builder: (context, snap) {
        if (snap.connectionState == ConnectionState.done) {
          if (server is GoLiveServerWithoutFolders && snap.hasData) {
            final wserver = server as GoLiveServerWithoutFolders;
            server = GoLiveServer(wserver: wserver, folders: snap.data!); // upgrade server
          }
          switch (type) {
            case StreamType.PROXY:
              return ProxyStreamPage.edit(server, editor, stream as ProxyStream);
            case StreamType.VOD_PROXY:
              return VodProxyStreamPage.edit(server, editor, stream as VodProxyStream, isEpisode);
            case StreamType.RELAY:
              return RelayStreamPage.edit(server as GoLiveServer, editor, stream as RelayStream);
            case StreamType.ENCODE:
              return EncodeStreamPage.edit(server as GoLiveServer, editor, stream as EncodeStream);
            case StreamType.TIMESHIFT_PLAYER:
              return TimeshiftRecorderStreamPage.edit(
                  server as GoLiveServer, editor, stream as TimeshiftRecorderStream);
            case StreamType.TIMESHIFT_RECORDER:
              return TimeshiftRecorderStreamPage.edit(
                  server as GoLiveServer, editor, stream as TimeshiftRecorderStream);
            case StreamType.CATCHUP:
              return CatchupStreamPage.edit(
                  server as GoLiveServer, editor, stream as CatchupStream);
            case StreamType.VOD_RELAY:
              return VodRelayStreamPage.edit(
                  server as GoLiveServer, editor, stream as VodRelayStream, isEpisode);
            case StreamType.VOD_ENCODE:
              return VodEncodeStreamPage.edit(
                  server as GoLiveServer, editor, stream as VodEncodeStream, isEpisode);
            case StreamType.COD_RELAY:
              return CodRelayStreamPage.edit(
                  server as GoLiveServer, editor, stream as CodRelayStream);
            case StreamType.COD_ENCODE:
              return CodEncodeStreamPage.edit(
                  server as GoLiveServer, editor, stream as CodEncodeStream);
            case StreamType.EVENT:
              return EventStreamPage.edit(server as GoLiveServer, editor, stream as EventStream);
            case StreamType.CV_DATA:
              return CvDataStreamPage.edit(server as GoLiveServer, editor, stream as CvDataStream);
            case StreamType.CHANGER_RELAY:
              return ChangerRelayStreamPage.edit(
                  server as GoLiveServer, editor, stream as ChangerRelayStream);
            case StreamType.CHANGER_ENCODE:
              return ChangerEncoderStreamPage.edit(
                  server as GoLiveServer, editor, stream as ChangerEncodeStream);
            case StreamType.LITE:
              return LiteStreamPage.edit(server as GoLiveServer, editor, stream as LiteStream);
            case StreamType.MOSAIC:
              return MosaicStreamPage.edit(server as GoLiveServer, editor, stream as MosaicStream);
            default:
              throw 'Un implemented type';
          }
        }
        return const Center(child: CircularProgressIndicator());
      },
      future: editor.folders());
}
