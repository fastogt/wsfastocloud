import 'dart:async';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/domain/entity/file.dart';
import 'package:wsfastocloud/domain/entity/uploaded_image.dart';
import 'package:wsfastocloud/domain/repositories/server_repository.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/add_edit_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';
import 'package:wsfastocloud/utils/file_picker.dart';

class SeasonDialog extends StatefulWidget {
  final ServerSeason init;
  final List<IStream> availableEpisodes;

  SeasonDialog.add(String icon, this.availableEpisodes)
      : init = ServerSeason(icon: icon, episodes: []);

  const SeasonDialog.edit(ServerSeason season, this.availableEpisodes) : init = season;

  @override
  _SeasonDialogState createState() {
    return _SeasonDialogState();
  }
}

class _SeasonDialogState extends State<SeasonDialog> {
  late ServerSeason _season;
  final TextEditingController _textEditingController = TextEditingController();
  IStream? _currentEid;
  final StreamController<String> _iconsUpdates = StreamController<String>.broadcast();

  @override
  void initState() {
    super.initState();
    _season = widget.init.copy();
  }

  bool isAdd() {
    return _season.id == null;
  }

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.narrow(
        add: isAdd(),
        children: <Widget>[
          Row(children: <Widget>[
            Expanded(child: _nameField()),
            Expanded(child: _descriptionField())
          ]),
          _iconField(),
          _backgroundUrlField(),
          _seasonField(),
          ..._episodes(),
          _addEpisode(),
          _visibleField()
        ],
        onSave: _save);
  }

  Widget _nameField() {
    return TextFieldEx(
        hintText: 'Name',
        errorText: 'Enter name',
        init: _season.name,
        onFieldChanged: (term) {
          _season.name = term;
        });
  }

  TextFieldEx _iconField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Icon',
        errorText: 'Enter icon url',
        init: _season.icon,
        decoration: InputDecoration(
            icon: StreamBuilder<Object>(
                initialData: _season.icon,
                stream: _iconsUpdates.stream,
                builder: (_, snapshot) {
                  return InkWell(
                      onTap: _browseImage,
                      child: PreviewIcon.live(_season.icon, width: 40, height: 40));
                })),
        onFieldChanged: (term) => setState(() => _season.icon = term));
  }

  Widget _backgroundUrlField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        hintText: 'Background URL',
        init: _season.background.isEmpty
            ? onBackgroundUrl(context.read<StreamsStoreBloc>().defaults().backgroundUrl)
            : _season.background,
        onFieldChanged: onBackgroundUrl);
  }

  Widget _descriptionField() {
    return TextFieldEx(
        maxSymbols: Description.MAX_LENGTH,
        hintText: 'Description',
        init: _season.description,
        onFieldChanged: (term) {
          _season.description = term;
        });
  }

  Widget _seasonField() {
    return NumberTextField.integer(
        minInt: 0,
        hintText: 'Season',
        canBeEmpty: false,
        initInt: _season.season,
        onFieldChangedInt: (term) {
          if (term != null) {
            _season.season = term;
          }
        });
  }

  List<Widget> _episodes() {
    final List<Widget> result = [];
    for (final String eid in _season.episodes) {
      for (final IStream avail in widget.availableEpisodes) {
        if (avail.id == eid) {
          final epi = _EpisodeEntry(eid, avail.name, () {
            _deleteEpisode(eid);
          });
          result.add(epi);
          break;
        }
      }
    }

    return result;
  }

  void _deleteEpisode(String eid) {
    setState(() {
      _season.episodes.remove(eid);
    });
  }

  Widget _addEpisode() {
    final Widget addButton = IconButton(
        tooltip: 'Add episode',
        onPressed: () {
          if (_currentEid == null) {
            return;
          }

          setState(() {
            final sid = _currentEid!.id!;
            _season.episodes.add(sid);
            _currentEid = null;
          });
        },
        icon: const Icon(Icons.add));
    final List<DropdownMenuItem<IStream>> items = [];
    for (final serial in widget.availableEpisodes) {
      final drop = DropdownMenuItem<IStream>(value: serial, child: Text(serial.name));
      items.add(drop);
    }

    final search = DropdownSearchData<IStream>(
        searchController: _textEditingController,
        searchInnerWidgetHeight: 0,
        searchInnerWidget: Padding(
            padding: const EdgeInsets.only(top: 8, bottom: 4, right: 8, left: 8),
            child: TextFormField(
                controller: _textEditingController,
                decoration: const InputDecoration(
                    isDense: true,
                    contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                    hintText: 'Search for an episode...'))),
        searchMatchFn: (item, searchValue) {
          final val = item.value as IStream;
          return val.name.toLowerCase().contains(searchValue.toLowerCase());
        });
    final Widget episodesWidget = DropdownButton2<IStream>(
        hint: const Text('Available episodes'),
        value: _currentEid,
        items: items,
        onChanged: (IStream? item) {
          setState(() {
            _currentEid = item;
          });
        },
        dropdownSearchData: search,
        onMenuStateChange: (bool isOpen) {
          if (!isOpen) {
            _textEditingController.clear();
          }
        });

    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(children: <Widget>[Expanded(child: episodesWidget), addButton]));
  }

  Widget _visibleField() {
    return StateCheckBox(
        title: context.l10n.visibleForSubscribers,
        init: _season.visible,
        onChanged: (value) {
          _season.visible = value;
        });
  }

  String onBackgroundUrl(String term) {
    _season.background = term;
    return _season.background;
  }

  void _save() {
    if (!_season.isValid()) {
      return;
    }

    Navigator.of(context).pop(_season);
  }

  Future<void> _browseImage() async {
    final ImageFilePicker picker = ImageFilePicker();

    final File? image = await picker.pickFile();
    if (image == null) {
      return;
    }

    final ServerRepository serverRepository = context.read<ServerRepository>();
    final res = serverRepository.uploadImage(image);
    res.then((UploadedImage uploadedFile) {
      _refreshIcon(uploadedFile.path);
    });
  }

  void _refreshIcon(String icon) {
    setState(() {
      _refreshIconInternal(icon);
    });
  }

  void _refreshIconInternal(String icon) {
    _season.icon = icon;
    _iconsUpdates.add(icon);
  }
}

class _EpisodeEntry extends StatelessWidget {
  final String id;
  final String name;
  final void Function() onDelete;

  const _EpisodeEntry(this.id, this.name, this.onDelete);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(child: TextFieldEx(hintText: 'Name', init: name)),
      IconButton(tooltip: 'Remove', icon: const Icon(Icons.close), onPressed: onDelete)
    ]);
  }
}
