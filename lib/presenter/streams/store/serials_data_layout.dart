import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class SerialsDataLayout<T> extends StatelessWidget {
  final DataSource<T> dataSource;
  final List<Widget> Function() headerActions;
  final List<Widget> Function(T) singleItemActions;
  final List<Widget> Function(List<T>) multipleItemActions;

  const SerialsDataLayout(
      {required this.dataSource,
      required this.headerActions,
      required this.singleItemActions,
      required this.multipleItemActions});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return DataTableEx.customActions(dataSource, _Header<T>(dataSource, headerActions), null, () {
      return _actions(context);
    }, null, null, null, true, null, theme.colorScheme.primary);
  }

  // private:
  Widget _actions(BuildContext context) {
    return _actionsRow();
  }

  Widget _actionsRow() {
    return Row(mainAxisSize: MainAxisSize.min, children: _actionsBuilder());
  }

  List<Widget> _actionsBuilder() {
    final items = dataSource.selectedItems();
    if (dataSource.selectedRowCount == 1) {
      return singleItemActions(items.first);
    } else if (dataSource.selectedRowCount > 1) {
      return multipleItemActions(items);
    }
    return [];
  }
}

class _Header<T> extends StatelessWidget {
  final DataSource<T> source;
  final List<Widget> Function() actions;

  const _Header(this.source, this.actions);

  @override
  Widget build(BuildContext context) {
    return DataTableSearchHeader(source: source, actions: actions());
  }
}
