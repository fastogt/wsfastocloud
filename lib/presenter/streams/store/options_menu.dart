import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/repositories/store_editor.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/streams/store/embed_output_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/make_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/output_player_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';

class VodOptionMenu extends StatelessWidget {
  final StreamsStoreBloc bloc;
  final LiveServer? getServer;
  final IStream stream;
  final bool canRefresh;
  final bool isSerial;

  const VodOptionMenu(
      {Key? key,
      required this.bloc,
      required this.stream,
      required this.canRefresh,
      required this.getServer,
      required this.isSerial})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _startStoreStream(StreamsStoreBloc bloc, IStream stream) {
      if (isSerial) {
        bloc.startEpisode(stream.id!);
        return;
      }
      bloc.startVod(stream.id!);
    }

    void _editStoreStream(StreamsStoreBloc bloc, IStream stream) {
      final cur = getServer;
      if (cur == null) {
        return;
      }

      showDialog(
          context: context,
          builder: (context) {
            return makeEditStreamDialog(cur, StoreEditor(bloc), stream, isSerial);
          });
    }

    void _copyToExternalStream(StreamsStoreBloc bloc, IStream stream, VodStream vod) {
      final copy = VodProxyStream.copyFrom(stream, vod);
      copy.id = null;
      copy.name = 'Copy ${stream.name}';
      _editStoreStream(bloc, copy);
    }

    void _embedOutput(StreamsStoreBloc bloc, IStream stream) {
      final result = showDialog(
          context: context,
          builder: (context) {
            return EmbedOutputDialog(stream.output);
          });
      result.then((value) {
        if (value == null) {
          return;
        }

        final resp = bloc.embedOutput(stream.output[value]);
        resp.then((result) {
          final TextEditingController _textController = TextEditingController(text: result);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(title: const Text('Get embed output'), children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(children: [
                        TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            controller: _textController),
                        const SizedBox(height: 5.0),
                        FlatButtonEx.filled(
                            text: 'Copy',
                            onPressed: () {
                              Clipboard.setData(ClipboardData(text: _textController.text));
                              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                                content: Text('Copied'),
                              ));
                            }),
                        const SizedBox(height: 5.0),
                        FlatButtonEx.filled(
                            text: 'Close',
                            onPressed: () {
                              Navigator.of(context).pop();
                            })
                      ]))
                ]);
              });
        }, onError: (error) {
          showError(context, error);
        });
      });
    }

    // many
    void _removeStoreStream(StreamsStoreBloc bloc, IStream stream) {
      if (isSerial) {
        bloc.removeEpisode(stream.id!);
      } else {
        bloc.removeVod(stream.id!);
      }
    }

    void _playOutputStream(IStream stream) {
      showDialog(
          context: context,
          builder: (context) {
            return OutputPlayerDialog(stream.name, stream.type(), stream.output);
          });
    }

    void _copyStream(StreamsStoreBloc bloc, IStream stream) {
      final copy = stream.copyWith(id: null, name: 'Copy ${stream.name}');
      _editStoreStream(bloc, copy);
    }

    // #FIXME: why do we need Future.delayed(const Duration(seconds: 0))?
    List<PopupMenuItem> vodActions(StreamsStoreBloc bloc, IStream stream, bool canRefresh) {
      final canCopyToExternal = stream.type() != StreamType.VOD_PROXY;
      return [
        if (canRefresh)
          PopupMenuItem(
              child: const Text('Refresh'),
              onTap: () {
                Future.delayed(const Duration(), () {
                  _startStoreStream(bloc, stream);
                });
              }),
        if (canCopyToExternal)
          PopupMenuItem(
              child: Text(context.l10n.copyToExternal),
              onTap: () {
                Future.delayed(const Duration(), () {
                  _copyToExternalStream(bloc, stream, stream as VodStream);
                });
              }),
        PopupMenuItem(
            child: Text(context.l10n.edit),
            onTap: () {
              Future.delayed(const Duration(), () {
                _editStoreStream(bloc, stream);
              });
            }),
        PopupMenuItem(
            child: Text(context.l10n.copy),
            onTap: () {
              Future.delayed(const Duration(), () {
                _copyStream(bloc, stream);
              });
            }),
        PopupMenuItem(
            child: Text(context.l10n.playOutput),
            onTap: () {
              Future.delayed(const Duration(), () {
                _playOutputStream(stream);
              });
            }),
        PopupMenuItem(
            child: Text(context.l10n.embedOutput),
            onTap: () {
              Future.delayed(const Duration(), () {
                _embedOutput(bloc, stream);
              });
            }),
        PopupMenuItem(
            child: Text(context.l10n.remove),
            onTap: () {
              Future.delayed(const Duration(), () {
                _removeStoreStream(bloc, stream);
              });
            }),
      ];
    }

    return PopupMenuButton(
        tooltip: context.l10n.options,
        padding: const EdgeInsets.all(0),
        icon: const Icon(Icons.more_horiz),
        itemBuilder: (BuildContext context) {
          return vodActions(bloc, stream, canRefresh);
        });
  }
}
