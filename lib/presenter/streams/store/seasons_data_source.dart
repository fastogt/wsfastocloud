import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/streams/store/utils_source.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

class SeasonsSource extends SortableDataSource<ServerSeason> {
  SeasonsSource(List<DataEntry<ServerSeason>> items) : super(items: items);

  @override
  String get itemsName {
    return 'seasons';
  }

  @override
  bool searchCondition(String text, ServerSeason season) {
    return season.name.toLowerCase().contains(text.toLowerCase());
  }

  @override
  Widget get noItems {
    return _NoSeasonsSource();
  }

  @override
  List<Widget> headers() {
    const TextStyle headerStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.w500);

    final List<String> titles = ['Name', 'Season', 'Episodes', 'Created Date'];

    final List<Widget> headerWidgets = [];
    for (var title in titles) {
      headerWidgets.add(Text(title, style: headerStyle));
    }

    return headerWidgets;
  }

  @override
  List<Widget> tiles(ServerSeason item) {
    const TextStyle textStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.w200);
    return [
      NameAndIcon(name: Text(item.name), icon: PreviewIcon.vod(item.icon, width: 40, height: 60)),
      Text('${item.season}', style: textStyle),
      Text('${item.episodes.length}', style: textStyle),
      Text(date(item.createdDate!), style: textStyle),
    ];
  }

  @override
  bool equalItemsCondition(ServerSeason item, ServerSeason listItem) {
    return listItem.id == item.id;
  }

  @override
  int compareItems(ServerSeason a, ServerSeason b, int index) {
    switch (index) {
      case 1:
        return a.name.compareTo(b.name);
      case 2:
        return a.season.compareTo(b.season);
      case 3:
        return a.episodes.length.compareTo(b.episodes.length);
      case 4:
        return a.createdDate!.compareTo(b.createdDate!);
      default:
        throw ArgumentError.value(index, 'index');
    }
  }
}

class _NoSeasonsSource extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: NonAvailableBuffer(icon: Icons.error, message: context.l10n.pleaseCreateSeasons));
  }
}
