part of 'seasons_store_bloc.dart';

abstract class SeasonEvent extends Equatable {
  const SeasonEvent();

  @override
  List<Object?> get props => [];
}

class LoadSeasonsEvent extends SeasonEvent {
  const LoadSeasonsEvent();

  @override
  List<Object?> get props => [];
}

class ListenSeasonEvent extends SeasonEvent {
  const ListenSeasonEvent();

  @override
  List<Object?> get props => [];
}

class InitialEvent extends SeasonEvent {
  const InitialEvent();

  @override
  List<Object?> get props => [];
}

class UpdateSeasonEvent extends SeasonEvent {
  const UpdateSeasonEvent(this.season);

  final ServerSeason season;

  @override
  List<Object?> get props => [season];
}

class RemoveSeasonEvent extends SeasonEvent {
  const RemoveSeasonEvent(this.season);

  final ServerSeason season;

  @override
  List<Object?> get props => [season];
}

class FailureEvent extends SeasonEvent {
  final String description;

  const FailureEvent(this.description);

  @override
  List<Object?> get props => [description];
}
