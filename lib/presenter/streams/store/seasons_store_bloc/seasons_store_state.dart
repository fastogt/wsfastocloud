part of 'seasons_store_bloc.dart';

abstract class SeasonsState extends Equatable {
  const SeasonsState();

  @override
  List<Object?> get props => [];
}

class SeasonsInitialState extends SeasonsState {
  const SeasonsInitialState();

  @override
  List<Object?> get props => [];
}

class SeasonsFailureState extends SeasonsState {
  final String description;

  const SeasonsFailureState(this.description);

  @override
  List<Object?> get props => [description];
}

class SeasonsDataState extends SeasonsState {
  final Map<String, ServerSeason> season;
  final SeasonsSource seasonsSource;

  const SeasonsDataState(this.season, this.seasonsSource);

  factory SeasonsDataState.create(List<ServerSeason> items, SeasonsSource storeStreamDataSource) {
    final streamsMap = Map<String, ServerSeason>.fromIterable(items, key: (stream) => stream.id);

    return SeasonsDataState(streamsMap, storeStreamDataSource);
  }

  factory SeasonsDataState.add(
      Map<String, dynamic> old, ServerSeason item, SeasonsSource storeStreamDataSource) {
    final streamsMap = Map<String, ServerSeason>.from(old);

    streamsMap[item.id!] = item;

    return SeasonsDataState(streamsMap, storeStreamDataSource);
  }

  factory SeasonsDataState.update(
      Map<String, dynamic> old, ServerSeason item, SeasonsSource storeStreamDataSource) {
    final streamsMap = Map<String, ServerSeason>.from(old);

    streamsMap[item.id!] = item;

    return SeasonsDataState(streamsMap, storeStreamDataSource);
  }

  factory SeasonsDataState.remove(
      Map<String, dynamic> old, String? sid, SeasonsSource storeStreamDataSource) {
    final streamsMap = Map<String, ServerSeason>.from(old);

    streamsMap.remove(sid);

    return SeasonsDataState(streamsMap, storeStreamDataSource);
  }

  @override
  List<Object?> get props => [season, seasonsSource];
}
