import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/streams/store/season_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_data_layout.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_data_source.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_store_bloc/seasons_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';

class SeasonsListLayout extends StatefulWidget {
  final SeasonsSource dataSource;

  const SeasonsListLayout(this.dataSource);

  @override
  _SeasonsListLayoutState createState() {
    return _SeasonsListLayoutState();
  }
}

class _SeasonsListLayoutState extends State<SeasonsListLayout> {
  StreamDBRepository get repo {
    final fetcher = context.read<StreamsStoreBloc>();
    return fetcher.streamRepository;
  }

  SeasonsBloc get seasonBloc {
    return context.read<SeasonsBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return SeasonsDataLayout<ServerSeason>(
        dataSource: widget.dataSource,
        headerActions: _headerActions,
        singleItemActions: singleStreamActions,
        multipleItemActions: multipleStreamActions);
  }

  // private:
  List<Widget> singleStreamActions(ServerSeason season) {
    return [
      _editSeasonButton(season),
      _copySeasonButton(season),
      _removeButton([season])
    ];
  }

  List<Widget> multipleStreamActions(List<ServerSeason> seasons) {
    return [_removeButton(seasons)];
  }

  List<Widget> _headerActions() {
    return <Widget>[_addSeasonButton()];
  }

  Widget _addSeasonButton() {
    return FlatButtonEx.filled(text: context.l10n.add, onPressed: () => _addSeason());
  }

  String defaultSeasonLogoIcon() {
    final defaults = repo.defaults();
    return defaults.streamLogoIcon;
  }

  void _addSeason() {
    final result = showDialog<ServerSeason>(
        context: context,
        builder: (context) {
          return FutureBuilder<List<IStream>>(
              builder: (context, snap) {
                if (snap.hasData) {
                  return SeasonDialog.add(defaultSeasonLogoIcon(), snap.data!);
                }
                return const Center(child: CircularProgressIndicator());
              },
              future: repo.loadEpisodes());
        });

    result.then((ServerSeason? season) {
      if (season == null) {
        return;
      }
      seasonBloc.addSeason(season);
    });
  }

  Widget _editSeasonButton(ServerSeason season) {
    return IconButton(
        icon: const Icon(Icons.edit),
        tooltip: context.l10n.edit,
        onPressed: () => _editSeason(season));
  }

  Widget _copySeasonButton(ServerSeason season) {
    return IconButton(
        icon: const Icon(Icons.copy),
        tooltip: context.l10n.copy,
        onPressed: () => _copySeason(season));
  }

  void _editSeason(ServerSeason season) {
    final result = showDialog<ServerSeason>(
        context: context,
        builder: (context) {
          return FutureBuilder<List<IStream>>(
              builder: (context, snap) {
                if (snap.hasData) {
                  return SeasonDialog.edit(season, snap.data!);
                }
                return const Center(child: CircularProgressIndicator());
              },
              future: repo.loadEpisodes());
        });
    result.then((ServerSeason? season) {
      if (season == null) {
        return;
      }
      seasonBloc.editSeason(season);
    });
  }

  Widget _removeButton(List<ServerSeason> seasons) {
    return IconButton(
        icon: const Icon(Icons.delete),
        tooltip: context.l10n.remove,
        onPressed: () {
          return _removeSeason(seasons);
        });
  }

  void _removeSeason(List<ServerSeason> seasons) {
    for (final season in seasons) {
      seasonBloc.removeSeason(season);
    }
  }

  void _copySeason(ServerSeason season) {
    final copy = season.copyWith(id: null, name: '${context.l10n.copy} ${season.name}');
    _editSeason(copy);
  }
}
