import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/presenter/streams/store/utils_source.dart';

class StoreStreamDataSource extends SortableDataSource<IStream> {
  StoreStreamDataSource(List<DataEntry<IStream>> items) : super(items: items);

  @override
  bool equalItemsCondition(IStream item, IStream listItem) {
    return item.id == listItem.id;
  }

  @override
  List<Widget> headers() {
    const headers = ['Id', 'Name', 'Created Date', 'Type'];

    final List<Widget> result = [];
    for (final head in headers) {
      result.add(Text(head, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w500)));
    }
    return result;
  }

  @override
  String get itemsName {
    return 'streams';
  }

  @override
  Widget get noItems {
    return const _NoStreamsAvailable();
  }

  @override
  bool searchCondition(String text, IStream stream) {
    final lower = stream.name.toLowerCase();
    return lower.contains(text.toLowerCase());
  }

  @override
  List<Widget> tiles(IStream s) {
    const TextStyle textStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.w200);
    return [
      Text(s.id.toString(), style: textStyle),
      streamTilesNameAndIcon(s),
      Text(date(s.createdDate!), style: textStyle),
      Text(s.type().toHumanReadableWS(), style: textStyle)
    ];
  }

  @override
  int compareItems(IStream a, IStream b, int index) {
    switch (index) {
      case 1:
        return a.id!.compareTo(b.id!);
      case 2:
        return a.name.compareTo(b.name);
      case 3:
        return a.createdDate!.compareTo(b.createdDate!);
      case 4:
        return a.type().compareTo(b.type());

      default:
        throw ArgumentError.value(index, 'index');
    }
  }
}

class _NoStreamsAvailable extends StatelessWidget {
  const _NoStreamsAvailable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Streams'));
  }
}
