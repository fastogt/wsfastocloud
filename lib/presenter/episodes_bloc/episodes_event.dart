part of 'episodes_bloc.dart';

abstract class EpisodesEvent {}

class InitEvent implements EpisodesEvent {
  const InitEvent();
}

class AddEpisodeEvent implements EpisodesEvent {
  final IStream stream;

  const AddEpisodeEvent({required this.stream});
}

class UpdateEpisodeEvent implements EpisodesEvent {
  final IStream stream;

  const UpdateEpisodeEvent({required this.stream});
}

class RemoveEpisodeEvent implements EpisodesEvent {
  final IStream stream;

  const RemoveEpisodeEvent({required this.stream});
}

class LoadEpisodeEvent implements EpisodesEvent {
  const LoadEpisodeEvent();
}

class EmptyEvent implements EpisodesEvent {
  const EmptyEvent();
}

class FailureEvent implements EpisodesEvent {
  final String description;

  const FailureEvent({required this.description});
}

class EnableEpisodeTableViewEvent extends EpisodesEvent {
  EnableEpisodeTableViewEvent();
}

class EnableEpisodeCardsViewEvent extends EpisodesEvent {
  EnableEpisodeCardsViewEvent();
}
