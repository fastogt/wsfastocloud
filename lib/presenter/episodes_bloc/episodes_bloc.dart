import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/presenter/streams/store/episodes_data_source.dart';

part 'episodes_event.dart';
part 'episodes_state.dart';

class EpisodesBloc extends Bloc<EpisodesEvent, EpisodesState> {
  StreamSubscription<WebSocketState>? _subscription;
  final WebSocketApiBloc webSocketApiBloc;
  final StreamDBRepository streamRepository;

  EpisodesBloc({required this.webSocketApiBloc, required this.streamRepository})
      : super(EpisodesState(streamDataSource: StoreEpisodesDataSource([]))) {
    on<InitEvent>(_onInit);
    on<AddEpisodeEvent>(_onAddEpisode);
    on<UpdateEpisodeEvent>(_onUpdateEpisode);
    on<RemoveEpisodeEvent>(_onRemoveEpisode);
    on<LoadEpisodeEvent>(_onLoadEpisode);
    on<EmptyEvent>(_onEmpty);
    on<FailureEvent>(_onFailure);
    on<EnableEpisodeCardsViewEvent>(_onEnableCardsView);
    on<EnableEpisodeTableViewEvent>(_enableTableView);
    add(const InitEvent());
  }

  void _onInit(InitEvent event, Emitter<EpisodesState> emit) async {
    _subscription ??= webSocketApiBloc.stream.listen(
      (event) {
        if (event is WebSocketApiMessage) {
          const String wsEpisodeAddedEvent = 'episode_added';
          const String wsEpisodeUpdatedEvent = 'episode_updated';
          const String wsEpisodeRemovedEvent = 'episode_removed';

          if (event.type == wsEpisodeUpdatedEvent) {
            final type = getStreamTypeFromJson(event.data);
            if (type == null) {
              return;
            }

            if (isVodStreamType(type)) {
              final episodes = makeStream(event.data);
              if (episodes != null && episodes is VodStream) {
                add(UpdateEpisodeEvent(stream: episodes));
              }
            }
          } else if (event.type == wsEpisodeAddedEvent) {
            final type = getStreamTypeFromJson(event.data);
            if (type == null) {
              return;
            }

            if (isVodStreamType(type)) {
              final episodes = makeStream(event.data);
              if (episodes != null && episodes is VodStream) {
                add(AddEpisodeEvent(stream: episodes));
              }
            }
          } else if (event.type == wsEpisodeRemovedEvent) {
            final type = getStreamTypeFromJson(event.data);
            if (type == null) {
              return;
            }

            if (isVodStreamType(type)) {
              final episodes = makeStream(event.data);
              if (episodes != null && episodes is VodStream) {
                add(RemoveEpisodeEvent(stream: episodes));
              }
            }
          }
        } else if (event is WebSocketFailure) {
          add(FailureEvent(description: event.description));
        } else if (event is WebSocketConnected) {
          add(const LoadEpisodeEvent());
        } else {
          add(const EmptyEvent());
        }
      },
    );

    if (webSocketApiBloc.isConnected()) {
      add(const LoadEpisodeEvent());
    }
  }

  void _onAddEpisode(AddEpisodeEvent event, Emitter<EpisodesState> emit) {
    final StoreEpisodesDataSource streamDataSource =
        StoreEpisodesDataSource([...state.streamDataSource.currentList]);

    streamDataSource.addItem(event.stream);

    emit(state.copyWith(streamDataSource: streamDataSource));
  }

  void _onUpdateEpisode(UpdateEpisodeEvent event, Emitter<EpisodesState> emit) {
    final StoreEpisodesDataSource streamDataSource =
        StoreEpisodesDataSource([...state.streamDataSource.currentList]);

    streamDataSource.updateItem(event.stream);

    emit(state.copyWith(streamDataSource: streamDataSource));
  }

  void _onRemoveEpisode(RemoveEpisodeEvent event, Emitter<EpisodesState> emit) {
    final StoreEpisodesDataSource streamDataSource =
        StoreEpisodesDataSource([...state.streamDataSource.currentList]);
    streamDataSource.removeItem(streamDataSource.currentList
        .firstWhere((DataEntry<IStream> entryPoint) => entryPoint.entry.id == event.stream.id)
        .entry);

    emit(state.copyWith(streamDataSource: streamDataSource));
  }

  void _onLoadEpisode(_, Emitter<EpisodesState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      final liveStreams = await streamRepository.loadEpisodes();

      emit(state.copyWith(
          isLoading: false, streamDataSource: StoreEpisodesDataSource([])..addItems(liveStreams)));
    } catch (e) {
      emit(state.copyWith(isLoading: false, errorMessage: 'Backend error: $e'));
    }
  }

  void _onEmpty(_, Emitter<EpisodesState> emit) {
    emit(state.copyWith(isEmpty: true));
  }

  void _onFailure(FailureEvent event, Emitter<EpisodesState> emit) {
    emit(state.copyWith(errorMessage: event.description));
  }

  void _onEnableCardsView(EnableEpisodeCardsViewEvent event, Emitter<EpisodesState> emit) {
    emit(state.copyWith(isEpisodesCardsViewEnabled: true));
  }

  FutureOr<void> _enableTableView(EnableEpisodeTableViewEvent event, Emitter<EpisodesState> emit) {
    emit(state.copyWith(isEpisodesCardsViewEnabled: false));
  }

  void dispose() {
    _subscription?.cancel();
    _subscription = null;
  }
}
