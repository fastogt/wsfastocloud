part of 'episodes_bloc.dart';

class EpisodesState extends Equatable {
  final StoreEpisodesDataSource streamDataSource;
  final String? errorMessage;
  final bool isLoading;
  final bool isEpisodesCardsViewEnabled;

  const EpisodesState(
      {required this.streamDataSource,
      this.errorMessage,
      this.isLoading = true,
      this.isEpisodesCardsViewEnabled = true});

  EpisodesState copyWith(
      {StoreEpisodesDataSource? streamDataSource,
      String? errorMessage,
      bool? isEmpty,
      bool? isLoading,
      bool? isEpisodesCardsViewEnabled}) {
    return EpisodesState(
        streamDataSource: streamDataSource ?? this.streamDataSource,
        errorMessage: errorMessage,
        isLoading: isLoading ?? this.isLoading,
        isEpisodesCardsViewEnabled: isEpisodesCardsViewEnabled ?? this.isEpisodesCardsViewEnabled);
  }

  @override
  List<Object?> get props {
    return [streamDataSource, errorMessage, isLoading, isEpisodesCardsViewEnabled];
  }
}
