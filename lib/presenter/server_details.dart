import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/material.dart';
import 'package:wsfastocloud/presenter/connect_and_config_row.dart';

class ServerDetailsPage extends StatelessWidget {
  final WsMode mode;

  const ServerDetailsPage({Key? key, required this.mode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final content = LayoutWidget(mode: mode);
    return Scaffold(resizeToAvoidBottomInset: false, body: SafeArea(bottom: false, child: content));
  }
}
