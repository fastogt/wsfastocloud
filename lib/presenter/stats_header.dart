import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/locator.dart';
import 'package:wsfastocloud/presenter/copy_link_button.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_statistics_bloc/server_statistics_bloc.dart';

class StatsButtons extends StatefulWidget {
  final Widget buttons;
  final WsMode mode;

  const StatsButtons({Key? key, required this.mode, required this.buttons}) : super(key: key);

  @override
  _StatsButtonsState createState() {
    return _StatsButtonsState();
  }
}

class _StatsButtonsState extends State<StatsButtons> {
  late String version;

  @override
  void initState() {
    final package = locator<PackageManager>();
    version = package.version();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(mobile: _mobile, desktop: _desktop);
  }

  Widget _mobile(BuildContext context) {
    return BlocBuilder<ServiceStatisticsBloc, ServiceStatisticsState>(builder: (context, state) {
      return Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text('${context.l10n.version} $version', style: const TextStyle(fontSize: 8)),
              if (state is ServiceStatisticsData)
                Text('${context.l10n.connectedTo} ${state.url}',
                    style: const TextStyle(fontSize: 8)),
              if (state is ServiceStatisticsData)
                const SizedBox(height: 30, child: CopyLinkButton())
            ]),
            Expanded(
                child: Align(
                    alignment: Alignment.centerRight,
                    child: FittedBox(
                        child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Column(children: _wrapButtons())))))
          ]));
    });
  }

  Widget _desktop(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child:
            BlocBuilder<ServiceStatisticsBloc, ServiceStatisticsState>(builder: (context, state) {
          return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text('${context.l10n.version} $version'),
              // const SizedBox(height: 4),
              // Text('Mode: ${widget.mode.toHumanReadable()}'),
              if (state is ServiceStatisticsData)
                Row(children: [
                  Text('${context.l10n.connectedTo} ${state.url}'),
                  const CopyLinkButton()
                ])
            ]),
            Expanded(child: _buttonsRow())
          ]);
        }));
  }

  Widget _buttonsRow() {
    return Align(
        alignment: Alignment.centerRight,
        child: FittedBox(
            child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(children: _wrapButtons()))));
  }

  List<Widget> _wrapButtons() {
    return [Padding(padding: const EdgeInsets.symmetric(horizontal: 8), child: widget.buttons)];
  }
}

class StatsButton extends StatelessWidget {
  final String title;
  final VoidCallback? onPressed;

  const StatsButton({Key? key, required this.title, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: FlatButtonEx.filled(text: title, onPressed: onPressed));
  }
}
