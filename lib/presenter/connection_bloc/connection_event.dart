part of 'connection_bloc.dart';

abstract class ConnectionEvent extends Equatable {
  const ConnectionEvent();

  @override
  List<Object?> get props => [];
}

class ConnectRequestedEvent extends ConnectionEvent {
  final ConnectionInfo? info;
  final WsMode mode;
  final GlobalTheme theme;
  final String locale;

  const ConnectRequestedEvent(this.info, this.mode, this.theme, this.locale);

  @override
  List<Object?> get props => [info, mode];
}

class ConnectFromLocalStorage extends ConnectionEvent {
  const ConnectFromLocalStorage();

  @override
  List<Object?> get props => [];
}

class DisconnectRequestedEvent extends ConnectionEvent {
  const DisconnectRequestedEvent();

  @override
  List<Object?> get props => [];
}

class InitializeEvent extends ConnectionEvent {
  const InitializeEvent();

  @override
  List<Object?> get props => [];
}
