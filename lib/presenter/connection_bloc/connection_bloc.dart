import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/foundation.dart';
import 'package:wsfastocloud/data/services/api/models/connection_info.dart';
import 'package:wsfastocloud/data/services/local_storage/local_storage_service.dart';
import 'package:wsfastocloud/locator.dart';
import 'package:wsfastocloud/utils/uri_host_and_port.dart';

part 'connection_event.dart';
part 'connection_state.dart';

class ConnectionBloc extends Bloc<ConnectionEvent, ConnectionState> {
  ConnectionBloc() : super(const ConnectionState(null)) {
    on<InitializeEvent>(_initialize);
    on<ConnectRequestedEvent>(connect);
    on<ConnectFromLocalStorage>(connectLocalStorage);
    on<DisconnectRequestedEvent>(disconnect);
  }

  WSServer? server;

  void _initialize(InitializeEvent event, Emitter<ConnectionState> emit) async {
    emit(const ConnectionState(null));
  }

  void connect(ConnectRequestedEvent event, Emitter<ConnectionState> emit) async {
    emit(const ConnectionState(null));
    final storage = locator<LocalStorageService>();
    if (event.info?.encodedAuth != null) {
      server = makeWSServerFromEncodedAuth(event.info!.originalUrl, event.info!.encodedAuth!);
    } else {
      server = WSServer(url: event.info!.originalUrl);
    }
    if (kIsWeb) {
      storage.setConnectedUrl(server!.makeWsUrlWithHost(
          UriHostAndPort().hostAndPort(), event.theme, event.mode, event.locale));
    } else {
      storage.setConnectedUrl(server!.makeWsUrl('latest', event.theme, event.mode, event.locale)!);
    }
    emit(ConnectionState(event.info));
  }

  void connectLocalStorage(ConnectFromLocalStorage event, Emitter<ConnectionState> emit) async {
    emit(const ConnectionState(null));
    final storage = locator<LocalStorageService>();
    final _originalUrl = storage.server();
    if (_originalUrl != null) {
      final _mode = storage.mode() ?? WsMode.OTT;
      final _theme = storage.theme();
      final _locale = storage.locale();
      server = _originalUrl;
      if (kIsWeb) {
        storage.setConnectedUrl(
            server!.makeWsUrlWithHost(UriHostAndPort().hostAndPort(), _theme, _mode, _locale));
      } else {
        storage.setConnectedUrl(server!.makeWsUrl('latest', _theme, _mode, _locale)!);
      }
      emit(ConnectionState(ConnectionInfo.fromUserInput(server!)));
    }
  }

  void disconnect(DisconnectRequestedEvent event, Emitter<ConnectionState> emit) async {
    final LocalStorageService storage = locator<LocalStorageService>();
    storage.removeConnectedUrl();
    emit(const ConnectionState(null));
  }
}
