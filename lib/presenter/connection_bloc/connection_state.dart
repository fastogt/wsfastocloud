part of 'connection_bloc.dart';

class ConnectionState extends Equatable {
  final ConnectionInfo? info;

  const ConnectionState(this.info);

  @override
  List<Object?> get props => [info];
}
