part of 'catchups_bloc.dart';

abstract class CatchupsEvent {}

class InitEvent implements CatchupsEvent {
  const InitEvent();
}

class AddCatchupEvent implements CatchupsEvent {
  final CatchupStream catchupStream;

  const AddCatchupEvent({required this.catchupStream});
}

class UpdateCatchupEvent implements CatchupsEvent {
  final CatchupStream catchupStream;

  const UpdateCatchupEvent({required this.catchupStream});
}

class RemoveCatchupEvent implements CatchupsEvent {
  final CatchupStream catchupStream;

  const RemoveCatchupEvent({required this.catchupStream});
}

class LoadCatchupEvent implements CatchupsEvent {
  const LoadCatchupEvent();
}

class EmptyEvent implements CatchupsEvent {
  const EmptyEvent();
}

class FailureEvent implements CatchupsEvent {
  final String description;

  const FailureEvent({required this.description});
}
