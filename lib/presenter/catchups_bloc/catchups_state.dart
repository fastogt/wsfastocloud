part of 'catchups_bloc.dart';

class CatchupsState extends Equatable {
  final StoreCatchupDataSource catchupDataSource;
  final String? errorMessage;
  final bool isLoading;

  const CatchupsState({required this.catchupDataSource, this.errorMessage, this.isLoading = true});

  CatchupsState copyWith(
      {StoreCatchupDataSource? catchupDataSource,
      String? errorMessage,
      bool? isEmpty,
      bool? isLoading}) {
    return CatchupsState(
        catchupDataSource: catchupDataSource ?? this.catchupDataSource,
        errorMessage: errorMessage,
        isLoading: isLoading ?? this.isLoading);
  }

  @override
  List<Object?> get props => [catchupDataSource, errorMessage, isLoading];
}
