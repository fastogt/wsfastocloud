import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/presenter/streams/store/catchup_data_source.dart';

part 'catchups_event.dart';
part 'catchups_state.dart';

class CatchupsBloc extends Bloc<CatchupsEvent, CatchupsState> {
  StreamSubscription<WebSocketState>? _subscription;
  final WebSocketApiBloc webSocketApiBloc;
  final StreamDBRepository streamRepository;

  CatchupsBloc({required this.webSocketApiBloc, required this.streamRepository})
      : super(CatchupsState(catchupDataSource: StoreCatchupDataSource([]))) {
    on<InitEvent>(_onInit);
    on<AddCatchupEvent>(_onAddCatchup);
    on<UpdateCatchupEvent>(_onUpdateCatchup);
    on<RemoveCatchupEvent>(_onRemoveCatchup);
    on<LoadCatchupEvent>(_onLoadCatchup);
    on<EmptyEvent>(_onEmpty);
    on<FailureEvent>(_onFailure);

    add(const InitEvent());
  }

  void _onInit(InitEvent event, Emitter<CatchupsState> emit) async {
    _subscription ??= webSocketApiBloc.stream.listen(
      (event) {
        if (event is WebSocketApiMessage) {
          const String wsCatcupAddedEvent = 'catchup_added';
          const String wsCatcupUpdatedEvent = 'catchup_updated';
          const String wsCatcupRemovedEvent = 'catchup_removed';

          if (event.type == wsCatcupUpdatedEvent) {
            final type = getStreamTypeFromJson(event.data);
            if (type == null) {
              return;
            }

            final catchup = makeStream(event.data);
            if (catchup != null && catchup is CatchupStream) {
              add(UpdateCatchupEvent(catchupStream: catchup));
            }
          } else if (event.type == wsCatcupAddedEvent) {
            final type = getStreamTypeFromJson(event.data);
            if (type == null) {
              return;
            }

            final catchup = makeStream(event.data);
            if (catchup != null && catchup is CatchupStream) {
              add(AddCatchupEvent(catchupStream: catchup));
            }
          } else if (event.type == wsCatcupRemovedEvent) {
            final type = getStreamTypeFromJson(event.data);
            if (type == null) {
              return;
            }

            final catchup = makeStream(event.data);
            if (catchup != null && catchup is CatchupStream) {
              add(RemoveCatchupEvent(catchupStream: catchup));
            }
          }
        } else if (event is WebSocketFailure) {
          add(FailureEvent(description: event.description));
        } else if (event is WebSocketConnected) {
          add(const LoadCatchupEvent());
        } else {
          add(const EmptyEvent());
        }
      },
    );

    if (webSocketApiBloc.isConnected()) {
      add(const LoadCatchupEvent());
    }
  }

  void _onAddCatchup(AddCatchupEvent event, Emitter<CatchupsState> emit) {
    final StoreCatchupDataSource catchupDataSource =
        StoreCatchupDataSource([...state.catchupDataSource.currentList]);

    catchupDataSource.addItem(event.catchupStream);

    emit(state.copyWith(catchupDataSource: catchupDataSource));
  }

  void _onUpdateCatchup(UpdateCatchupEvent event, Emitter<CatchupsState> emit) {
    final StoreCatchupDataSource catchupDataSource =
        StoreCatchupDataSource([...state.catchupDataSource.currentList]);

    catchupDataSource.updateItem(event.catchupStream);

    emit(state.copyWith(catchupDataSource: catchupDataSource));
  }

  void _onRemoveCatchup(RemoveCatchupEvent event, Emitter<CatchupsState> emit) {
    final StoreCatchupDataSource catchupDataSource =
        StoreCatchupDataSource([...state.catchupDataSource.currentList]);
    for (final it in catchupDataSource.currentList) {
      if (it.entry.id == event.catchupStream.id) {
        catchupDataSource.removeItem(it.entry);
        emit(state.copyWith(catchupDataSource: catchupDataSource));
        break;
      }
    }
  }

  void _onLoadCatchup(_, Emitter<CatchupsState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      final catchupStreams = await streamRepository.loadCatchups();

      emit(state.copyWith(
          isLoading: false,
          catchupDataSource: StoreCatchupDataSource([])..addItems(catchupStreams)));
    } catch (e) {
      emit(state.copyWith(isLoading: false, errorMessage: 'Backend error: $e'));
    }
  }

  void _onEmpty(_, Emitter<CatchupsState> emit) {
    emit(state.copyWith(isEmpty: true));
  }

  void _onFailure(FailureEvent event, Emitter<CatchupsState> emit) {
    emit(state.copyWith(errorMessage: event.description));
  }

  void dispose() {
    _subscription?.cancel();
    _subscription = null;
  }
}
