import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/presenter/streams/store/vods_data_source.dart';

part 'vods_event.dart';
part 'vods_state.dart';

class VodsBloc extends Bloc<VodsEvent, VodsState> {
  StreamSubscription<WebSocketState>? _subscription;

  final WebSocketApiBloc webSocketApiBloc;
  final StreamDBRepository streamRepository;

  VodsBloc({required this.webSocketApiBloc, required this.streamRepository})
      : super(VodsState(streamDataSource: StoreVodsDataSource([]))) {
    on<InitEvent>(_onInit);
    on<AddVodEvent>(_onAddStream);
    on<UpdateVodEvent>(_onUpdateStream);
    on<RemoveVodEvent>(_onRemoveStream);
    on<LoadStreamsEvent>(_onLoadStreams);
    on<EmptyEvent>(_onEmpty);
    on<FailureEvent>(_onFailure);
    on<EnableVodCardsViewEvent>(_onEnableCardsView);
    on<EnableVodTableViewEvent>(_enableTableView);
    add(const InitEvent());
  }

  void _onInit(InitEvent event, Emitter<VodsState> emit) async {
    _subscription ??= webSocketApiBloc.stream.listen(
      (event) {
        if (event is WebSocketApiMessage) {
          const String wsVodAddedEvent = 'vod_added';
          const String wsVodUpdatedEvent = 'vod_updated';
          const String wsVodRemovedEvent = 'vod_removed';

          if (event.type == wsVodUpdatedEvent) {
            final type = getStreamTypeFromJson(event.data);
            if (type == null) {
              return;
            }

            if (isVodStreamType(type)) {
              final vods = makeStream(event.data);
              if (vods != null && vods is VodStream) {
                add(UpdateVodEvent(stream: vods));
              }
            }
          } else if (event.type == wsVodAddedEvent) {
            final type = getStreamTypeFromJson(event.data);
            if (type == null) {
              return;
            }

            if (isVodStreamType(type)) {
              final vods = makeStream(event.data);
              if (vods != null && vods is VodStream) {
                add(AddVodEvent(stream: vods));
              }
            }
          } else if (event.type == wsVodRemovedEvent) {
            final type = getStreamTypeFromJson(event.data);
            if (type == null) {
              return;
            }

            if (isVodStreamType(type)) {
              final vods = makeStream(event.data);
              if (vods != null && vods is VodStream) {
                add(RemoveVodEvent(stream: vods));
              }
            }
          }
        } else if (event is WebSocketFailure) {
          add(FailureEvent(description: event.description));
        } else if (event is WebSocketConnected) {
          add(const LoadStreamsEvent());
        } else {
          add(const EmptyEvent());
        }
      },
    );

    if (webSocketApiBloc.isConnected()) {
      add(const LoadStreamsEvent());
    }
  }

  void _onAddStream(AddVodEvent event, Emitter<VodsState> emit) {
    final StoreVodsDataSource streamDataSource =
        StoreVodsDataSource([...state.streamDataSource.currentList]);

    streamDataSource.addItem(event.stream);

    emit(state.copyWith(streamDataSource: streamDataSource));
  }

  void _onUpdateStream(UpdateVodEvent event, Emitter<VodsState> emit) {
    final StoreVodsDataSource streamDataSource =
        StoreVodsDataSource([...state.streamDataSource.currentList]);

    streamDataSource.updateItem(event.stream);

    emit(state.copyWith(streamDataSource: streamDataSource));
  }

  void _onRemoveStream(RemoveVodEvent event, Emitter<VodsState> emit) {
    final StoreVodsDataSource streamDataSource =
        StoreVodsDataSource([...state.streamDataSource.currentList]);
    streamDataSource.removeItem(streamDataSource.currentList
        .firstWhere((DataEntry<IStream> entryPoint) => entryPoint.entry.id == event.stream.id)
        .entry);

    emit(state.copyWith(streamDataSource: streamDataSource));
  }

  void _onLoadStreams(_, Emitter<VodsState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      final liveStreams = await streamRepository.loadVods();

      emit(state.copyWith(
          isLoading: false, streamDataSource: StoreVodsDataSource([])..addItems(liveStreams)));
    } catch (e) {
      emit(state.copyWith(isLoading: false, errorMessage: 'Backend error: $e'));
    }
  }

  void _onEmpty(_, Emitter<VodsState> emit) {
    emit(state.copyWith(isEmpty: true));
  }

  void _onFailure(FailureEvent event, Emitter<VodsState> emit) {
    emit(state.copyWith(errorMessage: event.description));
  }

  void dispose() {
    _subscription?.cancel();
    _subscription = null;
  }

  void _onEnableCardsView(EnableVodCardsViewEvent event, Emitter<VodsState> emit) {
    emit(state.copyWith(isVodsCardsViewEnabled: true));
  }

  FutureOr<void> _enableTableView(EnableVodTableViewEvent event, Emitter<VodsState> emit) {
    emit(state.copyWith(isVodsCardsViewEnabled: false));
  }
}
