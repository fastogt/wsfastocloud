part of 'vods_bloc.dart';

abstract class VodsEvent {}

class InitEvent implements VodsEvent {
  const InitEvent();
}

class AddVodEvent implements VodsEvent {
  final IStream stream;

  const AddVodEvent({required this.stream});
}

class UpdateVodEvent implements VodsEvent {
  final IStream stream;

  const UpdateVodEvent({required this.stream});
}

class RemoveVodEvent implements VodsEvent {
  final IStream stream;

  const RemoveVodEvent({required this.stream});
}

class LoadStreamsEvent implements VodsEvent {
  const LoadStreamsEvent();
}

class EmptyEvent implements VodsEvent {
  const EmptyEvent();
}

class FailureEvent implements VodsEvent {
  final String description;

  const FailureEvent({required this.description});
}

class EnableVodTableViewEvent extends VodsEvent {
  EnableVodTableViewEvent();
}

class EnableVodCardsViewEvent extends VodsEvent {
  EnableVodCardsViewEvent();
}
