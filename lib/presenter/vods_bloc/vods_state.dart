part of 'vods_bloc.dart';

class VodsState extends Equatable {
  final StoreVodsDataSource streamDataSource;
  final String? errorMessage;
  final bool isLoading;
  final bool isVodsCardsViewEnabled;

  const VodsState(
      {required this.streamDataSource,
      this.errorMessage,
      this.isLoading = true,
      this.isVodsCardsViewEnabled = true});

  VodsState copyWith(
      {StoreVodsDataSource? streamDataSource,
      String? errorMessage,
      bool? isEmpty,
      bool? isLoading,
      bool? isVodsCardsViewEnabled}) {
    return VodsState(
        streamDataSource: streamDataSource ?? this.streamDataSource,
        errorMessage: errorMessage,
        isLoading: isLoading ?? this.isLoading,
        isVodsCardsViewEnabled: isVodsCardsViewEnabled ?? this.isVodsCardsViewEnabled);
  }

  @override
  List<Object?> get props {
    return [streamDataSource, errorMessage, isLoading, isVodsCardsViewEnabled];
  }
}
