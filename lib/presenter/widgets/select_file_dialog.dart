import 'dart:developer';
import 'dart:io';

import 'package:dart_common/dart_common.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:provider/provider.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/widgets/select_file_dialog_types.dart';

enum UploadDialogs { dialog_true, dialog_false, dialog_showData }

class SelectFileDialog extends StatefulWidget {
  final bool isFileUpload;

  const SelectFileDialog.file() : isFileUpload = true;
  const SelectFileDialog.video() : isFileUpload = false;
  @override
  _SelectFileDialogState createState() {
    return _SelectFileDialogState();
  }
}

class _SelectFileDialogState extends State<SelectFileDialog> {
  Map<String, List<int>> _filesData = {};
  UploadDialogs _parsingStreams = UploadDialogs.dialog_false;
  MediaInfo? mData;
  String? path;

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        title: Text(context.l10n.fileUpload),
        children: _parsingStreams == UploadDialogs.dialog_false ? _fields() : uploadOut());
  }

  List<Widget> _fields() {
    return <Widget>[
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(text: context.l10n.selectAFile, onPressed: _startFilePicker)),
      const SizedBox(height: 8),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(
              text: context.l10n.upload, onPressed: _filesData.isEmpty ? null : _makeRequest))
    ];
  }

  List<Widget> uploadOut() {
    if (_parsingStreams == UploadDialogs.dialog_true) {
      return <Widget>[
        const Center(
            child: Padding(padding: EdgeInsets.all(8.0), child: CircularProgressIndicator()))
      ];
    } else if (_parsingStreams == UploadDialogs.dialog_showData) {
      final generateHttpUrl = context.read<Fetcher>().getBackendEndpoint(path!);
      final MediaInfo data = mData!;
      return <Widget>[
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
                'Rotation: ${data.rotation.toString()}°\nSize: ${data.width} x ${data.height}\nDuration: ${printDuration(data.getDuration())}')),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextFieldEx.readOnly(hint: context.l10n.fileUrl, init: data.url.toString())),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child:
                TextFieldEx.readOnly(hint: context.l10n.httpUrl, init: generateHttpUrl.toString())),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: FlatButtonEx.filled(
                text: context.l10n.close,
                onPressed: () {
                  Navigator.of(context).pop();
                }))
      ];
    }
    return <Widget>[
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Text(context.l10n.errorInUpload)),
      Padding(
          padding: const EdgeInsets.all(16.0),
          child: FlatButtonEx.filled(
              text: context.l10n.close,
              onPressed: () {
                Navigator.of(context).pop();
              }))
    ];
  }

  // private:
  void _startFilePicker() async {
    _filesData = {};
    try {
      final FilePickerResult? result = await FilePicker.platform.pickFiles(withData: true);
      if (result == null) {
        return;
      }

      for (int i = 0; i < result.files.length; ++i) {
        final PlatformFile file = result.files[i];
        if (kIsWeb) {
          final pfile = file.xFile;
          final data = await pfile.readAsBytes();
          _filesData[file.name] = data;
        } else if (file.path != null) {
          final pfile = File(file.path!);
          final data = await pfile.readAsBytes();
          _filesData[file.name] = data;
        }
      }
      setState(() {});
    } on PlatformException catch (e) {
      log('Unsupported operation: $e');
    } catch (err) {
      log('Exception: $err');
    }
  }

  void _makeRequest() {
    setState(() {
      _parsingStreams = UploadDialogs.dialog_true;
    });

    final Map<String, dynamic> field = {};

    context
        .read<Fetcher>()
        .sendFiles(
            widget.isFileUpload ? '/server/file/upload' : '/server/video/upload', _filesData, field)
        .then((resp) {
      if (resp.statusCode == HttpStatus.ok) {
        final respData = httpDataResponseFromString(resp.body);
        if (respData != null) {
          final content = respData.contentMap();
          if (widget.isFileUpload) {
            if (content == null) return;
            Navigator.pop(context, content['path']);
            return;
          }

          setState(() {
            _parsingStreams = UploadDialogs.dialog_showData;
            mData = MediaInfo.fromJson(content!['info']);
            path = content['path'];
          });
        } else {
          throw ErrorHttp(401, 'backend response', 'Not handled case');
        }
      } else {
        final respData = httpDataResponseFromString(resp.body);
        if (respData != null) {
          final err = respData.error();
          if (err != null) {
            throw ErrorHttp(resp.statusCode, 'backend response', err);
          } else {
            throw ErrorHttp(401, 'backend response', 'Not handled case');
          }
        } else {
          throw ErrorHttp(401, 'backend response', 'Not handled case');
        }
      }
    }, onError: (error) {
      setState(() {
        _parsingStreams = UploadDialogs.dialog_false;
      });
      showError(context, error);
    });
  }
}
