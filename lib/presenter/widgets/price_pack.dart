import 'package:fastocloud_dart_media_models/models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/l10n/l10n.dart';

class PricePackField extends StatelessWidget {
  final PricePack price;

  const PricePackField(this.price);

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Expanded(child: _price(context)),
      Expanded(child: _currency(context)),
      Expanded(child: _type(context))
    ]);
  }

  Widget _price(BuildContext context) {
    return NumberTextField.decimal(
        minDouble: Price.MIN,
        maxDouble: Price.MAX,
        hintText: context.l10n.price,
        canBeEmpty: false,
        initDouble: price.price,
        onFieldChangedDouble: (term) {
          if (term != null) {
            price.price = term;
          }
        });
  }

  Widget _currency(BuildContext context) {
    return DropdownButtonEx<Currency>(
        hint: 'Currency',
        value: price.currency,
        values: Currency.values,
        onChanged: (c) {
          price.currency = c;
        },
        itemBuilder: (Currency value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  Widget _type(BuildContext context) {
    return DropdownButtonEx<PriceType>(
        hint: 'Type',
        value: price.type,
        values: PriceType.values,
        onChanged: (c) {
          price.type = c;
        },
        itemBuilder: (PriceType value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }
}
