import 'dart:js_interop';
import 'dart:js_interop_unsafe';

import 'package:dart_common/dart_common.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:provider/provider.dart';
import 'package:web/web.dart' as web;
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/widgets/select_file_dialog_types.dart';

enum UploadDialogs { dialog_true, dialog_false, dialog_showData }

class SelectFileWebDialog extends StatefulWidget {
  final bool isFileSelection;

  const SelectFileWebDialog.file() : isFileSelection = true;
  const SelectFileWebDialog.video() : isFileSelection = false;

  @override
  _SelectFileWebDialogState createState() {
    return _SelectFileWebDialogState();
  }
}

class _SelectFileWebDialogState extends State<SelectFileWebDialog> {
  web.FileList? files;
  final worker = web.Worker('js/upload_worker.js'.toJS);

  UploadDialogs _parsingStreams = UploadDialogs.dialog_false;
  MediaInfo? mData;
  String? path;

  double _progress = 0.0;

  @override
  void initState() {
    void onMessage(web.MessageEvent message) {
      final decoded = message.data as JSObject;
      if (decoded.has('status')) {
        final jstatus = decoded.getProperty<JSNumber>('status'.toJS);
        final int status = jstatus.toDartInt;
        if (status == 200) {
          final jdata = decoded.getProperty<JSObject?>('data'.toJS);
          if (jdata != null) {
            final jdata2 = jdata.getProperty<JSObject?>('data'.toJS);
            if (jdata2 != null) {
              final jpath = jdata2.getProperty<JSString>('path'.toJS);
              if (widget.isFileSelection) {
                setState(() {
                  Navigator.pop(context, jpath.toDart);
                });
              } else {
                setState(() {
                  _parsingStreams = UploadDialogs.dialog_showData;
                  final jinfo = jdata2.getProperty<JSObject>('info'.toJS);
                  mData = MediaInfo.fromJSObject(jinfo);
                  path = jpath.toDart;
                });
              }
            }
          }
        } else {
          final jdata = decoded.getProperty<JSObject?>('data'.toJS);
          if (jdata != null) {
            final jerror = jdata.getProperty<JSObject?>('error'.toJS);
            if (jerror != null) {
              final err = ErrorJson.fromJSObject(jerror);
              setState(() {
                _parsingStreams = UploadDialogs.dialog_false;
              });
              showError(context, ErrorHttp(status, 'backend response', err));
              return;
            }
          }

          setState(() {
            _parsingStreams = UploadDialogs.dialog_false;
          });
          showError(context, ErrorHttp(status, 'backend response unknown'));
        }
      } else if (decoded.has('progress')) {
        final jprog = decoded.getProperty<JSNumber>('progress'.toJS);
        final prog = jprog.toDartDouble;
        setState(() {
          _progress = prog;
        });
      }
    }

    worker.onmessage = onMessage.toJS;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        title: Text(context.l10n.fileUpload),
        children: _parsingStreams == UploadDialogs.dialog_false ? _fields() : uploadOut());
  }

  List<Widget> _fields() {
    return <Widget>[
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(text: context.l10n.selectAFile, onPressed: _startFilePicker)),
      const SizedBox(height: 8),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(
              text: context.l10n.upload, onPressed: files == null ? null : _makeRequest))
    ];
  }

  List<Widget> uploadOut() {
    if (_parsingStreams == UploadDialogs.dialog_true) {
      final progress = Stack(children: <Widget>[
        SizedBox(height: 5, child: LinearProgressIndicator(value: _progress)),
        Align(child: Text('${fixedDouble(_progress * 100.0)} %'), alignment: Alignment.topCenter)
      ]);
      return <Widget>[Center(child: Padding(padding: const EdgeInsets.all(8.0), child: progress))];
    } else if (_parsingStreams == UploadDialogs.dialog_showData) {
      final generateHttpUrl = context.read<Fetcher>().getBackendEndpoint(path!);
      final MediaInfo data = mData!;
      return <Widget>[
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
                'Rotation: ${data.rotation.toString()}°\nSize: ${data.width} x ${data.height}\nDuration: ${printDuration(data.getDuration())}')),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextFieldEx.readOnly(hint: context.l10n.fileUrl, init: data.url.toString())),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child:
                TextFieldEx.readOnly(hint: context.l10n.httpUrl, init: generateHttpUrl.toString())),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: FlatButtonEx.filled(
                text: context.l10n.close,
                onPressed: () {
                  Navigator.of(context).pop();
                }))
      ];
    }
    return <Widget>[
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Text(context.l10n.errorInUpload)),
      Padding(
          padding: const EdgeInsets.all(16.0),
          child: FlatButtonEx.filled(
              text: context.l10n.close,
              onPressed: () {
                Navigator.of(context).pop();
              }))
    ];
  }

  // private:
  void _startFilePicker() async {
    final uploadInput = web.HTMLInputElement();
    uploadInput.type = 'file';
    uploadInput.draggable = true;
    uploadInput.style.display = 'none';
    uploadInput.multiple = false;
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      setState(() {
        files = uploadInput.files;
      });
    });
  }

  void _makeRequest() {
    setState(() {
      _parsingStreams = UploadDialogs.dialog_true;
    });
    final fetcher = context.read<Fetcher>();

    final generateHttpUrl = widget.isFileSelection
        ? fetcher.getBackendEndpoint('/server/file/upload')
        : fetcher.getBackendEndpoint('/server/video/upload');
    final fl = files!.item(0);
    final data = {"file": fl, "uri": generateHttpUrl.toString(), "auth": fetcher.authorization};
    final js = mapToJsObject(data);
    worker.postMessage(js);
  }
}

JSAny mapToJsObject(Map<String, Object?> map) {
  final jsMap = JSObject();
  map.forEach((key, value) {
    jsMap.setProperty(key.toJS, _mapToJsObject(value));
  });
  return jsMap;
}

JSAny _mapToJsObject(Object? dartObject) {
  if (dartObject == null) {
    return 0.toJS;
  } else if (dartObject is num) {
    return dartObject.toJS;
  } else if (dartObject is bool) {
    return dartObject.toJS;
  } else if (dartObject is String) {
    return dartObject.toJS;
  } else if (dartObject is JSObject) {
    return dartObject;
  }

  if (dartObject is Map<String, Object?>) {
    final jsMap = JSObject();
    dartObject.forEach((key, value) {
      jsMap.setProperty(key.toJS, _mapToJsObject(value));
    });
    return jsMap;
  }
  throw Exception('Unknpown type of object: $dartObject');
}
