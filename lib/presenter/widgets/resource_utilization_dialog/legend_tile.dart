import 'package:flutter/material.dart';

class LegendTile extends StatelessWidget {
  final String title;
  final Color color;

  const LegendTile(this.title, this.color);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: [
      Container(color: color, width: 16, height: 16),
      Padding(padding: const EdgeInsets.all(8.0), child: Text(title))
    ]);
  }
}
