import 'dart:math';

import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:intl/intl.dart';
import 'package:wsfastocloud/l10n/l10n.dart';

import 'legend_tile.dart';

class ServerAnalytics extends StatefulWidget {
  const ServerAnalytics({Key? key, required this.data}) : super(key: key);

  final List<Machine> data;

  @override
  State<ServerAnalytics> createState() => _ServerAnalyticsState();
}

class _ServerAnalyticsState extends State<ServerAnalytics> {
  String _currentType = CPU_GPU;

  final List<DateTime> _dates = [];

  final Map<String, List<num>> _data = {};

  static const CPU_FIELD = 'cpu';
  static const GPU_FIELD = 'gpu';
  static const HDD_USED = 'hdd_used';
  static const MEMORY_TOTAL_FIELD = 'memory_total';
  static const MEMORY_FREE_FIELD = 'memory_free';
  static const MEMORY_USED = 'memory_used';
  static const HDD_TOTAL_FIELD = 'hdd_total';
  static const HDD_FREE_FIELD = 'hdd_free';
  static const BANDWIDTH_IN_FIELD = 'bandwidth_in';
  static const BANDWIDTH_OUT_FIELD = 'bandwidth_out';

  static const CPU_GPU = 'CPU/GPU';
  static const MEMORY = 'Memory';
  static const HDD = 'HDD';
  static const BANDWIDTH = 'Network';

  double _maxHardware = 0;
  int _maxMemory = 0;
  int _maxHDD = 0;
  int _maxBandwidth = 0;

  double reservedSize = 64;

  void _updateServerStats(List<Machine> stats) {
    _data[CPU_FIELD] = <double>[];
    _data[GPU_FIELD] = <double>[];
    _data[MEMORY_TOTAL_FIELD] = <int>[];
    _data[MEMORY_FREE_FIELD] = <int>[];
    _data[MEMORY_USED] = <int>[];
    _data[HDD_TOTAL_FIELD] = <int>[];
    _data[HDD_FREE_FIELD] = <int>[];
    _data[HDD_USED] = <int>[];
    _data[BANDWIDTH_IN_FIELD] = <int>[];
    _data[BANDWIDTH_OUT_FIELD] = <int>[];

    for (final element in stats) {
      final timestamp = DateTime.fromMillisecondsSinceEpoch(element.timestamp);
      _dates.add(timestamp);

      _data[CPU_FIELD]!.add(element.cpu);
      _data[GPU_FIELD]!.add(element.gpu);
      final _maxCpuGpu = max<double>(element.cpu, element.gpu);
      _maxHardware = max<double>(_maxCpuGpu, _maxHardware);

      _data[MEMORY_TOTAL_FIELD]!.add(element.memoryTotal);
      _data[MEMORY_FREE_FIELD]!.add(element.memoryFree);
      _data[MEMORY_USED]!.add(element.memoryTotal - element.memoryFree);
      _maxMemory = max<int>(element.memoryTotal, _maxMemory);

      _data[HDD_TOTAL_FIELD]!.add(element.hddTotal);
      _data[HDD_FREE_FIELD]!.add(element.hddFree);
      _data[HDD_USED]!.add(element.hddTotal - element.hddFree);
      _maxHDD = max<int>(element.hddTotal, _maxHDD);

      _data[BANDWIDTH_IN_FIELD]!.add(element.bandwidthIn);
      _data[BANDWIDTH_OUT_FIELD]!.add(element.bandwidthOut);
      final _maxBandwidthIO = max<int>(element.bandwidthIn, element.bandwidthOut);
      _maxBandwidth = max<int>(_maxBandwidthIO, _maxBandwidth);
    }
  }

  Widget _typePicker() {
    return DropdownButtonEx<String>(
        value: _currentType,
        values: const [CPU_GPU, MEMORY, HDD, BANDWIDTH],
        onChanged: (c) {
          setState(() {
            _currentType = c;
          });
        },
        itemBuilder: (String type) {
          return DropdownMenuItem(child: Text(type), value: type);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
            child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
              Row(children: [SizedBox(width: 128, child: _typePicker()), Text(_getUnit()!)]),
              Expanded(child: _serverPlotBuilder())
            ])));
  }

  Widget _serverPlotBuilder() {
    return widget.data.isEmpty ? _NoServerData() : _serverPlot(widget.data);
  }

  Widget _serverPlot(List<Machine> stats) {
    _updateServerStats(stats);
    const top = AxisTitles();
    final bottom = AxisTitles(
        sideTitles: SideTitles(
            reservedSize: reservedSize,
            interval: _dates.length / 10,
            showTitles: true,
            getTitlesWidget: (value, meta) => _formatTime(_dates[value.toInt()])));

    final left = AxisTitles(sideTitles: _getLabelY());

    const right = AxisTitles();

    return Row(children: [
      Flexible(
          child: LineChart(LineChartData(
              minY: 0,
              maxY: _getMaxY(),
              lineBarsData: _getCurrentData(stats),
              gridData: FlGridData(horizontalInterval: _getInterval()),
              titlesData: FlTitlesData(
                bottomTitles: bottom,
                topTitles: top,
                leftTitles: left,
                rightTitles: right,
              )))),
      const SizedBox(width: 16),
      Column(mainAxisSize: MainAxisSize.min, children: _getLegend())
    ]);
  }

  List<LineChartBarData> _getCurrentData(List<Machine> stats) {
    switch (_currentType) {
      case CPU_GPU:
        return [
          _getBarData(stats, CPU_FIELD, Colors.green),
          _getBarData(stats, GPU_FIELD, Colors.blue)
        ];
      case MEMORY:
        return [
          _getBarData(stats, MEMORY_FREE_FIELD, Colors.green),
          _getBarData(stats, MEMORY_USED, Colors.orange),
          _getBarData(stats, MEMORY_TOTAL_FIELD, Colors.blue)
        ];
      case HDD:
        return [
          _getBarData(stats, HDD_FREE_FIELD, Colors.green),
          _getBarData(stats, HDD_USED, Colors.orange),
          _getBarData(stats, HDD_TOTAL_FIELD, Colors.blue)
        ];
      case BANDWIDTH:
        return [
          _getBarData(stats, BANDWIDTH_IN_FIELD, Colors.green),
          _getBarData(stats, BANDWIDTH_OUT_FIELD, Colors.blue)
        ];
      default:
        return [];
    }
  }

  LineChartBarData _getBarData(List<Machine> stats, String key, Color color) {
    return LineChartBarData(
        spots: List<FlSpot>.generate(
            stats.length, (index) => FlSpot(index.toDouble(), _data[key]![index].toDouble())),
        color: color,
        dotData: const FlDotData(show: false));
  }

  List<Widget> _getLegend() {
    switch (_currentType) {
      case CPU_GPU:
        return [
          LegendTile(context.l10n.cpu, Colors.green),
          LegendTile(context.l10n.cpu, Colors.blue)
        ];
      case MEMORY:
        return [
          LegendTile(context.l10n.free, Colors.green),
          LegendTile(context.l10n.used, Colors.orange),
          LegendTile(context.l10n.total, Colors.blue)
        ];
      case HDD:
        return [
          LegendTile(context.l10n.free, Colors.green),
          LegendTile(context.l10n.used, Colors.orange),
          LegendTile(context.l10n.total, Colors.blue)
        ];
      case BANDWIDTH:
        return [
          LegendTile(context.l10n.inside, Colors.green),
          LegendTile(context.l10n.out, Colors.blue)
        ];
      default:
        return const [];
    }
  }

  SideTitles _getLabelY() {
    switch (_currentType) {
      case CPU_GPU:
        return SideTitles(
            reservedSize: reservedSize,
            getTitlesWidget: (value, meta) => _makeTextWidget('${fixedDouble(value)}%'),
            showTitles: true,
            interval: _getInterval());
      case MEMORY:
        return SideTitles(
            reservedSize: reservedSize,
            getTitlesWidget: (value, meta) =>
                _makeTextWidget('${fixedDouble(value / (1024 * 1024 * 1024))}'),
            showTitles: true,
            interval: _getInterval());
      case HDD:
        return SideTitles(
            reservedSize: reservedSize,
            getTitlesWidget: (value, meta) =>
                _makeTextWidget('${fixedDouble(value / (1024 * 1024 * 1024))}'),
            showTitles: true,
            interval: _getInterval());
      case BANDWIDTH:
        return SideTitles(
            reservedSize: reservedSize,
            getTitlesWidget: (value, meta) =>
                _makeTextWidget('${fixedDouble(value / (1024 * 1024))}'),
            showTitles: true,
            interval: _getInterval());
      default:
        return SideTitles(
            reservedSize: reservedSize,
            getTitlesWidget: (value, meta) => _makeTextWidget('${fixedDouble(value)}%'),
            showTitles: true,
            interval: _getInterval());
    }
  }

  String? _getUnit() {
    switch (_currentType) {
      case CPU_GPU:
        return '%';
      case MEMORY:
        return 'Gb';
      case HDD:
        return 'Gb';
      case BANDWIDTH:
        return 'Mb/s';
      default:
        return null;
    }
  }

  double? _getInterval() {
    switch (_currentType) {
      case CPU_GPU:
        return 10.0;
      case MEMORY:
        return _maxMemory / 10;
      case HDD:
        return _maxHDD / 10;
      case BANDWIDTH:
        return _maxBandwidth / 10;
      default:
        return null;
    }
  }

  double? _getMaxY() {
    switch (_currentType) {
      case CPU_GPU:
        return 100.0;
      case MEMORY:
        return _maxMemory * 1.05;
      case HDD:
        return _maxHDD * 1.05;
      case BANDWIDTH:
        return _maxBandwidth * 1.05;
      default:
        return null;
    }
  }

  Widget _formatTime(DateTime time) {
    final timeHours = DateFormat('HH:mm').format(time);
    final daysDate = DateFormat('dd.MM.yyyy').format(time);
    return _makeTextWidget('${timeHours.toString()}\n${daysDate.toString()}');
  }

  Widget _makeTextWidget(String text) {
    return Text(text, textAlign: TextAlign.center, style: const TextStyle(fontSize: 12));
  }
}

class _NoServerData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: NonAvailableBuffer(icon: Icons.data_usage, message: context.l10n.noMachineDataYet));
  }
}
