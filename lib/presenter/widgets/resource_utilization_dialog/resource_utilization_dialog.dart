// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/loader.dart';
import 'package:provider/provider.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/resource_utilization_bloc/resource_utilization_bloc.dart';
import 'package:wsfastocloud/presenter/resource_utilization_bloc/resource_utilization_state.dart';

import 'server_analytics.dart';

class ResourceUtilizationHistoryDialog extends StatefulWidget {
  const ResourceUtilizationHistoryDialog({super.key});

  @override
  State<ResourceUtilizationHistoryDialog> createState() => _ResourceUtilizationHistoryDialogState();
}

class _ResourceUtilizationHistoryDialogState extends State<ResourceUtilizationHistoryDialog> {
  late final ResourceUtilizationBloc _blocLoader;

  @override
  void initState() {
    _blocLoader = context.read<ResourceUtilizationBloc>();
    _blocLoader.load();
    super.initState();
  }

  @override
  void dispose() {
    _blocLoader.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 1200,
        height: 670,
        child: LoaderWidget<ResourceUtilizationLoadedData>(
            loader: _blocLoader,
            builder: (context, state) {
              return Column(children: [
                Expanded(child: ServerAnalytics(data: state.data)),
                Container(
                    alignment: Alignment.bottomRight,
                    padding: const EdgeInsets.all(16),
                    child: ElevatedButton(
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text(context.l10n.close)))
              ]);
            }));
  }
}
