import 'dart:async';
import 'dart:convert';

import 'package:dart_common/dart_common.dart';

import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:http/http.dart' as http;
import 'package:wsfastocloud/data/services/api/models/master_config_info.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/master_connect_dialog.dart';

class MasterFetcher {
  final String _backendServerUrl;
  final Credentials creds;

  MasterFetcher(this._backendServerUrl, this.creds);

  Future<MasterConfigInfo> getConfig() async {
    final resp = await get('/server/config');
    final respData = httpDataResponseFromString(resp.body);
    if (respData != null) {
      final err = respData.error();
      if (err != null) {
        throw 'Response error ${err.message}';
      }
      final content = respData.contentMap()!;
      return MasterConfigInfo.fromJson(content);
    }

    throw 'Wrong response code ${resp.statusCode}';
  }

  Future<String> getHardwareHash(NodeAlgoInfo algo) async {
    final resp = await post('/server/hardware_hash', algo.toJson());
    final respData = httpDataResponseFromString(resp.body);
    if (respData != null) {
      final err = respData.error();
      if (err != null) {
        throw 'Response error ${err.message}';
      }
      final content = respData.contentMap()!;
      return content['key'];
    }

    throw 'Wrong response code ${resp.statusCode}';
  }

  Future<String> getLicenseHardwareHash(AlgoType algo) async {
    final resp = await get('/server/hardware_hash/${algo.toInt() - 48}'); // #FIXME: -48
    final respData = httpDataResponseFromString(resp.body);
    if (respData != null) {
      final err = respData.error();
      if (err != null) {
        throw 'Response error ${err.message}';
      }
      final content = respData.contentMap()!;
      return content['key'];
    }

    throw 'Wrong response code ${resp.statusCode}';
  }

  Future<MasterConfigInfo> setConfig(MasterConfigInfo data) async {
    final resp = await post('/server/config', data.toJson());
    final respData = httpDataResponseFromString(resp.body);
    if (respData != null) {
      final err = respData.error();
      if (err != null) {
        throw 'Response error ${err.message}';
      }
      final content = respData.contentMap()!;
      return MasterConfigInfo.fromJson(content);
    }

    throw 'Wrong response code ${resp.statusCode}';
  }

  Future<http.Response> get(String endpoint) {
    final url = _generateBackEndEndpoint(endpoint);

    return http.get(url, headers: _getJsonHeaders());
  }

  Future<http.Response> post(String endpoint, Map<String, dynamic> data) {
    final url = _generateBackEndEndpoint(endpoint);

    final body = json.encode(data);
    return http.post(url, headers: _getJsonHeaders(), body: body);
  }

  Uri _generateBackEndEndpoint(String path) {
    return Uri.parse('$_backendServerUrl$path');
  }

  String get _authorization => base64Encode(utf8.encode('${creds.login}:${creds.password}'));

  Map<String, String> _getJsonHeaders() {
    return {
      'content-type': 'application/json',
      'accept': 'application/json',
      'authorization': 'Basic $_authorization'
    };
  }
}
