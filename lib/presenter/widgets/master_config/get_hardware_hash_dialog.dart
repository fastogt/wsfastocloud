import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/l10n/l10n.dart';

class GetLicenseHardwareHashDialog extends StatefulWidget {
  const GetLicenseHardwareHashDialog();

  @override
  _GetLicenseHardwareHashDialogState createState() {
    return _GetLicenseHardwareHashDialogState();
  }
}

class _GetLicenseHardwareHashDialogState extends State<GetLicenseHardwareHashDialog> {
  AlgoType algo = AlgoType.HDD;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text(context.l10n.getHardwareHash),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          DropdownButtonEx<AlgoType>(
              hint: algo.toHumanReadable(),
              value: algo,
              values: AlgoType.values,
              onChanged: (m) {
                algo = m;
              },
              itemBuilder: (AlgoType type) {
                return DropdownMenuItem(child: Text(type.toHumanReadable()), value: type);
              })
        ])),
        actions: <Widget>[
          FlatButtonEx.notFilled(text: context.l10n.cancel, onPressed: Navigator.of(context).pop),
          FlatButtonEx.filled(
              text: context.l10n.get,
              onPressed: () {
                Navigator.of(context).pop(algo);
              })
        ]);
  }
}

class GetHardwareHashDialog extends StatelessWidget {
  GetHardwareHashDialog(this.host) : _algo = NodeAlgoInfo(host, AlgoType.HDD);

  final String host;

  final NodeAlgoInfo _algo;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text(context.l10n.getHardwareHash),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          DropdownButtonEx<AlgoType>(
              hint: _algo.algo.toHumanReadable(),
              value: _algo.algo,
              values: AlgoType.values,
              onChanged: (m) {
                _algo.algo = m;
              },
              itemBuilder: (AlgoType type) {
                return DropdownMenuItem(child: Text(type.toHumanReadable()), value: type);
              })
        ])),
        actions: <Widget>[
          FlatButtonEx.notFilled(text: context.l10n.cancel, onPressed: Navigator.of(context).pop),
          FlatButtonEx.filled(
              text: context.l10n.get,
              onPressed: () {
                final copy = _algo.copy();
                Navigator.of(context).pop(copy);
              })
        ]);
  }
}
