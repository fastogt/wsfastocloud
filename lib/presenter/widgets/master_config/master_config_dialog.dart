import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/services/api/models/master_config_info.dart';
import 'package:wsfastocloud/data/services/api/models/types.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/widgets/add_edit_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/get_hardware_hash_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/master_config_widgets.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';

class MasterConfigDialog extends StatefulWidget {
  final MasterConfigInfo init;
  final List<StreamType> availableTypes;
  final void Function(AlgoType algo) onLicenseHashRequested;
  final void Function(NodeAlgoInfo algo) onNodeHashRequested;

  MasterConfigDialog.edit(this.availableTypes, MasterConfigInfo value, this.onLicenseHashRequested,
      this.onNodeHashRequested)
      : init = value.copy();

  @override
  State<MasterConfigDialog> createState() {
    return _MasterConfigDialogState();
  }
}

class _MasterConfigDialogState extends State<MasterConfigDialog> {
  MasterConfigInfo get config {
    return widget.init;
  }

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.wide(
        children: <Widget>[
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                _header('Alias'),
                _aliasField(),
                _header('License key'),
                _licenseKeyField(),
                _header('HLS host'),
                _hlsHostAndAliasField(),
                _header('VODs host'),
                _vodsHostAndAliasField(),
                _header('CODs host'),
                _codsHostAndAliasField(),
                Row(children: [Expanded(child: _nodeField())]),
                Row(children: [Expanded(child: _webRTCField())]),
                Row(children: [
                  Expanded(child: _corsField()),
                  Expanded(child: _maxFileSizeField())
                ]),
                _authField(),
                _mailSettingsField(),
                _header('Auth content'),
                _authContent(),
                _vodInfoSettingsField(),
                _blackListField(),
                _scanFolderField(),
                _httpsField()
              ])
        ],
        onSave: () {
          Navigator.pop(context, config);
        });
  }

  Widget _header(String text) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Text(text, style: const TextStyle(fontWeight: FontWeight.bold)));
  }

  //fields

  Widget _blackListField() {
    Widget _blackListDialog() {
      final TextEditingController _textController = TextEditingController();
      final content = Column(mainAxisSize: MainAxisSize.min, children: [
        TextFieldEx(
            validator: (String text) {
              return HostAndPort.isValidHost(text) ? null : 'Invalid IP address';
            },
            decoration:
                const InputDecoration(border: OutlineInputBorder(), labelText: 'Input IP Address'),
            controller: _textController)
      ]);
      return AlertDialog(title: const Text('Add to Black list'), content: content, actions: [
        FlatButtonEx.filled(
            text: 'Close',
            onPressed: () {
              Navigator.of(context).pop();
            }),
        FlatButtonEx.filled(
            text: 'Add',
            onPressed: () {
              if (HostAndPort.isValidHost(_textController.text)) {
                setState(() {
                  config.blacklist.add(IpAddress(ip: _textController.text));
                  Navigator.pop(context, config);
                });
              }
            })
      ]);
    }

    List<Widget> _blackList() {
      final List<Widget> blackList = [];
      for (final ipAddress in config.blacklist) {
        blackList.add(Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Expanded(child: ROIpAddressField(value: ipAddress)),
          IconButton(
              onPressed: () {
                setState(() {
                  config.blacklist.remove(ipAddress);
                });
              },
              icon: const Icon(Icons.delete))
        ]));
      }
      return blackList;
    }

    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        _header('Black list'),
        IconButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return _blackListDialog();
                  });
            },
            icon: const Icon(Icons.add))
      ]),
      Column(children: _blackList())
    ]);
  }

  Widget _scanFolderField() {
    Widget _dialog() {
      final TextEditingController _textController = TextEditingController();
      final types = widget.availableTypes;
      StreamType _type = types[0];
      final content = Column(mainAxisSize: MainAxisSize.min, children: [
        Row(children: [
          Expanded(
              child: DropdownButtonEx<StreamType>(
                  hint: _type.toHumanReadableWS(),
                  value: _type,
                  values: types,
                  onChanged: (c) {
                    _type = c;
                  },
                  itemBuilder: (StreamType value) {
                    return DropdownMenuItem(child: Text(value.toHumanReadableWS()), value: value);
                  })),
          Expanded(
              flex: 3,
              child: TextFieldEx(
                  validator: (String text) {
                    return text.isNotEmpty
                        ? null
                        : 'Invalid folder path'; // #FIXME path folder check
                  },
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Input Folder for scanning'),
                  controller: _textController))
        ])
      ]);
      return AlertDialog(title: const Text('Add to scan folder list'), content: content, actions: [
        FlatButtonEx.filled(
            text: 'Close',
            onPressed: () {
              Navigator.of(context).pop();
            }),
        FlatButtonEx.filled(
            text: 'Add',
            onPressed: () {
              setState(() {
                config.scanFolders.add(ScanFolder(mediaType: _type, path: _textController.text));
                Navigator.pop(context, config);
              });
            })
      ]);
    }

    List<Widget> _scanList() {
      final List<Widget> scanList = [];
      for (final scan in config.scanFolders) {
        scanList.add(Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Expanded(child: ROScanFolderField(value: scan)),
          IconButton(
              onPressed: () {
                setState(() {
                  config.scanFolders.remove(scan);
                });
              },
              icon: const Icon(Icons.delete))
        ]));
      }
      return scanList;
    }

    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        _header('Scan folders'),
        IconButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return _dialog();
                  });
            },
            icon: const Icon(Icons.add))
      ]),
      Column(children: _scanList())
    ]);
  }

  Widget _httpsField() {
    return OptionalFieldTile(
        title: 'Https',
        init: config.https != null,
        onChanged: (value) {
          config.https = value ? Https.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: HttpsField(value: config.https!));
        });
  }

  Widget _aliasField() {
    return TextFieldEx(
        hintText: 'Alias',
        errorText: 'Input alias',
        init: config.alias,
        onFieldChanged: (val) {
          config.alias = val;
        });
  }

  Widget _licenseKeyField() {
    return Row(children: [
      Expanded(
          child: LicenseKeyField(
              init: config.licenseKey,
              onFieldChanged: (term, isValid) {
                config.licenseKey = term;
              })),
      StatsButton(
          title: 'Get hash',
          onPressed: () {
            final hashFor = showDialog<AlgoType?>(
                context: context,
                builder: (context) {
                  return const GetLicenseHardwareHashDialog();
                });
            hashFor.then((AlgoType? algo) {
              if (algo == null) {
                return;
              }

              widget.onLicenseHashRequested(algo);
            });
          })
    ]);
  }

  Widget _hlsHostAndAliasField() {
    return TextFieldEx(
        hintText: 'Host http(s) url',
        errorText: 'Input http(s) host url',
        init: config.hlsHost,
        onFieldChanged: (val) {
          config.hlsHost = val;
        });
  }

  Widget _codsHostAndAliasField() {
    return TextFieldEx(
        hintText: 'Host http(s) url',
        errorText: 'Input http(s) host url',
        init: config.codsHost,
        onFieldChanged: (val) {
          config.codsHost = val;
        });
  }

  Widget _vodsHostAndAliasField() {
    return TextFieldEx(
        hintText: 'Host http(s) url',
        errorText: 'Input http(s) host url',
        init: config.vodsHost,
        onFieldChanged: (val) {
          config.vodsHost = val;
        });
  }

  Widget _nodeField() {
    return OptionalFieldTile(
        title: 'Node',
        init: config.node != null,
        onChanged: (value) {
          config.node = value ? Node.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child:
                  NodeField(value: config.node!, onNodeHashRequested: widget.onNodeHashRequested));
        });
  }

  Widget _maxFileSizeField() {
    return NumberTextField.integer(
        canBeEmpty: false,
        hintText: 'Max file upload size MB',
        initInt: config.maxUploadFileSize,
        onFieldChangedInt: (val) {
          if (val != null) {
            config.maxUploadFileSize = val;
          }
        });
  }

  Widget _webRTCField() {
    return OptionalFieldTile(
        title: 'WebRTC',
        init: config.webrtc != null,
        onChanged: (value) {
          config.webrtc = value ? Webrtc.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: WebRTCField(value: config.webrtc!));
        });
  }

  Widget _authField() {
    return OptionalFieldTile(
        title: 'Auth',
        init: config.auth != null,
        onChanged: (value) {
          config.auth = value ? Auth.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: AuthField(value: config.auth!));
        });
  }

  Widget _mailSettingsField() {
    return OptionalFieldTile(
        title: 'Alarm settings',
        init: config.streamNotification != null,
        onChanged: (value) {
          if (value) {
            config.streamNotification = MailConfig.createDefault();
          } else {
            config.streamNotification = null;
          }
        },
        builder: () {
          final currentNotification = config.streamNotification!;
          return Column(children: <Widget>[
            DropdownButtonEx<NotificationType>(
                onChanged: (value) {
                  if (value == NotificationType.MAIL) {
                    setState(() {
                      config.streamNotification = MailConfig.createDefault();
                    });
                  }
                },
                value: currentNotification.type,
                values: const [NotificationType.MAIL],
                itemBuilder: (value) {
                  return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
                }),
            if (currentNotification is MailConfig)
              MailConfigField(currentNotification)
            else
              Container()
          ]);
        });
  }

  Widget _authContent() {
    return DropdownButtonEx<AuthContentType>(
        value: config.authContent,
        values: AuthContentType.values,
        onChanged: (c) {
          setState(() {
            config.authContent = c;
          });
        },
        itemBuilder: (AuthContentType data) {
          return DropdownMenuItem(child: Text(data.toHumanReadable()), value: data);
        });
  }

  Widget _vodInfoSettingsField() {
    return OptionalFieldTile(
        title: 'Vod Info Settings',
        init: config.vodInfo != null,
        onChanged: (value) {
          config.vodInfo = value ? VodInfoSettings.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: VodInfoSettingsField(vodInfo: config.vodInfo!));
        });
  }

  Widget _corsField() {
    return Row(children: [
      Expanded(
          child: CheckboxListTile(
              title: const Text('Cors'),
              value: config.cors,
              onChanged: (val) {
                setState(() {
                  config.cors = val!;
                });
              }))
    ]);
  }
}
