import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/data/services/api/models/types.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/get_hardware_hash_dialog.dart';

class LicenseKeyField extends StatefulWidget {
  static const KEY_LENGTH = ActivationKey.KEY_LENGTH;

  final String? init;
  final void Function(String term, bool isValid)? onFieldChanged;

  const LicenseKeyField({this.init, this.onFieldChanged});

  @override
  _LicenseKeyFieldState createState() {
    return _LicenseKeyFieldState();
  }
}

class _LicenseKeyFieldState extends State<LicenseKeyField> {
  late final TextEditingController _controller = TextEditingController(text: widget.init);

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextFieldEx(
        controller: _controller,
        formatters: <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp("[a-f0-9]"))],
        init: widget.init,
        errorText: 'Enter key',
        minSymbols: LicenseKeyField.KEY_LENGTH,
        maxSymbols: LicenseKeyField.KEY_LENGTH,
        hintText: 'License key',
        onFieldChanged: (String term) {
          widget.onFieldChanged?.call(term, term.length == LicenseKeyField.KEY_LENGTH);
        });
  }
}

class WebRTCField extends StatelessWidget {
  final Webrtc value;

  const WebRTCField({Key? key, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[Expanded(child: _stun()), Expanded(child: _turn())]);
  }

  Widget _stun() {
    return TextFieldEx(
        hintText: 'Stun',
        errorText: 'Input stun',
        init: value.stun,
        onFieldChanged: (val) {
          value.stun = val;
        });
  }

  Widget _turn() {
    return TextFieldEx(
        hintText: 'Turn',
        errorText: 'Input turn',
        init: value.turn,
        onFieldChanged: (val) {
          value.turn = val;
        });
  }
}

class ROIpAddressField extends StatelessWidget {
  final IpAddress value;

  const ROIpAddressField({Key? key, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldEx.readOnly(hint: 'IP', init: value.ip);
  }
}

class ROScanFolderField extends StatelessWidget {
  final ScanFolder value;

  const ROScanFolderField({Key? key, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [Expanded(child: _type()), Expanded(flex: 3, child: _path())]);
  }

  Widget _type() {
    return TextFieldEx.readOnly(
        hint: value.mediaType.toHumanReadableWS(), init: value.mediaType.toHumanReadableWS());
  }

  Widget _path() {
    return TextFieldEx.readOnly(hint: 'Path', init: value.path);
  }
}

class AuthField extends StatelessWidget {
  final Auth value;

  const AuthField({super.key, required this.value});

  @override
  Widget build(BuildContext context) {
    return Row(children: [Expanded(child: _login()), Expanded(child: _password())]);
  }

  Widget _login() {
    return TextFieldEx(
        hintText: 'Login',
        errorText: 'Input Login',
        init: value.login,
        onFieldChanged: (val) {
          value.login = val;
        });
  }

  Widget _password() {
    return PassWordTextField(
        hintText: 'Password',
        errorText: 'Input Password',
        init: value.password,
        onFieldChanged: (val) {
          value.password = val;
        });
  }
}

class MailConfigField extends StatelessWidget {
  final MailConfig mailConfig;

  const MailConfigField(this.mailConfig);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Row(children: <Widget>[_mailServerField(), _mailPortField()]),
      Row(children: <Widget>[_userField(), _userPasswordField()])
    ]);
  }

  Widget _mailServerField() {
    return Expanded(
        child: TextFieldEx(
            hintText: 'Mail server',
            errorText: 'Input mail server',
            init: mailConfig.mailServer,
            onFieldChanged: (val) {
              mailConfig.mailServer = val;
            }));
  }

  Widget _mailPortField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Mail port',
            initInt: mailConfig.mailPort,
            onFieldChangedInt: (val) {
              if (val != null) {
                mailConfig.mailPort = val;
              }
            }));
  }

  Widget _userField() {
    return Expanded(
        child: TextFieldEx(
            hintText: 'User',
            errorText: 'Input user',
            init: mailConfig.user,
            onFieldChanged: (val) {
              mailConfig.user = val;
            }));
  }

  Widget _userPasswordField() {
    return Expanded(
        child: PassWordTextField(
            hintText: 'User password',
            errorText: 'Input user password',
            init: mailConfig.mailPassword,
            onFieldChanged: (val) {
              mailConfig.mailPassword = val;
            }));
  }
}

class VodInfoSettingsField extends StatelessWidget {
  final VodInfoSettings vodInfo;

  const VodInfoSettingsField({required this.vodInfo, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [Expanded(child: _type()), Expanded(child: _key())],
    );
  }

  Widget _type() {
    return DropdownButtonEx<TypeVodService>(
        hint: 'Vod Settings Type',
        value: vodInfo.service,
        values: TypeVodService.values,
        onChanged: (c) {
          vodInfo.service = c;
        },
        itemBuilder: (TypeVodService value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  Widget _key() {
    return TextFieldEx(
        hintText: 'Key',
        errorText: 'Input Key',
        init: vodInfo.tmdb.key,
        onFieldChanged: (val) {
          vodInfo.tmdb.key = val;
        });
  }
}

class NodeField extends StatelessWidget {
  final Node value;
  final void Function(NodeAlgoInfo algo) onNodeHashRequested;

  const NodeField({Key? key, required this.value, required this.onNodeHashRequested})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Row(children: [
        Expanded(
            child: TextFieldEx(
                hintText: 'Host http(s) url',
                errorText: 'Input http(s) host url',
                init: value.host,
                onFieldChanged: (val) {
                  value.host = val;
                })),
        StatsButton(
            title: 'Get hash',
            onPressed: () async {
              final hashFor = await showDialog<NodeAlgoInfo?>(
                  context: context,
                  builder: (context) {
                    return GetHardwareHashDialog(value.host);
                  });
              if (hashFor == null) {
                return;
              }

              onNodeHashRequested.call(hashFor);
            })
      ]),
      _key()
    ]);
  }

  Widget _key() {
    return LicenseKeyField(
        init: value.key,
        onFieldChanged: (term, isValid) {
          value.key = term;
        });
  }
}

class HttpsField extends StatelessWidget {
  final Https value;

  const HttpsField({Key? key, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[_cert(), _key()]);
  }

  Widget _cert() {
    return TextFieldEx(
        hintText: 'Certificate',
        errorText: 'Input certificate',
        init: value.cert,
        onFieldChanged: (val) {
          value.cert = val;
        });
  }

  Widget _key() {
    return TextFieldEx(
        hintText: 'Private key',
        errorText: 'Input private key',
        init: value.key,
        onFieldChanged: (val) {
          value.key = val;
        });
  }
}
