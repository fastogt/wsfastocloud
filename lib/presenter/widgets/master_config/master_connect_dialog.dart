import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/l10n/l10n.dart';

class Credentials {
  String login;
  String password;

  Credentials(this.login, this.password);
}

class MasterConnectDialog extends StatefulWidget {
  const MasterConnectDialog({Key? key}) : super(key: key);

  @override
  State createState() {
    return _ConnectDialogState();
  }
}

class _ConnectDialogState extends State<MasterConnectDialog> {
  final _loginController = TextEditingController(text: 'master');
  final _passwordController = TextEditingController(text: 'master');

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text(context.l10n.connect),
        content: SingleChildScrollView(
            child: Column(mainAxisSize: MainAxisSize.min, children: [_loginAndPassword()])),
        actions: [
          FlatButtonEx.filled(text: context.l10n.connect, onPressed: () => _save(context))
        ]);
  }

  Widget _loginAndPassword() {
    return Column(children: [
      TextFieldEx(hintText: context.l10n.login, controller: _loginController),
      PassWordTextField(
          hintText: context.l10n.password,
          controller: _passwordController,
          iconColor: Theme.of(context).colorScheme.primary)
    ]);
  }

  void _save(BuildContext context) {
    Navigator.pop(context, Credentials(_loginController.text, _passwordController.text));
  }
}
