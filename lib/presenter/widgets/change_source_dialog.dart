import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/l10n/l10n.dart';

class ChangeSourceDialog extends StatefulWidget {
  final List<InputUrl> inputs;

  const ChangeSourceDialog(this.inputs);

  @override
  _ChangeSourceDialogState createState() {
    return _ChangeSourceDialogState();
  }
}

class _ChangeSourceDialogState extends State<ChangeSourceDialog> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text(context.l10n.changeSource),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButtonEx<int>(
                    hint: context.l10n.changeSource,
                    value: _index,
                    values: List<int>.generate(widget.inputs.length, (index) => index),
                    onChanged: (t) {
                      _index = t;
                    },
                    itemBuilder: (index) {
                      final InputUrl input = widget.inputs[index];
                      return DropdownMenuItem(child: Text(input.uri), value: index);
                    }))),
        actions: <Widget>[
          FlatButtonEx.notFilled(text: context.l10n.cancel, onPressed: _cancel),
          FlatButtonEx.filled(text: context.l10n.change, onPressed: _save)
        ]);
  }

  void _cancel() {
    Navigator.of(context).pop();
  }

  void _save() {
    Navigator.of(context).pop(_index);
  }
}
