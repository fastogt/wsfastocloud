import 'package:flutter/material.dart';

class OptionalFieldTile extends StatefulWidget {
  final bool init;
  final String title;
  final void Function(bool)? onChanged;
  final Widget Function() builder;

  const OptionalFieldTile(
      {required this.title, required this.init, this.onChanged, required this.builder});

  @override
  _OptionalFieldTileState createState() {
    return _OptionalFieldTileState();
  }
}

class _OptionalFieldTileState extends State<OptionalFieldTile> {
  late bool _value;

  @override
  void initState() {
    super.initState();
    _value = widget.init;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          if (_value) const Divider(),
          CheckboxListTile(
              title: Text(widget.title),
              value: _value,
              onChanged: (value) {
                widget.onChanged?.call(value!);
                setState(() {
                  _value = value!;
                });
              }),
          if (_value) widget.builder(),
        ]);
  }
}
