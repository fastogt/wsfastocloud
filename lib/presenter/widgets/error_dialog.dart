import 'package:flutter/material.dart';
import 'package:wsfastocloud/l10n/l10n.dart';

class ErrorDialog extends StatelessWidget {
  final String error;

  const ErrorDialog({Key? key, required this.error}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text(context.l10n.error),
        contentPadding: const EdgeInsets.all(8),
        content: Text(error));
  }
}
