import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/presenter/widgets/urls/base.dart';

class ScanFolderDialog extends StatefulWidget {
  final String path;
  final bool visiblePath;

  const ScanFolderDialog({this.path = '/home/fastocloud/streamer', this.visiblePath = true});

  @override
  _ScanFolderDialogState createState() {
    return _ScanFolderDialogState();
  }
}

class _ScanFolderDialogState extends State<ScanFolderDialog> {
  static const EXTENSIONS = ['ts', 'm3u8', 'mpd', 'mp4', 'mkv', 'mov', 'mp3'];
  late ScanFolderExt _scan;
  Future<http.Response>? _future;

  @override
  void initState() {
    super.initState();
    _scan = ScanFolderExt(directory: widget.path, extensions: [EXTENSIONS[0]]);
  }

  @override
  Widget build(BuildContext context) {
    return _future == null ? _folderFields() : _filePicker();
  }

  Widget _folderFields() {
    return AlertDialog(
        title: const Text('Scan folder'),
        contentPadding: const EdgeInsets.all(8),
        content: SizedBox(
            width: 300,
            height: 400,
            child: SingleChildScrollView(
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[_directoryField(), _extensionsField()]))),
        actions: <Widget>[
          FlatButtonEx.notFilled(text: 'Cancel', onPressed: () => _cancel()),
          FlatButtonEx.filled(text: 'Scan', onPressed: () => _onScanFolderPressed())
        ]);
  }

  Widget _directoryField() {
    if (!widget.visiblePath) {
      return const SizedBox();
    }

    return TextFieldEx(
        hintText: 'Path',
        errorText: 'Enter path',
        init: _scan.directory,
        onFieldChanged: (term) {
          _scan.directory = term;
        });
  }

  Widget _extensionsField() {
    final extensions = List.generate(
      EXTENSIONS.length,
      (index) => _extensionChip(EXTENSIONS[index]),
    );

    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Wrap(children: extensions, spacing: 8, runSpacing: 8));
  }

  Widget _extensionChip(String extension) {
    return FilterChip(
        label: Text(extension),
        selected: _scan.extensions.contains(extension),
        onSelected: (value) {
          if (value) {
            setState(() => _scan.extensions.add(extension));
          } else {
            setState(() => _scan.extensions.remove(extension));
          }
        });
  }

  Widget _filePicker() {
    return AlertDialog(
        title: const Text('Scan folder'),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(child: _filesList()),
        actions: <Widget>[_changeFolderButton()]);
  }

  Widget _filesList() {
    return FutureBuilder<http.Response>(
        builder: (context, snap) {
          final resp = snap.data;
          if (resp == null) {
            return const Center(child: CircularProgressIndicator());
          }

          if (resp.statusCode != 200) {
            return const Text('No files found');
          }

          final data = json.decode(resp.body);
          final files = data['data']['files'] as List<dynamic>;
          if (files.isEmpty) {
            return const Text('No files found');
          }

          final _tiles = List<Widget>.generate(files.length, (index) {
            final file = files[index];
            return ListTile(
                title: Text(file),
                onTap: () {
                  Navigator.of(context).pop(file);
                });
          });
          return Column(mainAxisSize: MainAxisSize.min, children: _tiles);
        },
        future: _future);
  }

  Widget _changeFolderButton() {
    return FlatButtonEx.notFilled(
        text: 'Change folder',
        onPressed: () {
          setState(() {
            _future = null;
          });
        });
  }

  void _onScanFolderPressed() {
    if (!_scan.isValid()) {
      return;
    }

    setState(() {
      final data = _scan.toJson();
      _future = context.read<Fetcher>().post('/media/folder/scan', data);
    });
  }

  void _cancel() {
    return Navigator.of(context).pop();
  }
}
