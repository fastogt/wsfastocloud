import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/widgets/urls/output.dart';

class TokenDialog extends StatefulWidget {
  final String _ip;
  final String _uri;
  final GetTokenCallback? _onGetToken;
  const TokenDialog(
      {required String ip, required String uri, required GetTokenCallback? onGetToken, super.key})
      : _ip = ip,
        _uri = uri,
        _onGetToken = onGetToken;

  @override
  State<TokenDialog> createState() => _TokenDialogState();
}

class _TokenDialogState extends State<TokenDialog> {
  final ValueNotifier<String?> tokenResp = ValueNotifier<String?>('');
  final TextEditingController _textController = TextEditingController();
  String get ip => widget._ip;
  String get uri => widget._uri;

  @override
  void initState() {
    _textController.text = ip;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: const Text('Add IP address'),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              TextFieldEx(
                controller: _textController,
                validator: (String text) {
                  return HostAndPort.isValidHost(text) ? null : 'Invalid IP address';
                },
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Input IP Address',
                ),
              ),
              const SizedBox(height: 5.0),
              ValueListenableBuilder<String?>(
                valueListenable: tokenResp,
                builder: (context, token, child) {
                  if (tokenResp.value != null && tokenResp.value!.isNotEmpty)
                    return Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                            child: Text('$uri?token=$token'),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: token != null
                              ? () {
                                  Clipboard.setData(ClipboardData(
                                    text: '$uri?token=$token',
                                  ));
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(content: Text('Copied')),
                                  );
                                }
                              : null,
                          child: const Text('Copy uri'),
                        ),
                      ],
                    );
                  return const SizedBox.shrink();
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('Close'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      final host = _textController.text;
                      if (HostAndPort.isValidHost(host)) {
                        final resp = widget._onGetToken?.call(host);
                        resp?.then((token) {
                          tokenResp.value = token;
                        });
                      }
                    },
                    child: const Text('Get token'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
