import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';

class ImportDialog extends StatelessWidget {
  final WSServer value;

  final String urlHint;
  final String urlError;

  const ImportDialog({required this.value, this.urlHint = 'URL', this.urlError = 'Input URL'});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text(context.l10n.import),
        content: SingleChildScrollView(
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(children: [Expanded(flex: 2, child: _url())]),
          Row(children: [Expanded(flex: 2, child: _useLoginAndPasswordField(context))])
        ])),
        actions: [FlatButtonEx.filled(text: context.l10n.import, onPressed: () => _save(context))]);
  }

  Widget _url() {
    return TextFieldEx(
        validator: (String text) {
          return WSServer(url: value.url).isValid() ? null : urlError;
        },
        hintText: urlHint,
        errorText: urlError,
        init: value.url,
        onFieldChanged: (val) {
          value.url = val;
        });
  }

  Widget _useLoginAndPasswordField(BuildContext context) {
    return OptionalFieldTile(
        title: context.l10n.useLoginAndPassword,
        init: value.needAuth(),
        onChanged: (state) {
          if (state) {
            value.password = WSServer.DEFAULT_PASSWORD;
            value.login = WSServer.DEFAULT_LOGIN;
          } else {
            value.login = null;
            value.password = null;
          }
        },
        builder: () {
          return Column(
              mainAxisSize: MainAxisSize.min,
              children: [_loginField(context), _passwordField(context)]);
        });
  }

  Widget _loginField(BuildContext context) {
    return TextFieldEx(
        hintText: context.l10n.login,
        errorText: context.l10n.enterLogin,
        init: value.login,
        keyboardType: TextInputType.text,
        onFieldChanged: (val) => value.login = val);
  }

  Widget _passwordField(BuildContext context) {
    return PassWordTextField(
        hintText: context.l10n.password,
        errorText: context.l10n.enterPassword,
        init: value.password,
        onFieldChanged: (val) => value.password = val);
  }

  void _save(BuildContext context) {
    Navigator.pop(context, value);
  }
}
