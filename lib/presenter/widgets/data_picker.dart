import 'package:flutter/material.dart';

class DatePicker extends StatefulWidget {
  final String title;
  final int initMs;
  final void Function(int) onChanged;

  const DatePicker(this.title, this.initMs, this.onChanged);

  @override
  _DatePickerState createState() {
    return _DatePickerState();
  }
}

class _DatePickerState extends State<DatePicker> {
  int _expDate = 0;

  @override
  void initState() {
    super.initState();
    _expDate = widget.initMs;
  }

  @override
  void didUpdateWidget(DatePicker oldWidget) {
    super.didUpdateWidget(oldWidget);
    _expDate = widget.initMs;
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text(widget.title),
        subtitle: Text(DateTime.fromMillisecondsSinceEpoch(_expDate).toString()),
        onTap: () {
          final future = showDatePicker(
              context: context,
              initialDate: DateTime.fromMillisecondsSinceEpoch(widget.initMs),
              firstDate: DateTime.fromMillisecondsSinceEpoch(0),
              lastDate: DateTime.now());
          future.then((date) {
            if (date != null) {
              setState(() {
                _expDate = date.millisecondsSinceEpoch;
              });
              widget.onChanged(date.millisecondsSinceEpoch);
            }
          });
        });
  }
}
