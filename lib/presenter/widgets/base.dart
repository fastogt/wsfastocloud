import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/widgets/color_picker.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';

class HostAndPortTextField extends StatelessWidget {
  final HostAndPort value;

  final String hostHint;
  final String hostError;
  final String portHint;
  final String portError;
  final int ratio;

  const HostAndPortTextField(
      {required this.value,
      this.hostHint = 'Host',
      this.hostError = 'Input host',
      this.portHint = 'Port',
      this.portError = 'Input port',
      this.ratio = 2});

  @override
  Widget build(BuildContext context) {
    return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[Expanded(flex: ratio, child: _host()), Expanded(child: _port(context))]);
  }

  Widget _host() {
    return TextFieldEx(
        validator: (String text) {
          return HostAndPort.isValidHost(text) ? null : hostError;
        },
        hintText: hostHint,
        errorText: hostError,
        init: value.host,
        onFieldChanged: (val) {
          value.host = val;
        });
  }

  Widget _port(BuildContext context) {
    return NumberTextField.integer(
        canBeEmpty: false,
        hintText: context.l10n.port,
        initInt: value.port,
        onFieldChangedInt: (val) {
          if (val != null) {
            value.port = val;
          }
        });
  }
}

class SizeField extends StatelessWidget {
  final Size value;

  const SizeField(this.value);

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_widthField(context), const Text('x'), _heightField(context)]);
  }

  Widget _widthField(BuildContext context) {
    return Expanded(
        child: NumberTextField.integer(
            canBeEmpty: false,
            hintText: context.l10n.width,
            initInt: value.width,
            minInt: Size.MIN_WIDTH,
            onFieldChangedInt: (term) {
              if (term != null) {
                value.width = term;
              }
            }));
  }

  Widget _heightField(BuildContext context) {
    return Expanded(
        child: NumberTextField.integer(
            canBeEmpty: false,
            hintText: context.l10n.height,
            initInt: value.height,
            minInt: Size.MIN_HEIGHT,
            onFieldChangedInt: (term) {
              if (term != null) {
                value.height = term;
              }
            }));
  }
}

class PointField extends StatelessWidget {
  final Point value;

  const PointField(this.value);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[_widthField(), _heightField()]);
  }

  Widget _widthField() {
    return Expanded(
        child: NumberTextField.integer(
            canBeEmpty: false,
            hintText: 'X',
            initInt: value.x,
            onFieldChangedInt: (term) {
              if (term != null) {
                value.x = term;
              }
            }));
  }

  Widget _heightField() {
    return Expanded(
        child: NumberTextField.integer(
            canBeEmpty: false,
            hintText: 'Y',
            initInt: value.y,
            onFieldChangedInt: (term) {
              if (term != null) {
                value.y = term;
              }
            }));
  }
}

class AspectRatioField extends StatelessWidget {
  final Rational value;

  const AspectRatioField(this.value);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[_numField(), _denField()]);
  }

  Widget _numField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Num',
            canBeEmpty: false,
            initInt: value.numerator,
            onFieldChangedInt: (term) {
              if (term != null) {
                value.numerator = term;
              }
            }));
  }

  Widget _denField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Den',
            canBeEmpty: false,
            initInt: value.denominator,
            onFieldChangedInt: (term) {
              if (term != null) {
                value.denominator = term;
              }
            }));
  }
}

class LogoField extends StatelessWidget {
  final Logo value;

  const LogoField(this.value);

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _pathField(context),
      SizeField(value.size),
      PointField(value.position),
      _alphaField(context)
    ]);
  }

  Widget _pathField(BuildContext context) {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: FilePath.MIN_PATH_LENGTH,
        maxSymbols: FilePath.MAX_PATH_LENGTH,
        hintText: context.l10n.path,
        errorText: context.l10n.enterPath,
        init: value.path,
        onFieldChanged: (term) {
          value.path = term;
        });
  }

  Widget _alphaField(BuildContext context) {
    return NumberTextField.decimal(
        hintText: context.l10n.alpha,
        initDouble: value.alpha,
        minDouble: Alpha.MIN,
        maxDouble: Alpha.MAX,
        canBeEmpty: false,
        onFieldChangedDouble: (term) {
          if (term != null) {
            value.alpha = term;
          }
        });
  }
}

class RsvgLogoField extends StatelessWidget {
  final RsvgLogo value;

  const RsvgLogoField(this.value);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_pathField(context), SizeField(value.size), PointField(value.position)]);
  }

  Widget _pathField(BuildContext context) {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: FilePath.MIN_PATH_LENGTH,
        maxSymbols: FilePath.MAX_PATH_LENGTH,
        hintText: context.l10n.path,
        errorText: context.l10n.enterPath,
        init: value.path,
        onFieldChanged: (term) {
          value.path = term;
        });
  }
}

class TextOverlayField extends StatelessWidget {
  final TextOverlay value;

  const TextOverlayField(this.value);

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _textField(context),
      _xAbsoluteField(context),
      _yAbsoluteField(context),
      _fontField(context)
    ]);
  }

  Widget _textField(BuildContext context) {
    return TextFieldEx(
        minSymbols: 1,
        maxSymbols: 512,
        hintText: context.l10n.text,
        errorText: context.l10n.enterText,
        init: value.text,
        onFieldChanged: (term) {
          value.text = term;
        });
  }

  Widget _xAbsoluteField(BuildContext context) {
    return NumberTextField.decimal(
        hintText: context.l10n.xabsolute,
        initDouble: value.xAbsolute,
        minDouble: 0.0,
        maxDouble: 1.0,
        canBeEmpty: false,
        onFieldChangedDouble: (term) {
          if (term != null) {
            value.xAbsolute = term;
          }
        });
  }

  Widget _yAbsoluteField(BuildContext context) {
    return NumberTextField.decimal(
        hintText: context.l10n.yabsolute,
        initDouble: value.yAbsolute,
        minDouble: 0.0,
        maxDouble: 1.0,
        canBeEmpty: false,
        onFieldChangedDouble: (term) {
          if (term != null) {
            value.yAbsolute = term;
          }
        });
  }

  Widget _fontField(BuildContext context) {
    return OptionalFieldTile(
        title: context.l10n.font,
        init: value.font != null,
        onChanged: (bool checked) {
          value.font = checked ? Font(family: 'Sans', size: 18) : null;
        },
        builder: () {
          return Row(mainAxisSize: MainAxisSize.min, children: [
            Expanded(
                child: TextFieldEx(
                    minSymbols: 1,
                    maxSymbols: 512,
                    hintText: context.l10n.fontFamily,
                    errorText: context.l10n.enterFontFamily,
                    init: value.font!.family,
                    onFieldChanged: (String term) {
                      value.font!.family = term;
                    })),
            Expanded(
                child: NumberTextField.integer(
                    initInt: value.font!.size,
                    minInt: 1,
                    maxInt: 1000,
                    onFieldChangedInt: (int? size) {
                      if (size != null) {
                        value.font!.size = size;
                      }
                    }))
          ]);
        });
  }
}

class MachineLearningField extends StatefulWidget {
  final MachineLearning init;

  const MachineLearningField(this.init);

  @override
  _MachineLearningFieldState createState() {
    return _MachineLearningFieldState();
  }
}

class _MachineLearningFieldState extends State<MachineLearningField> {
  MachineLearning get init => widget.init;

  _MachineLearningFieldState();

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_backendField(), _modelPathField(), _trackingField(), _overlayField()]);
  }

  Widget _backendField() {
    return DropdownButtonEx<MlBackend>(
        hint: init.backend.toHumanReadable(),
        value: init.backend,
        values: MlBackend.values,
        onChanged: (c) {
          init.backend = c;
        },
        itemBuilder: (MlBackend value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  List<Widget> _modelsList() {
    final List<Widget> modelsList = [];
    for (int i = 0; i < init.models.length; ++i) {
      final model = init.models[i];
      final content = Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        _MlModelField(value: model),
        if (i != 0)
          IconButton(
              onPressed: () {
                setState(() {
                  init.models.remove(model);
                });
              },
              icon: const Icon(Icons.delete))
      ]);
      modelsList.add(content);
    }
    return modelsList;
  }

  Widget _modelPathField() {
    Widget _header(String text) {
      return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Text(text, style: const TextStyle(fontWeight: FontWeight.bold)));
    }

    Widget _modelDialog() {
      final TextEditingController _textController = TextEditingController();
      return SimpleDialog(title: Text(context.l10n.addModel), children: [
        Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(children: [
              TextFieldEx(
                  validator: (String text) {
                    return text.isNotEmpty ? null : context.l10n.invalidModelPath;
                  },
                  formatters: <TextInputFormatter>[TextFieldFilter.root],
                  decoration: InputDecoration(
                      border: const OutlineInputBorder(), labelText: context.l10n.modelPath),
                  controller: _textController),
              const SizedBox(height: 5.0),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                FlatButtonEx.filled(
                    text: context.l10n.close,
                    onPressed: () {
                      Navigator.of(context).pop();
                    }),
                FlatButtonEx.filled(
                    text: context.l10n.add,
                    onPressed: () {
                      if (_textController.text.isNotEmpty) {
                        setState(() {
                          init.models.add(MlModel(path: _textController.text));
                          Navigator.pop(context, init);
                        });
                      }
                    })
              ])
            ]))
      ]);
    }

    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        _header('Models'),
        IconButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return _modelDialog();
                  });
            },
            icon: const Icon(Icons.add))
      ]),
      Column(children: _modelsList())
    ]);
  }

  Widget _trackingField() {
    return StateCheckBox(
        title: 'Tracking',
        init: init.tracking,
        onChanged: (tracking) {
          init.tracking = tracking;
        });
  }

  Widget _overlayField() {
    return StateCheckBox(
        title: 'Overlay',
        init: init.overlay,
        onChanged: (overlay) {
          init.overlay = overlay;
        });
  }
}

class BackgroundEffectField<S extends EncodeStream> extends StatefulWidget {
  final S stream;

  const BackgroundEffectField(this.stream);

  @override
  _BackgroundEffectFieldState<S> createState() {
    return _BackgroundEffectFieldState<S>();
  }
}

class _BackgroundEffectFieldState<S extends EncodeStream> extends State<BackgroundEffectField<S>> {
  BackgroundEffect get value => widget.stream.backgroundEffect!;

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      DropdownButtonEx<BackgroundEffectType>(
          hint: value.type.toHumanReadable(),
          value: value.type,
          values: BackgroundEffectType.values,
          onChanged: (c) {
            setState(() {
              switch (c) {
                case BackgroundEffectType.BLUR:
                  widget.stream.backgroundEffect = BlurBackgroundEffect();
                  break;
                case BackgroundEffectType.IMAGE:
                  widget.stream.backgroundEffect = ImageBackgroundEffect.createDefault();
                  break;
                case BackgroundEffectType.COLOR:
                  widget.stream.backgroundEffect = ColorBackgroundEffect();
                  break;
              }
            });
          },
          itemBuilder: (BackgroundEffectType value) {
            return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
          }),
      if (value.type == BackgroundEffectType.BLUR) _blur(),
      if (value.type == BackgroundEffectType.IMAGE) _image(),
      if (value.type == BackgroundEffectType.COLOR) _color()
    ]);
  }

  Widget _blur() {
    final blur = value as BlurBackgroundEffect;
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(children: [
          const Text('0.0'),
          Expanded(
              child: Slider(
                  value: blur.strength,
                  onChanged: (newValue) {
                    setState(() {
                      blur.strength = newValue;
                    });
                  })),
          const Text('1.0')
        ]));
  }

  Widget _image() {
    final effect = value as ImageBackgroundEffect;
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'URL',
        errorText: 'Enter URL',
        init: effect.path,
        onFieldChanged: (term) {
          effect.path = term;
        });
  }

  Widget _color() {
    final color = value as ColorBackgroundEffect;
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ColorPicker(
            initialColor: Color(color.hex),
            onChanged: (Color cl) {
              color.hex = cl.value;
            }));
  }
}

class StreamTTLField extends StatelessWidget {
  final StreamTTL init;

  const StreamTTLField(this.init);

  @override
  Widget build(BuildContext context) {
    return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[Expanded(child: _autoExitField()), Expanded(child: _phoenixField())]);
  }

  Widget _autoExitField() {
    return NumberTextField.integer(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minInt: AutoExitTime.MIN,
        maxInt: AutoExitTime.MAX,
        hintText: 'Stream TTL',
        canBeEmpty: false,
        initInt: init.ttl,
        onFieldChangedInt: (term) {
          if (term != null) {
            init.ttl = term;
          }
        });
  }

  Widget _phoenixField() {
    return StateCheckBox(
        title: 'Phoenix',
        init: init.phoenix,
        onChanged: (value) {
          init.phoenix = value;
        });
  }
}

class StreamNotificationField extends StatefulWidget {
  final List<NotificationStreamContact> notificationContacts;

  const StreamNotificationField(this.notificationContacts);

  @override
  State<StreamNotificationField> createState() {
    return _StreamNotificationFieldState();
  }
}

class _StreamNotificationFieldState extends State<StreamNotificationField> {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(children: [
        Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return _contactFields(widget.notificationContacts[index]);
                },
                itemCount: widget.notificationContacts.length))
      ]),
      _addNotificationContact()
    ]);
  }

  Widget _contactFields(NotificationStreamContact contact) {
    return Row(children: <Widget>[
      Expanded(
          flex: 3,
          child: TextFieldEx(
              hintText: 'Contact',
              onFieldChanged: (value) {
                contact.email = value;
              },
              init: contact.email)),
      Expanded(
          flex: 3,
          child: DropdownButtonEx<NotificationStreamType>(
              hint: 'Notification type',
              value: contact.type,
              values: NotificationStreamType.values,
              onChanged: (value) {
                contact.type = value;
              },
              itemBuilder: (value) {
                return DropdownMenuItem<NotificationStreamType>(
                    child: Text(value.toHumanReadable()), value: value);
              })),
      Expanded(
          child: IconButton(
              onPressed: () {
                setState(() {
                  widget.notificationContacts.remove(contact);
                });
              },
              icon: const Icon(Icons.delete)))
    ]);
  }

  Widget _addNotificationContact() {
    return ElevatedButton(
        onPressed: () {
          setState(() {
            widget.notificationContacts.add(
                NotificationStreamContact(email: '', type: NotificationStreamType.STREAM_FINISHED));
          });
        },
        child: const Text('Add contact'));
  }
}

class _MlModelField extends StatelessWidget {
  final MlModel value;

  const _MlModelField({Key? key, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: TextFieldEx(
            hintText: 'Path',
            errorText: 'Input path',
            init: value.path,
            onFieldChanged: (val) {
              value.path = val;
            }));
  }
}
