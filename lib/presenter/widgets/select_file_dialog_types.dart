import 'dart:js_interop';
import 'dart:js_interop_unsafe';

class MediaInfo {
  static const _ROTATION_FIELD = 'rotation';
  static const _WIDTH_FIELD = 'width';
  static const _HEIGHT_FIELD = 'height';
  static const _DURATION_FIELD = 'duration';
  static const _URL_FIELD = 'url';

  int rotation;
  int width;
  int height;
  int duration;
  String url;

  MediaInfo(
      {required this.rotation,
      required this.width,
      required this.height,
      required this.duration,
      required this.url});

  factory MediaInfo.fromJson(Map<String, dynamic> json) {
    return MediaInfo(
        rotation: json[_ROTATION_FIELD],
        width: json[_WIDTH_FIELD],
        height: json[_HEIGHT_FIELD],
        duration: json[_DURATION_FIELD],
        url: json[_URL_FIELD]);
  }

  factory MediaInfo.fromJSObject(JSObject jobj) {
    final jrotation = jobj.getProperty<JSNumber>(_ROTATION_FIELD.toJS);
    final jwidth = jobj.getProperty<JSNumber>(_WIDTH_FIELD.toJS);
    final jheight = jobj.getProperty<JSNumber>(_HEIGHT_FIELD.toJS);
    final jduration = jobj.getProperty<JSNumber>(_DURATION_FIELD.toJS);
    final jurl = jobj.getProperty<JSString>(_URL_FIELD.toJS);
    return MediaInfo(
        rotation: jrotation.toDartInt,
        width: jwidth.toDartInt,
        height: jheight.toDartInt,
        duration: jduration.toDartInt,
        url: jurl.toDart);
  }

  Duration getDuration() {
    return Duration(seconds: duration ~/ 1000);
  }
}

String printDuration(Duration duration) {
  String twoDigits(int n) => n.toString().padLeft(2, '0');
  final twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
  final twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
  return '${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds';
}
