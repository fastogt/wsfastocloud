import 'package:flutter/material.dart';
import 'package:wsfastocloud/l10n/l10n.dart';

class StreamActionIcon extends IconButton {
  final void Function() onTap;

  StreamActionIcon.start(this.onTap, BuildContext context)
      : super(icon: const Icon(Icons.play_arrow), tooltip: context.l10n.start, onPressed: onTap);

  StreamActionIcon.embedOutput(this.onTap, BuildContext context)
      : super(
            icon: const Icon(Icons.my_library_add),
            tooltip: context.l10n.embedOutput,
            onPressed: onTap);

  StreamActionIcon.copyStream(this.onTap, BuildContext context)
      : super(icon: const Icon(Icons.copy), tooltip: context.l10n.copy, onPressed: onTap);

  StreamActionIcon.copyToExternalStream(this.onTap, BuildContext context)
      : super(
            icon: const Icon(Icons.file_copy),
            tooltip: context.l10n.copyToExternal,
            onPressed: onTap);

  StreamActionIcon.stop(this.onTap, BuildContext context)
      : super(icon: const Icon(Icons.stop), tooltip: context.l10n.stop, onPressed: onTap);

  StreamActionIcon.kill(this.onTap, BuildContext context)
      : super(icon: const Icon(Icons.not_interested), tooltip: context.l10n.kill, onPressed: onTap);

  StreamActionIcon.restart(this.onTap, BuildContext context)
      : super(icon: const Icon(Icons.loop), tooltip: context.l10n.restart, onPressed: onTap);

  StreamActionIcon.changeStream(this.onTap, BuildContext context)
      : super(
            icon: const Icon(Icons.switch_left_sharp),
            tooltip: context.l10n.changeStream,
            onPressed: onTap);

  StreamActionIcon.play(this.onTap, BuildContext context)
      : super(icon: const Icon(Icons.playlist_play), tooltip: context.l10n.play, onPressed: onTap);

  StreamActionIcon.playOutput(this.onTap, BuildContext context)
      : super(icon: const Icon(Icons.tv), tooltip: context.l10n.playOutput, onPressed: onTap);

  StreamActionIcon.getLog(this.onTap, BuildContext context)
      : super(
            icon: const Icon(Icons.file_download), tooltip: context.l10n.getLog, onPressed: onTap);

  StreamActionIcon.viewLog(this.onTap, BuildContext context)
      : super(icon: const Icon(Icons.info), tooltip: context.l10n.viewLog, onPressed: onTap);

  StreamActionIcon.getPipeline(this.onTap, BuildContext context)
      : super(
            icon: const Icon(Icons.trending_flat),
            tooltip: context.l10n.getPipeline,
            onPressed: onTap);

  StreamActionIcon.viewPipeline(this.onTap, BuildContext context)
      : super(
            icon: const Icon(Icons.trending_flat),
            tooltip: context.l10n.viewPipeline,
            onPressed: onTap);

  StreamActionIcon.refresh(this.onTap, BuildContext context)
      : super(icon: const Icon(Icons.refresh), tooltip: context.l10n.refresh, onPressed: onTap);

  StreamActionIcon.getConfig(this.onTap, BuildContext context)
      : super(icon: const Icon(Icons.settings), tooltip: context.l10n.getConfig, onPressed: onTap);
}
