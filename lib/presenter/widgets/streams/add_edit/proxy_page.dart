import 'package:fastocloud_dart_media_models/models.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/istream_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/vod.dart';

class ProxyStreamPage extends IStreamPage<ProxyStream> {
  ProxyStreamPage.add(LiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.PROXY, false);

  ProxyStreamPage.edit(LiveServer server, IStreamEditor editor, ProxyStream stream)
      : super.edit(server, editor, stream, false);

  @override
  _ProxyStreamPageState createState() {
    return _ProxyStreamPageState();
  }
}

class _ProxyStreamPageState extends IStreamPageState<ProxyStreamPage, ProxyStream> {
  @override
  bool validate() {
    return stream.isValid();
  }
}

class VodProxyStreamPage extends IStreamPage<VodProxyStream> {
  VodProxyStreamPage.add(LiveServer server, IStreamEditor editor, bool isSerial)
      : super.add(server, editor, StreamType.VOD_PROXY, isSerial);

  VodProxyStreamPage.edit(
      LiveServer server, IStreamEditor editor, VodProxyStream stream, bool isSerial)
      : super.edit(server, editor, stream, isSerial);

  @override
  _VodProxyStreamPageState createState() {
    return _VodProxyStreamPageState();
  }
}

class _VodProxyStreamPageState extends IStreamPageState<VodProxyStreamPage, VodProxyStream> {
  @override
  List<TabWidget> tabs() {
    final def = widget.editor.defaults();
    return [
      ...super.tabs(),
      TabWidget(
          widget.isEpisode ? 'Episode' : 'VOD',
          VodSection<VodProxyStream>(stream, setBaseFields, def.backgroundUrl, def.streamLogoIcon,
              isSerial: widget.isEpisode))
    ];
  }

  @override
  bool validate() {
    return stream.isValid();
  }
}
