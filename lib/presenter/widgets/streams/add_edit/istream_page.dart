import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/base.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/meta.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/output.dart';

abstract class IStreamPage<S extends IStream> extends BaseStreamPage<S> {
  IStreamPage.add(LiveServer server, IStreamEditor editor, StreamType type, bool isEpisode)
      : super.add(server, editor, type, isEpisode);

  IStreamPage.edit(LiveServer server, IStreamEditor editor, S stream, bool isEpisode)
      : super.edit(server, editor, stream, isEpisode);
}

abstract class IStreamPageState<T extends IStreamPage, S extends IStream>
    extends BaseStreamPageState<T, S> {
  final GlobalKey<BaseSectionState> _baseKey = GlobalKey<BaseSectionState>();

  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', base()),
      //TabWidget('Meta urls', metaUrls()),
      TabWidget('Output urls', outputUrls())
    ];
  }

  Widget base() {
    return BaseSection<S>(stream, widget.isEpisode, key: _baseKey);
  }

  Widget metaUrls() {
    return MetaUrlSection<S>(stream);
  }

  Widget outputUrls() {
    if (widget.server is GoLiveServer) {
      final server = widget.server as GoLiveServer;
      return FutureBuilder(
          builder: (context, snap) {
            if (snap.hasData) {
              return OutputUrlSection<S>(stream, server, snap.data!, genWebRTC, probeRawUrl);
            }
            return const Center(child: CircularProgressIndicator());
          },
          future: widget.editor.folders());
    }

    return LiveOutputUrlSection<S>(stream, widget.server, genWebRTC);
  }

  WebRTCOutputUrl genWebRTC(int oid, String? sid) {
    String stabled = 'null';
    if (sid != null) {
      stabled = sid;
    }
    return widget.editor.genWebRTC(oid, stabled);
  }

  void probeRawUrl(OutputUrl url) {
    final resp = widget.editor.probeOutUrl(url);
    resp.then((value) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text('Probe Stream'),
              content: SizedBox(
                  width: 600,
                  height: 400,
                  child: SingleChildScrollView(child: Text(value, softWrap: true))),
              actions: [
                FlatButtonEx.filled(text: 'Ok', onPressed: () => Navigator.pop(context)),
              ],
            );
          });
    }, onError: (error) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text('Error'),
              content: SizedBox(
                  width: 600,
                  height: 400,
                  child: SingleChildScrollView(child: Text(error, softWrap: true))),
              actions: [
                FlatButtonEx.filled(text: 'Ok', onPressed: () => Navigator.pop(context)),
              ],
            );
          });
    });
  }

  void probeUrl(InputUrl url) {
    final resp = widget.editor.probeInUrl(url);
    resp.then((value) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text('Probe Stream'),
              content: SizedBox(
                  width: 600,
                  height: 400,
                  child: SingleChildScrollView(child: Text(value, softWrap: true))),
              actions: [
                FlatButtonEx.filled(text: 'Ok', onPressed: () => Navigator.pop(context)),
              ],
            );
          });
    }, onError: (error) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text('Error'),
              content: SizedBox(
                  width: 600,
                  height: 400,
                  child: SingleChildScrollView(child: Text(error, softWrap: true))),
              actions: [
                FlatButtonEx.filled(text: 'Ok', onPressed: () => Navigator.pop(context)),
              ],
            );
          });
    });
  }

  void setBaseFields(String name, String icon, String description) {
    _baseKey.currentState!.setFields(name: name, icon: icon, description: description);
  }
}
