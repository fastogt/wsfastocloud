import 'dart:async';

import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/domain/entity/file.dart';
import 'package:wsfastocloud/domain/entity/uploaded_image.dart';
import 'package:wsfastocloud/domain/repositories/server_repository.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_statistics_bloc/server_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/data_picker.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';
import 'package:wsfastocloud/utils/file_picker.dart';

class VodSection<S extends VodStream> extends StatefulWidget {
  const VodSection(
      this.stream, this.onVodDetailsChanged, this.defaultBackgroundUrl, this.defaultBannerUrl,
      {Key? key, required this.isSerial})
      : super(key: key);

  final S stream;
  final void Function(String name, String icon, String description) onVodDetailsChanged;
  final String defaultBackgroundUrl;
  final String defaultBannerUrl;
  final bool isSerial;

  @override
  _VodSectionState createState() {
    return _VodSectionState();
  }
}

class _VodSectionState extends State<VodSection> {
  final StreamController<String> _backgroundUpdates = StreamController<String>.broadcast();

  @override
  void dispose() {
    _backgroundUpdates.close();
    super.dispose();
  }

  VodDetails? tmdb;
  List<VodDetails> tmdbResults = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final bloc = context.read<ServiceStatisticsBloc>();
      if (!widget.isSerial) {
        bloc.tmdbMoviesRequest(widget.stream.name).then((result) {
          setState(() {
            tmdbResults = result;
          });
        });
      } else {
        bloc.tmdbEpisodesRequest(widget.stream.name).then((result) {
          setState(() {
            tmdbResults = result;
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return ScrollbarEx(builder: (controller) {
      return ListView(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          controller: controller,
          children: [
            //   typeField(),
            _tmdbField(),
            _backgroundUrlField(),
            _trailerUrlField(),
            Row(children: <Widget>[Expanded(child: _castField()), Expanded(child: _genresField())]),
            Row(children: <Widget>[
              Expanded(child: _productionField()),
              Expanded(child: _directorsField())
            ]),
            Row(children: <Widget>[
              Expanded(child: _userScoreField()),
              Expanded(child: _countryField())
            ]),
            Row(children: <Widget>[
              Expanded(child: _primeDateField()),
              Expanded(child: _durationField())
            ])
          ]);
    });
  }

  // Widget typeField() {
  //   return DropdownButtonEx<VodType>(
  //       hint: 'Movie type',
  //       value: widget.stream.vodType,
  //       values: VodType.values,
  //       onChanged: (type) {
  //         widget.stream.vodType = type;
  //       },
  //       itemBuilder: (VodType type) {
  //         return DropdownMenuItem(child: Text(type.toHumanReadable()), value: type);
  //       });
  // }

  Widget _backgroundUrlField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        hintText: 'Background URL',
        errorText: 'Enter background URL',
        init: widget.stream.backgroundUrl,
        decoration: InputDecoration(
            icon: StreamBuilder<String>(
                initialData: widget.stream.backgroundUrl,
                stream: _backgroundUpdates.stream,
                builder: (_, snapshot) {
                  return InkWell(
                      onTap: _browseImage,
                      child: PreviewIcon.live(snapshot.data!, width: 60, height: 40));
                })),
        onFieldChanged: (term) {
          widget.stream.backgroundUrl = term;
          _backgroundUpdates.add(term);
        });
  }

  Widget _castField() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ChipListField(
            hintText: 'Actors',
            values: widget.stream.cast,
            onItemAdded: (item) => setState(() {
                  final List<String> copy = [];
                  for (final c in widget.stream.cast) {
                    copy.add(c);
                  }
                  copy.add(item);
                  widget.stream.cast = copy;
                }),
            onItemRemoved: (index) => setState(() {
                  widget.stream.cast.removeAt(index);
                })));
  }

  Widget _productionField() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ChipListField(
            hintText: 'Production',
            values: widget.stream.production,
            onItemAdded: (item) => setState(() {
                  final List<String> copy = [];
                  for (final c in widget.stream.production) {
                    copy.add(c);
                  }
                  copy.add(item);
                  widget.stream.production = copy;
                }),
            onItemRemoved: (index) => setState(() {
                  widget.stream.production.removeAt(index);
                })));
  }

  Widget _genresField() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ChipListField(
            hintText: 'Genres',
            values: widget.stream.genres,
            onItemAdded: (item) => setState(() {
                  final List<String> copy = [];
                  for (final c in widget.stream.genres) {
                    copy.add(c);
                  }
                  copy.add(item);
                  widget.stream.genres = copy;
                }),
            onItemRemoved: (index) => setState(() {
                  widget.stream.genres.removeAt(index);
                })));
  }

  Widget _directorsField() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ChipListField(
            hintText: 'Directors',
            values: widget.stream.directors,
            onItemAdded: (item) => setState(() {
                  final List<String> copy = [];
                  for (final c in widget.stream.directors) {
                    copy.add(c);
                  }
                  copy.add(item);
                  widget.stream.directors = copy;
                }),
            onItemRemoved: (index) => setState(() {
                  widget.stream.directors.removeAt(index);
                })));
  }

  Widget _trailerUrlField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: TrailerUrl.MIN_LENGTH,
        maxSymbols: TrailerUrl.MAX_LENGTH,
        hintText: 'Trailer URL',
        init: widget.stream.trailerUrl,
        onFieldChanged: (term) {
          widget.stream.trailerUrl = term;
        });
  }

  Widget _userScoreField() {
    return NumberTextField.decimal(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minDouble: UserScore.MIN,
        maxDouble: UserScore.MAX,
        hintText: 'User score',
        canBeEmpty: false,
        initDouble: widget.stream.userScore,
        onFieldChangedDouble: (double? term) {
          widget.stream.userScore = term ?? 0;
        });
  }

  Widget _primeDateField() {
    return DatePicker('Premiere date', widget.stream.primeDate, (int date) {
      widget.stream.primeDate = date;
    });
  }

  Widget _countryField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        maxSymbols: Country.MAX_LENGTH,
        hintText: 'Country',
        errorText: 'Enter country',
        init: widget.stream.country,
        onFieldChanged: (term) {
          widget.stream.country = term;
        });
  }

  Widget _durationField() {
    final int inSeconds = widget.stream.duration ~/ 1000;
    return NumberTextField.integer(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minInt: VodDuration.MIN,
        maxInt: VodDuration.MAX,
        hintText: 'Duration (sec)',
        canBeEmpty: false,
        initInt: inSeconds,
        onFieldChangedInt: (term) {
          if (term != null) {
            widget.stream.duration = term * 1000;
          }
        });
  }

  Widget _tmdbField() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(children: <Widget>[
          const SizedBox(width: 8),
          Expanded(
              child: DropdownButtonEx<VodDetails?>(
                  padding: const EdgeInsets.all(0),
                  hint: 'TMDB Results',
                  value: tmdb,
                  values: tmdbResults,
                  onChanged: (t) async {
                    if (t != null) {
                      final bloc = context.read<ServiceStatisticsBloc>();
                      if (!widget.isSerial) {
                        await bloc.tmdbMoviesSecondRequest(t.id).then((value) => setState(() {
                              tmdb = value;
                            }));
                      } else {
                        await bloc.tmdbEpisodesSecondRequest(t.id).then((value) => setState(() {
                              tmdb = value;
                            }));
                      }
                      setState(() {
                        widget.stream.userScore = tmdb?.userScore ?? 0;
                        widget.stream.primeDate = tmdb?.primeDate ?? 0;
                        widget.stream.duration = tmdb?.duration ?? 0;
                        widget.stream.country = tmdb?.country ?? '';
                        widget.stream.backgroundUrl =
                            tmdb?.backgroundLogoUrl ?? widget.defaultBackgroundUrl;
                        widget.stream.production = tmdb?.production ?? [];
                        widget.stream.cast = tmdb?.cast ?? [];
                        widget.stream.directors = tmdb?.directors ?? [];
                        widget.stream.genres = tmdb?.genres ?? [];
                      });
                      widget.onVodDetailsChanged(
                          tmdb?.name ?? '', tmdb?.icon ?? '', tmdb?.description ?? '');
                    }
                  },
                  itemBuilder: (VodDetails? result) {
                    return DropdownMenuItem(child: Text(result!.name!), value: result);
                  })),
          IconButton(
              tooltip: 'Refresh',
              icon: const Icon(Icons.refresh),
              onPressed: () {
                final bloc = context.read<ServiceStatisticsBloc>();
                if (!widget.isSerial) {
                  bloc.tmdbMoviesRequest(widget.stream.name).then((result) {
                    tmdbResults = result;
                    setState(() {});
                  });
                } else {
                  bloc.tmdbEpisodesRequest(widget.stream.name).then((result) {
                    tmdbResults = result;
                    setState(() {});
                  });
                }
              })
        ]));
  }

  Future<void> _browseImage() async {
    final ImageFilePicker picker = ImageFilePicker();

    final File? image = await picker.pickFile();

    if (image == null) {
      return;
    }

    final ServerRepository serverRepository = context.read<ServerRepository>();
    final res = serverRepository.uploadImage(image);
    res.then((UploadedImage uploadedFile) {
      _refreshIcon(uploadedFile.path);
    });
  }

  void _refreshIcon(String icon) {
    setState(() {
      _refreshIconInternal(icon);
    });
  }

  void _refreshIconInternal(String icon) {
    widget.stream.backgroundUrl = icon;
    _backgroundUpdates.add(icon);
  }
}
