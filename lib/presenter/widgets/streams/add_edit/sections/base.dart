import 'dart:async';
import 'dart:developer';

import 'package:fastocloud_dart_media_models/models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/widgets.dart';
import 'package:rxdart/subjects.dart';
import 'package:wsfastocloud/domain/entity/file.dart';
import 'package:wsfastocloud/domain/entity/uploaded_image.dart';
import 'package:wsfastocloud/domain/repositories/server_repository.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';
import 'package:wsfastocloud/presenter/widgets/price_pack.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/latlng.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/subtitles.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';
import 'package:wsfastocloud/utils/file_picker.dart';

class BaseSection<S extends IStream> extends StatefulWidget {
  final S stream;
  final bool isEpisode;

  const BaseSection(this.stream, this.isEpisode, {Key? key}) : super(key: key);

  bool isAdd() {
    return stream.id == null;
  }

  @override
  State<BaseSection<S>> createState() {
    return BaseSectionState<S>();
  }
}

class BaseSectionState<S extends IStream> extends State<BaseSection<S>> {
  bool get hasEpg => (widget.stream.epgId ?? '').isNotEmpty;
  final BehaviorSubject<bool> _hasEpg = BehaviorSubject<bool>();

  final StreamController<String> _iconsUpdates = StreamController<String>.broadcast();

  @override
  void dispose() {
    _iconsUpdates.close();
    _hasEpg.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Row(children: [
        if (!widget.isAdd()) Expanded(flex: 2, child: roIDField()),
        Expanded(flex: 3, child: nameField()),
      ]),
      descriptionField(),
      iconField(),
      if (!widget.stream.isVod()) epgField(),
      Row(children: <Widget>[
        Expanded(child: priceField(), flex: 3),
        Expanded(child: LatLngSection<S>(widget.stream), flex: 2),
        Expanded(child: iarcField())
      ]),
      Row(children: <Widget>[
        Expanded(child: SubtitlesSection<S>(widget.stream)),
        if (!widget.isEpisode) Expanded(flex: 2, child: _Groups<S>(widget.stream))
      ]),
      if (!widget.isEpisode) Expanded(child: visibleField())
    ]);
  }

  Widget epgField() {
    return Row(children: [
      Expanded(
          flex: 2,
          child: TextFieldEx(
              cursorColor: Theme.of(context).colorScheme.primary,
              key: const ValueKey<String>('Epg field'),
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              maxSymbols: EpgName.MAX_LENGTH,
              hintText: 'Epg ID',
              init: widget.stream.epgId,
              onFieldChanged: (term) {
                if (hasEpg != term.isNotEmpty) {
                  _hasEpg.add(term.isNotEmpty);
                }
                widget.stream.epgId = term;
              })),
      Expanded(child: archive())
    ]);
  }

  Widget priceField() {
    return OptionalFieldTile(
        title: context.l10n.price,
        init: widget.stream.price != null,
        onChanged: (value) {
          widget.stream.price =
              value ? PricePack(currency: Currency.USD, price: 1, type: PriceType.LIFE_TIME) : null;
        },
        builder: () {
          return PricePackField(widget.stream.price!);
        });
  }

  Widget archive() {
    return StreamBuilder(
        initialData: hasEpg,
        stream: _hasEpg.stream,
        builder: (context, snapshot) {
          if (hasEpg) {
            return StateCheckBox(
                title: 'Archive',
                init: widget.stream.archive,
                onChanged: (value) {
                  widget.stream.archive = value;
                });
          }
          return IgnorePointer(
              ignoring: !hasEpg,
              child:
                  Opacity(opacity: hasEpg ? 1 : 0.5, child: const StateCheckBox(title: 'Archive')));
        });
  }

  TextFieldEx roIDField() {
    return TextFieldEx(
        cursorColor: Theme.of(context).colorScheme.primary,
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        init: widget.stream.id!,
        readOnly: true,
        hintText: 'ID',
        decoration: const InputDecoration().copyWith(
            suffixIcon: IconButton(
                icon: const Icon(Icons.copy),
                tooltip: 'Copy',
                onPressed: () {
                  Clipboard.setData(ClipboardData(text: widget.stream.id!));
                })));
  }

  TextFieldEx nameField() {
    return TextFieldEx(
        cursorColor: Theme.of(context).colorScheme.primary,
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        maxSymbols: StreamName.MAX_LENGTH,
        hintText: 'Name',
        errorText: 'Enter name',
        init: widget.stream.name,
        onFieldChanged: (term) {
          widget.stream.name = term;
        });
  }

  TextFieldEx descriptionField() {
    return TextFieldEx(
        cursorColor: Theme.of(context).colorScheme.primary,
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        maxSymbols: Description.MAX_LENGTH,
        hintText: 'Description',
        init: widget.stream.description,
        onFieldChanged: (term) {
          widget.stream.description = term;
        });
  }

  TextFieldEx iconField() {
    return TextFieldEx(
        cursorColor: Theme.of(context).colorScheme.primary,
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Icon',
        errorText: 'Enter icon url',
        init: widget.stream.icon,
        decoration: InputDecoration(
            icon: StreamBuilder<String>(
                initialData: widget.stream.icon,
                stream: _iconsUpdates.stream,
                builder: (_, snapshot) {
                  return InkWell(
                      onTap: _browseImage,
                      child: PreviewIcon.live(snapshot.data!, width: 40, height: 40));
                })),
        onFieldChanged: (term) {
          _refreshIconInternal(term);
        });
  }

  Future<void> _browseImage() async {
    final ImageFilePicker picker = ImageFilePicker();

    final File? image = await picker.pickFile();

    if (image == null) {
      return;
    }

    final ServerRepository serverRepository = context.read<ServerRepository>();

    try {
      final UploadedImage uploadedFile = await serverRepository.uploadImage(image);

      _refreshIcon(uploadedFile.path);
    } catch (e) {
      log('Error uploading image: $e');
    }
  }

  void _refreshIcon(String icon) {
    setState(() {
      _refreshIconInternal(icon);
    });
  }

  void _refreshIconInternal(String icon) {
    widget.stream.icon = icon;
    _iconsUpdates.add(icon);
  }

  Widget iarcField() {
    return NumberTextField.integer(
        cursorColor: Theme.of(context).colorScheme.primary,
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minInt: IARC.MIN,
        maxInt: IARC.MAX,
        hintText: 'IARC',
        canBeEmpty: false,
        initInt: widget.stream.iarc,
        onFieldChangedInt: (term) {
          if (term != null) {
            widget.stream.iarc = term;
          }
        });
  }

  Widget visibleField() {
    return StateCheckBox(
        title: context.l10n.visibleForSubscribers,
        init: widget.stream.visible,
        onChanged: (value) {
          widget.stream.visible = value;
        });
  }

  void setFields({String? name, String? icon, String? description}) {
    if (name != null) {
      widget.stream.name = name;
    }
    if (icon != null) {
      widget.stream.icon = icon;
      _iconsUpdates.add(icon);
    }
    if (description != null) {
      widget.stream.description = description;
    }
    setState(() {});
  }
}

class _Groups<S extends IStream> extends StatefulWidget {
  const _Groups(this.stream, {Key? key}) : super(key: key);

  final S stream;

  @override
  _GroupsState createState() {
    return _GroupsState();
  }
}

class _GroupsState extends State<_Groups> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: ChipListField(
            hintText: 'Groups',
            values: widget.stream.groups,
            onItemAdded: (item) => setState(() {
                  final List<String> copy = [];
                  for (final c in widget.stream.groups) {
                    copy.add(c);
                  }
                  copy.add(item);
                  widget.stream.groups = copy;
                }),
            onItemRemoved: (index) => setState(() => widget.stream.groups.removeAt(index))));
  }
}
