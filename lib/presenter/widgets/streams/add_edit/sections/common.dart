import 'package:flutter/material.dart';

class IOFieldBorder extends StatelessWidget {
  final Widget child;
  final VoidCallback? onDelete;

  const IOFieldBorder({required this.child, this.onDelete});

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
          child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Theme.of(context).brightness == Brightness.light
                              ? Colors.black38
                              : Colors.white38),
                      borderRadius: const BorderRadius.all(Radius.circular(8.0))),
                  child: child))),
      if (onDelete != null)
        IconButton(tooltip: 'Remove', icon: const Icon(Icons.close), onPressed: onDelete)
    ]);
  }
}

class IOTypeRadioTile extends StatelessWidget {
  final bool selected;
  final VoidCallback onChanged;
  final String title;

  const IOTypeRadioTile({
    Key? key,
    required this.title,
    required this.selected,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onChanged,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: <Widget>[
            Container(
              width: 16,
              height: 16,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: selected ? Theme.of(context).colorScheme.primary : Colors.transparent,
                border: Border.all(
                  color: selected ? Theme.of(context).colorScheme.primary : Colors.grey,
                  width: selected ? 6 : 2,
                ),
              ),
              child: selected
                  ? Center(
                      child: Container(
                        width: 12,
                        height: 12,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                        ),
                      ),
                    )
                  : null,
            ),
            const SizedBox(width: 8.0),
            Text(title)
          ],
        ),
      ),
    );
  }
}
