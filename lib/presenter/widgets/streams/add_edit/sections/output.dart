import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_builder/src/widget_builders.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/common.dart';
import 'package:wsfastocloud/presenter/widgets/urls/output.dart';

class LiveOutputUrlSection<S extends IStream> extends StatefulWidget {
  final S stream;
  final LiveServer server;
  final WebRTCOutputUrl Function(int oid, String? sid) genWebRTC;

  const LiveOutputUrlSection(this.stream, this.server, this.genWebRTC);

  @override
  _LiveOutputUrlSectionState<S> createState() {
    return _LiveOutputUrlSectionState<S>();
  }
}

class _LiveOutputUrlSectionState<S extends IStream> extends State<LiveOutputUrlSection<S>> {
  final GlobalKey<ScrollbarExState> _scrollKey = GlobalKey<ScrollbarExState>();

  S get stream => widget.stream;

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: TextButton.icon(
              icon: const Icon(Icons.add),
              label: const Text('Add output URL'),
              onPressed: _addOutput)),
      Expanded(
          child: ScrollbarEx(
              key: _scrollKey,
              builder: (controller) {
                return ListView.builder(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    controller: controller,
                    itemCount: stream.output.length,
                    itemBuilder: (context, index) {
                      return IOFieldBorder(
                          onDelete: stream.output.length == 1 ? null : () => _deleteOutput(index),
                          child: Padding(
                              padding: const EdgeInsets.all(8.0), child: _createTile(index)));
                    });
              }))
    ]);
  }

  Widget _createTile(int index) {
    final OutputUrl url = stream.output[index];
    final bool isWhip = url is HttpOutputUrl && url.isWhip();
    final bool isHttp = url is HttpOutputUrl && !url.isWhip() && !url.isHls();
    final bool isHLS = url is HttpOutputUrl && url.isHls();
    final bool isSrt = url is SrtOutputUrl;
    final bool isGs = url is GoogleOutputUrl;
    final bool isS3 = url is S3OutputUrl;
    final bool isRtmp = url is RtmpOutputUrl;
    final bool isUDP = url is UdpOutputUrl;
    final bool isUnknown = url is UnknownOutputUrl;
    final bool isWebRTC = url is WebRTCOutputUrl;

    final childsStand = [
      IOTypeRadioTile(
          title: 'HLS',
          selected: isHLS,
          onChanged: () {
            final uri = widget.server.genTemplateHLSHttpOutputUrl(index);
            _setUrlType(index, uri);
          }),
      IOTypeRadioTile(
          title: 'HTTP',
          selected: isHttp,
          onChanged: () {
            _setUrlType(index,
                HttpOutputUrl.createDefaultHTTP(id: index, uri: 'http://0.0.0.0:8444/master'));
          }),
      IOTypeRadioTile(
          title: 'WHEP',
          selected: isWhip,
          onChanged: () {
            _setUrlType(index, widget.server.genTemplateWhipHttpOutputUrl(index));
          }),
      IOTypeRadioTile(
          title: 'SRT',
          selected: isSrt,
          onChanged: () {
            _setUrlType(index, SrtOutputUrl.createDefault(id: index));
          }),
      IOTypeRadioTile(
          title: 'UDP',
          selected: isUDP,
          onChanged: () {
            _setUrlType(index, UdpOutputUrl.createDefault(id: index));
          }),
      IOTypeRadioTile(
          title: 'RTMP',
          selected: isRtmp,
          onChanged: () {
            _setUrlType(index, RtmpOutputUrl.createDefault(id: index));
          })
    ];

    final childSpec = [
      IOTypeRadioTile(
          title: 'WebRTC',
          selected: isWebRTC,
          onChanged: () {
            final url = widget.genWebRTC(index, stream.id);
            _setUrlType(index, url);
          }),
      IOTypeRadioTile(
          title: 'Google Storage',
          selected: isGs,
          onChanged: () {
            _setUrlType(index, GoogleOutputUrl.createDefault(id: index));
          }),
      IOTypeRadioTile(
          title: 'S3 Storage',
          selected: isS3,
          onChanged: () {
            _setUrlType(index, S3OutputUrl.createDefault(id: index));
          }),
      IOTypeRadioTile(
          title: 'Specific',
          selected: isUnknown,
          onChanged: () {
            _setUrlType(index, UnknownOutputUrl.createTest(id: index));
          }),
      IOTypeRadioTile(
          title: 'Other',
          selected: !(isHttp ||
              isWhip ||
              isHLS ||
              isSrt ||
              isGs ||
              isS3 ||
              isUDP ||
              isRtmp ||
              isWebRTC ||
              isUnknown),
          onChanged: () {
            _setUrlType(index, OutputUrl.createInvalid(id: index));
          })
    ];

    return ResponsiveBuilder(builder: (context, sizingInfo) {
      return sizingInfo.isDesktop
          ? Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Row(children: childsStand),
              Row(children: childSpec),
              _createOutputField(index),
            ])
          : Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Row(children: childsStand.sublist(0, 4)),
              Row(children: childsStand.sublist(4) + childSpec.sublist(0, 1)),
              Row(children: childSpec.sublist(1, 3)),
              Row(children: childSpec.sublist(3)),
              _createOutputField(index)
            ]);
    });
  }

  Widget _createOutputField(int index) {
    final OutputUrl url = stream.output[index];
    if (url is HttpOutputUrl) {
      if (!url.isHls()) {
        if (url.isWhip()) {
          return WhipHttpOutputUrlField(url);
        }
        return HttpOutputUrlField(url, null);
      }

      return HLSOutputUrlField(url, null, null, isExternal: true);
    } else if (url is SrtOutputUrl) {
      return SrtOutputUrlField(url, null);
    } else if (url is GoogleOutputUrl) {
      return GoogleOutputUrlField(url, null);
    } else if (url is S3OutputUrl) {
      return S3OutputUrlField(url, null);
    } else if (url is UdpOutputUrl) {
      return UdpOutputUrlField(url, null);
    } else if (url is RtmpOutputUrl) {
      return RtmpOutputUrlField(url, null);
    } else if (url is WebRTCOutputUrl) {
      return WebRTCOutputUrlField(url, null);
    } else if (url is UnknownOutputUrl) {
      return UnknownOutputUrlField(url, null);
    }
    return OutputUrlField(url, null);
  }

  void _addOutput() {
    setState(() {
      final int index = stream.output.length;
      stream.output.add(OutputUrl.createInvalid(id: index));
    });
    _scrollKey.currentState!.refresh();
  }

  void _deleteOutput(int index) {
    setState(() {
      stream.output.removeAt(index);
    });
    _scrollKey.currentState!.refresh();
  }

  void _setUrlType(int index, OutputUrl url) {
    setState(() {
      stream.output[index] = url;
    });
  }
}

class OutputUrlSection<S extends IStream> extends StatefulWidget {
  final S stream;
  final GoLiveServer server;
  final MediaFolders folders;
  final WebRTCOutputUrl Function(int oid, String? sid) genWebRTC;
  final void Function(OutputUrl) probe;

  const OutputUrlSection(this.stream, this.server, this.folders, this.genWebRTC, this.probe);

  @override
  _OutputUrlSectionState<S> createState() {
    return _OutputUrlSectionState<S>();
  }
}

class _OutputUrlSectionState<S extends IStream> extends State<OutputUrlSection<S>> {
  final GlobalKey<ScrollbarExState> _scrollKey = GlobalKey<ScrollbarExState>();

  S get stream => widget.stream;

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: TextButton.icon(
              icon: const Icon(Icons.add),
              label: const Text('Add output URL'),
              onPressed: _addOutput)),
      Expanded(
          child: ScrollbarEx(
              key: _scrollKey,
              builder: (controller) {
                return ListView.builder(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    controller: controller,
                    itemCount: stream.output.length,
                    itemBuilder: (context, index) {
                      return IOFieldBorder(
                          onDelete: stream.output.length == 1 ? null : () => _deleteOutput(index),
                          child: Padding(
                              padding: const EdgeInsets.all(8.0), child: _createTile(index)));
                    });
              }))
    ]);
  }

  Widget _createTile(int index) {
    final OutputUrl url = stream.output[index];
    final bool isWhip = url is HttpOutputUrl && url.isWhip();
    final bool isHttp = url is HttpOutputUrl && !url.isWhip() && !url.isHls();
    final bool isHLS = url is HttpOutputUrl && url.isHls();
    final bool isSrt = url is SrtOutputUrl;
    final bool isGs = url is GoogleOutputUrl;
    final bool isS3 = url is S3OutputUrl;
    final bool isRtmp = url is RtmpOutputUrl;
    final bool isUDP = url is UdpOutputUrl;
    final bool isUnknown = url is UnknownOutputUrl;
    final bool isWebRTC = url is WebRTCOutputUrl;
    final bool isProxy = stream.type() == StreamType.PROXY || stream.type() == StreamType.VOD_PROXY;

    final childsStand = [
      IOTypeRadioTile(
          title: 'HLS',
          selected: isHLS,
          onChanged: () {
            HttpOutputUrl uri;
            if (stream.type() == StreamType.COD_ENCODE || stream.type() == StreamType.COD_RELAY) {
              uri = widget.server.genTemplateCodsHttpOutputUrl(index);
            } else if (stream.type() == StreamType.VOD_PROXY ||
                stream.type() == StreamType.VOD_RELAY ||
                stream.type() == StreamType.VOD_ENCODE) {
              uri = widget.server.genTemplateVodsHttpOutputUrl(index);
            } else {
              uri = widget.server.genTemplateHLSHttpOutputUrl(index);
            }
            _setUrlType(index, uri);
          }),
      IOTypeRadioTile(
          title: 'HTTP',
          selected: isHttp,
          onChanged: () {
            _setUrlType(index,
                HttpOutputUrl.createDefaultHTTP(id: index, uri: 'http://0.0.0.0:8444/master'));
          }),
      IOTypeRadioTile(
          title: isProxy ? 'WHEP' : 'WHIP',
          selected: isWhip,
          onChanged: () {
            _setUrlType(index, widget.server.genTemplateWhipHttpOutputUrl(index));
          }),
      IOTypeRadioTile(
          title: 'SRT',
          selected: isSrt,
          onChanged: () {
            _setUrlType(index, SrtOutputUrl.createDefault(id: index));
          }),
      IOTypeRadioTile(
          title: 'UDP',
          selected: isUDP,
          onChanged: () {
            _setUrlType(index, UdpOutputUrl.createDefault(id: index));
          }),
      IOTypeRadioTile(
          title: 'RTMP',
          selected: isRtmp,
          onChanged: () {
            _setUrlType(index, RtmpOutputUrl.createDefault(id: index));
          })
    ];

    final childSpec = [
      IOTypeRadioTile(
          title: 'WebRTC',
          selected: isWebRTC,
          onChanged: () {
            final url = widget.genWebRTC(index, stream.id);
            _setUrlType(index, url);
          }),
      IOTypeRadioTile(
          title: 'Google Storage',
          selected: isGs,
          onChanged: () {
            _setUrlType(index, GoogleOutputUrl.createDefault(id: index));
          }),
      IOTypeRadioTile(
          title: 'S3 Storage',
          selected: isS3,
          onChanged: () {
            _setUrlType(index, S3OutputUrl.createDefault(id: index));
          }),
      IOTypeRadioTile(
          title: 'Specific',
          selected: isUnknown,
          onChanged: () {
            _setUrlType(index, UnknownOutputUrl.createTest(id: index));
          }),
      IOTypeRadioTile(
          title: 'Other',
          selected: !(isHttp ||
              isWhip ||
              isHLS ||
              isSrt ||
              isGs ||
              isS3 ||
              isUDP ||
              isRtmp ||
              isWebRTC ||
              isUnknown),
          onChanged: () {
            _setUrlType(index, OutputUrl.createInvalid(id: index));
          })
    ];

    return ResponsiveBuilder(builder: (context, sizingInfo) {
      return sizingInfo.isDesktop
          ? Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Row(children: childsStand),
              Row(children: childSpec),
              _createOutputField(index),
            ])
          : Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Row(children: childsStand.sublist(0, 4)),
              Row(children: childsStand.sublist(4) + childSpec.sublist(0, 1)),
              Row(children: childSpec.sublist(1, 3)),
              Row(children: childSpec.sublist(3)),
              _createOutputField(index)
            ]);
    });
  }

  Widget _createOutputField(int index) {
    final OutputUrl url = stream.output[index];
    final probeCB = widget.server.isPro() ? widget.probe : null;
    if (url is HttpOutputUrl) {
      final bool isProxy =
          stream.type() == StreamType.PROXY || stream.type() == StreamType.VOD_PROXY;
      if (isProxy) {
        if (!url.isHls()) {
          if (url.isWhip()) {
            return WhipHttpOutputUrlField(url);
          }
          return HttpOutputUrlField(url, probeCB);
        }

        return HLSOutputUrlField(url, probeCB, null, proxyDirectory: widget.folders.proxyDirectory,
            onPrepareOutputTemplate: (url) {
          return _convertProxyFileToHttp(url)!;
        }, isExternal: true);
      }

      if (!url.isHls()) {
        if (url.isWhip()) {
          return WhipHttpOutputUrlField(url);
        }
        return HttpOutputUrlField(url, probeCB);
      }

      Future<String> getToken(String host) {
        return context.read<StreamDBRepository>().getTokenUrl(host);
      }

      return GoHttpOutputUrlField(url, probeCB, getToken);
    } else if (url is SrtOutputUrl) {
      return SrtOutputUrlField(url, probeCB);
    } else if (url is GoogleOutputUrl) {
      return GoogleOutputUrlField(url, probeCB);
    } else if (url is S3OutputUrl) {
      return S3OutputUrlField(url, probeCB);
    } else if (url is UdpOutputUrl) {
      return UdpOutputUrlField(url, probeCB);
    } else if (url is RtmpOutputUrl) {
      return RtmpOutputUrlField(url, probeCB);
    } else if (url is WebRTCOutputUrl) {
      return WebRTCOutputUrlField(url, probeCB);
    } else if (url is UnknownOutputUrl) {
      return UnknownOutputUrlField(url, probeCB);
    }
    return OutputUrlField(url, probeCB);
  }

  String? _convertProxyFileToHttp(String path) {
    final url = widget.server.generateHttpProxyUrl(path);
    if (url == null) {
      return null;
    }
    return url.toString();
  }

  void _addOutput() {
    setState(() {
      final int index = stream.output.length;
      stream.output.add(OutputUrl.createInvalid(id: index));
    });
    _scrollKey.currentState!.refresh();
  }

  void _deleteOutput(int index) {
    setState(() {
      stream.output.removeAt(index);
    });
    _scrollKey.currentState!.refresh();
  }

  void _setUrlType(int index, OutputUrl url) {
    setState(() {
      stream.output[index] = url;
    });
  }
}
