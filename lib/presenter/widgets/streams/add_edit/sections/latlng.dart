import 'package:fastocloud_dart_media_models/models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';

class LatLngSection<S extends IStream> extends StatefulWidget {
  final S stream;

  const LatLngSection(this.stream);

  @override
  _LatLngSectionSectionState<S> createState() {
    return _LatLngSectionSectionState<S>();
  }
}

class _LatLngSectionSectionState<S extends IStream> extends State<LatLngSection<S>> {
  @override
  Widget build(BuildContext context) {
    return OptionalFieldTile(
        title: 'Location',
        init: widget.stream.location != null,
        onChanged: (value) {
          widget.stream.location = value ? LatLng.createDefault() : null;
        },
        builder: () {
          return LatLngField(widget.stream.location);
        });
  }
}

class LatLngField extends StatelessWidget {
  final LatLng? location;

  const LatLngField(this.location);

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[latitudeField(), longitudeField()]);
  }

  Widget longitudeField() {
    return Expanded(
        child: NumberTextField.decimal(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            hintText: 'Longitude',
            initDouble: location?.longitude,
            onFieldChangedDouble: (term) {
              if (term != null) location?.longitude = term;
            }));
  }

  Widget latitudeField() {
    return Expanded(
        child: NumberTextField.decimal(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            hintText: 'Latitude',
            initDouble: location?.latitude,
            onFieldChangedDouble: (term) {
              if (term != null) location?.latitude = term;
            }));
  }
}
