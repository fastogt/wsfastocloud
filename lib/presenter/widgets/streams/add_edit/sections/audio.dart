import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/l10n/l10n.dart' as lang;
import 'package:wsfastocloud/presenter/widgets/optional.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';

class AudioSection<S extends HardwareStream> extends StatefulWidget {
  final S stream;

  const AudioSection(this.stream);

  @override
  _AudioSectionState<S> createState() {
    return _AudioSectionState<S>();
  }
}

class _AudioSectionState<S extends HardwareStream> extends State<AudioSection<S>> {
  S get stream => widget.stream;

  @override
  Widget build(BuildContext context) {
    return ScrollbarEx(builder: (controller) {
      return ListView(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          controller: controller,
          children: [
            StateCheckBox(
                title: 'Has audio',
                init: stream.haveAudio,
                onChanged: (value) {
                  setState(() {
                    stream.haveAudio = value;
                  });
                }),
            if (stream is EncodeStream)
              StateCheckBox(
                  title: context.l10n.relayAudio,
                  init: (stream as EncodeStream).relayAudio,
                  onChanged: (value) {
                    setState(() {
                      (stream as EncodeStream).relayAudio = value;
                    });
                  }),
            if (stream.haveAudio) ...fields(),
          ]);
    });
  }

  List<Widget> fields() {
    return [
      _ChangeAudioTrack<S>(stream, context.l10n.changeAudioTrack, context.l10n.audioTrack),
      _TrackNumber<S>(stream)
    ];
  }
}

class AudioSectionRelay<S extends RelayStream> extends AudioSection<S> {
  const AudioSectionRelay(S stream) : super(stream);

  @override
  _AudioSectionRelayState<S> createState() {
    return _AudioSectionRelayState<S>();
  }
}

class _AudioSectionRelayState<S extends RelayStream> extends _AudioSectionState<S> {
  @override
  List<Widget> fields() {
    return super.fields() + [_audioParser()];
  }

  Widget _audioParser() {
    return OptionalFieldTile(
        title: context.l10n.audioParser,
        init: stream.audioParser != null,
        onChanged: (value) {
          stream.audioParser = value ? AudioParser.AAC : null;
        },
        builder: () {
          return DropdownButtonEx<AudioParser>(
              hint: context.l10n.audioParser,
              value: stream.audioParser!,
              values: AudioParser.values,
              onChanged: (t) {
                stream.audioParser = t;
              },
              itemBuilder: (AudioParser parser) {
                return DropdownMenuItem(child: Text(parser.toHumanReadable()), value: parser);
              });
        });
  }
}

class AudioSectionEncode<S extends EncodeStream> extends AudioSection<S> {
  const AudioSectionEncode(S stream) : super(stream);

  @override
  _AudioSectionEncodeState<S> createState() {
    return _AudioSectionEncodeState<S>();
  }
}

class _AudioSectionEncodeState<S extends EncodeStream> extends _AudioSectionState<S> {
  @override
  List<Widget> fields() {
    final List<Widget> noRelayAudio = [
      ...super.fields(),
      _Resample<S>(stream),
      OptionalVolumeFieldTile(stream),
      _AudioBitrateWithConverter(stream: stream),
      _AudioChannelsCount<S>(stream),
      _AudioStabilizationField(stream: stream),
    ];

    return [_AudioCodec<S>(stream), if (!stream.relayAudio) ...noRelayAudio];
  }
}

class _ChangeAudioTrack<S extends HardwareStream> extends OptionalFieldTile {
  _ChangeAudioTrack(S stream, String title, String hint)
      : super(
            title: title,
            init: stream.audioSelect != null,
            onChanged: (value) {
              stream.audioSelect = value ? 0 : null;
            },
            builder: () {
              return NumberTextField.integer(
                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  minInt: AudioSelect.MIN,
                  maxInt: AudioSelect.MAX,
                  hintText: hint,
                  initInt: stream.audioSelect,
                  onFieldChangedInt: (term) {
                    stream.audioSelect = term;
                  });
            });
}

class _TrackNumber<S extends HardwareStream> extends NumberTextField {
  _TrackNumber(S stream)
      : super.integer(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            minInt: AudioTracksCount.MIN,
            maxInt: AudioTracksCount.MAX,
            hintText: 'Audio tracks count',
            canBeEmpty: false,
            initInt: stream.audioTracksCount,
            onFieldChangedInt: (term) {
              if (term != null) stream.audioTracksCount = term;
            });
}

class _Resample<S extends EncodeStream> extends StateCheckBox {
  _Resample(S stream)
      : super(
            title: 'Resample',
            init: stream.resample ?? false,
            onChanged: (value) {
              stream.resample = value ? value : null;
            });
}

class OptionalVolumeFieldTile extends StatefulWidget {
  final EncodeStream stream;

  const OptionalVolumeFieldTile(this.stream);

  @override
  _OptionalVolumeFieldTile createState() {
    return _OptionalVolumeFieldTile();
  }
}

class _OptionalVolumeFieldTile extends State<OptionalVolumeFieldTile> {
  @override
  Widget build(BuildContext context) {
    bool checked = widget.stream.volume != null;
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          if (checked) const Divider(),
          CheckboxListTile(
              title: const Text('Change volume'),
              value: checked,
              onChanged: (value) {
                setState(() {
                  checked = value!;
                  if (checked) {
                    widget.stream.volume = Volume.DEFAULT;
                  } else {
                    widget.stream.volume = null;
                  }
                });
              }),
          if (checked) content(),
          if (checked) const Divider()
        ]);
  }

  Widget content() {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(children: [
          Text(Volume.MIN.toString()),
          Expanded(
              child: Slider(
                  value: widget.stream.volume == null ? Volume.DEFAULT : widget.stream.volume!,
                  min: Volume.MIN, // ignore: avoid_redundant_argument_values
                  max: Volume.MAX,
                  onChanged: (term) {
                    setState(() {
                      widget.stream.volume = term;
                    });
                  })),
          Text(Volume.MAX.toString())
        ]));
  }
}

class _AudioCodec<S extends EncodeStream> extends DropdownButtonEx<AudioCodec> {
  _AudioCodec(S stream)
      : super(
            hint: 'Audio codec',
            value: stream.audioCodec,
            values: AudioCodec.values,
            onChanged: (t) {
              stream.audioCodec = t;
            },
            itemBuilder: (AudioCodec codec) {
              return DropdownMenuItem(child: Text(codec.toHumanReadable()), value: codec);
            });
}

class _AudioBitrateWithConverter extends StatefulWidget {
  final EncodeStream stream;

  const _AudioBitrateWithConverter({required this.stream, Key? key}) : super(key: key);

  @override
  _AudioBitrateWithConverterState createState() {
    return _AudioBitrateWithConverterState();
  }
}

class _AudioBitrateWithConverterState extends State<_AudioBitrateWithConverter> {
  static const DEFAULT_BITRATE = 125 * 1024;
  final _bitController = TextEditingController();
  final _kBitController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _bitController.addListener(onBitrateChange);
  }

  void onBitrateChange() {
    final _bitrate = double.tryParse(_bitController.text);

    if (_bitrate != null) {
      _kBitController.value =
          TextEditingValue(text: (widget.stream.audioBitRate! / 1024).toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return OptionalFieldTile(
        title: 'Change bitrate',
        init: widget.stream.audioBitRate != null,
        onChanged: (value) {
          widget.stream.audioBitRate = value ? DEFAULT_BITRATE : null;
        },
        builder: () {
          return Row(children: [
            Expanded(
                child: NumberTextField.integer(
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    hintText: 'Bitrate',
                    canBeEmpty: false,
                    textEditingController: _bitController,
                    minInt: 1,
                    initInt: widget.stream.audioBitRate,
                    onFieldChangedInt: (term) {
                      widget.stream.audioBitRate = term;
                      _bitController.value =
                          TextEditingValue(text: (widget.stream.audioBitRate! / 1024).toString());
                    }),
                flex: 4),
            Expanded(
              child: TextFieldEx(
                  readOnly: true,
                  controller: _kBitController
                    ..text = (widget.stream.audioBitRate! / 1024).toString(),
                  keyboardType: TextInputType.number,
                  decoration:
                      const InputDecoration(labelText: 'Kb/s', border: OutlineInputBorder())),
            )
          ]);
        });
  }
}

class _AudioChannelsCount<S extends EncodeStream> extends OptionalFieldTile {
  _AudioChannelsCount(S stream)
      : super(
            title: 'Change channels count',
            init: stream.audioChannelsCount != null,
            onChanged: (value) {
              stream.audioChannelsCount = value ? 2 : null;
            },
            builder: () {
              return NumberTextField.integer(
                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  canBeEmpty: false,
                  minInt: AudioChannelsCount.MIN,
                  maxInt: AudioChannelsCount.MAX,
                  hintText: 'Channels Count',
                  initInt: stream.audioChannelsCount,
                  onFieldChangedInt: (term) {
                    stream.audioChannelsCount = term;
                  });
            });
}

class _AudioStabilizationField extends StatefulWidget {
  final EncodeStream stream;

  const _AudioStabilizationField({Key? key, required this.stream}) : super(key: key);

  @override
  _AudioStabilizationFieldState createState() {
    return _AudioStabilizationFieldState();
  }
}

class _AudioStabilizationFieldState extends State<_AudioStabilizationField> {
  static const DEFAULT_VALUE =
      AudioStabilization(model: GpuModel.NVIDIA_T4, type: AudioEffectType.DENOISER);

  AudioStabilization? get audioStabilization => widget.stream.audioStabilization;

  set audioStabilization(AudioStabilization? value) => widget.stream.audioStabilization = value;

  @override
  Widget build(BuildContext context) {
    return OptionalFieldTile(
        title: 'Audio stabilization',
        init: audioStabilization != null,
        onChanged: (value) {
          audioStabilization = value ? DEFAULT_VALUE : null;
        },
        builder: () => Column(children: [
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButtonFormField<GpuModel>(
                      value: audioStabilization?.model,
                      items: [
                        for (final value in GpuModel.values)
                          DropdownMenuItem(child: Text(value.toHumanReadable()), value: value)
                      ],
                      onChanged: (value) {
                        audioStabilization = audioStabilization?.copyWith(model: value);
                      })),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButtonFormField<AudioEffectType>(
                      value: audioStabilization?.type,
                      items: [
                        for (final value in AudioEffectType.values)
                          DropdownMenuItem(child: Text(value.toHumanReadable()), value: value),
                      ],
                      onChanged: (value) {
                        audioStabilization = audioStabilization?.copyWith(type: value);
                      }))
            ]));
  }
}
