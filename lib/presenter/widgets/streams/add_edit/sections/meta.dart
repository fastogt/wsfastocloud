import 'package:fastocloud_dart_media_models/models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/common.dart';

class MetaUrlSection<S extends IStream> extends StatefulWidget {
  final S stream;

  const MetaUrlSection(this.stream);

  @override
  _MetaUrlSectionState<S> createState() {
    return _MetaUrlSectionState<S>();
  }
}

class _MetaUrlSectionState<S extends IStream> extends State<MetaUrlSection<S>> {
  final GlobalKey<ScrollbarExState> _scrollKey = GlobalKey<ScrollbarExState>();

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: TextButton.icon(
              icon: const Icon(Icons.add),
              label: const Text('Add meta URL'),
              onPressed: () {
                setState(() {
                  widget.stream.meta.insert(0, MetaUrl('Test', 'http://localhost/test.doc'));
                });
                _scrollKey.currentState!.refresh();
              })),
      Expanded(
          child: ScrollbarEx(
              key: _scrollKey,
              builder: (controller) {
                return ListView.builder(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    controller: controller,
                    itemCount: widget.stream.meta.length,
                    itemBuilder: (_, int index) {
                      return _Tile(widget.stream.meta[index], () {
                        setState(() {
                          widget.stream.meta.removeAt(index);
                        });
                        _scrollKey.currentState!.refresh();
                      });
                    });
              }))
    ]);
  }
}

class _Tile extends StatelessWidget {
  const _Tile(this.meta, this.onDelete, {Key? key}) : super(key: key);

  final MetaUrl meta;
  final VoidCallback onDelete;

  @override
  Widget build(BuildContext context) {
    return IOFieldBorder(
        onDelete: onDelete,
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          TextFieldEx(
              hintText: 'Name',
              errorText: 'Enter name',
              init: meta.name,
              onFieldChanged: (val) {
                meta.name = val;
              }),
          TextFieldEx(
              formatters: <TextInputFormatter>[TextFieldFilter.url],
              hintText: 'Url',
              errorText: 'Enter url',
              init: meta.url,
              onFieldChanged: (val) {
                meta.url = val;
              })
        ]));
  }
}
