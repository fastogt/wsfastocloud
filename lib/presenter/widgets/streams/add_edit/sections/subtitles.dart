import 'package:fastocloud_dart_media_models/models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/l10n/l10n.dart' as l10n;
import 'package:wsfastocloud/presenter/widgets/select_file_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/select_file_web_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/common.dart';

class SubtitlesSection<S extends IStream> extends StatefulWidget {
  final S _stream;

  const SubtitlesSection(this._stream);

  @override
  _SubtitlesSectionState<S> createState() {
    return _SubtitlesSectionState<S>();
  }
}

class _SubtitlesSectionState<S extends IStream> extends State<SubtitlesSection<S>> {
  List<Subtitle> subtitles = [];
  late Future<LocalesResp> _init;
  final TextEditingController _textController = TextEditingController();

  @override
  void initState() {
    final fetcher = context.read<Fetcher>();
    _init = fetcher.getLocale();
    for (final sub in widget._stream.subtitles) {
      subtitles.add(sub);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<LocalesResp>(
        future: _init,
        builder: (contextb, snap) {
          if (snap.hasData) {
            final respa = snap.data!;
            final child = subtitles.isNotEmpty
                ? SizedBox(
                    height: 140,
                    child: ListView.builder(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        itemCount: subtitles.length,
                        itemBuilder: (context, index) {
                          return _createTile(index, respa.languages);
                        }))
                : const SizedBox();

            return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: TextButton.icon(
                      icon: const Icon(Icons.add),
                      label: const Text('Add subtitles'),
                      onPressed: () {
                        _addSub(respa.currentLanguage, respa.languages);
                      })),
              child
            ]);
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  void _addSub(String current, List<List<String>> languages) {
    final res = showDialog<Subtitle?>(
        context: context,
        builder: (_) {
          return _subtitleListFieldDialog(current, languages);
        });
    res.then((value) {
      if (value != null) {
        setState(() {
          subtitles.add(value);
          widget._stream.subtitles = subtitles;
        });
      }
    });
  }

  Widget _createTile(int index, List<List<String>> languages) {
    final sub = subtitles[index];
    return _Tile(sub, languages: languages, () {
      setState(() {
        subtitles.removeAt(index);
        widget._stream.subtitles = subtitles;
      });
    });
  }

  Widget _subtitleListFieldDialog(String current, List<List<String>> languages) {
    return SimpleDialog(title: const Text('Add to subtitles list'), children: [
      Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(children: <Widget>[
            LocalePicker(
                current: current,
                values: languages,
                title: context.l10n.language,
                onChanged: (c) {
                  current = c!;
                }),
            Row(mainAxisSize: MainAxisSize.min, children: [
              Expanded(
                  flex: 8,
                  child: TextFieldEx(
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Input subtitle url'),
                      controller: _textController)),
              Expanded(
                  child: IconButton(
                      icon: const Icon(Icons.file_upload),
                      tooltip: 'Upload media',
                      onPressed: () {
                        _upload(context);
                      }))
            ]),
            const SizedBox(height: 5.0),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              FlatButtonEx.filled(
                  text: context.l10n.close,
                  onPressed: () {
                    Navigator.of(context).pop();
                  }),
              FlatButtonEx.filled(
                  text: context.l10n.add,
                  onPressed: () {
                    final item = Subtitle(language: current, url: _textController.text);
                    Navigator.pop(context, item);
                  })
            ])
          ]))
    ]);
  }

  void _upload(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          if (kIsWeb) {
            return const SelectFileWebDialog.file();
          }
          return const SelectFileDialog.file();
        }).then((value) {
      setState(() {
        if (value != null) {
          final String? host = context.read<Fetcher>().backendServerUrl;
          if (host == null) {
            return;
          }
          _textController.text = host + value!;
        }
      });
    });
  }
}

class _Tile extends StatelessWidget {
  const _Tile(this.meta, this.onDelete, {required this.languages, Key? key}) : super(key: key);

  final Subtitle meta;
  final VoidCallback onDelete;
  final List<List<String>> languages;

  @override
  Widget build(BuildContext context) {
    return IOFieldBorder(
        onDelete: onDelete,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          _languagePicker(),
          TextFieldEx(
              formatters: <TextInputFormatter>[TextFieldFilter.url],
              hintText: 'Url',
              errorText: 'Enter url',
              init: meta.url,
              onFieldChanged: (val) {
                meta.url = val;
              })
        ]));
  }

  Widget _languagePicker() {
    return LocalePicker(
        current: meta.language,
        values: languages,
        title: 'Language',
        onChanged: (c) {
          meta.language = c!;
        });
  }
}
