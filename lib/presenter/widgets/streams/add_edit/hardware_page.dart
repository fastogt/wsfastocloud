import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:wsfastocloud/l10n/l10n.dart' as lang;
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/istream_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/audio.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/input.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/other.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/video.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/vod.dart';

abstract class RawHardwareStreamPage<S extends RawHardwareStream> extends IStreamPage<S> {
  RawHardwareStreamPage.add(GoLiveServer server, IStreamEditor editor, StreamType type,
      {bool isSerial = false})
      : super.add(server, editor, type, isSerial);

  RawHardwareStreamPage.edit(GoLiveServer server, IStreamEditor editor, RawHardwareStream stream,
      {bool isSerial = false})
      : super.edit(server, editor, stream as S, isSerial);
}

abstract class _RawHardwareStreamPageState<T extends RawHardwareStreamPage,
    S extends RawHardwareStream> extends IStreamPageState<T, S> {
  String? scanFolderOid;

  @override
  List<TabWidget> tabs() {
    return <TabWidget>[
      TabWidget(context.l10n.base, base()),
      //  TabWidget('Meta urls', metaUrls()),
      TabWidget(context.l10n.inputUrls, inputUrls()),
      TabWidget(context.l10n.outputUrls, outputUrls()),
      TabWidget(context.l10n.other, other())
    ];
  }

  Widget inputUrls() =>
      InputUrlSection<S>(stream, widget.server as GoLiveServerWithoutFolders, probeUrl);

  Widget other() => OtherSection<S>(stream);

  @override
  bool validate() {
    return stream.isValid();
  }
}

abstract class HardwareStreamPage<S extends HardwareStream> extends IStreamPage<S> {
  HardwareStreamPage.add(GoLiveServer server, IStreamEditor editor, StreamType type,
      {bool isSerial = false})
      : super.add(server, editor, type, isSerial);

  HardwareStreamPage.edit(GoLiveServer server, IStreamEditor editor, HardwareStream stream,
      {bool isSerial = false})
      : super.edit(server, editor, stream as S, isSerial);
}

abstract class _HardwareStreamPageState<T extends HardwareStreamPage, S extends HardwareStream>
    extends IStreamPageState<T, S> {
  String? scanFolderOid;

  @override
  List<TabWidget> tabs() {
    return [
      TabWidget(context.l10n.base, base()),
      //  TabWidget('Meta urls', metaUrls()),
      TabWidget(context.l10n.inputUrls, inputUrls()),
      TabWidget(context.l10n.outputUrls, outputUrls()),
      TabWidget(context.l10n.audio, audio()),
      TabWidget(context.l10n.video, video()),
      TabWidget(context.l10n.other, other())
    ];
  }

  Widget inputUrls() =>
      InputUrlSection<S>(stream, widget.server as GoLiveServerWithoutFolders, probeUrl);

  Widget audio() => AudioSection<S>(stream);

  Widget video() => VideoSection<S>(stream);

  Widget other() => OtherSection<S>(stream);

  @override
  bool validate() {
    return stream.isValid();
  }
}

// lite

class LiteStreamPage extends RawHardwareStreamPage<LiteStream> {
  LiteStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.LITE);

  LiteStreamPage.edit(GoLiveServer server, IStreamEditor editor, LiteStream stream)
      : super.edit(server, editor, stream);

  @override
  _LiteStreamPageState createState() => _LiteStreamPageState();
}

class _LiteStreamPageState extends _RawHardwareStreamPageState<LiteStreamPage, LiteStream> {}

// relay
abstract class RelayStreamPageBase<S extends RelayStream> extends HardwareStreamPage<S> {
  RelayStreamPageBase.add(GoLiveServer server, IStreamEditor editor, StreamType type,
      {bool isSerial = false})
      : super.add(server, editor, type, isSerial: isSerial);

  RelayStreamPageBase.edit(GoLiveServer server, IStreamEditor editor, S stream,
      {bool isSerial = false})
      : super.edit(server, editor, stream, isSerial: isSerial);
}

abstract class _RelayStreamPageBaseState<T extends RelayStreamPageBase, S extends RelayStream>
    extends _HardwareStreamPageState<T, S> {
  @override
  Widget audio() => AudioSectionRelay<S>(stream);

  @override
  Widget video() => VideoSectionRelay<S>(stream);

  @override
  Widget other() => OtherSectionRelay<S>(stream);
}

class RelayStreamPage extends RelayStreamPageBase<RelayStream> {
  RelayStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.RELAY);

  RelayStreamPage.edit(GoLiveServer server, IStreamEditor editor, RelayStream stream)
      : super.edit(server, editor, stream);

  @override
  _RelayStreamPageState createState() => _RelayStreamPageState();
}

class _RelayStreamPageState extends _RelayStreamPageBaseState<RelayStreamPage, RelayStream> {}

abstract class EncodeStreamPageBase<S extends EncodeStream> extends HardwareStreamPage<S> {
  EncodeStreamPageBase.add(GoLiveServer server, IStreamEditor editor, StreamType type,
      {bool isSerial = false})
      : super.add(server, editor, type, isSerial: isSerial);

  EncodeStreamPageBase.edit(GoLiveServer server, IStreamEditor editor, EncodeStream stream,
      {bool isSerial = false})
      : super.edit(server, editor, stream, isSerial: isSerial);
}

abstract class _EncodeStreamPageBaseState<T extends EncodeStreamPageBase, S extends EncodeStream>
    extends _HardwareStreamPageState<T, S> {
  @override
  Widget audio() => AudioSectionEncode<S>(stream);

  @override
  Widget video() => VideoSectionEncode<S>(stream);

  @override
  Widget other() => OtherSectionEncode<S>(stream);
}

// encode
class EncodeStreamPage extends EncodeStreamPageBase<EncodeStream> {
  EncodeStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.ENCODE);

  EncodeStreamPage.edit(GoLiveServer server, IStreamEditor editor, EncodeStream stream)
      : super.edit(server, editor, stream);

  @override
  _EncodeStreamPageState createState() {
    return _EncodeStreamPageState();
  }
}

class _EncodeStreamPageState extends _EncodeStreamPageBaseState<EncodeStreamPage, EncodeStream> {}

// timeshift recorder
abstract class TimeshiftRecorderStreamPageBase<S extends TimeshiftRecorderStream>
    extends RelayStreamPageBase<S> {
  TimeshiftRecorderStreamPageBase.add(GoLiveServer server, IStreamEditor editor, StreamType type)
      : super.add(server, editor, type);

  TimeshiftRecorderStreamPageBase.edit(GoLiveServer server, IStreamEditor editor, S stream)
      : super.edit(server, editor, stream);
}

abstract class _TimeshiftRecorderStreamPageBaseState<T extends TimeshiftRecorderStreamPageBase,
    S extends TimeshiftRecorderStream> extends _RelayStreamPageBaseState<T, S> {}

class TimeshiftRecorderStreamPage extends TimeshiftRecorderStreamPageBase<TimeshiftRecorderStream> {
  TimeshiftRecorderStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.TIMESHIFT_RECORDER);

  TimeshiftRecorderStreamPage.edit(
      GoLiveServer server, IStreamEditor editor, TimeshiftRecorderStream stream)
      : super.edit(server, editor, stream);

  @override
  _TimeshiftRecorderStreamPageState createState() {
    return _TimeshiftRecorderStreamPageState();
  }
}

class _TimeshiftRecorderStreamPageState extends _TimeshiftRecorderStreamPageBaseState<
    TimeshiftRecorderStreamPage, TimeshiftRecorderStream> {
  @override
  List<TabWidget> tabs() {
    return [
      TabWidget(context.l10n.base, base()),
      TabWidget(context.l10n.inputUrls, inputUrls()),
      TabWidget(context.l10n.outputUrls, outputUrls()),
      TabWidget(context.l10n.audio, audio()),
      TabWidget(context.l10n.video, video()),
      TabWidget(context.l10n.other, other())
    ];
  }
}

// catchup
class CatchupStreamPage extends TimeshiftRecorderStreamPageBase<CatchupStream> {
  CatchupStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.CATCHUP);

  CatchupStreamPage.edit(GoLiveServer server, IStreamEditor editor, CatchupStream stream)
      : super.edit(server, editor, stream);

  @override
  _CatchupStreamPageState createState() {
    return _CatchupStreamPageState();
  }
}

class _CatchupStreamPageState
    extends _TimeshiftRecorderStreamPageBaseState<CatchupStreamPage, CatchupStream> {}

// timeshift player
class TimeshiftPlayerStreamPage extends RelayStreamPageBase<TimeshiftPlayerStream> {
  TimeshiftPlayerStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.TIMESHIFT_PLAYER);

  TimeshiftPlayerStreamPage.edit(
      GoLiveServer server, IStreamEditor editor, TimeshiftPlayerStream stream)
      : super.edit(server, editor, stream);

  @override
  _TimeshiftPlayerStreamPageState createState() {
    return _TimeshiftPlayerStreamPageState();
  }
}

class _TimeshiftPlayerStreamPageState
    extends _RelayStreamPageBaseState<TimeshiftPlayerStreamPage, TimeshiftPlayerStream> {}

// cvdata
class CvDataStreamPage extends EncodeStreamPageBase<CvDataStream> {
  CvDataStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.CV_DATA);

  CvDataStreamPage.edit(GoLiveServer server, IStreamEditor editor, CvDataStream stream)
      : super.edit(server, editor, stream);

  @override
  _CvDataStreamPageState createState() {
    return _CvDataStreamPageState();
  }
}

class _CvDataStreamPageState extends _EncodeStreamPageBaseState<CvDataStreamPage, CvDataStream> {
  @override
  List<TabWidget> tabs() {
    return [
      TabWidget(context.l10n.base, base()),
      TabWidget(context.l10n.inputUrls, inputUrls()),
      TabWidget(context.l10n.audio, audio()),
      TabWidget(context.l10n.video, video()),
      TabWidget(context.l10n.other, other())
    ];
  }
}

// changer
class ChangerRelayStreamPage extends RelayStreamPageBase<ChangerRelayStream> {
  ChangerRelayStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.CHANGER_RELAY);

  ChangerRelayStreamPage.edit(GoLiveServer server, IStreamEditor editor, ChangerRelayStream stream)
      : super.edit(server, editor, stream);

  @override
  _ChangerRelayStreamPageState createState() {
    return _ChangerRelayStreamPageState();
  }
}

class _ChangerRelayStreamPageState
    extends _RelayStreamPageBaseState<ChangerRelayStreamPage, ChangerRelayStream> {}

class ChangerEncoderStreamPage extends EncodeStreamPageBase<ChangerEncodeStream> {
  ChangerEncoderStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.CHANGER_ENCODE);

  ChangerEncoderStreamPage.edit(
      GoLiveServer server, IStreamEditor editor, ChangerEncodeStream stream)
      : super.edit(server, editor, stream);

  @override
  _ChangerEncoderStreamPageState createState() {
    return _ChangerEncoderStreamPageState();
  }
}

class _ChangerEncoderStreamPageState
    extends _EncodeStreamPageBaseState<ChangerEncoderStreamPage, ChangerEncodeStream> {}

// vod relay
//? only adds vods fields
class VodRelayStreamPage extends RelayStreamPageBase<VodRelayStream> {
  VodRelayStreamPage.add(GoLiveServer server, IStreamEditor editor, bool isSerial)
      : super.add(server, editor, StreamType.VOD_RELAY, isSerial: isSerial);

  VodRelayStreamPage.edit(
      GoLiveServer server, IStreamEditor editor, VodRelayStream stream, bool isSerial)
      : super.edit(server, editor, stream, isSerial: isSerial);

  @override
  _VodRelayStreamPage createState() {
    return _VodRelayStreamPage();
  }
}

class _VodRelayStreamPage extends _RelayStreamPageBaseState<VodRelayStreamPage, VodRelayStream> {
  @override
  List<TabWidget> tabs() {
    final def = widget.editor.defaults();
    return [
      TabWidget(context.l10n.base, base()),
      TabWidget(context.l10n.inputUrls, inputUrls()),
      TabWidget(context.l10n.outputUrls, outputUrls()),
      TabWidget(
          widget.isEpisode ? context.l10n.episode : context.l10n.vod,
          VodSection<VodRelayStream>(stream, setBaseFields, def.backgroundUrl, def.streamLogoIcon,
              isSerial: widget.isEpisode)),
      TabWidget(context.l10n.audio, audio()),
      TabWidget(context.l10n.video, video()),
      TabWidget(context.l10n.other, other())
    ];
  }
}

// vod encode
//? only adds vods fields
class VodEncodeStreamPage extends EncodeStreamPageBase<VodEncodeStream> {
  VodEncodeStreamPage.add(GoLiveServer server, IStreamEditor editor, bool isSerial)
      : super.add(server, editor, StreamType.VOD_ENCODE, isSerial: isSerial);

  VodEncodeStreamPage.edit(
      GoLiveServer server, IStreamEditor editor, VodEncodeStream stream, bool isSerial)
      : super.edit(server, editor, stream, isSerial: isSerial);

  @override
  _VodEncodeStreamPageState createState() {
    return _VodEncodeStreamPageState();
  }
}

class _VodEncodeStreamPageState
    extends _EncodeStreamPageBaseState<VodEncodeStreamPage, VodEncodeStream> {
  @override
  List<TabWidget> tabs() {
    final def = widget.editor.defaults();
    return [
      TabWidget(context.l10n.base, base()),
      TabWidget(context.l10n.inputUrls, inputUrls()),
      TabWidget(context.l10n.outputUrls, outputUrls()),
      TabWidget(
          widget.isEpisode ? context.l10n.episode : context.l10n.vod,
          VodSection<VodEncodeStream>(
            stream,
            setBaseFields,
            def.backgroundUrl,
            def.streamLogoIcon,
            isSerial: widget.isEpisode,
          )),
      TabWidget(context.l10n.audio, audio()),
      TabWidget(context.l10n.video, video()),
      TabWidget(context.l10n.other, other())
    ];
  }
}

// cod relay
class CodRelayStreamPage extends RelayStreamPageBase<CodRelayStream> {
  CodRelayStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.COD_RELAY);

  CodRelayStreamPage.edit(GoLiveServer server, IStreamEditor editor, CodRelayStream stream)
      : super.edit(server, editor, stream);

  @override
  _CodRelayStreamPageState createState() {
    return _CodRelayStreamPageState();
  }
}

class _CodRelayStreamPageState
    extends _RelayStreamPageBaseState<CodRelayStreamPage, CodRelayStream> {}

// cod encode
class CodEncodeStreamPage extends EncodeStreamPageBase<CodEncodeStream> {
  CodEncodeStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.COD_ENCODE);

  CodEncodeStreamPage.edit(GoLiveServer server, IStreamEditor editor, CodEncodeStream stream)
      : super.edit(server, editor, stream);

  @override
  _CodEncodeStreamPageState createState() {
    return _CodEncodeStreamPageState();
  }
}

class _CodEncodeStreamPageState
    extends _EncodeStreamPageBaseState<CodEncodeStreamPage, CodEncodeStream> {}

// mosaic
class MosaicStreamPage extends EncodeStreamPageBase<MosaicStream> {
  MosaicStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.MOSAIC);

  MosaicStreamPage.edit(GoLiveServer server, IStreamEditor editor, MosaicStream stream)
      : super.edit(server, editor, stream);

  @override
  _MosaicStreamPageState createState() {
    return _MosaicStreamPageState();
  }
}

class _MosaicStreamPageState extends _EncodeStreamPageBaseState<MosaicStreamPage, MosaicStream> {}

// event stream
class EventStreamPage extends EncodeStreamPageBase<EventStream> {
  EventStreamPage.add(GoLiveServer server, IStreamEditor editor)
      : super.add(server, editor, StreamType.EVENT);

  EventStreamPage.edit(GoLiveServer server, IStreamEditor editor, EventStream stream)
      : super.edit(server, editor, stream);

  @override
  _EventStreamPageState createState() {
    return _EventStreamPageState();
  }
}

class _EventStreamPageState extends _EncodeStreamPageBaseState<EventStreamPage, EventStream> {}
