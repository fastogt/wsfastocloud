import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:player/common/controller.dart';
import 'package:player/player.dart';

abstract class _OutputBottomControls extends StatefulWidget {
  final IPlayerController controller;
  final void Function()? onPrev;
  final void Function()? onNext;

  const _OutputBottomControls({required this.controller, this.onPrev, this.onNext});
}

abstract class _OutputBottomControlsState<T extends _OutputBottomControls> extends State<T> {
  IPlayerController get controller => widget.controller;

  Color get disabled => Theme.of(context).disabledColor;

  Color get active => Colors.black;

  Widget previousButton() {
    if (widget.onPrev == null) {
      return const SizedBox();
    }
    return PlayerButtons.previous(onPressed: widget.onPrev!, color: active);
  }

  Widget nextButton() {
    if (widget.onNext == null) {
      return const SizedBox();
    }
    return PlayerButtons.next(onPressed: widget.onNext!, color: active);
  }

  Widget playButton() {
    return PlayerButtons.play(
        onPressed: () {
          setState(() {
            controller.play();
          });
        },
        color: active);
  }

  Widget pauseButton() {
    return PlayerButtons.pause(
        onPressed: () {
          setState(() {
            controller.pause();
          });
        },
        color: active);
  }

  Widget playPauseButton() {
    return PlayerStateBuilder(widget.controller,
        builder: (BuildContext context, IPlayerState? state) {
      if (widget.controller is IStandartPlayerController) {
        if (state is PlayingIPlayerState) {
          if (controller.isPlaying()) {
            return pauseButton();
          }
          return playButton();
        }
      }
      return PlayerButtons.play(color: disabled);
    });
  }
}

class StreamBottomControls extends _OutputBottomControls {
  const StreamBottomControls(
      {required IPlayerController controller, void Function()? onPrev, void Function()? onNext})
      : super(controller: controller, onPrev: onPrev, onNext: onNext);

  @override
  _StreamBottomControlsState createState() {
    return _StreamBottomControlsState();
  }
}

class _StreamBottomControlsState extends _OutputBottomControlsState<StreamBottomControls> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[previousButton(), playPauseButton(), nextButton()]));
  }
}

class VodBottomControls extends _OutputBottomControls {
  const VodBottomControls(
      {required IPlayerController controller, void Function()? onPrev, void Function()? onNext})
      : super(controller: controller, onPrev: onPrev, onNext: onNext);

  @override
  _VodBottomControlsState createState() {
    return _VodBottomControlsState();
  }
}

class _VodBottomControlsState extends _OutputBottomControlsState<VodBottomControls> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _timeLine(),
      Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            previousButton(),
            _seekBackward(),
            playPauseButton(),
            _seekForward(),
            nextButton()
          ]))
    ]);
  }

  Widget _timeLine() {
    if (controller is IStandartPlayerController) {
      return LitePlayerTimeline(controller as IStandartPlayerController);
    }
    return const SizedBox();
  }

  Widget _seekBackward() {
    if (controller is IStandartPlayerController) {
      return PlayerStateBuilder(widget.controller,
          builder: (BuildContext context, IPlayerState? state) {
        if (state is PlayingIPlayerState) {
          return PlayerButtons.seekBackward(onPressed: controller.seekBackward, color: active);
        }
        return PlayerButtons.seekBackward(color: disabled);
      });
    }
    return const SizedBox();
  }

  Widget _seekForward() {
    if (controller is IStandartPlayerController) {
      return PlayerStateBuilder(widget.controller,
          builder: (BuildContext context, IPlayerState? state) {
        if (state is PlayingIPlayerState) {
          return PlayerButtons.seekForward(onPressed: controller.seekForward, color: active);
        }
        return PlayerButtons.seekForward(color: disabled);
      });
    }
    return const SizedBox();
  }
}
