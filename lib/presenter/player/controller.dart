import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:player/common/controller.dart';
import 'package:player/player.dart';

class StandartPlayerController extends IStandartPlayerController {
  final String _link;

  @override
  String get currentLink {
    return _link;
  }

  StandartPlayerController(String url)
      : _link = url,
        super();
}

class WebrtcPlayerController extends IWebRTCVideoPLayerController {
  final String _link;

  @override
  String get currentLink {
    return _link;
  }

  WebrtcPlayerController(String url)
      : _link = url,
        super();
}

class WhepVideoPlayerController extends IWhepVideoPlayerController {
  final String _link;

  @override
  String get currentLink {
    return _link;
  }

  WhepVideoPlayerController(String url)
      : _link = url,
        super();
}

IPlayerController makeController(OutputUrl url) {
  if (url is WebRTCOutputUrl) {
    return WebrtcPlayerController(url.uri);
  } else if (url is HttpOutputUrl && url.isWhip()) {
    return WhepVideoPlayerController(url.uri);
  }
  return StandartPlayerController(url.uri);
}
