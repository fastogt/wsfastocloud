part of 'service_info_bloc.dart';

abstract class ServiceInfoState extends Equatable {
  const ServiceInfoState();

  @override
  List<Object?> get props => [];
}

class ServiceInfoInitial extends ServiceInfoState {
  const ServiceInfoInitial();

  @override
  List<Object?> get props => [];
}

class ServiceInfoData extends ServiceInfoState {
  final LiveServer goconfig;

  const ServiceInfoData(this.goconfig);

  @override
  List<Object?> get props => [goconfig];
}

class ServiceInfoFailure extends ServiceInfoState {
  final String description;

  const ServiceInfoFailure(this.description);

  @override
  List<Object?> get props => [description];
}
