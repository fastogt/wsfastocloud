part of 'service_info_bloc.dart';

abstract class ServiceInfoEvent extends Equatable {
  const ServiceInfoEvent();

  @override
  List<Object?> get props => [];
}

class ListenEvent extends ServiceInfoEvent {
  const ListenEvent();

  @override
  List<Object?> get props => [];
}

class InitialEvent extends ServiceInfoEvent {
  const InitialEvent();

  @override
  List<Object?> get props => [];
}

class FetchEvent extends ServiceInfoEvent {
  const FetchEvent();

  @override
  List<Object?> get props => [];
}

class FailureEvent extends ServiceInfoEvent {
  final String description;

  const FailureEvent(this.description);

  @override
  List<Object?> get props => [description];
}
