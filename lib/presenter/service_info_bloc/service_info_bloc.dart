import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dart_common/dart_common.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';

part 'service_info_event.dart';
part 'service_info_state.dart';

class ServiceInfoBloc extends Bloc<ServiceInfoEvent, ServiceInfoState> {
  late final StreamSubscription _subscription;

  final WebSocketApiBloc webSocketApiBloc;
  final Fetcher fetcher;

  ServiceInfoBloc(this.webSocketApiBloc, this.fetcher) : super(const ServiceInfoInitial()) {
    on<ListenEvent>(_listen);
    on<FetchEvent>(_fetch);
    on<InitialEvent>(_init);
    on<FailureEvent>(_failure);
    add(const ListenEvent());
  }

  void dispose() {
    _subscription.cancel();
  }

  void _init(InitialEvent event, Emitter<ServiceInfoState> emit) {
    emit(const ServiceInfoInitial());
  }

  void _listen(ListenEvent event, Emitter<ServiceInfoState> emit) async {
    _subscription = webSocketApiBloc.stream.listen((event) {
      if (event is WebSocketApiMessage) {
      } else if (event is WebSocketFailure) {
        add(FailureEvent(event.description));
      } else if (event is WebSocketConnected) {
        add(const FetchEvent());
      } else {
        add(const InitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(const FetchEvent());
    }
  }

  void _fetch(FetchEvent event, Emitter<ServiceInfoState> emit) async {
    try {
      final response = await fetcher.getServiceInfo();
      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        emit(ServiceInfoFailure(
            'Backend error: invalid response format, code: ${response.statusCode}'));
        return;
      }

      final err = respData.error();
      if (err != null) {
        emit(ServiceInfoFailure('Backend error: ${err.message}'));
        return;
      }

      final live = LiveServer.fromJson(respData.contentMap()!);
      emit(ServiceInfoData(live));
    } catch (e) {
      emit(ServiceInfoFailure('Backend error: $e'));
    }
  }

  void _failure(FailureEvent event, Emitter<ServiceInfoState> emit) {
    emit(ServiceInfoFailure(event.description));
  }
}
