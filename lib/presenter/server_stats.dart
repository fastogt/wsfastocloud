import 'package:fastocloud_dart_media_models/models.dart';
import 'package:fastocloud_dart_media_models/utils.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:fastotv_dart/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/l10n/l10n.dart';

const CIRCLE_HEIGHT = 94.0;
const BOLD_TEXT_SIZE = 20.0;
const TILE_HEIGHT_1 = BOLD_TEXT_SIZE + 8;
const TILE_HEIGHT_2 = TILE_HEIGHT_1 * 2;
const TILE_HEIGHT_3 = TILE_HEIGHT_1 * 3;
const TILE_HEIGHT_4 = TILE_HEIGHT_1 * 4;
const TILE_HEIGHT_5 = TILE_HEIGHT_1 * 5;

String _subtitle(String value) {
  return '$value: ';
}

String _formatTime(int? msSinceEpoch) {
  return DateTime.fromMillisecondsSinceEpoch(msSinceEpoch ?? 0).toString();
}

// Templates
class ServerStatsTile extends StatelessWidget {
  final String title;
  final Widget content;
  final double? height;

  const ServerStatsTile(this.title, this.content, this.height);

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(title, maxLines: 1, overflow: TextOverflow.fade),
                  const SizedBox(height: 4),
                  SizedBox(height: height, child: content)
                ])));
  }
}

class BoldText extends StatelessWidget {
  final String? title;
  final double size;

  const BoldText(this.title, {this.size = BOLD_TEXT_SIZE});

  @override
  Widget build(BuildContext context) {
    return Text(title ?? 'null', style: TextStyle(fontWeight: FontWeight.bold, fontSize: size));
  }
}

class CombinedText extends StatelessWidget {
  final String title;
  final String? data;

  const CombinedText(this.title, this.data);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[Text(title), BoldText(data)]);
  }
}

// Common tiles

class ExpDateTile extends StatelessWidget {
  static const DAY = 86400000;
  static const WEEK = 7 * DAY;
  static const MONTH = 30 * DAY;

  final UtcTimeMsec msSinceEpoch;
  final double? height;

  const ExpDateTile(int? expDate, {this.height = CIRCLE_HEIGHT}) : msSinceEpoch = expDate ?? 0;

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        context.l10n.expirationDate,
        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
          CircleIndicator(fixedDouble(_percentage()), CIRCLE_HEIGHT,
              color: _color(), circleText: _text()),
          Flexible(child: BoldText(_time()))
        ]),
        height);
  }

  // private:
  String _time() {
    final List<String> parts = _formatTime(msSinceEpoch).split(' ');
    return '${parts.first}\n${parts.last}';
  }

  double _percentage() {
    final _now = makeUTCTimestamp();
    if (msSinceEpoch - _now > MONTH) {
      return 100;
    } else if (msSinceEpoch > _now) {
      return MONTH / (msSinceEpoch - _now) * 100;
    } else if (msSinceEpoch == 0) {
      return 0;
    }
    return 100;
  }

  Color _color() {
    final _now = makeUTCTimestamp();
    if (_now > msSinceEpoch) {
      return Colors.redAccent;
    } else if (msSinceEpoch - _now <= WEEK) {
      return Colors.orangeAccent;
    } else if (msSinceEpoch - _now <= MONTH) {
      return Colors.yellowAccent;
    }
    return Colors.greenAccent;
  }

  String _text() {
    final _now = makeUTCTimestamp();
    if (msSinceEpoch > _now) {
      final days = ((msSinceEpoch - _now) / DAY).ceil();
      return '$days\ndays';
    }
    return '0\ndays';
  }
}

class HardwareStatsTile extends StatelessWidget {
  final String title;
  final double value;

  const HardwareStatsTile.cpu(this.value) : title = 'CPU';

  const HardwareStatsTile.gpu(this.value) : title = 'GPU';

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        title, CircleIndicator(fixedDouble(value), CIRCLE_HEIGHT), CIRCLE_HEIGHT);
  }
}

class MemoryStatsTile extends StatelessWidget {
  final int total;
  final int free;
  final String title;

  const MemoryStatsTile.hdd(this.total, this.free) : title = 'HDD';

  const MemoryStatsTile.memory(this.total, this.free) : title = 'RAM';

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(mobile: _mobile, desktop: _desktop);
  }

  Widget _mobile(BuildContext context) {
    return ServerStatsTile(
        title, Center(child: FittedBox(child: _serverStatsTile(context))), CIRCLE_HEIGHT);
  }

  Widget _desktop(BuildContext context) {
    return ServerStatsTile(title, _serverStatsTile(context), CIRCLE_HEIGHT);
  }

  String _format(int value) {
    final double inGb = fixedDouble(value / (1024 * 1024 * 1024));
    return '$inGb Gb';
  }

  Widget _serverStatsTile(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <Widget>[
      CircleIndicator(fixedDouble(((total - free) / total) * 100), CIRCLE_HEIGHT),
      Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        CombinedText(_subtitle(context.l10n.used), _format(total - free)),
        CombinedText(_subtitle(context.l10n.free), _format(free)),
        CombinedText(_subtitle(context.l10n.total), _format(total))
      ])
    ]);
  }
}

class NetworkStatsTile extends StatelessWidget {
  final int bandwidthIn;
  final int bandwidthOut;
  final double height;

  const NetworkStatsTile(this.bandwidthIn, this.bandwidthOut, {double? height})
      : height = height ?? TILE_HEIGHT_2;

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        context.l10n.network, height < TILE_HEIGHT_2 ? _row() : _column(), height);
  }

  // private:
  String _format(int value) {
    final double inMegabytes = fixedDouble(8 * value / (1024 * 1024));
    return '$inMegabytes Mb/s';
  }

  Widget _column() {
    return Column(children: <Widget>[Expanded(child: _in), Expanded(child: _out)]);
  }

  Widget _row() {
    return Row(children: <Widget>[Expanded(child: _in), Expanded(child: _out)]);
  }

  Widget get _in {
    return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[const Icon(Icons.arrow_downward), BoldText(_format(bandwidthIn))]);
  }

  Widget get _out {
    return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[const Icon(Icons.arrow_upward), BoldText(_format(bandwidthOut))]);
  }
}

class OnlineUsersEpgStatsTile extends StatelessWidget {
  final int? daemon;
  final double? height;

  const OnlineUsersEpgStatsTile(this.daemon, {this.height = TILE_HEIGHT_4});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        context.l10n.onlineUsers, CombinedText(_subtitle(context.l10n.daemon), '$daemon'), height);
  }
}

class OnlineUsersLBStatsTile extends StatelessWidget {
  final int? daemon;
  final int? subscribers;
  final double? height;

  const OnlineUsersLBStatsTile(this.daemon, this.subscribers, {this.height = TILE_HEIGHT_4});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        context.l10n.onlineUsers,
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          CombinedText(_subtitle(context.l10n.daemon), '$daemon'),
          CombinedText(_subtitle(context.l10n.subscribers), '$subscribers')
        ]),
        height);
  }
}

class OnlineUsersServerStatsTile extends StatelessWidget {
  final int? daemon;
  final int? cods;
  final int? vods;
  final int? http;
  final double height;

  const OnlineUsersServerStatsTile.server(this.daemon, this.cods, this.vods, this.http,
      {double? height})
      : height = height ?? TILE_HEIGHT_4;

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(
        context.l10n.onlineUsers, height < TILE_HEIGHT_4 ? _rows() : _columns(), height);
  }

  // private:
  Widget _rows() {
    Widget _rowItem(Widget item1, Widget item2) {
      return SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[item1, item2]));
    }

    return Row(children: <Widget>[
      _rowItem(_daemon, _cods),
      const SizedBox(width: 4),
      _rowItem(_vods, _http)
    ]);
  }

  Widget _columns() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[_daemon, _cods, _vods, _http]);
  }

  Widget get _daemon {
    return CombinedText(_subtitle('Daemon'), '$daemon');
  }

  Widget get _cods {
    return CombinedText(_subtitle('CODs'), '$cods');
  }

  Widget get _vods {
    return CombinedText(_subtitle('VODs'), '$vods');
  }

  Widget get _http {
    return CombinedText(_subtitle('Http'), '$http');
  }
}

class OsMediaStatsTile extends StatelessWidget {
  final OperationSystem os;
  final String vsys;
  final double height;

  const OsMediaStatsTile(this.os, this.vsys, {double? height}) : height = height ?? TILE_HEIGHT_4;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (_, constraints) => ServerStatsTile(
            context.l10n.os,
            Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Expanded(
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                CombinedText(_subtitle(context.l10n.name), os.name.toHumanReadable()),
                CombinedText(_subtitle(context.l10n.arch), os.arch),
                _version(context)
              ])),
              if (constraints.maxWidth > height * 2)
                Padding(padding: const EdgeInsets.only(left: 8.0), child: _logo())
            ]),
            height));
  }

  // private:
  Widget _version(BuildContext context) {
    return Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: TILE_HEIGHT_1, child: Center(child: Text('${context.l10n.version}: '))),
          Expanded(
              child: Text(os.version,
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: BOLD_TEXT_SIZE),
                  softWrap: true,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis))
        ]);
  }

  Widget _logo() {
    final platform = os.name.toString();
    if (vsys == "docker") {
      final icon = 'install/assets/platforms/$platform.png';
      const HEIGHT = 64.0;
      final widget = Image.asset(icon, scale: 2);
      return Stack(children: <Widget>[
        const CustomAssetLogo('install/assets/platforms/docker.png'),
        Positioned(left: HEIGHT / 2, top: HEIGHT / 2, right: 0, bottom: 0, child: widget)
      ]);
    }
    return CustomAssetLogo('install/assets/platforms/$platform.png');
  }
}

class StatusTile extends StatelessWidget {
  final ServerStatus? status;
  final double? height;

  const StatusTile(this.status, {this.height = TILE_HEIGHT_1});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(context.l10n.status, BoldText(status.toString()), height);
  }
}

class TimestampTile extends StatelessWidget {
  final int timestampMs;
  final double? height;

  const TimestampTile(this.timestampMs, {this.height = TILE_HEIGHT_4});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(context.l10n.timestamp, BoldText(_formatTime(timestampMs)), height);
  }
}

class UptimeStatsTile extends StatelessWidget {
  final int sec;
  final double? height;

  const UptimeStatsTile(this.sec, {this.height = TILE_HEIGHT_2});

  @override
  Widget build(BuildContext context) {
    return ServerStatsTile(context.l10n.uptime, BoldText(_format), height);
  }

  String get _format {
    final d = Duration(seconds: sec);
    int seconds = d.inSeconds;
    final int days = seconds ~/ Duration.secondsPerDay;
    seconds -= days * Duration.secondsPerDay;
    final int hours = seconds ~/ Duration.secondsPerHour;
    seconds -= hours * Duration.secondsPerHour;
    final int minutes = seconds ~/ Duration.secondsPerMinute;
    seconds -= minutes * Duration.secondsPerMinute;

    final List<String> output = [];
    if (days != 0) {
      output.add('${days}d');
    }
    if (output.isNotEmpty || hours != 0) {
      output.add('${hours}h');
    }
    if (output.isNotEmpty || minutes != 0) {
      output.add('${minutes}m');
    }
    output.add('${seconds}s');

    return output.join(':');
  }
}

class VersionTile extends StatelessWidget {
  final String? project;
  final String? version;
  final double? height;

  const VersionTile(this.project, this.version, {this.height = TILE_HEIGHT_4});

  @override
  Widget build(BuildContext context) {
    String projectToHumanReadable() {
      if (project == 'fastocloud_pro') {
        return 'FastoCloud PRO';
      } else if (project == 'fastocloud_pro_ml') {
        return 'FastoCloud PRO ML';
      } else if (project == 'gofastocloud' || project == 'gofastocloud_backend') {
        return 'GoFastoCloud API';
      }
      return 'FastoCloud';
    }

    final result = projectToHumanReadable();
    return ServerStatsTile('${context.l10n.version} ($result)', BoldText(version), height);
  }
}
