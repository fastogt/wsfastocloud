import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wsfastocloud/data/services/api/models/server_statistics.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_statistics_bloc/server_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_stats_layout.dart';

class ServerStatsStoreWidget extends StatefulWidget {
  const ServerStatsStoreWidget({Key? key}) : super(key: key);

  @override
  State createState() {
    return _ServerStatsStoreWidgetState();
  }
}

class _ServerStatsStoreWidgetState extends State<ServerStatsStoreWidget> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ServiceStatisticsBloc, ServiceStatisticsState>(builder: (context, state) {
      if (state is ServiceStatisticsData) {
        return Column(children: [ServerStatsLayout(state.serverInfo)]);
      } else if (state is ServiceStatisticsFailure) {
        final init = ServerStatistics.createInit();
        return Column(children: [Text(state.description), ServerStatsLayout(init)]);
      }
      final init = ServerStatistics.createInit();
      return Column(children: [ServerStatsLayout(init)]);
    });
  }
}
