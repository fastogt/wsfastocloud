part of 'store_tabs_bloc.dart';

abstract class StoreTabsState extends Equatable {
  const StoreTabsState();

  @override
  List<Object?> get props => [];
}

class StoreTabsInitial extends StoreTabsState {
  const StoreTabsInitial();

  @override
  List<Object?> get props => [];
}

class StoreTabsConnectedState extends StoreTabsState {
  const StoreTabsConnectedState(this.show);

  final Tab show;

  @override
  List<Object?> get props => [show];
}

class StoreTabsFailure extends StoreTabsState {
  const StoreTabsFailure(this.description);

  final String description;

  @override
  List<Object?> get props => [description];
}
