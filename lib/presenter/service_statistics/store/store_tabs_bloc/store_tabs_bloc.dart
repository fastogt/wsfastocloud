import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/tab_enum.dart';

part 'store_tabs_event.dart';

part 'store_tabs_state.dart';

class StoreTabsBloc extends Bloc<StoreTabsEvent, StoreTabsState> {
  late final StreamSubscription _subscription;
  final WebSocketApiBloc webSocketApiBloc;

  StoreTabsBloc(this.webSocketApiBloc) : super(const StoreTabsInitial()) {
    on<ListenEvent>(_listen);
    on<StoreInitialEvent>(_init);
    on<StoreFailureEvent>(_failure);
    on<ConnectEvent>(_connect);
    on<ShowStatsEvent>(_stats);
    on<ShowStreamsEvent>(_streams);
    on<ShowCatchupsEvent>(_catchups);
    on<ShowVodsEvent>(_vods);
    on<ShowEpisodesEvent>(_episodes);
    on<ShowSeasonsEvent>(_seasons);
    on<ShowSerialsEvent>(_serials);
    add(const ListenEvent());
  }

  void dispose() {
    _subscription.cancel();
  }

  void _listen(ListenEvent event, Emitter<StoreTabsState> emit) async {
    _subscription = webSocketApiBloc.stream.listen((state) {
      if (state is WebSocketConnected) {
        add(const ConnectEvent());
      } else if (state is WebSocketFailure) {
        add(StoreFailureEvent(state.description));
      } else if (state is WebSocketApiMessage) {
      } else {
        add(const StoreInitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(const ConnectEvent());
    }
  }

  void _init(StoreInitialEvent event, Emitter<StoreTabsState> emit) {
    emit(const StoreTabsInitial());
  }

  void _connect(ConnectEvent event, Emitter<StoreTabsState> emit) {
    emit(const StoreTabsConnectedState(Tab.streams));
  }

  void _stats(ShowStatsEvent event, Emitter<StoreTabsState> emit) {
    emit(const StoreTabsConnectedState(Tab.stats));
  }

  void _streams(ShowStreamsEvent event, Emitter<StoreTabsState> emit) {
    emit(const StoreTabsConnectedState(Tab.streams));
  }

  void _catchups(ShowCatchupsEvent event, Emitter<StoreTabsState> emit) {
    emit(const StoreTabsConnectedState(Tab.catchups));
  }

  void _vods(ShowVodsEvent event, Emitter<StoreTabsState> emit) {
    emit(const StoreTabsConnectedState(Tab.vods));
  }

  void _episodes(ShowEpisodesEvent event, Emitter<StoreTabsState> emit) {
    emit(const StoreTabsConnectedState(Tab.episodes));
  }

  void _seasons(ShowSeasonsEvent event, Emitter<StoreTabsState> emit) {
    emit(const StoreTabsConnectedState(Tab.seasons));
  }

  void _serials(ShowSerialsEvent event, Emitter<StoreTabsState> emit) {
    emit(const StoreTabsConnectedState(Tab.serials));
  }

  void _failure(StoreFailureEvent event, Emitter<StoreTabsState> emit) {
    emit(StoreTabsFailure(event.description));
  }
}
