part of 'store_tabs_bloc.dart';

abstract class StoreTabsEvent extends Equatable {
  const StoreTabsEvent();

  @override
  List<Object?> get props => [];
}

class ShowStatsEvent extends StoreTabsEvent {
  const ShowStatsEvent();

  @override
  List<Object?> get props => [];
}

class ShowStreamsEvent extends StoreTabsEvent {
  const ShowStreamsEvent();

  @override
  List<Object?> get props => [];
}

class ShowCatchupsEvent extends StoreTabsEvent {
  const ShowCatchupsEvent();

  @override
  List<Object?> get props => [];
}

class ShowSeasonsEvent extends StoreTabsEvent {
  const ShowSeasonsEvent();

  @override
  List<Object?> get props => [];
}

class ShowSerialsEvent extends StoreTabsEvent {
  const ShowSerialsEvent();

  @override
  List<Object?> get props => [];
}

class ShowEpisodesEvent extends StoreTabsEvent {
  const ShowEpisodesEvent();

  @override
  List<Object?> get props => [];
}

class ShowVodsEvent extends StoreTabsEvent {
  const ShowVodsEvent();

  @override
  List<Object?> get props => [];
}

class ListenEvent extends StoreTabsEvent {
  const ListenEvent();

  @override
  List<Object?> get props => [];
}

class StoreInitialEvent extends StoreTabsEvent {
  const StoreInitialEvent();

  @override
  List<Object?> get props => [];
}

class ConnectEvent extends StoreTabsEvent {
  const ConnectEvent();

  @override
  List<Object?> get props => [];
}

class StoreFailureEvent extends StoreTabsEvent {
  final String description;

  const StoreFailureEvent(this.description);

  @override
  List<Object?> get props => [description];
}
