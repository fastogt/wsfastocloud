import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/data/services/api/models/server_statistics.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/service_info_bloc/service_info_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/stats_buttons_wrap.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_statistics_bloc/server_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_stats_layout.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/store_tabs_bloc/store_tabs_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/tab_enum.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_data_source.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_store_bloc/seasons_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/serials_data_source.dart';
import 'package:wsfastocloud/presenter/streams/store/serials_store_bloc/serials_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/store_catchups_widget.dart';
import 'package:wsfastocloud/presenter/streams/store/store_episodes_widget.dart';
import 'package:wsfastocloud/presenter/streams/store/store_live_widget.dart';
import 'package:wsfastocloud/presenter/streams/store/store_seasons_widget.dart';
import 'package:wsfastocloud/presenter/streams/store/store_serials_widget.dart';
import 'package:wsfastocloud/presenter/streams/store/store_vods_widget.dart';
import 'package:wsfastocloud/presenter/widgets/error_dialog.dart';

class ServerStatsStoreDescWidget extends StatefulWidget {
  final WsMode mode;
  final MediaServerInfoEx? mediaServerInfo;

  const ServerStatsStoreDescWidget({Key? key, required this.mode, required this.mediaServerInfo})
      : super(key: key);

  @override
  State<ServerStatsStoreDescWidget> createState() {
    return _ServerStatsStoreDescWidgetState();
  }
}

class _ServerStatsStoreDescWidgetState extends State<ServerStatsStoreDescWidget> {
  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return BlocBuilder<StoreTabsBloc, StoreTabsState>(builder: (context, state) {
        if (state is StoreTabsConnectedState) {
          if (state.show.isStats) {
            final _init = ServerStatistics.createInit();
            return Column(children: <Widget>[
              _StatsStoreHeader(mode: widget.mode),
              SizedBox(
                  height: sizingInformation.isMobile ? 1300 : 430,
                  child: BlocBuilder<ServiceStatisticsBloc, ServiceStatisticsState>(
                      builder: (context, state) {
                    if (state is ServiceStatisticsData) {
                      return ServerStatsLayout(state.serverInfo);
                    } else if (state is ServiceStatisticsFailure) {
                      return ErrorDialog(error: state.description);
                    }
                    return ServerStatsLayout(_init);
                  }))
            ]);
          } else if (state.show.isSeasons) {
            return Column(children: [
              _StatsStoreHeader(mode: widget.mode),
              BlocBuilder<SeasonsBloc, SeasonsState>(builder: (context, state) {
                if (state is SeasonsDataState) {
                  return SeasonsListLayout(state.seasonsSource);
                } else if (state is SeasonsFailureState) {
                  return ErrorDialog(error: state.description);
                }
                return SeasonsListLayout(SeasonsSource([]));
              })
            ]);
          } else if (state.show.isSerials) {
            return Column(children: [
              _StatsStoreHeader(mode: widget.mode),
              BlocBuilder<SerialsBloc, SerialsState>(builder: (context, state) {
                if (state is SerialsDataState) {
                  return SerialsListLayout(state.serialsSource);
                } else if (state is SerialsFailureState) {
                  return ErrorDialog(error: state.description);
                }
                return SerialsListLayout(SerialsSource([]));
              })
            ]);
          } else if (state.show.isStreams) {
            return Column(children: [
              _StatsStoreHeader(mode: widget.mode),
              BlocBuilder<ServiceInfoBloc, ServiceInfoState>(builder: (context, state) {
                if (state is ServiceInfoData) {
                  if (widget.mediaServerInfo != null) {
                    final copy = GoLiveServerWithoutFolders(
                        live: state.goconfig, media: widget.mediaServerInfo!);
                    return StoreLiveWidget(mediaServer: copy);
                  }
                  return StoreLiveWidget(mediaServer: state.goconfig);
                }
                return const StoreLiveWidget(mediaServer: null);
              })
            ]);
          } else if (state.show.isCatchups) {
            return Column(children: [
              _StatsStoreHeader(mode: widget.mode),
              BlocBuilder<ServiceInfoBloc, ServiceInfoState>(builder: (context, state) {
                if (state is ServiceInfoData) {
                  if (widget.mediaServerInfo != null) {
                    final copy = GoLiveServerWithoutFolders(
                        live: state.goconfig, media: widget.mediaServerInfo!);
                    return StoreCatchupsWidget(mediaServer: copy);
                  }
                  return StoreCatchupsWidget(mediaServer: state.goconfig);
                }
                return const StoreCatchupsWidget(mediaServer: null);
              })
            ]);
          } else if (state.show.isEpisodes) {
            return Column(children: [
              _StatsStoreHeader(mode: widget.mode),
              BlocBuilder<ServiceInfoBloc, ServiceInfoState>(builder: (context, state) {
                if (state is ServiceInfoData) {
                  if (widget.mediaServerInfo != null) {
                    final copy = GoLiveServerWithoutFolders(
                        live: state.goconfig, media: widget.mediaServerInfo!);
                    return StoreEpisodesWidget(mediaServer: copy);
                  }
                  return StoreEpisodesWidget(mediaServer: state.goconfig);
                }
                return const StoreEpisodesWidget(mediaServer: null);
              })
            ]);
          } else if (state.show.isVods) {
            return Column(children: [
              _StatsStoreHeader(mode: widget.mode),
              BlocBuilder<ServiceInfoBloc, ServiceInfoState>(builder: (context, state) {
                if (state is ServiceInfoData) {
                  if (widget.mediaServerInfo != null) {
                    final copy = GoLiveServerWithoutFolders(
                        live: state.goconfig, media: widget.mediaServerInfo!);
                    return StoreVodsWidget(mediaServer: copy);
                  }
                  return StoreVodsWidget(mediaServer: state.goconfig);
                }
                return const StoreVodsWidget(mediaServer: null);
              })
            ]);
          }
        } else if (state is StoreTabsFailure) {
          return Column(children: [
            _StatsStoreHeader(mode: widget.mode),
            ErrorDialog(error: state.description)
          ]);
        }
        return Column(children: [
          _StatsStoreHeader(mode: widget.mode),
          ServerStatsLayout(ServerStatistics.createInit())
        ]);
      });
    });
  }
}

class _StatsStoreHeader extends StatefulWidget {
  final WsMode mode;

  const _StatsStoreHeader({Key? key, required this.mode}) : super(key: key);

  @override
  State<_StatsStoreHeader> createState() {
    return _StatsStoreHeaderState();
  }
}

class _StatsStoreHeaderState extends State<_StatsStoreHeader> {
  @override
  Widget build(BuildContext context) {
    return StatsButtonsWrap(buttons: _desktop());
  }

  Widget _desktop() {
    return BlocBuilder<StoreTabsBloc, StoreTabsState>(builder: (context, state) {
      final isCCTV = widget.mode == WsMode.CCTV || widget.mode == WsMode.DRONE;
      if (state is StoreTabsConnectedState) {
        return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          showStatsButton(!state.show.isStats),
          showStreamsButton(!state.show.isStreams),
          if (widget.mode == WsMode.IPTV) showCatchupsButton(!state.show.isCatchups),
          if (!isCCTV) showVodsButton(!state.show.isVods),
          if (!isCCTV) episodesButton(!state.show.isEpisodes),
          if (!isCCTV) seasonsButton(!state.show.isSeasons),
          if (!isCCTV) serialsButton(!state.show.isSerials)
        ]);
      }
      return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        showStatsButton(false),
        showStreamsButton(false),
        if (widget.mode == WsMode.IPTV) showCatchupsButton(false),
        if (!isCCTV) showVodsButton(false),
        if (!isCCTV) episodesButton(false),
        if (!isCCTV) seasonsButton(false),
        if (!isCCTV) serialsButton(false)
      ]);
    });
  }

  StatsButton showStatsButton(bool active) {
    return StatsButton(
        title: context.l10n.showStatistics, onPressed: active ? () => _showStats() : null);
  }

  StatsButton showStreamsButton(bool active) {
    return StatsButton(
        title: context.l10n.showStreams, onPressed: active ? () => _showStreams() : null);
  }

  StatsButton showCatchupsButton(bool active) {
    return StatsButton(
        title: context.l10n.showCatchups, onPressed: active ? () => _showCatchups() : null);
  }

  StatsButton showVodsButton(bool active) {
    return StatsButton(title: context.l10n.showVods, onPressed: active ? () => _showVods() : null);
  }

  StatsButton episodesButton(bool active) {
    return StatsButton(
        title: context.l10n.showEpisodes, onPressed: active ? () => _showEpisodes() : null);
  }

  StatsButton seasonsButton(bool active) {
    return StatsButton(
        title: context.l10n.showSeasons, onPressed: active ? () => _showSeasons() : null);
  }

  StatsButton serialsButton(bool active) {
    return StatsButton(
        title: context.l10n.showSeries, onPressed: active ? () => _showSerials() : null);
  }

  // buttons
  void _showStats() {
    context.read<StoreTabsBloc>().add(const ShowStatsEvent());
  }

  void _showStreams() {
    context.read<StoreTabsBloc>().add(const ShowStreamsEvent());
  }

  void _showCatchups() {
    context.read<StoreTabsBloc>().add(const ShowCatchupsEvent());
  }

  void _showVods() {
    context.read<StoreTabsBloc>().add(const ShowVodsEvent());
  }

  void _showSeasons() {
    context.read<StoreTabsBloc>().add(const ShowSeasonsEvent());
  }

  void _showEpisodes() {
    context.read<StoreTabsBloc>().add(const ShowEpisodesEvent());
  }

  void _showSerials() {
    context.read<StoreTabsBloc>().add(const ShowSerialsEvent());
  }
}
