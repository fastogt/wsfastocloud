import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dart_common/dart_common.dart';

import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/server_statistics.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';

part 'server_statistics_event.dart';

part 'server_statistics_state.dart';

class ServiceStatisticsBloc extends Bloc<ServiceStatisticsEvent, ServiceStatisticsState> {
  ServiceStatisticsBloc(this.webSocketApiBloc, Fetcher fetcher)
      : _fetcher = fetcher,
        super(const ServiceStatisticsInitial()) {
    on<ListenEvent>(_listen);
    on<FetchServerStatisticsEvent>(_fetch);
    on<UpdateServerStatsEvent>(_updateStats);
    on<InitialEvent>(_init);
    on<FailureEvent>(_failure);
    add(const ListenEvent());
  }

  late final StreamSubscription _subscription;
  final WebSocketApiBloc webSocketApiBloc;
  final Fetcher _fetcher;

  void dispose() {
    _subscription.cancel();
  }

  Future<List<VodDetails>> tmdbMoviesRequest(String name) {
    final defaults = _fetcher.defaults();
    final response = _fetcher.post('/server/tmdb/search/movies', {
      'text': name,
      'stream_logo_url': defaults.streamLogoIcon,
      'background_url': defaults.backgroundUrl,
      'language': 'en'
    });
    return response.then((value) {
      final List<VodDetails> result = [];
      if (value.statusCode == HttpStatus.ok) {
        final respData = httpDataResponseFromString(value.body);
        final data = respData!.contentMap()!;

        final movies = data['movies'];
        movies.forEach((s) {
          final res = VodDetails.fromJson(s);
          result.add(res);
        });
      }
      return result;
    });
  }

  Future<VodDetails> tmdbMoviesSecondRequest(String? id) {
    final defaults = _fetcher.defaults();
    final response = _fetcher.post('/server/tmdb/search/movies/$id', {
      'stream_logo_url': defaults.streamLogoIcon,
      'background_url': defaults.backgroundUrl,
      'language': 'en'
    });
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      final movie = data['movie'];
      final res = VodDetails.fromJson(movie);
      return res;
    });
  }

  Future<List<VodDetails>> tmdbEpisodesRequest(String name) {
    final defaults = _fetcher.defaults();
    final response = _fetcher.post('/server/tmdb/search/episodes', {
      'text': name,
      'stream_logo_url': defaults.streamLogoIcon,
      'background_url': defaults.backgroundUrl,
      'language': 'en'
    });
    return response.then((value) {
      final List<VodDetails> result = [];
      if (value.statusCode == HttpStatus.ok) {
        final respData = httpDataResponseFromString(value.body);
        final data = respData!.contentMap()!;
        final movies = data['movies'];
        movies.forEach((s) {
          final res = VodDetails.fromJson(s);
          result.add(res);
        });
      }
      return result;
    });
  }

  Future<VodDetails> tmdbEpisodesSecondRequest(String? id) {
    final defaults = _fetcher.defaults();
    final response = _fetcher.post('/server/tmdb/search/episodes/$id', {
      'stream_logo_url': defaults.streamLogoIcon,
      'background_url': defaults.backgroundUrl,
      'language': 'en'
    });
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      final movie = data['movie'];
      final res = VodDetails.fromJson(movie);
      return res;
    });
  }

  void _listen(ListenEvent event, Emitter<ServiceStatisticsState> emit) async {
    _subscription = webSocketApiBloc.stream.listen((state) {
      if (state is WebSocketApiMessage) {
        if (state.type == 'statistic_service') {
          final serverStats = ServerStatistics.fromJson(state.data);
          add(UpdateServerStatsEvent(serverStats));
        }
      } else if (state is WebSocketFailure) {
        add(FailureEvent(state.description));
      } else if (state is WebSocketConnected) {
        add(const FetchServerStatisticsEvent());
      } else {
        add(const InitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(const FetchServerStatisticsEvent());
    }
  }

  void _fetch(FetchServerStatisticsEvent event, Emitter<ServiceStatisticsState> emit) async {
    try {
      final response = await _fetcher.getServerStatistics();
      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        emit(ServiceStatisticsFailure(
            'Backend error: invalid response format, code: ${response.statusCode}'));
        return;
      }
      final err = respData.error();
      if (err != null) {
        emit(ServiceStatisticsFailure('Backend error: ${err.message}'));
        return;
      }

      final content = respData.contentMap();
      final serverInfo = ServerStatistics.fromJson(content!);
      final url = _fetcher.backendServerUrl!;
      emit(ServiceStatisticsData(serverInfo: serverInfo, url: url));
    } catch (e) {
      emit(ServiceStatisticsFailure('Backend error: $e'));
    }
  }

  void _updateStats(UpdateServerStatsEvent event, Emitter<ServiceStatisticsState> emit) async {
    final url = _fetcher.backendServerUrl!;
    emit(ServiceStatisticsData(serverInfo: event.serverInfo, url: url));
  }

  void _init(InitialEvent event, Emitter<ServiceStatisticsState> emit) {
    emit(const ServiceStatisticsInitial());
  }

  void _failure(FailureEvent event, Emitter<ServiceStatisticsState> emit) {
    emit(ServiceStatisticsFailure(event.description));
  }
}
