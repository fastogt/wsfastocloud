import 'package:flutter/cupertino.dart';
import 'package:responsive_builder/responsive_builder.dart';

class StatsButtonsWrap extends StatefulWidget {
  final Widget buttons;

  const StatsButtonsWrap({Key? key, required this.buttons}) : super(key: key);

  @override
  _StatsButtonsWrapState createState() {
    return _StatsButtonsWrapState();
  }
}

class _StatsButtonsWrapState extends State<StatsButtonsWrap> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(mobile: _mobile, desktop: _desktop);
  }

  Widget _mobile(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Expanded(
          child: Align(
              alignment: Alignment.centerRight,
              child: FittedBox(
                  child: Padding(
                      padding: const EdgeInsets.symmetric(),
                      child: Row(children: _wrapButtons())))))
    ]);
  }

  Widget _desktop(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [Expanded(child: _buttonsRow())]));
  }

  Widget _buttonsRow() {
    return Align(
        alignment: Alignment.centerRight,
        child: FittedBox(
            child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(children: _wrapButtons()))));
  }

  List<Widget> _wrapButtons() {
    return [Padding(padding: const EdgeInsets.symmetric(horizontal: 8), child: widget.buttons)];
  }
}
