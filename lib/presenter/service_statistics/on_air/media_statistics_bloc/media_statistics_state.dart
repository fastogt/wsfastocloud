part of 'media_statistics_bloc.dart';

abstract class MediaStatisticsState extends Equatable {
  const MediaStatisticsState();

  @override
  List<Object?> get props => [];
}

class MediaStatisticsInitial extends MediaStatisticsState {
  const MediaStatisticsInitial();

  @override
  List<Object?> get props => [];
}

class MediaStatisticsData extends MediaStatisticsState {
  final MediaServerInfoEx mediaInfo;

  const MediaStatisticsData({required this.mediaInfo});

  @override
  List<Object?> get props => [mediaInfo];
}

class MediaStatisticsFailure extends MediaStatisticsState {
  final String description;

  const MediaStatisticsFailure(this.description);

  @override
  List<Object?> get props => [description];
}
