part of 'media_statistics_bloc.dart';

abstract class MediaStatisticsEvent extends Equatable {
  const MediaStatisticsEvent();

  @override
  List<Object?> get props => [];
}

class ListenEvent extends MediaStatisticsEvent {
  const ListenEvent();

  @override
  List<Object?> get props => [];
}

class MediaInitialEvent extends MediaStatisticsEvent {
  const MediaInitialEvent();

  @override
  List<Object?> get props => [];
}

class FetchMediaStatisticsEvent extends MediaStatisticsEvent {
  const FetchMediaStatisticsEvent();

  @override
  List<Object?> get props => [];
}

class FailureEvent extends MediaStatisticsEvent {
  final String description;

  const FailureEvent(this.description);

  @override
  List<Object?> get props => [description];
}

class UpdateMediaStatsEvent extends MediaStatisticsEvent {
  final MediaServerInfo mediaStats;

  const UpdateMediaStatsEvent(this.mediaStats);

  @override
  List<Object?> get props => [mediaStats];
}
