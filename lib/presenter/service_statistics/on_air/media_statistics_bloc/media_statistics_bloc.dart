import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dart_common/dart_common.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';

part 'media_statistics_event.dart';
part 'media_statistics_state.dart';

class MediaState {
  static const _STATUS_FIELD = 'status';

  ServerStatus status;

  MediaState({required this.status});

  factory MediaState.fromJson(Map<String, dynamic> json) {
    final status = ServerStatus.fromInt(json[_STATUS_FIELD]);
    return MediaState(status: status);
  }
}

class MediaStatisticsBloc extends Bloc<MediaStatisticsEvent, MediaStatisticsState> {
  MediaStatisticsBloc(this.webSocketApiBloc, this.fetcher) : super(const MediaStatisticsInitial()) {
    on<ListenEvent>(_listen);
    on<FetchMediaStatisticsEvent>(_fetchMediaStatistics);
    on<UpdateMediaStatsEvent>(_updateMediaStats);
    on<FailureEvent>(_failure);
    on<MediaInitialEvent>(_init);
    add(const ListenEvent());
  }

  late final StreamSubscription _subscription;
  final WebSocketApiBloc webSocketApiBloc;
  final Fetcher fetcher;

  void dispose() {
    _subscription.cancel();
  }

  void _listen(ListenEvent event, Emitter<MediaStatisticsState> emit) {
    _subscription = webSocketApiBloc.stream.listen((state) {
      if (state is WebSocketApiMessage) {
        if (state.type == 'media_statistic_service') {
          final serverStats = MediaServerInfo.fromJson(state.data);
          add(UpdateMediaStatsEvent(serverStats));
        } else if (state.type == 'media_state_service') {
          final status = MediaState.fromJson(state.data);
          if (status.status == ServerStatus.INIT) {
            add(const MediaInitialEvent());
          } else if (status.status == ServerStatus.CONNECTED) {
            if (state is! MediaStatisticsData) {
              // trying reconnect the panel
              add(const FetchMediaStatisticsEvent());
            }
          }
        }
      } else if (state is WebSocketFailure) {
        emit(MediaStatisticsFailure(state.description));
      } else if (state is WebSocketConnected) {
        add(const FetchMediaStatisticsEvent());
      } else {
        add(const MediaInitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(const FetchMediaStatisticsEvent());
    }
  }

  void _fetchMediaStatistics(
      FetchMediaStatisticsEvent event, Emitter<MediaStatisticsState> emit) async {
    try {
      final response = await fetcher.getMediaServiceStatistics();
      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        emit(MediaStatisticsFailure(
            'Backend error: invalid response format, code: ${response.statusCode}'));
        return;
      }

      final err = respData.error();
      if (err != null) {
        add(FailureEvent('Backend error: ${err.message}'));
        return;
      }

      final content = respData.contentMap();
      final mediaInfo = MediaServerInfoEx.fromJson(content!);
      emit(MediaStatisticsData(mediaInfo: mediaInfo));
    } catch (e) {
      emit(MediaStatisticsFailure('Backend error: $e'));
    }
  }

  void _updateMediaStats(UpdateMediaStatsEvent event, Emitter<MediaStatisticsState> emit) {
    if (state is MediaStatisticsData) {
      final oldMedia = (state as MediaStatisticsData).mediaInfo;
      final merge = _mergeMediaStats(oldMedia, event.mediaStats);
      emit(MediaStatisticsData(mediaInfo: merge));
    } else {
      // emit(MediaStatisticsData(mediaInfo: event.mediaStats));
    }
  }

  void _init(MediaInitialEvent event, Emitter<MediaStatisticsState> emit) {
    emit(const MediaStatisticsInitial());
  }

  MediaServerInfoEx _mergeMediaStats(MediaServerInfoEx? old, MediaServerInfo value) {
    OperationSystem os = OperationSystem.createUnknown();
    LicenseProjectInfo lic = LicenseProjectInfo(project: '', version: '', expirationTime: 0);
    String vsys = '';
    String vrole = '';
    if (old != null) {
      os = old.os;
      lic = old.project;
      vrole = old.vrole;
      vsys = old.vsystem;
    }
    return MediaServerInfoEx(
        cpu: value.cpu,
        gpu: value.gpu,
        loadAverage: value.loadAverage,
        memoryTotal: value.memoryTotal,
        memoryFree: value.memoryFree,
        hddTotal: value.hddTotal,
        hddFree: value.hddFree,
        bandwidthIn: value.bandwidthIn,
        bandwidthOut: value.bandwidthOut,
        uptime: value.uptime,
        timestamp: value.timestamp,
        totalBytesIn: value.totalBytesIn,
        totalBytesOut: value.totalBytesOut,
        //
        onlineUsers: value.onlineUsers,
        os: os,
        project: lic,
        vsystem: vsys,
        vrole: vrole);
  }

  void _failure(FailureEvent event, Emitter<MediaStatisticsState> emit) {
    emit(MediaStatisticsFailure(event.description));
  }
}
