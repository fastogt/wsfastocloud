part of 'on_air_tab_bloc.dart';

abstract class OnAirTabEvent extends Equatable {
  const OnAirTabEvent();

  @override
  List<Object?> get props => [];
}

class ShowStatsEvent extends OnAirTabEvent {
  const ShowStatsEvent();

  @override
  List<Object?> get props => [];
}

class ShowStreamsEvent extends OnAirTabEvent {
  const ShowStreamsEvent();

  @override
  List<Object?> get props => [];
}

class ShowCatchupsEvent extends OnAirTabEvent {
  const ShowCatchupsEvent();

  @override
  List<Object?> get props => [];
}

class ListenEvent extends OnAirTabEvent {
  const ListenEvent();

  @override
  List<Object?> get props => [];
}

class InitialEvent extends OnAirTabEvent {
  const InitialEvent();

  @override
  List<Object?> get props => [];
}

class ConnectEvent extends OnAirTabEvent {
  const ConnectEvent();

  @override
  List<Object?> get props => [];
}

class FailureEvent extends OnAirTabEvent {
  final String description;

  const FailureEvent(this.description);

  @override
  List<Object?> get props => [description];
}
