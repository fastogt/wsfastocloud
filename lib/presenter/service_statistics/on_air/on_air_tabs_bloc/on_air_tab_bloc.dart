import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/tab_enum.dart';

part 'on_air_tab_event.dart';
part 'on_air_tab_state.dart';

class OnAirTabBloc extends Bloc<OnAirTabEvent, OnAirTabState> {
  OnAirTabBloc(this.webSocketApiBloc) : super(const OnAirTabInitial()) {
    on<ListenEvent>(_listen);
    on<InitialEvent>(_init);
    on<FailureEvent>(_failure);
    on<ConnectEvent>(_connect);
    on<ShowStatsEvent>(_stats);
    on<ShowStreamsEvent>(_streams);
    on<ShowCatchupsEvent>(_catchups);
    add(const ListenEvent());
  }

  late final StreamSubscription _subscription;
  final WebSocketApiBloc webSocketApiBloc;

  void dispose() {
    _subscription.cancel();
  }

  void _listen(ListenEvent event, Emitter<OnAirTabState> emit) async {
    _subscription = webSocketApiBloc.stream.listen((state) {
      if (state is WebSocketConnected) {
        add(const ConnectEvent());
      } else if (state is WebSocketFailure) {
        add(FailureEvent(state.description));
      } else if (state is WebSocketApiMessage) {
      } else {
        add(const InitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(const ConnectEvent());
    }
  }

  void _init(InitialEvent event, Emitter<OnAirTabState> emit) {
    emit(const OnAirTabInitial());
  }

  void _connect(ConnectEvent event, Emitter<OnAirTabState> emit) {
    emit(const OnAirTabConnectedState(Tab.streams));
  }

  void _stats(ShowStatsEvent event, Emitter<OnAirTabState> emit) {
    emit(const OnAirTabConnectedState(Tab.stats));
  }

  void _streams(ShowStreamsEvent event, Emitter<OnAirTabState> emit) {
    emit(const OnAirTabConnectedState(Tab.streams));
  }

  void _catchups(ShowCatchupsEvent event, Emitter<OnAirTabState> emit) {
    emit(const OnAirTabConnectedState(Tab.catchups));
  }

  void _failure(FailureEvent event, Emitter<OnAirTabState> emit) {
    emit(OnAirTabFailure(event.description));
  }
}
