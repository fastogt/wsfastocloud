import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastocloud_dart_media_models/models.dart' as models;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/errors.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_statistics_bloc/media_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_stats_layout.dart';
import 'package:wsfastocloud/presenter/service_statistics/stats_buttons_wrap.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';

class ServerStatsOnAirWidget extends StatelessWidget {
  const ServerStatsOnAirWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MediaStatisticsBloc, MediaStatisticsState>(builder: (context, state) {
      if (state is MediaStatisticsData) {
        return Column(children: [
          _StatsAirHeader(server: state.mediaInfo, connected: true),
          MediaStatsLayout(state.mediaInfo)
        ]);
      }
      if (state is MediaStatisticsFailure) {
        return Column(children: [
          _StatsAirHeader(server: MediaServerInfoEx.createInit(), connected: false),
          Text(state.description),
          MediaStatsLayout(MediaServerInfoEx.createInit())
        ]);
      }
      return Column(children: [
        _StatsAirHeader(server: MediaServerInfoEx.createInit(), connected: false),
        MediaStatsLayout(MediaServerInfoEx.createInit())
      ]);
    });
  }
}

class _StatsAirHeader extends StatelessWidget {
  final models.MediaServerInfoEx? server;
  final bool connected;

  const _StatsAirHeader({Key? key, required this.server, required this.connected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StatsButtonsWrap(buttons: _buttonsRow(context));
  }

  Widget _buttonsRow(BuildContext context) {
    return SizedBox(
        child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [getLogButton(context)]));
  }

  StatsButton getLogButton(BuildContext context) {
    return StatsButton(
        title: 'Get log',
        onPressed: server != null ? (connected ? () => _getLog(context) : null) : null);
  }

  void _getLog(BuildContext context) {
    context.read<Fetcher>().launchUrlEx('/media/logs').then((resp) {}, onError: (error) {
      showError(context, error);
    });
  }
}
