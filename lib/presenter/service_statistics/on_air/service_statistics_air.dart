import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastocloud_dart_media_models/models.dart' as models;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/errors.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_statistics_bloc/media_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_stats_layout.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/on_air_tabs_bloc/on_air_tab_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/stats_buttons_wrap.dart';
import 'package:wsfastocloud/presenter/service_statistics/tab_enum.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_on_air.dart';
import 'package:wsfastocloud/presenter/widgets/error_dialog.dart';

class ServerStatsOnAirDescWidget extends StatelessWidget {
  const ServerStatsOnAirDescWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return BlocBuilder<OnAirTabBloc, OnAirTabState>(builder: (context, state) {
        final init = MediaServerInfoEx.createInit();
        if (state is OnAirTabConnectedState) {
          if (state.show.isStats) {
            return SizedBox(
                height: sizingInformation.isMobile ? 1300 : 520,
                child: BlocBuilder<MediaStatisticsBloc, MediaStatisticsState>(
                  builder: (context, state) {
                    if (state is MediaStatisticsData) {
                      return Column(children: [
                        _StatsAirHeader(server: state.mediaInfo, connected: true),
                        MediaStatsLayout(state.mediaInfo)
                      ]);
                    } else if (state is MediaStatisticsFailure) {
                      return Column(children: [
                        _StatsAirHeader(server: init, connected: false),
                        ErrorDialog(error: state.description)
                      ]);
                    }
                    return Column(children: [
                      _StatsAirHeader(server: init, connected: false),
                      MediaStatsLayout(init)
                    ]);
                  },
                ));
          } else if (state.show.isStreams) {
            return Column(children: [
              BlocBuilder<MediaStatisticsBloc, MediaStatisticsState>(builder: (context, state) {
                if (state is MediaStatisticsData) {
                  return _StatsAirHeader(server: state.mediaInfo, connected: true);
                }
                return _StatsAirHeader(server: init, connected: false);
              }),
              const OnAirStreamsTabs()
            ]);
          } else if (state.show.isCatchups) {
            return Column(children: [
              BlocBuilder<MediaStatisticsBloc, MediaStatisticsState>(builder: (context, state) {
                if (state is MediaStatisticsData) {
                  return _StatsAirHeader(server: state.mediaInfo, connected: true);
                }
                return _StatsAirHeader(server: init, connected: false);
              }),
              const OnAirStreamsTabs()
            ]);
          }
        } else if (state is OnAirTabFailure) {
          final init = MediaServerInfoEx.createInit();
          return Column(children: [
            _StatsAirHeader(server: init, connected: false),
            ErrorDialog(error: state.description),
          ]);
        }
        return Column(
            children: [_StatsAirHeader(server: init, connected: false), MediaStatsLayout(init)]);
      });
    });
  }
}

class _StatsAirHeader extends StatefulWidget {
  final models.MediaServerInfoEx? server;
  final bool connected;

  const _StatsAirHeader({Key? key, required this.server, required this.connected})
      : super(key: key);

  @override
  State<_StatsAirHeader> createState() {
    return _StatsAirHeaderState();
  }
}

class _StatsAirHeaderState extends State<_StatsAirHeader> {
  @override
  Widget build(BuildContext context) {
    return StatsButtonsWrap(buttons: _buttonsRow(context));
  }

  Widget _buttonsRow(BuildContext context) {
    return BlocBuilder<OnAirTabBloc, OnAirTabState>(builder: (context, state) {
      if (state is OnAirTabConnectedState) {
        return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          showStatsButton(context, !state.show.isStats),
          showStreamsButton(context, !state.show.isStreams),
          Row(children: [getLogButton(context)])
        ]);
      }
      return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        showStatsButton(context, false),
        showStreamsButton(context, false),
        Row(children: [getLogButton(context)])
      ]);
    });
  }

  StatsButton getLogButton(BuildContext context) {
    return StatsButton(
        title: context.l10n.getLog,
        onPressed:
            widget.server != null ? (widget.connected ? () => _getLog(context) : null) : null);
  }

  StatsButton showStatsButton(BuildContext context, bool active) {
    return StatsButton(
        title: context.l10n.showStatistics, onPressed: active ? () => _showStats(context) : null);
  }

  StatsButton showStreamsButton(BuildContext context, bool active) {
    return StatsButton(
        title: context.l10n.showStreams, onPressed: active ? () => _showStreams(context) : null);
  }

  void _getLog(BuildContext context) {
    context.read<Fetcher>().launchUrlEx('/media/logs').then((resp) {}, onError: (error) {
      showError(context, error);
    });
  }

  void _showStats(BuildContext context) {
    context.read<OnAirTabBloc>().add(const ShowStatsEvent());
  }

  void _showStreams(BuildContext context) {
    context.read<OnAirTabBloc>().add(const ShowStreamsEvent());
  }
}
