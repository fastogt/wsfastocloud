enum Tab { stats, streams, catchups, vods, episodes, seasons, serials }

extension TabX on Tab {
  bool get isStats => this == Tab.stats;

  bool get isStreams => this == Tab.streams;

  bool get isCatchups => this == Tab.catchups;

  bool get isVods => this == Tab.vods;

  bool get isEpisodes => this == Tab.episodes;

  bool get isSeasons => this == Tab.seasons;

  bool get isSerials => this == Tab.serials;
}
