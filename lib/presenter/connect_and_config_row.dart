import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:provider/provider.dart' as prov;
import 'package:provider/provider.dart';
import 'package:wsfastocloud/app_colors.dart';
import 'package:wsfastocloud/data/repositories/resource_utilization_repository.dart';
import 'package:wsfastocloud/data/repositories/store_editor.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/connection_info.dart';
import 'package:wsfastocloud/data/services/local_storage/local_storage_service.dart';
import 'package:wsfastocloud/l10n/app_localization.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/l10n/locale_provider.dart';
import 'package:wsfastocloud/locator.dart';
import 'package:wsfastocloud/presenter/connection_bloc/connection_bloc.dart';
import 'package:wsfastocloud/presenter/main_tab_bar.dart';
import 'package:wsfastocloud/presenter/resource_utilization_bloc/resource_utilization_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_statistics_bloc/media_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/store_tabs_bloc/store_tabs_bloc.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/connect_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/import/import_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/master_config_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/master_connect_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/master_fetcher.dart';
import 'package:wsfastocloud/presenter/widgets/resource_utilization_dialog/resource_utilization_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/select_file_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/select_file_web_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/select_m3u_package_dialog.dart';

class LayoutWidget extends StatelessWidget {
  final WsMode mode;

  const LayoutWidget({Key? key, required this.mode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StoreTabsBloc, StoreTabsState>(builder: (context, state) {
      final connected = state is StoreTabsConnectedState;
      return ListView(
          physics: const NeverScrollableScrollPhysics(),
          children: [_StatsHeader(connected: connected, mode: mode), OnAirStoreTabBar(mode: mode)]);
    });
  }
}

class _StatsHeader extends StatelessWidget {
  final bool connected;
  final WsMode mode;

  static const availableTypesIfHaveMedia = [StreamType.PROXY, StreamType.RELAY, StreamType.ENCODE];
  static const availableTypes = [StreamType.PROXY];

  const _StatsHeader({Key? key, required this.connected, required this.mode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StatsButtons(mode: mode, buttons: _buttonsRow(context));
  }

  Widget _buttonsRow(BuildContext context) {
    return BlocBuilder<MediaStatisticsBloc, MediaStatisticsState>(builder: (context, state) {
      final getServer = state is MediaStatisticsData ? ServerStatus.CONNECTED : ServerStatus.INIT;
      final content = Column(mainAxisAlignment: MainAxisAlignment.end, children: [
        Row(children: [
          connectionButton(context),
          resourceUtilizationButton(context),
          getConfig(context, getServer),
          _themePicker(context),
          _langPicker(context)
        ]),
        Row(children: <Widget>[
          playlistButton(context),
          uploadPlaylistButton(context, getServer),
          uploadButton(context),
          importButton(context)
        ])
      ]);

      return content;
    });
  }

  IconButton _themePicker(BuildContext context) {
    final theme = context.watch<ThemeProvider>();
    return IconButton(
        icon: theme.theme == GlobalTheme.DARK
            ? const Icon(Icons.dark_mode)
            : const Icon(Icons.light_mode),
        onPressed: () {
          if (theme.theme == GlobalTheme.LIGHT) {
            theme.setTheme(GlobalTheme.DARK);
          } else {
            theme.setTheme(GlobalTheme.LIGHT);
          }
        });
  }

  Widget _langPicker(BuildContext context) {
    final locale = context.watch<LocaleProvider>();
    final bool themeMode = Theme.of(context).brightness == Brightness.dark;
    final List<DropdownMenuItem<Locale>> locales = [];
    for (final loc in AppLocalization.supportedLocales) {
      locales.add(DropdownMenuItem<Locale>(value: loc, child: Text(loc.languageCode)));
    }
    return SizedBox(
        height: 60,
        child: Container(
            margin: const EdgeInsets.all(15.0),
            padding: const EdgeInsets.only(left: 10.0, right: 10.0),
            decoration: BoxDecoration(
                color: themeMode ? AppColors.violet_2 : AppColors.violet,
                borderRadius: const BorderRadius.all(Radius.circular(10.0))),
            child: DropdownButton<Locale>(
                underline: const SizedBox(),
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                icon: Icon(Icons.arrow_drop_down,
                    color: themeMode ? AppColors.black : AppColors.white_2),
                dropdownColor: themeMode ? AppColors.violet_2 : AppColors.violet,
                value: locale.locale,
                style: themeMode
                    ? const TextStyle(color: AppColors.black)
                    : const TextStyle(color: AppColors.white_2),
                items: locales,
                onChanged: (Locale? c) {
                  if (c != null) {
                    locale.setLocale(c);
                  }
                })));
  }

  Widget resourceUtilizationButton(BuildContext context) {
    final onPressed = connected ? () => _openResourceUtilization(context) : null;
    return IconButton(
        icon: const Icon(Icons.history),
        tooltip: context.l10n.resourceUtilization,
        onPressed: onPressed);
  }

  StatsButton connectionButton(BuildContext context) {
    return StatsButton(
        title: connected ? context.l10n.disconnect : context.l10n.connect,
        onPressed: () => connected ? _disconnect(context) : _connect(context));
  }

  Widget playlistButton(BuildContext context) {
    final onPressed = connected ? () => _getPlaylist(context) : null;
    return StatsButton(title: context.l10n.playlist, onPressed: onPressed);
  }

  Widget uploadPlaylistButton(BuildContext context, ServerStatus getServer) {
    final bloc = context.read<StreamsStoreBloc>();
    final onPressed = connected
        ? () {
            _postPlaylist(context, bloc, getServer);
          }
        : null;
    return StatsButton(title: context.l10n.uploadPlaylist, onPressed: onPressed);
  }

  Widget uploadButton(BuildContext context) {
    final onPressed = connected ? () => _upload(context) : null;
    return IconButton(
        icon: const Icon(Icons.file_upload),
        tooltip: context.l10n.uploadMedia,
        onPressed: onPressed);
  }

  Widget importButton(BuildContext context) {
    final onPressed = connected ? () => _import(context) : null;
    return IconButton(
        icon: const Icon(Icons.system_update_alt),
        tooltip: context.l10n.import,
        onPressed: onPressed);
  }

  Widget getConfig(BuildContext context, ServerStatus getServer) {
    final onPressed = connected
        ? () {
            _getConfig(context, getServer);
          }
        : null;
    return IconButton(
        icon: const Icon(Icons.settings), tooltip: context.l10n.config, onPressed: onPressed);
  }

  // buttons
  void _connect(BuildContext context) async {
    final info = await showDialog<ConnectionInfo>(
        context: context,
        builder: (_) {
          return ConnectDialog(value: WSServer.createDefault());
        });

    if (info != null) {
      final storage = locator<LocalStorageService>();
      final bloc = context.read<ConnectionBloc>();
      final req = ConnectRequestedEvent(info, mode, storage.theme(), storage.locale());
      bloc.add(req);
    }
  }

  void _disconnect(BuildContext context) {
    final bloc = context.read<ConnectionBloc>();
    bloc.add(const DisconnectRequestedEvent());
  }

  void _getPlaylist(BuildContext context) {
    final fetcher = context.read<Fetcher>();
    fetcher.launchUrlEx('/server/playlist.m3u').then((resp) {}, onError: (error) {
      showError(context, error);
    });
  }

  void _postPlaylist(BuildContext context, StreamsStoreBloc bloc, ServerStatus getServer) {
    List<StreamType> types = availableTypes;
    if (getServer == ServerStatus.CONNECTED) {
      types = availableTypesIfHaveMedia;
    }

    showDialog(
        context: context,
        builder: (context) {
          return SelectM3uPackageDialog(types: types, editor: StoreEditor(bloc));
        });
  }

  void _import(BuildContext context) async {
    final result = await showDialog<WSServer>(
        context: context,
        builder: (context) {
          return ImportDialog(value: WSServer.createDefault());
        });
    if (result == null) {
      return;
    }

    final fetcher = context.read<Fetcher>();
    fetcher.importContent(result).then((resp) {}, onError: (error) {
      showError(context, error);
    });
  }

  void _upload(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          if (kIsWeb) {
            return const SelectFileWebDialog.video();
          }
          return const SelectFileDialog.video();
        });
  }

  void _openResourceUtilization(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) =>
            ProxyProvider<Fetcher, ResourceUtilizationBloc>(update: (_, fetcher, __) {
              return ResourceUtilizationBloc(
                  repository: ResourceUtilizationRepositoryImpl(fetcher));
            }, dispose: (_, blocLoader) {
              blocLoader.dispose();
            }, builder: (context, child) {
              return const Dialog(child: ResourceUtilizationHistoryDialog());
            }));
  }

  void _getConfig(BuildContext context, ServerStatus getServer) async {
    final info = await showDialog<Credentials>(
        context: context,
        builder: (context) {
          return const MasterConnectDialog();
        });

    if (info != null) {
      final fetcher = context.read<Fetcher>();
      if (fetcher.backendServerUrl == null) {
        return;
      }

      final master = MasterFetcher(fetcher.backendServerUrl!, info);
      final config = await master.getConfig();
      final updated = await showDialog(
          context: context,
          builder: (context) {
            void getHardwareHash(NodeAlgoInfo algo) {
              final resp = master.getHardwareHash(algo);
              resp.then((body) {
                final TextEditingController _textController = TextEditingController(text: body);
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return SimpleDialog(
                          title: Text(context.l10n.getHardwareHash),
                          children: <Widget>[
                            Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(children: [
                                  TextField(
                                      keyboardType: TextInputType.multiline,
                                      maxLines: null,
                                      controller: _textController),
                                  const SizedBox(height: 5.0),
                                  FlatButtonEx.filled(
                                      text: context.l10n.copy,
                                      onPressed: () {
                                        Clipboard.setData(
                                            ClipboardData(text: _textController.text));
                                        ScaffoldMessenger.of(context).showSnackBar(
                                            SnackBar(content: Text(context.l10n.copied)));
                                      }),
                                  const SizedBox(height: 5.0),
                                  FlatButtonEx.filled(
                                      text: context.l10n.close,
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      })
                                ]))
                          ]);
                    });
              }, onError: (error) {
                showError(context, error);
              });
            }

            void getLicenseHardwareHash(AlgoType algo) {
              final resp = master.getLicenseHardwareHash(algo);
              resp.then((body) {
                final TextEditingController _textController = TextEditingController(text: body);
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return SimpleDialog(
                          title: Text(context.l10n.getHardwareHash),
                          children: <Widget>[
                            Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(children: [
                                  TextField(
                                      keyboardType: TextInputType.multiline,
                                      maxLines: null,
                                      controller: _textController),
                                  const SizedBox(height: 5.0),
                                  FlatButtonEx.filled(
                                      text: context.l10n.copy,
                                      onPressed: () {
                                        Clipboard.setData(
                                            ClipboardData(text: _textController.text));
                                        ScaffoldMessenger.of(context).showSnackBar(
                                            SnackBar(content: Text(context.l10n.copied)));
                                      }),
                                  const SizedBox(height: 5.0),
                                  FlatButtonEx.filled(
                                      text: context.l10n.close,
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      })
                                ]))
                          ]);
                    });
              }, onError: (error) {
                showError(context, error);
              });
            }

            final types = getServer == ServerStatus.CONNECTED
                ? [StreamType.VOD_PROXY, StreamType.VOD_RELAY, StreamType.VOD_ENCODE]
                : [StreamType.VOD_PROXY];
            return MasterConfigDialog.edit(types, config, getLicenseHardwareHash, getHardwareHash);
          });
      if (updated != null) {
        master.setConfig(updated);
      }
    } //master:master
  }
}
