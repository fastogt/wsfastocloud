import 'package:auto_size_text/auto_size_text.dart';
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wsfastocloud/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_statistics_bloc/media_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/service_statistics_air.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/service_statistics_store.dart';

class OnAirStoreTabBar extends StatefulWidget {
  final WsMode mode;

  const OnAirStoreTabBar({Key? key, required this.mode}) : super(key: key);

  @override
  _OnAirStoreTabBarState createState() {
    return _OnAirStoreTabBarState();
  }
}

class _OnAirStoreTabBarState extends State<OnAirStoreTabBar> with SingleTickerProviderStateMixin {
  late final TabController _tabController = TabController(length: 2, vsync: this);

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MediaStatisticsBloc, MediaStatisticsState>(builder: (context, state) {
      final size = MediaQuery.of(context).size;
      final height = kIsWeb ? size.height * 0.78 : size.height;
      if (state is MediaStatisticsData) {
        final tabs = TabBar(
            controller: _tabController,
            tabs: [Tab(text: context.l10n.store), Tab(text: context.l10n.onAir)],
            labelColor: Colors.grey);
        final content = SizedBox(
            height: height,
            child:
                TabBarView(physics: const ScrollPhysics(), controller: _tabController, children: [
              SingleChildScrollView(
                  child: ServerStatsStoreDescWidget(
                      mode: widget.mode, mediaServerInfo: state.mediaInfo)),
              const SingleChildScrollView(child: ServerStatsOnAirDescWidget())
            ]));
        return Column(children: [tabs, content]);
      }
      final tabs = TabBar(
          controller: _tabController,
          tabs: [Tab(text: context.l10n.store), _lockIcon()],
          labelColor: Colors.black);
      final content = SizedBox(
          height: height,
          child: SingleChildScrollView(
              child: ServerStatsStoreDescWidget(mode: widget.mode, mediaServerInfo: null)));

      return Column(children: [AbsorbPointer(child: tabs), content]);
    });
  }

  Widget _lockIcon() {
    return Wrap(children: [
      const Icon(Icons.lock, color: Colors.black, size: 25),
      const SizedBox(width: 5),
      AutoSizeText(maxLines: 1, context.l10n.onAirNeedFastocloudComproml, minFontSize: 2)
    ]);
  }
}
