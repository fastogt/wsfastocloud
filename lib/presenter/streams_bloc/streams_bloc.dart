import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/presenter/streams/store/stream_data_source.dart';

part 'streams_event.dart';

part 'streams_state.dart';

class StreamsBloc extends Bloc<StreamsEvent, StreamsState> {
  StreamSubscription<WebSocketState>? _subscription;

  final WebSocketApiBloc webSocketApiBloc;
  final StreamDBRepository streamRepository;
  StreamsBloc({required this.webSocketApiBloc, required this.streamRepository})
      : super(StreamsState(streamDataSource: StoreStreamDataSource([]))) {
    on<InitEvent>(_onInit);
    on<AddLiveStreamEvent>(_onAddLiveStream);
    on<UpdateLiveStreamEvent>(_onUpdateLiveStream);
    on<RemoveLiveStreamEvent>(_onRemoveLiveStream);
    on<LoadLiveStreamsEvent>(_onLoadLiveStreams);
    on<EmptyEvent>(_onEmpty);
    on<FailureEvent>(_onFailure);

    add(const InitEvent());
  }

  void _onInit(InitEvent event, Emitter<StreamsState> emit) async {
    _subscription ??= webSocketApiBloc.stream.listen((event) {
      if (event is WebSocketApiMessage) {
        const String wsStreamAddedEvent = 'stream_added';
        const String wsStreamUpdatedEvent = 'stream_updated';
        const String wsStreamRemovedEvent = 'stream_removed';

        if (event.type == wsStreamUpdatedEvent) {
          final type = getStreamTypeFromJson(event.data);
          if (type == null) {
            return;
          }

          final stream = makeStream(event.data);
          if (stream != null) {
            add(UpdateLiveStreamEvent(stream: stream));
          }
        } else if (event.type == wsStreamAddedEvent) {
          final type = getStreamTypeFromJson(event.data);
          if (type == null) {
            return;
          }

          final stream = makeStream(event.data);
          if (stream != null) {
            add(AddLiveStreamEvent(stream: stream));
          }
        } else if (event.type == wsStreamRemovedEvent) {
          final type = getStreamTypeFromJson(event.data);
          if (type == null) {
            return;
          }

          final stream = makeStream(event.data);
          if (stream != null) {
            add(RemoveLiveStreamEvent(stream: stream));
          }
        }
      } else if (event is WebSocketFailure) {
        add(FailureEvent(description: event.description));
      } else if (event is WebSocketConnected) {
        add(const LoadLiveStreamsEvent());
      } else {
        add(const EmptyEvent());
      }
    });

    if (webSocketApiBloc.isConnected()) {
      add(const LoadLiveStreamsEvent());
    }
  }

  void _onAddLiveStream(AddLiveStreamEvent event, Emitter<StreamsState> emit) {
    final StoreStreamDataSource streamDataSource =
        StoreStreamDataSource([...state.streamDataSource.currentList]);

    streamDataSource.addItem(event.stream);

    emit(state.copyWith(streamDataSource: streamDataSource));
  }

  void _onUpdateLiveStream(UpdateLiveStreamEvent event, Emitter<StreamsState> emit) {
    final StoreStreamDataSource streamDataSource =
        StoreStreamDataSource([...state.streamDataSource.currentList]);

    streamDataSource.updateItem(event.stream);

    emit(state.copyWith(streamDataSource: streamDataSource));
  }

  void _onRemoveLiveStream(RemoveLiveStreamEvent event, Emitter<StreamsState> emit) {
    final StoreStreamDataSource streamDataSource =
        StoreStreamDataSource([...state.streamDataSource.currentList]);
    streamDataSource.removeItem(streamDataSource.currentList
        .firstWhere((DataEntry<IStream> entryPoint) => entryPoint.entry.id == event.stream.id)
        .entry);

    emit(state.copyWith(streamDataSource: streamDataSource));
  }

  void _onLoadLiveStreams(_, Emitter<StreamsState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      final liveStreams = await streamRepository.loadLiveStreams();

      emit(state.copyWith(
          isLoading: false, streamDataSource: StoreStreamDataSource([])..addItems(liveStreams)));
    } catch (e) {
      emit(state.copyWith(isLoading: false, errorMessage: 'Backend error: $e'));
    }
  }

  void _onEmpty(_, Emitter<StreamsState> emit) {
    emit(state.copyWith(isEmpty: true));
  }

  void _onFailure(FailureEvent event, Emitter<StreamsState> emit) {
    emit(state.copyWith(errorMessage: event.description));
  }

  void dispose() {
    _subscription?.cancel();
    _subscription = null;
  }
}
