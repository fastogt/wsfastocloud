part of 'streams_bloc.dart';

abstract class StreamsEvent {}

class InitEvent implements StreamsEvent {
  const InitEvent();
}

class AddLiveStreamEvent implements StreamsEvent {
  final IStream stream;

  const AddLiveStreamEvent({required this.stream});
}

class UpdateLiveStreamEvent implements StreamsEvent {
  final IStream stream;

  const UpdateLiveStreamEvent({required this.stream});
}

class RemoveLiveStreamEvent implements StreamsEvent {
  final IStream stream;

  const RemoveLiveStreamEvent({required this.stream});
}

class LoadLiveStreamsEvent implements StreamsEvent {
  const LoadLiveStreamsEvent();
}

class EmptyEvent implements StreamsEvent {
  const EmptyEvent();
}

class FailureEvent implements StreamsEvent {
  final String description;

  const FailureEvent({required this.description});
}
