part of 'streams_bloc.dart';

class StreamsState extends Equatable {
  final StoreStreamDataSource streamDataSource;
  final String? errorMessage;
  final bool isLoading;

  const StreamsState({required this.streamDataSource, this.errorMessage, this.isLoading = true});

  StreamsState copyWith(
      {StoreStreamDataSource? streamDataSource,
      String? errorMessage,
      bool? isEmpty,
      bool? isLoading}) {
    return StreamsState(
        streamDataSource: streamDataSource ?? this.streamDataSource,
        errorMessage: errorMessage,
        isLoading: isLoading ?? this.isLoading);
  }

  @override
  List<Object?> get props => [streamDataSource, errorMessage, isLoading];
}
