import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/domain/repositories/resource_utilization_repository.dart';
import 'package:wsfastocloud/presenter/resource_utilization_bloc/resource_utilization_state.dart';

class ResourceUtilizationBloc extends ItemBloc {
  final ResourceUtilizationRepository repository;

  ResourceUtilizationBloc({required this.repository});

  @override
  Future<ItemDataState<List<Machine>>> loadDataRoute() async {
    final List<Machine> machines = await repository.getResourceUtilization();

    return ResourceUtilizationLoadedData(machines);
  }
}
