import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart' as models;
import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart' as ott_localizations;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:wsfastocloud/constants.dart';
import 'package:wsfastocloud/data/repositories/server_repository_impl.dart';
import 'package:wsfastocloud/data/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/data/repositories/stream_repository.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/connection_info.dart';
import 'package:wsfastocloud/data/services/local_storage/local_storage_service.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/server_repository.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/domain/repositories/stream_repository.dart';
import 'package:wsfastocloud/l10n/app_localization.dart';
import 'package:wsfastocloud/l10n/locale_provider.dart';
import 'package:wsfastocloud/locator.dart';
import 'package:wsfastocloud/presenter/catchups_bloc/catchups_bloc.dart';
import 'package:wsfastocloud/presenter/connection_bloc/connection_bloc.dart';
import 'package:wsfastocloud/presenter/episodes_bloc/episodes_bloc.dart';
import 'package:wsfastocloud/presenter/server_details.dart';
import 'package:wsfastocloud/presenter/service_info_bloc/service_info_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_statistics_bloc/media_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/on_air_tabs_bloc/on_air_tab_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_statistics_bloc/server_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/store_tabs_bloc/store_tabs_bloc.dart';
import 'package:wsfastocloud/presenter/streams/on_air/stream_statistics_bloc/stream_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_store_bloc/seasons_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/serials_store_bloc/serials_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams_bloc/streams_bloc.dart';
import 'package:wsfastocloud/presenter/vods_bloc/vods_bloc.dart';
import 'package:wsfastocloud/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  Route? _onGenerateRoute(RouteSettings settings) {
    if (settings.name == null) {
      return null;
    }

    final url = settings.name!;
    return PageRouteBuilder(
        pageBuilder: (ctx, _, __) {
          final result = models.makeWSServerFromUrl(url);
          final storage = locator<LocalStorageService>();
          GlobalTheme th = storage.theme();
          String lc = storage.locale();
          WsMode mode = WsMode.OTT;

          if (result != null) {
            if (result.locale != null) {
              final locale = ctx.watch<LocaleProvider>();
              lc = result.locale!;
              locale.setLocale(Locale(lc));
            }
            if (result.theme != null) {
              final theme = ctx.watch<ThemeProvider>();
              th = GlobalTheme.fromInt(result.theme!);
              theme.setTheme(th);
            }
            if (result.mode != null) {
              mode = result.mode!;
            }
            final req =
                ConnectRequestedEvent(ConnectionInfo.fromUserInput(result.server), mode, th, lc);
            ctx.read<ConnectionBloc>().add(req);
          } else {
            final m = storage.mode();
            if (m != null) {
              mode = m;
            }
            ctx.read<ConnectionBloc>().add(const ConnectFromLocalStorage());
          }
          return ServerDetailsPage(mode: mode);
        },
        transitionDuration: Duration.zero,
        settings: settings);
  }

  @override
  Widget build(BuildContext context) {
    final providers = _setupProviders();
    const delegates = [
      ott_localizations.AppLocalizations.delegate,
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate
    ];

    return MultiProvider(
        providers: providers,
        child: FutureBuilder(
            future: locator.allReady(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                final locale = context.watch<LocaleProvider>();
                final theme = context.watch<ThemeProvider>();

                final materialApp = MaterialApp(
                    localizationsDelegates: delegates,
                    supportedLocales: AppLocalization.supportedLocales,
                    locale: locale.locale,
                    localeResolutionCallback: (locale, supportedLocales) {
                      for (final supportedLocale in supportedLocales) {
                        if (locale != null) {
                          if (supportedLocale.languageCode == locale.languageCode &&
                              supportedLocale.countryCode == locale.countryCode) {
                            return supportedLocale;
                          }
                        }
                      }
                      return supportedLocales.first;
                    },
                    title: PROJECT,
                    theme:
                        theme.theme == GlobalTheme.LIGHT ? AppTheme.lightTheme : AppTheme.darkTheme,
                    onGenerateRoute: _onGenerateRoute);
                return materialApp;
              }
              return const CircularProgressIndicator();
            }));
  }

  List<SingleChildWidget> _setupProviders() {
    return [
      Provider<ConnectionBloc>(create: (_) => ConnectionBloc()),
      ProxyProvider<ConnectionBloc, Fetcher>(
          update: (_, connection, __) => Fetcher(connection),
          dispose: (_, fetcher) {
            fetcher.dispose();
          }),
      ProxyProvider<ConnectionBloc, WebSocketApiBloc>(
          update: (_, connection, __) => WebSocketApiBloc(connection),
          dispose: (_, bloc) {
            bloc.dispose();
          }),
      ProxyProvider<Fetcher, StreamRepository>(
          update: (_, fetcher, __) => StreamRepositoryImpl(fetcher)),
      ProxyProvider<WebSocketApiBloc, StoreTabsBloc>(update: (_, ws, __) {
        return StoreTabsBloc(ws);
      }, dispose: (_, bloc) {
        bloc.dispose();
      }),
      ProxyProvider<WebSocketApiBloc, OnAirTabBloc>(update: (_, ws, __) {
        return OnAirTabBloc(ws);
      }, dispose: (_, bloc) {
        bloc.dispose();
      }),
      ProxyProvider<Fetcher, StreamDBRepository>(
          update: (_, fetcher, __) => StreamDBRepositoryImpl(fetcher)),
      ProxyProvider2<WebSocketApiBloc, Fetcher, ServiceInfoBloc>(update: (_, ws, fetcher, __) {
        return ServiceInfoBloc(ws, fetcher);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, Fetcher, MediaStatisticsBloc>(update: (_, ws, fetcher, __) {
        return MediaStatisticsBloc(ws, fetcher);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, Fetcher, ServiceStatisticsBloc>(
          update: (_, ws, fetcher, __) {
        return ServiceStatisticsBloc(ws, fetcher);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider3<WebSocketApiBloc, StreamRepository, StreamDBRepository, StreamsStatisticsBloc>(
          update: (_, ws, streamRepository, streamDbRepository, __) {
        return StreamsStatisticsBloc(ws, streamRepository, streamDbRepository);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider<StreamDBRepository, StreamsStoreBloc>(update: (_, repo, __) {
        return StreamsStoreBloc(streamRepository: repo);
      }),
      ProxyProvider2<WebSocketApiBloc, StreamDBRepository, VodsBloc>(update: (_, ws, repo, __) {
        return VodsBloc(webSocketApiBloc: ws, streamRepository: repo);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, StreamDBRepository, CatchupsBloc>(update: (_, ws, repo, __) {
        return CatchupsBloc(webSocketApiBloc: ws, streamRepository: repo);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, StreamDBRepository, StreamsBloc>(update: (_, ws, repo, __) {
        return StreamsBloc(webSocketApiBloc: ws, streamRepository: repo);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, StreamDBRepository, EpisodesBloc>(update: (_, ws, repo, __) {
        return EpisodesBloc(webSocketApiBloc: ws, streamRepository: repo);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, StreamDBRepository, SeasonsBloc>(update: (_, ws, repo, __) {
        return SeasonsBloc(ws, repo);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, StreamDBRepository, SerialsBloc>(update: (_, ws, repo, __) {
        return SerialsBloc(ws, repo);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider<Fetcher, ServerRepository>(update: (_, fetcher, __) {
        return ServerRepositoryImpl(fetcher: fetcher);
      }),
      ChangeNotifierProvider(create: (context) => LocaleProvider()),
      ChangeNotifierProvider(create: (context) => ThemeProvider())
    ];
  }
}
