import 'package:equatable/equatable.dart';

class File extends Equatable {
  final String name;
  final List<int> bytes;

  const File({required this.name, required this.bytes});

  @override
  List<Object?> get props => [name, bytes];
}
