import 'package:equatable/equatable.dart';

class UploadedImage extends Equatable {
  final String path;

  const UploadedImage({required this.path});

  @override
  List<Object?> get props => [path];
}
