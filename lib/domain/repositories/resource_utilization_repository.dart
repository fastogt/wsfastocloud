import 'package:fastocloud_dart_media_models/models.dart';

abstract class ResourceUtilizationRepository {
  Future<List<Machine>> getResourceUtilization();
}
