import 'package:wsfastocloud/domain/entity/file.dart';
import 'package:wsfastocloud/domain/entity/uploaded_image.dart';

abstract class ServerRepository {
  Future<UploadedImage> uploadImage(File file);
}
