import 'package:fastocloud_dart_media_models/fastocloud_dart_media_models.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';

abstract class StreamDBRepository {
  Future<bool> uploadM3uFile(
      StreamType type,
      bool isSeries,
      double? imageCheckTime,
      final bool skipGroups,
      final bool fillInfo,
      final List<String> groups,
      Map<String, List<int>> data);

  Future<bool> uploadM3uPackageFile(StreamType type, double? imageCheckTime, final bool skipGroups,
      final bool fillInfo, final List<String> groups, Map<String, List<int>> data);

  // LIVE
  Future<bool> addLiveStream(IStream stream);
  Future<bool> editLiveStream(IStream stream);
  Future<bool> removeLiveStream(String id);

  // CATCHUPS
  //Future<bool> addCatchupStream(IStream stream);
  //Future<bool> editCatchupStream(IStream stream);
  Future<bool> removeCatchupStream(String id);

  // VOD
  Future<bool> addVod(IStream stream);
  Future<bool> editVod(IStream stream);
  Future<bool> removeVod(String id);

  // EPISODE
  Future<bool> addEpisode(IStream stream);
  Future<bool> editEpisode(IStream stream);
  Future<bool> removeEpisode(String id);

  Future<String> probeOutUrl(OutputUrl url);

  Future<String> getTokenUrl(String ip);

  Future<String> embedOutput(OutputUrl url);

  Future<String> probeInUrl(InputUrl url);

  Future<List<IStream>> loadEpisodes();

  Future<IStream?> loadEpisodeById(String id);

  Future<List<CatchupStream>> loadCatchups();

  Future<CatchupStream?> loadCatchupById(String id);

  Future<List<IStream>> loadLiveStreams();

  Future<IStream?> loadLiveStreamById(String id);

  Future<List<IStream>> loadVods();

  Future<IStream?> loadVodById(String id);

  // seasons
  Future<List<ServerSeason>> loadSeasons();

  Future<void> addSeason(ServerSeason season);

  Future<void> editSeason(ServerSeason season);

  Future<void> removeSeason(ServerSeason season);

  // serials
  Future<List<ServerSerial>> loadSerials();

  Future<void> addSerial(ServerSerial serial);

  Future<void> editSerial(ServerSerial serial);

  Future<void> removeSerial(ServerSerial serial);

  Future<void> startLive(String id);
  Future<void> startVod(String id);
  Future<void> startEpisode(String id);

  Defaults defaults();

  WebRTCOutputUrl genWebRTC(int oid, String sid);

  Future<MediaFolders?> folders();
}
