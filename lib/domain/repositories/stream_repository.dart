import 'package:wsfastocloud/data/services/api/models/stream_statistics.dart';

abstract class StreamRepository {
  Future<List<StreamStatisticsBack>> getStreamsStatistics();

  Future<void> stopStream(String id, [bool force = false]);

  Future<void> restartStream(String id);

  Future<void> getLogStream(String id);

  Future<void> changeSource(String id, int index);

  Future<void> getPipeLineStream(String id);

  Future<String> getConfigStreamString(String id);

  Future<Map<String, dynamic>> getConfigStreamMap(String id);
}
