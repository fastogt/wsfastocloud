import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart' as ott_localizations;

extension AppLocalizationsX on BuildContext {
  ott_localizations.AppLocalizations get l10n => ott_localizations.AppLocalizations.of(this);
}
