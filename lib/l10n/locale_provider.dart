import 'package:fastotv_dart/commands_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:wsfastocloud/l10n/app_localization.dart';

class LocaleProvider extends ChangeNotifier {
  Locale _locale;

  LocaleProvider({Locale locale = AppLocalization.en}) : _locale = locale;

  Locale get locale => _locale;

  void setLocale(Locale locale) {
    if (!AppLocalization.supportedLocales.contains(locale)) {
      return;
    }

    if (_locale == locale) {
      return;
    }

    _locale = locale;
    notifyListeners();
  }
}

class ThemeProvider extends ChangeNotifier {
  GlobalTheme _theme;

  ThemeProvider({GlobalTheme theme = GlobalTheme.LIGHT}) : _theme = theme;

  GlobalTheme get theme => _theme;

  void setTheme(GlobalTheme theme) {
    if (_theme == theme) {
      return;
    }

    _theme = theme;
    notifyListeners();
  }
}
