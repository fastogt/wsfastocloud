import 'package:flutter/material.dart';

// #FIXME: Why do we need this class with static members?

class AppLocalization {
  static const Locale en = Locale('en');
  static const Locale ru = Locale('ru');

  static List<Locale> get supportedLocales => [en, ru];
}
