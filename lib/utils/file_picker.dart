import 'package:file_picker/file_picker.dart' as file_picker_package;
import 'package:wsfastocloud/domain/entity/file.dart';

class ImageFilePicker {
  static final _allowedImageExtension = [
    'jpg',
    'jpeg',
    'png',
    'bmp',
    'webp',
    'gif',
    'tif',
    'tiff',
    'apng'
  ];

  Future<File?> pickFile() async {
    final pickedFiles = await file_picker_package.FilePicker.platform.pickFiles(
        type: file_picker_package.FileType.custom, allowedExtensions: _allowedImageExtension);

    if (pickedFiles == null || pickedFiles.files.isEmpty || pickedFiles.files.first.bytes == null) {
      return null;
    }

    final file = pickedFiles.files.first;
    final File imageFile = File(name: file.name, bytes: file.bytes!);
    return imageFile;
  }
}
