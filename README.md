# FastoCloud ONE

[![Join the chat at https://discord.com/invite/cnUXsws](https://img.shields.io/discord/584773460585086977?label=discord)](https://discord.com/invite/cnUXsws)

### Description:
UI for FastoCloud Media Services, backend you can find [here](https://gitlab.com/fastocloud/gofastocloud_backend) and in binary form [here](https://fastocloud.com/downloads.html) (FastoCloud Backend API project)

### Features:
- Upload media files
- Uplaod m3u file
- VODs/Restream/Transcode/Catchups/Timeshifts/External streams
- Realtime statistics
- Streams managment

### Demo servers:
- https://ws.fastocloud.com

### Android version:
- https://play.google.com/store/apps/details?id=com.wsfastocloud

### Docker version:
- https://hub.docker.com/repository/docker/fastocloud/wsfastocloud

![UI on Web](https://gitlab.com/fastocloud/wsfastocloud/raw/main/docs/images/fastocloud_one_web.png)

### Installation:
- [Installation backend](https://gitlab.com/fastocloud/wsfastocloud/-/wikis/Installation-on-clean-machine-backend)
- [Installation frontend](https://gitlab.com/fastocloud/wsfastocloud/-/wikis/Installation-on-clean-machine-frontend)