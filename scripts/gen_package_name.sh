#!/bin/sh

version=$(echo $(echo $(cat pubspec.yaml) | sed -ne 's/[^0-9]*\(\([0-9]*\.\)\{0,4\}[0-9][^.]\).*/\1/p') | sed 's/+//')
hash=$(git rev-parse --short HEAD)
echo wsfastocloud-$version-browser-$hash
