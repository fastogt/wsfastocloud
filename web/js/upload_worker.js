self.addEventListener('message', async (event) => {
    var file = event.data.file;
    var url = event.data.uri;
    var auth = event.data.auth;
    uploadFile(file, url, auth);
});

function uploadFile(file, url, auth) {
    var xhr = new XMLHttpRequest();
    var formdata = new FormData();

    formdata.append('file', file);

    xhr.upload.addEventListener('progress', function (e) {
        //Use this if you want to have a progress bar
        if (e.lengthComputable) {
            var uploadPercent = (e.loaded / e.total);
            postMessage({ progress: uploadPercent });
        }
    }, false);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            let resp;
            try {
                resp = JSON.parse(xhr.response);
            } catch (e) {
                var err = { error: { code: -5101, message: JSON.stringify(xhr.response) } };
                postMessage({ status: xhr.status, data: err });
                return; // error in the above string (in this case, yes)!
            }
            postMessage({ status: xhr.status, data: resp });
        }
    }
    xhr.onerror = function () {
        // only triggers if the request couldn't be made at all
        var err = { error: { code: -5100, message: 'UI upload failed' } };
        postMessage({ status: 400, data: err });
    };

    xhr.open('POST', url, true);
    if (auth !== null) {
        xhr.setRequestHeader("Authorization", auth);
    }
    xhr.send(formdata);
}