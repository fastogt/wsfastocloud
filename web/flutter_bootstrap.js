{{flutter_js}}
{{flutter_build_config}}


const loadingDiv = document.createElement('div');
loadingDiv.className = "loading";
document.body.appendChild(loadingDiv);
const loaderDiv = document.createElement('div');
loaderDiv.className = "loader";
loadingDiv.appendChild(loaderDiv);
const loaderText = document.createElement('div');
loaderText.className = "loader-text";
loaderText.innerText = "Loading...";
loadingDiv.appendChild(loaderText);

// renderer=canvaskit renderer=skwasm
const searchParams = new URLSearchParams(window.location.search);
const renderer = searchParams.get('renderer');
const userConfig = renderer ? { 'renderer': renderer } : {};

console.log("renderer config: " + JSON.stringify(userConfig, undefined, 2))
_flutter.loader.load({
  config: userConfig,
  onEntrypointLoaded: async function (engineInitializer) {
    const appRunner = await engineInitializer.initializeEngine();
    if (document.body.contains(loadingDiv)) {
      document.body.removeChild(loadingDiv);
    }
    const isRunningWithWasm = typeof window._flutter_skwasmInstance !== 'undefined';
    console.log("renderer is wasm: " + isRunningWithWasm)
    await appRunner.runApp();
  }
});
